This instructions will not cover everything and it is expected that you knows Docker and some basic of Linux as well.

If you feel lost, you may prefer to use the other methods.

# Build the image

Yet this image is not being hosted and you will need to build (and update when needed) it locally.

## Without compose

### 1. Build

1.1. Save the [.env](./.env) and [Dockerfile](./Dockerfile) files in the same directory.

1.2. Load the `.env` contents on your current shell session:
```sh
source .env
```

1.3. Build it giving the recommended tags: `upsies:latest` and `upsies:$VERSION`
```sh
docker build . --build-arg VERSION=$VERSION --tag upsies:latest --tag upsies:$VERSION
```

### 2. Run

Without compose, everytime you need upsies you will need to type an extensive command including the volumes. Lets make an alias.

2.1. Add the alias to the `~/.bashrc` (or `~/.zshrc` if you use zsh) file. Adjust the volumes as your needs:
```sh
upsies() {
    docker run -it --rm --network host \
        -u 1000:1000 \
        -v /my-persistant-upsies:/app/upsies \
        -v /data/torrents:/data/torrents:ro \
        upsies "$@";
}
```

2.2. Close the current shell session and enter again **or** reload the file you just changed.

2.3. Run `upsies --help`

### 3. Update

3.1 Update the `VERSION` value on `.env` file

3.2 Follow the step \#1 again.

---

## With compose

### 1. Build

1.1. Save the [.env](./.env), [Dockerfile](./Dockerfile) and [docker-compose.yml](./docker-compose.yml) files in the same directory.

1.2. Edit the `docker-compose.yml` as your needs. Set your **volumes** and **user** as your needs.

1.3. Build it:
```sh
docker compose build
```

### 2. Run

2.1. Run with compose is easy **but** you need to be in the directory as `docker-compose.yml` is (or pass the path all the time). Lets make an alias.

2.1. Add the alias to the `~/.bashrc` (or `~/.zshrc` if you use zsh) file. Adjust the file location as your needs:
```sh
upsies() {
    docker compose -f /path-to-compose-file/docker-compose.yml run --rm upsies "$@";
}
```

2.2. Close the current shell session and enter again **or** reload the file you just changed.

2.3. Run `upsies --help`

### 3. Update

3.1 Update the value on `.env` file

3.2 Follow the step \#1 again.
