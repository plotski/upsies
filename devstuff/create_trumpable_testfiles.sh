#!/bin/bash
#
# Create a bunch of test video files from a (preferably small) source video. The test videos have
# different audio and subtitle tracks with different language tags, e.g. "No English audio and no
# English subtitles" or "English audio and no subtitles", etc.

set -o errexit
set -o nounset

videofile="$1"
outputdir="$2"
subfile='/tmp/trumpable.test.srt'
subfile_en='/tmp/trumpable.test.en.srt'
subfile_fi='/tmp/trumpable.test.fi.srt'
subfile_und='/tmp/trumpable.test.srt'

echo "1
00:03:00,00 --> 00:03:06,000
Vat languash iz dis?
" >"$subfile"

mkdir -p "$outputdir"

function create_video {
    # Create video file from provided audio and subtitle languages.
    audio_languages="$1"
    subtitle_languages="$2"
    args=()
    outputfile="$outputdir"/"trumpable.test"

    # Video track
    args+=(--no-subtitles --no-audio "$videofile")

    # Audio tracks (assume the audio track in $videofile has ID 1)
    for lang in ${audio_languages//,/ }; do
        args+=(--no-video --no-subtitles --language "1:$lang" --regenerate-track-uids "$videofile")
    done
    outputfile="$outputfile.a=$audio_languages"

    # Subtitle tracks
    for lang in ${subtitle_languages//,/ }; do
        args+=(--language "0:$lang" "$subfile")
    done
    outputfile="$outputfile.s=$subtitle_languages"
    outputfile="$outputfile.mkv"

    # for arg in "${args[@]}"; do
    #     echo "[$arg]"
    # done
    [ ! -e "$outputfile" ] && mkvmerge --quiet -o "$outputfile" "${args[@]}"
    show_video "$outputfile"
}

function show_video {
    # Show audio and subtitle languages for video file.
    filepath="$1"
    echo " * $filepath"
    echo -n '   Audio languages: '
    mediainfo "$1"  --Output=JSON \
       | jq -rM '[ .media.track[] | select(.["@type"]=="Audio") | .Language // "und"  ] | join(", ")'
    echo -n '   Subtitle languages: '
    mediainfo "$1"  --Output=JSON \
       | jq -rM '[ .media.track[] | select(.["@type"]=="Text") | .Language // "und" ] | join(", ")'
}

# Interesting language combinations are:
#  - None (silent film)
#  - Undefined (no language tag)
#  - English
#  - English + Undefined
#  - Non-English
#  - Non-English + English dub
#  - Non-English + Undefined

for audio_languages in '' und en en,und it it,en it,und; do
    for subtitle_languages in '' und en en,und ro ro,en ro,und; do
        create_video "$audio_languages" "$subtitle_languages"
    done
done
