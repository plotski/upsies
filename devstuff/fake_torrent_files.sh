#!/bin/bash
#
# Create the same file structure as in a given torrent file without actually
# using any space.

set -o nounset   # Don't allow unset variables
set -o errexit   # Exit if any command fails

fake_file() {
    local filepath="$1"
    local filesize="$2"
    echo "$filepath ($filesize bytes)"
    mkdir -p "$(dirname "$filepath")"
    truncate -s "$filesize" "$filepath"
}


torrent="$1"
[ -n "$torrent" ] || exit 1

if [ "$(torf -mi "$torrent" | jq -r '.info | has("files")')" = 'true' ]; then
    directory="$(torf -mi "$torrent" | jq -r '.info.name')"
    files="$(torf -mi "$torrent" | jq -r '.info.files[] | "'"$directory"'/" + .path[0] + "\t" + (.length | tostring)')"

elif [ "$(torf -mi "$torrent" | jq -r '.info | has("name")')" = 'true' ]; then
    filepath="$(torf -mi "$torrent" | jq -r '.info.name')"
    filesize="$(torf -mi "$torrent" | jq -r '.info.length')"
    files="$filepath"$'\t'"$filesize"

else
    echo 'Could not find any files in torrent:'
    torf -mi "$torrent"

fi

echo "$files"
IFS=$'\n'
for line in $files; do
    filepath=$(echo $line | cut -f 1)
    filesize=$(echo $line | cut -f 2)
    fake_file "$filepath" "$filesize"
done
