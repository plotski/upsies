#!/bin/zsh
#
# Extract the same mediainfo information from lots of video files.
#
# Example usage:
#
#     $ migrep.sh Text Format,CodecID
#     $ migrep.sh Audio Language,Title
#     $ migrep.sh Video Width,Height,Stored_Width,Stored_Height,Sampled_Width,Sampled_Height

set -o nounset   # Don't allow unset variables
set -o errexit   # Exit if any command fails

track_type="$1"
keys=($(echo "$2" | tr ',' '\n'))
keys='(?:'$(IFS='|' ; echo "${keys[*]}")')'


function video_files() {
    find /srv/torrents -regextype egrep -name '*.mkv' -not -iregex '.*+E[0-9]+.*'
}

function get_info() {
    mediainfo --Output=JSON "$1" \
        | jq '.media["@ref"],.media.track[]
              | select(.["@type"]? == "'"$track_type"'")
              | with_entries(if (.key | test("^'"$keys"'$")) then ( {key: .key, value: .value } ) else empty end)
              + {"path": '"\"$1\""'}'
}

echo '['

function close() {
    echo ']'
}

trap close EXIT TERM INT

IFS=$'\n'
for f in $(video_files); do
    get_info "$f"
done
