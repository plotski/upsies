include = [
    "upsies/*.py",
    "tests/*.py",
]
line-length = 130

[format]
quote-style = "double"
line-ending = "lf"

[lint]
pydocstyle.convention = "pep257"
select = ["ALL"]

# TODO: Remove this block when E* rules are no longer in preview.
preview = true
explicit-preview-rules = true
extend-select = [
    "E201",  # Whitespace after '('
    "E221",  # Multiple spaces before operator
]

ignore = [
    "A",       # flake8-builtins
    "ANN",     # flake8-annotations
    "D",       # pydocstyle
    "EM",      # flake8-errmsg
    "FIX",     # flake8-fixme
    "PTH",     # flake8-use-pathlib
    "RET",     # flake8-return
    "SLOT",    # Subclasses of `...` should define `__slots__`
    "TD",      # flake8-todos

    "ARG002",   # Unused method argument
    "ASYNC230", # Async functions should not open files with blocking methods like `open`
    "B006",     # Do not use mutable data structures for argument defaults
    "B008",     # Do not perform function call `...` in argument defaults
    "B012",     # `return` inside `finally` blocks cause exceptions to be silenced
    "B027",     # `...` is an empty method in an abstract base class, but has no abstract decorator
    "BLE001",   # Do not catch blind exception
    "C901",     # `FUNCTION` is too complex
    "COM812",   # Trailing comma missing (NOTE: This complains about multiline strings.)
    "DTZ005",   # `datetime.datetime.now()` called without a `tz` argument
    "DTZ006",   # `datetime.datetime.fromtimestamp()` called without a `tz` argument
    "ERA001",   # Found commented-out code
    "FBT003",   # Boolean positional value in function call
    "FLY002",   # Consider f-string instead of string join
    "ISC003",   # Explicitly concatenated string should be implicitly concatenated
    "N801",     # Class name `...` should use CapWords convention
    "N802",     # Function name `...` should be lowercase
    "N803",     # Argument name `ClassName` should be lowercase
    "N804",     # First argument of a class method should be named `cls`
    "N805",     # First argument of a method should be named `self`
    "N806",     # Variable `...` in function should be lowercase
    "N807",     # Function name should not start and end with `__`
    "N812",     # Lowercase `xdg_config_home` imported as non-lowercase `XDG_CONFIG_HOME`
    "N818",     # Exception name `...` should be named with an Error suffix
    "PERF203",  # `try`-`except` within a loop incurs performance overhead
    "PIE790",   # Unnecessary `pass` statement
    "PIE796",   # Enum contains duplicate value
    "PIE808",   # Unnecessary `start` argument in `range`
    "PLR0911",  # Too many return statements
    "PLR0912",  # Too many branches
    "PLR0913",  # Too many arguments in function definition
    "PLR0915",  # Too many statements
    "PLR1704",  # Redefining argument with the local name `...`
    "PLR2004",  # Magic value used in comparison, consider replacing `...` with a constant variable
    "PLW2901",  # `for` loop variable `...` overwritten by assignment target
    "PT018",    # Assertion should be broken down into multiple parts
    "PYI024",   # Use `typing.NamedTuple` instead of `collections.namedtuple`
    "Q000",     # Single quotes found but double quotes preferred
    "RSE102",   # Unnecessary parentheses on raised exception
    "RUF001",   # String contains ambiguous `´` (ACUTE ACCENT). Did you mean ``` (GRAVE ACCENT)?
    "RUF012",   # Mutable class attributes should be annotated with `typing.ClassVar`
    "RUF015",   # Prefer `next(iter(...))` over single element slice
    "S101",     # Use of `assert` detected
    "S103",     # `os.chmod` setting a permissive mask `0o777` on file or directory
    "S301",     # `pickle` and modules that wrap it can be unsafe when used to deserialize untrusted data
    "S307",     # Use of possibly insecure function; consider using `ast.literal_eval`
    "S311",     # Standard pseudo-random generators are not suitable for cryptographic purposes
    "S324",     # Probable use of insecure hash functions in `hashlib`: `md5`
    "SIM105",   # Use `contextlib.suppress(asyncio.queues.QueueEmpty)` instead of `try`-`except`-`pass`
    "SIM108",   # Use ternary operator
    "SIM110",   # Use `return all(...)` instead of `for` loop
    "SIM116",   # Use a dictionary instead of consecutive `if` statements
    "SIM211",   # Use `not ...` instead of `False if ... else True`
    "SLF001",   # Private member accessed
    "TID252",   # Prefer absolute imports over relative imports from parent modules
    "TRY003",   # Avoid specifying long messages outside the exception class
    "TRY004",   # Prefer `TypeError` exception for invalid type
    "TRY201",   # Use `raise` without specifying exception name
    "UP015",    # Unnecessary open mode parameters
    "UP036",    # Version block is outdated for minimum Python version
]

[lint.per-file-ignores]
"__init__.py" = [
    "F401",     # <module> imported but unused
]
"tests/*" = [
    "ARG001",  # Unused function argument
    "ARG005",  # Unused lambda argument
    "B018",    # Found useless expression. Either assign it to a variable or remove it.
    "E501",    # Line too long
    "INP001",  # File `...` is part of an implicit namespace package. Add an `__init__.py`.
    "PT006",   # Wrong type passed to first argument of `@pytest.mark.parametrize`; expected `tuple`
    "PT007",   # Wrong values type in `@pytest.mark.parametrize` expected `list` of `tuple`
    "PT011",   # `pytest.raises(ValueError)` is too broad
    "PT019",   # Fixture `_hdr_format` without value is injected as parameter, use `@pytest.mark.usefixtures` instead
    "Q001",    # Single quote multiline found but double quotes preferred
    "S105",    # Possible hardcoded password
    "S106",    # Possible hardcoded password assigned to argument: "password"
    "SIM115",  # Use a context manager for opening files
    "SIM117",  # Use a single `with` statement with multiple contexts instead of nested `with` statements
    "T201",    # `print` found
    "UP012",   # Unnecessary call to `encode` as UTF-8
]
