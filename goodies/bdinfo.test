#!/bin/bash
#
# This is a mock script that should behave mostly like the `bdinfo` script, but only takes a few
# seconds to run. This can be used for testing the communication between `upsies` and `bdinfo`.


set -o nounset

if [ "$1" = "--list" ]; then
    BD_PATH="$2"

    sleep 1
    cat <<EOF
Please wait while we scan the disc...
#   Group  Playlist File  Length    Estimated Bytes Measured Bytes

EOF
    for filename in "$BD_PATH"/BDMV/PLAYLIST/*; do
        echo "1   1      $(basename "$filename")     01:29:00  23,680,800,768  -"
    done
    exit
fi


MPLS="$1"
BD_PATH="$2"
REPORT_DEST="$3"


on_SIGHUP() {
    trap - SIGHUP
    cancel_main
    exit 1
}
trap on_SIGHUP SIGHUP

on_SIGINT() {
    trap - SIGINT
    cancel_main
    exit 2
}
trap on_SIGINT SIGINT

on_SIGQUIT() {
    trap - SIGQUIT
    cancel_main
    exit 3
}
trap on_SIGQUIT SIGQUIT

on_SIGABRT() {
    trap - SIGABRT
    cancel_main
    exit 4
}
trap on_SIGABRT SIGABRT

on_SIGALRM() {
    trap - SIGALRM
    cancel_main
    exit 5
}
trap on_SIGALRM SIGALRM

on_SIGTERM() {
    trap - SIGTERM
    cancel_main
    exit 6
}
trap on_SIGTERM SIGTERM


cancel_main() {
    kill "$main_pid"
    wait
}

main() {
    sleep 1
    for i in 10 29 87 90 100; do
        remaining_total="$(echo "100 - $i" | bc)"
        remaining_minutes="$(echo "$remaining_total / 60" | bc)"
        remaining_seconds="$(echo "$remaining_total - ($remaining_minutes * 60)" | bc)"
        echo "Scanning  $i% - 01234.MPLS     00:00:03  |  00:$remaining_minutes:$remaining_seconds"
        sleep 0.3
    done

    mkdir -p "$REPORT_DEST"
    cat <<EOF > "$REPORT_DEST"/bdinfo
Disc Label:     $(basename "$BD_PATH")
Disc Size:      648,756,560 bytes
Protection:     AACS
BDInfo:         0.7.5.5

Notes:          

BDINFO HOME:
  Cinema Squid (old)
    http://www.cinemasquid.com/blu-ray/tools/bdinfo
  UniqProject GitHub (new)
   https://github.com/UniqProject/BDInfo

INCLUDES FORUMS REPORT FOR:
  AVS Forum Blu-ray Audio and Video Specifications Thread
    http://www.avsforum.com/avs-vb/showthread.php?t=1155731


********************
PLAYLIST: 00003.MPLS
********************

<--- BEGIN FORUMS PASTE --->
[code]
                                                                                                                Total   Video                                             
Title                                                           Codec   Length  Movie Size      Disc Size       Bitrate Bitrate Main Audio Track                          Secondary Audio Track
-----                                                           ------  ------- --------------  --------------  ------- ------- ------------------                        ---------------------
00003.MPLS                                                      AVC     0:04:48 648,751,104     648,756,560     17.99   16.50   DD AC3 5.1 640Kbps                        
[/code]

[code]

DISC INFO:

Disc Label:     $(basename "$BD_PATH")
Disc Size:      648,756,560 bytes
Protection:     AACS
BDInfo:         0.7.5.5

PLAYLIST REPORT:

Name:                   $MPLS
Length:                 0:04:48.511 (h:m:s.ms)
Size:                   648,751,104 bytes
Total Bitrate:          17.99 Mbps

VIDEO:

Codec                   Bitrate             Description     
-----                   -------             -----------     
MPEG-4 AVC Video        16497 kbps          1080p / 23.976 fps / 16:9 / High Profile 4.1

AUDIO:

Codec                           Language        Bitrate         Description     
-----                           --------        -------         -----------     
Dolby Digital Audio             English         640 kbps        5.1 / 48 kHz /   640 kbps / DN -27dB

FILES:

Name            Time In         Length          Size            Total Bitrate   
----            -------         ------          ----            -------------   
00003.M2TS      0:00:00.000     0:04:48.511     648,751,104     17,987          

CHAPTERS:

Number          Time In         Length          Avg Video Rate  Max 1-Sec Rate  Max 1-Sec Time  Max 5-Sec Rate  Max 5-Sec Time  Max 10Sec Rate  Max 10Sec Time  Avg Frame Size  Max Frame Size  Max Frame Time  
------          -------         ------          --------------  --------------  --------------  --------------  --------------  --------------  --------------  --------------  --------------  --------------  
1               0:00:00.000     0:04:48.511     16,498 kbps     38,395 kbps     00:03:52.357    36,527 kbps     00:03:52.440    35,924 kbps     00:03:49.479    86,006 bytes    451,563 bytes   00:04:01.824    

STREAM DIAGNOSTICS:

File            PID             Type            Codec           Language                Seconds                 Bitrate                 Bytes           Packets         
----            ---             ----            -----           --------                --------------          --------------          -------------   -----           
00003.M2TS      4113 (0x1011)   0x1B            AVC                                     288.371                 16,506                  594,990,330     3,237,614       
00003.M2TS      4352 (0x1100)   0x81            AC3             eng (English)           288.371                 640                     23,083,520      135,255         

[/code]
<---- END FORUMS PASTE ---->

QUICK SUMMARY:

Disc Label: $(basename "$BD_PATH")
Disc Size: 648,756,560 bytes
Protection: AACS
Playlist: $MPLS
Size: 648,751,104 bytes
Length: 0:04:48.511
Total Bitrate: 17.99 Mbps
Video: MPEG-4 AVC Video / 16497 kbps / 1080p / 23.976 fps / 16:9 / High Profile 4.1
Audio: English / Dolby Digital Audio / 5.1 / 48 kHz /   640 kbps / DN -27dB

EOF
}

# While `main` is running, bash cannot handle signals. If we run `main` in the background and wait
# for it, this is issue is solved for some reason. (Magic, I presume.)
# https://stackoverflow.com/questions/33240221
main &
main_pid="$!"
wait "$main_pid"

# # Redirect
# the_process <&0 >&1 2>&2 &


# # Wait for "docker run ..." while we also wait for SIGTERM. (I think?)
# wait "$!"
