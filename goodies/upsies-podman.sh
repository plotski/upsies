#!/bin/bash
#
# Wrapper around podman that builds a rootless image and runs upsies inside it.
#
# Run this script without arguments to get usage instructions.
#
# Four directories are shared between the guest and the host:
#
#   - upsies configuration files (UPSIES_CONFIG_DIRECTORY=~/.config/upsies)
#   - upsies cache files (UPSIES_CACHE_DIRECTORY=~/.config/upsies/cache)
#   - Directory that contains releases (RELEASES_DIRECTORY=<MUST BE CONFIGURED>)
#   - Directory that contains torrents (TORRENTS_DIRECTORY=/tmp)
#
# REQUIREMENTS (on the host system)
#
#   - podman <https://podman.io>
#   - pasta <https://passt.top>
#   - newuidmap <https://github.com/shadow-maint/shadow>
#
# CONFIGURATION
#
#   ADJUST THIS SCRIPT
#
#   1.1 Set TORRENTS_DIRECTORY to the directory that contains existing torrents you are seeding and
#       point upsies to the mount point in the guest system.
#
#         $ upsies set config.torrent-create.reuse_torrent_paths ~/torrents
#
#   1.2 Set RELEASES_DIRECTORY to the directory that contains the releases you want to share. This
#       is also mounted read-only.
#
#   1.3 If they are not owned by USER_NAME, make TORRENTS_DIRECTORY and RELEASES_DIRECTORY
#       world-readable:
#
#         $ chmod -R o+r $TORRENTS_DIRECTORY
#         $ chmod -R o+r $RELEASES_DIRECTORY
#
#   BITTORRENT CLIENT
#
#   1.1 Configure upsies to connect to your BitTorrent client via localhost:BTCLIENT_RPC_PORT.
#
#         $ upsies set clients.YOURCLIENT.url localhost:PORT
#
#   1.2 Configure your BitTorrent client to listen for RPC requests on localhost:BTCLIENT_RPC_PORT.
#       You may have to use "127.0.0.1" instead of "localhost" or even adjust the --network argument
#       for `podman run`.

RELEASES_DIRECTORY=/media/my/collection
TORRENTS_DIRECTORY=/tmp
BTCLIENT_RPC_PORT=8080
UPSIES_CONFIG_DIRECTORY="$HOME"/.config/upsies
UPSIES_CACHE_DIRECTORY="$HOME"/.cache/upsies

USER_ID=$(id -u)
USER_NAME=$(id -un)

set -o nounset  # Exit if an unassigned variable is used.
set -o errexit  # Exit if any command fails.

IMAGE_TEMPDIR=/tmp/upsies_image.podman
IMAGE_TAG='upsies:latest'

print_usage() {
    cat <<-EOF
	$(basename "$0") -h|--help
	  Display this help screen.

	$(basename "$0") build [--no-cache]
	  Build podman image. With --no-cache, ignore any cached base images. The
	  --no-cache argument should be used for upgrading upsies to a new version.

	$(basename "$0") shell|run EXECUTABLE ARGUMENT1 ARGUMENT2 ...
	  Run EXECUTABLE inside the container. If no EXECUTABLE is provided, you are
	  dropped into a shell.

	$(basename "$0") shell|run [--dev PATH]
	  PATH is mounted in the container and upsies is installed from there
	  with the --editable option so you can make changes to the code outside
	  of the container and they take effect immediately inside the container.

	$(basename "$0") ARGUMENTS
	  Pass all ARGUMENTS to upsies. See \`$(basename "$0") run upsies -h\`.

	EOF
}

image_build() {
    local build_args=''
    while [ -n "${1:-}" ]; do
        case "$1" in
            --no-cache)       build_args="$build_args --no-cache"
                              shift;;
            *)                echo "Unknown option: $1" >&2
                              shift;
                              exit 1;;
        esac
    done

    mkdir -p "$IMAGE_TEMPDIR"

    DOCKERFILE_PATH="$IMAGE_TEMPDIR/Dockerfile"
    cat <<-EOF >"$DOCKERFILE_PATH"
	FROM alpine:latest

	# Install dependencies.
	RUN apk add --no-cache pipx ffmpeg mediainfo oxipng

	# Create same non-root user as on the host and become that user.
	RUN adduser -D -g '' -u $USER_ID $USER_NAME
	USER $USER_NAME:$USER_NAME
	WORKDIR /home/$USER_NAME

	# Install upsies.
	# NOTE: pipx puts executables in ~/.local/bin.
	ENV PATH="$PATH:$HOME/.local/bin"
	RUN pipx install upsies
	EOF

    echo -e "\n########## $DOCKERFILE_PATH ##########\n"
    cat "$DOCKERFILE_PATH"
    echo -e "\n########## $DOCKERFILE_PATH ##########\n"

    cd "$IMAGE_TEMPDIR"
    podman image prune --filter label=upsies --force
    podman build . --tag "$IMAGE_TAG" --label upsies $build_args

    rm -r "$IMAGE_TEMPDIR"
}

container_run() {
    # --userns=keep-id
    # This is a Podman feature that magically fixes ownership of shared directories.
    # https://github.com/mamba-org/micromamba-docker/issues/407#issuecomment-2088523507
    #
    # --volume "$RELEASES_DIRECTORY":"$RELEASES_DIRECTORY":ro
    # It is important that RELEASES_DIRECTORY is the same on host and guest. It allows for CLI tab
    # completion and the correct path is used when adding the torrent to the BitTorrent client.
    #
    # --network pasta:-T,"$BTCLIENT_RPC_PORT",--ipv4-only
    # Forward BTCLIENT_RPC_PORT from guest's localhost to hosts' localhost. Disable IPv6 because
    # /etc/hosts seems to prefer IPv6 and clients are more likely to listen on IPv4.
    local podman_cmd=(
        podman run -it --rm
               --network pasta:-T,"$BTCLIENT_RPC_PORT",--ipv4-only
               --userns=keep-id
               --volume "$UPSIES_CONFIG_DIRECTORY":"/home/$USER_NAME/.config/upsies"
               --volume "$UPSIES_CACHE_DIRECTORY":"/home/$USER_NAME/.cache/upsies"
               --volume "$TORRENTS_DIRECTORY":"/home/$USER_NAME/torrents":ro
               --volume "$RELEASES_DIRECTORY":"$RELEASES_DIRECTORY":ro
    )
    local entrypoint_arg=("$@")

    # Optionally run upsies from host inside guest.
    if [ "${1:-}" = '--dev' ]; then
        if [ "${2:-}" = '' ]; then
            echo "Missing argument for $1" >&2
            exit 1
        else
            upsies_path="$(realpath "$2")"
            shift ; shift
            podman_cmd+=(--volume "$upsies_path:$upsies_path")
            entrypoint_arg=(sh -c "pipx install --editable --force '$upsies_path' ; /bin/sh")
        fi
    fi

    # for x in "${podman_cmd[@]}" "$IMAGE_TAG" "${entrypoint_arg[@]}" ; do echo "$x"; done
    "${podman_cmd[@]}" "$IMAGE_TAG" "${entrypoint_arg[@]}"
}

if [ "${1:-}" = '' -o "${1:-}" = '-h' -o "${1:-}" = '--help' ]; then
    print_usage

elif [ "${1:-}" = 'build' ]; then
    shift
    image_build "$@"

elif [ "${1:-}" = 'shell' -o "${1:-}" = 'run' ]; then
    # Remove leading subargument (e.g. "shell") from "$@" and pass the rest as the command to run
    # inside the container.
    shift
    container_run "$@"

else
    container_run upsies "$@"

fi
