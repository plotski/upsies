import re
from unittest.mock import Mock, PropertyMock

import pytest

from upsies import errors
from upsies.trackers.base import rules as base_rules
from upsies.trackers.bhd import rules as bhd_rules


def test_BhdBannedGroup_banned_groups():
    assert bhd_rules.BhdBannedGroup.banned_groups == {
        '4K4U',
        'AOC',
        'C4K',
        'd3g',
        'EASports',
        'FGT',  # Unless no other encode is available.
        'MeGusta',
        'MezRips',
        'nikt0',
        'ProRes',
        'RARBG',
        'ReaLHD',
        'SasukeducK',
        'Sicario',
        'TEKNO3D',  # They have requested their torrents are not shared off site.
        'Telly',
        'tigole',
        'TOMMY',
        'WKS',
        'x0r',
        'YIFY',
    }


@pytest.mark.parametrize(
    argnames='group, source, exp_exception',
    argvalues=(
        # No remux from iFT.
        ('iFT', 'BluRay', None),
        ('iFT', 'BluRay Remux', errors.BannedGroup('iFT', additional_info=('No remuxes from iFT'))),
        ('NOGROUP', 'BluRay Remux', None),

        # No encode from EVO.
        ('EVO', 'WEB-DL', None),
        ('EVO', 'BluRay', errors.BannedGroup('EVO', additional_info=('No encodes, only WEB-DL'))),
        ('NOGROUP', 'BluRay', None),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_BhdBannedGroup__check_custom(group, source, exp_exception, mocker):
    assert issubclass(bhd_rules.BhdBannedGroup, base_rules.TrackerRuleBase)

    rule = bhd_rules.BhdBannedGroup(Mock())
    mocker.patch.object(type(rule), 'release_name', PropertyMock(return_value=Mock(
        group=group,
        source=source,
    )))

    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await rule._check()
    else:
        return_value = await rule._check()
        assert return_value is None
