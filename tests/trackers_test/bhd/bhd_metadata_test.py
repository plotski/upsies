import pytest

from upsies.trackers.bhd import metadata


@pytest.mark.parametrize('text_transformer',(
    lambda text: text,
    str.capitalize,
    str.lower,
    str.upper,
))
@pytest.mark.parametrize(
    argnames='text, exp_source',
    argvalues=(
        ('', None),

        # Blu-ray
        ('UHD Blu-ray Remux', 'Blu-ray'),
        ('UHD BluRay Remux', 'Blu-ray'),
        ('BluRay', 'Blu-ray'),
        ('BlueRay', None),
        ('BD25', 'Blu-ray'),
        ('BD50', 'Blu-ray'),
        ('BD66', 'Blu-ray'),
        ('BD100', 'Blu-ray'),
        ('BD24', None),

        # DVD
        ('DVD', 'DVD'),
        ('DVDRip', 'DVD'),
        ('DVD5', 'DVD'),
        ('DVD9', 'DVD'),
        ('DV', None),

        # WEB
        ('WEB-DL', 'WEB'),
        ('WEBRip', 'WEB'),
        ('W00B', None),

        # HD-DVD
        ('HDDVD', 'HD-DVD'),
        ('HD-DVD', 'HD-DVD'),
        ('HD=DVD', None),

        # HDTV
        ('HD-TV', 'HDTV'),
        ('HDTV', 'HDTV'),
        ('TV', 'HDTV'),
        ('TeeVee', None),
    ),
)
def test_sources(text, exp_source, text_transformer):
    def matching_source(text):
        for source, regex in metadata.sources.items():
            if regex.search(text):
                print(repr(regex.pattern), 'matches', repr(text))
                return source
            else:
                print(repr(regex.pattern), 'does not match', repr(text))

    assert matching_source(text_transformer(text)) == exp_source
