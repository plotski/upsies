import re
from unittest.mock import Mock, PropertyMock, call

import pytest

from upsies import errors
from upsies.trackers.base import rules as base_rules
from upsies.trackers.mtv import rules as mtv_rules


def test_MtvBannedGroup_banned_groups():
    assert mtv_rules.MtvBannedGroup.banned_groups == {
        '3LTON',
        '[Oj]',
        'aXXo',
        'BDP',
        'BRrip',
        'CM8',
        'CMCT',
        'CrEwSaDe',
        'DeadFish',
        'DNL',
        'ELiTE',
        'FaNGDiNG0',
        'FRDS',
        'FUM',
        'h65',
        'HD2DVD',
        'HDTime',
        'ION10',
        'iPlanet',
        'JIVE',
        'KiNGDOM',
        'Leffe',
        'LOAD',
        'mHD',
        'mRS',
        'mSD',
        'NhaNc3',
        'nHD',
        'nikt0',
        'nSD',
        'PandaRG',
        'PRODJi',
        'QxR',
        'RARBG',
        'RDN',
        'SANTi',
        'STUTTERSHIT',
        'TERMiNAL',
        'TM',
        'WAF',
        'x0r',
        'XS',
        'YIFY',
        'ZKBL',
        'ZmN',
    }


@pytest.mark.parametrize(
    argnames='release_name, exp_exception',
    argvalues=(
        # No encode from EVO.
        (Mock(group='EVO', source='WEB-DL', video_format=''), None),
        (Mock(group='EVO', source='BluRay', video_format=''), errors.BannedGroup('EVO', additional_info=('No encodes, only WEB-DL (Rule 2.2.1)'))),
        (Mock(group='ASDF', source='BluRay', video_format=''), None),

        # No XviD from ViSiON.
        (Mock(group='ViSiON', source='', video_format='whatever'), None),
        (Mock(group='ViSiON', source='', video_format='XviD'), errors.BannedGroup('ViSiON', additional_info=('No XviD encodes'))),
        (Mock(group='ASDF', source='', video_format='XviD'), None),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_MtvBannedGroup__check_custom(release_name, exp_exception, mocker):
    assert issubclass(mtv_rules.MtvBannedGroup, base_rules.TrackerRuleBase)

    rule = mtv_rules.MtvBannedGroup(Mock())
    mocker.patch.object(type(rule), 'release_name', PropertyMock(return_value=release_name))

    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await rule._check()
    else:
        return_value = await rule._check()
        assert return_value is None


@pytest.mark.asyncio
async def test_MtvBannedContainerFormat__banned_file_extensions():
    assert mtv_rules.MtvBannedContainerFormat._banned_file_extensions == {
        '3ivx',
        'asf',
        'f4v',
        'flv',
        'mov',
        'ogg',
        'qt',
        'rm',
        'rmv',
        'wmv',
    }


@pytest.mark.parametrize(
    argnames='banned_file_extensions, release_name, file_list, exp_exception, exp_mock_calls',
    argvalues=(
        (
            {'foo', 'bar', 'baz'},
            Mock(path='path/to/release'),
            (
                'path/to/release/this.aaa',
                'path/to/release/that.bbb',
                'path/to/release/that.ccc',
            ),
            None,
            [
                call.file_list('path/to/release'),
            ],
        ),
        (
            {'foo', 'bar', 'baz'},
            Mock(path='path/to/release'),
            (
                'path/to/release/this.aaa',
                'path/to/release/that.Foo',
                'path/to/release/that.ccc',
            ),
            errors.RuleBroken('Banned container format: foo: path/to/release/that.Foo'),
            [
                call.file_list('path/to/release'),
            ],
        ),
        (
            {'foo', 'bar', 'baz'},
            Mock(path='path/to/release'),
            (
                'path/to/release/this.aaa',
                'path/to/release/that.BAR',
                'path/to/release/that.ccc',
            ),
            errors.RuleBroken('Banned container format: bar: path/to/release/that.BAR'),
            [
                call.file_list('path/to/release'),
            ],
        ),
        (
            {'foo', 'bar', 'baz'},
            Mock(path='path/to/release'),
            (
                'path/to/release/this.aaa',
                'path/to/release/that.bAz',
                'path/to/release/that.ccc',
            ),
            errors.RuleBroken('Banned container format: baz: path/to/release/that.bAz'),
            [
                call.file_list('path/to/release'),
            ],
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_MtvBannedContainerFormat__check(banned_file_extensions, release_name, file_list, exp_exception, exp_mock_calls, mocker):
    assert issubclass(mtv_rules.MtvBannedContainerFormat, base_rules.TrackerRuleBase)

    rule = mtv_rules.MtvBannedContainerFormat(Mock())
    mocker.patch.object(type(rule), '_banned_file_extensions', banned_file_extensions)
    mocker.patch.object(type(rule), 'release_name', PropertyMock(return_value=release_name))
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.fs.file_list', return_value=file_list), 'file_list')

    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await rule._check()
    else:
        return_value = await rule._check()
        assert return_value is None

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize('mangle_group', (str, str.lower, str.upper))
@pytest.mark.parametrize(
    argnames='resolution_int, release_name, exempt_groups, exp_exception',
    argvalues=(
        pytest.param(
            576,
            Mock(video_format='H.264', group='NOGROUP'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='576p, H.264, non-exempt group',
        ),
        pytest.param(
            576,
            Mock(video_format='H.264', group='BAR'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='576p, H.264, exempt group',
        ),
        pytest.param(
            576,
            Mock(video_format='H.265', group='NOGROUP'),
            ('FOO', 'BAR', 'BAZ'),
            errors.RuleBroken('HEVC is only allowed for UHD'),
            id='576p, H.265, non-exempt group',
        ),
        pytest.param(
            576,
            Mock(video_format='H.265', group='BAR'),
            ('FOO', 'BAR', 'BAZ'),
            errors.RuleBroken('HEVC is only allowed for HD'),
            id='576p, H.265, exempt group',
        ),

        pytest.param(
            720,
            Mock(video_format='H.264', group='NOGROUP'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='720p, H.264, non-exempt group',
        ),
        pytest.param(
            720,
            Mock(video_format='H.264', group='BAR'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='720p, H.264, exempt group',
        ),
        pytest.param(
            720,
            Mock(video_format='H.265', group='NOGROUP'),
            ('FOO', 'BAR', 'BAZ'),
            errors.RuleBroken('HEVC is only allowed for UHD'),
            id='720p, H.265, non-exempt group',
        ),
        pytest.param(
            720,
            Mock(video_format='H.265', group='BAR'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='720p, H.265, exempt group',
        ),

        pytest.param(
            1080,
            Mock(video_format='H.264', group='NOGROUP'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='1080p, H.264, non-exempt group',
        ),
        pytest.param(
            1080,
            Mock(video_format='H.264', group='BAR'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='1080p, H.264, exempt group',
        ),
        pytest.param(
            1080,
            Mock(video_format='H.265', group='NOGROUP'),
            ('FOO', 'BAR', 'BAZ'),
            errors.RuleBroken('HEVC is only allowed for UHD'),
            id='1080p, H.265, non-exempt group',
        ),
        pytest.param(
            1080,
            Mock(video_format='H.265', group='BAR'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='1080p, H.265, exempt group',
        ),


        pytest.param(
            2160,
            Mock(video_format='H.264', group='NOGROUP'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='2160p, H.264, non-exempt group',
        ),
        pytest.param(
            2160,
            Mock(video_format='H.264', group='BAR'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='2160p, H.264, exempt group',
        ),
        pytest.param(
            2160,
            Mock(video_format='H.265', group='NOGROUP'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='2160p, H.265, non-exempt group',
        ),
        pytest.param(
            2160,
            Mock(video_format='H.265', group='BAR'),
            ('FOO', 'BAR', 'BAZ'),
            None,
            id='2160p, H.265, exempt group',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_MtvHevcUhdOnly__check(resolution_int, release_name, exempt_groups, exp_exception, mangle_group, mocker):
    assert issubclass(mtv_rules.MtvHevcUhdOnly, base_rules.TrackerRuleBase)

    rule = mtv_rules.MtvHevcUhdOnly(Mock())
    mocker.patch.object(type(rule), '_exempt_groups', exempt_groups)
    mocker.patch.object(type(rule), 'release_name', PropertyMock(return_value=release_name))
    rule.release_name.path = 'path/to/release'
    rule.release_name.group = mangle_group(rule.release_name.group)
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.utils.mediainfo.video.get_resolution_int', return_value=resolution_int),
        'get_resolution_int',
    )

    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await rule._check()
    else:
        return_value = await rule._check()
        assert return_value is None

    assert mocks.mock_calls == [call.get_resolution_int('path/to/release')]
