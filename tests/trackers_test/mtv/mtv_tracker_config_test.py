import base64

import pytest

from upsies import utils
from upsies.trackers import base, mtv


@pytest.fixture
def tracker_config():
    return mtv.config.MtvTrackerConfig()


def test_MtvTrackerConfig_defaults(tracker_config):
    assert set(tracker_config) == {
        'base_url',
        'username',
        'password',
        'cookies_filepath',
        'announce_url',
        'randomize_infohash',
        'image_host',
        'screenshots_count',
        'exclude',
        'anonymous',

        # Inherited from TrackerConfigBase
        'add_to',
        'copy_to',
    }


def test_MtvTrackerConfig_defaults_base_url(tracker_config):
    assert tracker_config['base_url'] == base64.b64decode('aHR0cHM6Ly93d3cubW9yZXRoYW50di5tZQ==').decode('ascii')


def test_MtvTrackerConfig_defaults_username(tracker_config):
    assert tracker_config['username'] == ''


def test_MtvTrackerConfig_defaults_password(tracker_config):
    assert tracker_config['password'] == ''


def test_MtvTrackerConfig_defaults_cookies_filepath(tracker_config):
    assert tracker_config['cookies_filepath'] == ''
    assert tracker_config['cookies_filepath'].description == (
        'File that stores permanent session cookies.\n'
        'If this is not set, a new user session is started for each upload.'
    )


def test_MtvTrackerConfig_defaults_announce_url(tracker_config):
    assert tracker_config['announce_url'] == ''
    assert tracker_config['announce_url'].description == (
        'Your personal announce URL. Automatically fetched from the website if not set.'
    )


def test_MtvTrackerConfig_defaults_image_host(tracker_config, assert_config_list_of_choice):
    exp_options = ('dummy', 'imgbox', 'ptpimg')
    assert_config_list_of_choice(
        items=tracker_config['image_host'],
        exp_items=('imgbox',),
        exp_options=exp_options,
        exp_description=(
            'List of image hosting service names. The first service is normally used '
            + 'with the others as backup if uploading to the first fails.\n'
            + 'Supported services: ' + ', '.join(sorted(exp_options))
        ),
    )


def test_MtvTrackerConfig_defaults_screenshots(tracker_config, assert_config_number):
    assert_config_number(
        number=tracker_config['screenshots_count'],
        value=4,
        min=3,
        max=10,
        description='How many screenshots to make.',
    )


def test_MtvTrackerConfig_defaults_exclude(tracker_config):
    assert tracker_config['exclude'] == (
        utils.types.RegEx(base.exclude.checksums),
        utils.types.RegEx(base.exclude.extras),
        utils.types.RegEx(base.exclude.images),
        utils.types.RegEx(base.exclude.nfo),
        utils.types.RegEx(base.exclude.samples),
        utils.types.RegEx(base.exclude.subtitles),
    )


def test_MtvTrackerConfig_defaults_anonymous(tracker_config):
    assert isinstance(tracker_config['anonymous'], utils.types.Bool)
    assert not tracker_config['anonymous']
    assert tracker_config['anonymous'] == 'no'
    assert tracker_config['anonymous'].description == (
        'Whether your username is displayed on your uploads.'
    )


def test_MtvTrackerConfig_arguments(tracker_config):
    exp_argument_definitions = {
        'submit': {
            ('--imdb', '--im'),
            ('--anonymous', '--an'),
            ('--screenshots-count', '--ssc'),
            ('--screenshots', '--ss'),
            ('--only-description', '--od'),
            ('--only-title', '--ot'),
            ('--ignore-dupes', '--id'),
        },
    }
    assert set(tracker_config.argument_definitions) == set(exp_argument_definitions)
    for command, exp_arguments in exp_argument_definitions.items():
        assert set(tracker_config.argument_definitions[command]) == exp_arguments


def test_MtvTrackerConfig_argument_definitions_submit_imdb(tracker_config):
    assert tracker_config.argument_definitions['submit'][('--imdb', '--im')] == {
        'help': 'IMDb ID or URL',
        'type': utils.argtypes.webdb_id('imdb'),
    }


def test_MtvTrackerConfig_argument_definitions_submit_anonymous(tracker_config):
    assert tracker_config.argument_definitions['submit'][('--anonymous', '--an')] == {
        'help': 'Hide your username for this submission',
        'action': 'store_true',
        'default': None,
    }


def test_MtvTrackerConfig_argument_definitions_submit_screenshots_count(tracker_config):
    assert tracker_config.argument_definitions['submit'][('--screenshots-count', '--ssc')] == {
        'help': ('How many screenshots to make '
                 f'(min={tracker_config["screenshots_count"].min}, '
                 f'max={tracker_config["screenshots_count"].max})'),
        'type': utils.argtypes.number_of_screenshots(
            min=tracker_config['screenshots_count'].min,
            max=tracker_config['screenshots_count'].max,
        ),
    }


def test_MtvTrackerConfig_argument_definitions_submit_screenshots(tracker_config):
    assert tracker_config.argument_definitions['submit'][('--screenshots', '--ss')] == {
        'help': (
            'Path(s) to existing screenshot file(s)\n'
            'Directories are searched recursively.\n'
            'Precreated screenshots are used in addition to automatically generated screenshots.'
        ),
        'nargs': '+',
        'action': 'extend',
        'type': utils.argtypes.files_with_extension('png'),
        'metavar': 'SCREENSHOT',
    }


def test_MtvTrackerConfig_argument_definitions_submit_only_description(tracker_config):
    assert tracker_config.argument_definitions['submit'][('--only-description', '--od')] == {
        'help': 'Only generate description (do not upload anything)',
        'action': 'store_true',
        'group': 'generate-metadata',
    }


def test_MtvTrackerConfig_argument_definitions_submit_only_title(tracker_config):
    assert tracker_config.argument_definitions['submit'][('--only-title', '--ot')] == {
        'help': 'Only generate title (do not upload anything)',
        'action': 'store_true',
        'group': 'generate-metadata',
    }


def test_MtvTrackerConfig_argument_definitions_submit_ignore_dupes(tracker_config):
    assert tracker_config.argument_definitions['submit'][('--ignore-dupes', '--id')] == {
        'help': 'Force submission even if the tracker reports duplicates',
        'action': 'store_true',
    }
