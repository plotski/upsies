import dataclasses
import re
from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from upsies import errors, utils
from upsies.trackers import ptp


@pytest.fixture
def tracker():
    tracker = Mock()
    tracker.name = 'ptp'
    return tracker


@pytest.fixture
def ptp_tracker_jobs(tracker, tmp_path):
    content_path = tmp_path / 'Foo 2000 1080p BluRay x264-ASDF'

    ptp_tracker_jobs = ptp.jobs.PtpTrackerJobs(
        content_path=str(content_path),
        tracker=tracker,
        image_hosts=(Mock(), Mock()),
        btclient=Mock(),
        torrent_destination=str(tmp_path / 'destination'),
        common_job_args={
            'home_directory': str(tmp_path / 'home_directory'),
            'ignore_cache': True,
        },
        options=None,
    )

    return ptp_tracker_jobs


@pytest.fixture
def mock_job_attributes(mocker):
    def mock_job_attributes(ptp_tracker_jobs):
        job_attrs = (
            'login_job',

            # Common interactive jobs
            'playlists_job',
            'imdb_job',
            'tmdb_job',
            'type_job',
            'ptp_group_id_job',
            'source_job',
            'scene_check_job',

            # Interactive jobs that only run if movie does not exists on PTP yet
            'title_job',
            'year_job',
            'edition_job',
            'plot_job',
            'tags_job',
            'poster_job',
            'artists_job',

            # Background jobs
            'create_torrent_job',
            'mediainfo_job',
            'bdinfo_job',
            'screenshots_job',
            'upload_screenshots_job',
            'audio_languages_job',
            'subtitle_languages_job',
            'trumpable_job',
            'description_job',
            'rules_job',
            'nfo_job',
        )
        for job_attr in job_attrs:
            mocker.patch.object(
                type(ptp_tracker_jobs),
                job_attr,
                PropertyMock(return_value=Mock(attr=job_attr)),
            )

    return mock_job_attributes


def test_jobs_before_upload_items(ptp_tracker_jobs, mock_job_attributes):
    mock_job_attributes(ptp_tracker_jobs)

    assert tuple(job.attr for job in ptp_tracker_jobs.jobs_before_upload) == (
        'login_job',

        # Common interactive jobs
        'playlists_job',
        'imdb_job',
        'tmdb_job',
        'type_job',
        'ptp_group_id_job',
        'source_job',
        'scene_check_job',

        # Interactive jobs that only run if movie does not exists on PTP yet
        'title_job',
        'year_job',
        'edition_job',
        'plot_job',
        'tags_job',
        'poster_job',
        'artists_job',

        # Background jobs
        'create_torrent_job',
        'mediainfo_job',
        'bdinfo_job',
        'screenshots_job',
        'upload_screenshots_job',
        'audio_languages_job',
        'subtitle_languages_job',
        'trumpable_job',
        'description_job',
        'rules_job',
        'nfo_job',
    )


def test_isolated_jobs__only_description(ptp_tracker_jobs, mock_job_attributes, mocker):
    mock_job_attributes(ptp_tracker_jobs)
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value={'only_description': True}))
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    assert ptp_tracker_jobs.isolated_jobs is ptp_tracker_jobs.get_job_and_dependencies.return_value
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [
        call(ptp_tracker_jobs.description_job)
    ]

def test_isolated_jobs__no_isolated_jobs(ptp_tracker_jobs, mock_job_attributes, mocker):
    mock_job_attributes(ptp_tracker_jobs)
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value={}))
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    assert ptp_tracker_jobs.isolated_jobs == ()
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == []


@pytest.mark.parametrize(
    argnames='options, exp_autofinish, exp_ignore_cache',
    argvalues=(
        ({}, False, False),
        ({'type': ''}, False, False),
        ({'type': None}, False, False),
        ({'type': 'Movie'}, True, True),
        ({'type': 'Short'}, True, True),
    ),
    ids=lambda v: repr(v),
)
def test_type_job(options, exp_autofinish, exp_ignore_cache, ptp_tracker_jobs, mocker):
    ChoiceJob_mock = mocker.patch('upsies.jobs.dialog.ChoiceJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value=options))
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'tmdb_job', PropertyMock())

    assert ptp_tracker_jobs.type_job is ChoiceJob_mock.return_value
    assert ChoiceJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Type',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        prejobs=ptp_tracker_jobs.get_job_and_dependencies.return_value,
        autodetect=ptp_tracker_jobs.autodetect_type,
        options=ptp.metadata.types,
        autofinish=exp_autofinish,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('type')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call('type_job')]
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [call(
        ptp_tracker_jobs.imdb_job,
        ptp_tracker_jobs.tmdb_job,
    )]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=exp_ignore_cache)]


@pytest.mark.parametrize(
    argnames='options, selected, main_video_duration, exp_return_value, exp_duration_calls',
    argvalues=(
        ({}, {'imdb': {}, 'tmdb': {}}, 90 * 60, None, True),
        ({'type': 'movie'}, {'imdb': {}, 'tmdb': {}}, 0, 'Feature Film', False),
        ({'type': 'short'}, {'imdb': {}, 'tmdb': {}}, 0, 'Short Film', False),
        ({'type': 'series'}, {'imdb': {}, 'tmdb': {}}, 0, 'Miniseries', False),
        ({'type': 'comedy'}, {'imdb': {}, 'tmdb': {}}, 0, 'Stand-up Comedy', False),
        ({'type': 'live'}, {'imdb': {}, 'tmdb': {}}, 0, 'Live Performance', False),
        ({'type': 'collection'}, {'imdb': {}, 'tmdb': {}}, 0, 'Movie Collection', False),
        ({'type': 'gobbledygook'}, {'imdb': {'type': utils.release.ReleaseType.season}, 'tmdb': {}}, 90 * 60, 'Miniseries', False),
        ({'type': 'gobbledygook'}, {'imdb': {'type': utils.release.ReleaseType.movie}, 'tmdb': {}}, 90 * 60, None, True),
        ({'type': 'gobbledygook'}, {'imdb': {}, 'tmdb': {'type': utils.release.ReleaseType.season}}, 90 * 60, 'Miniseries', False),
        ({'type': 'gobbledygook'}, {'imdb': {}, 'tmdb': {'type': utils.release.ReleaseType.movie}}, 90 * 60, None, True),
        ({}, {'imdb': {}, 'tmdb': {}}, 45 * 60, 'Short Film', True),
        ({}, {'imdb': {}, 'tmdb': {}}, (45 * 60) + 1, None, True),
    ),
    ids=lambda v: str(v),
)
def test_autodetect_type(
        options, selected, main_video_duration,
        exp_return_value, exp_duration_calls,
        ptp_tracker_jobs, mocker,
):
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value=options))
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock(return_value=Mock(
        selected=selected['imdb'],
    )))
    mocker.patch.object(type(ptp_tracker_jobs), 'tmdb_job', PropertyMock(return_value=Mock(
        selected=selected['tmdb'],
    )))

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.utils.fs.find_main_videos', return_value=(
            'Foo.S01E01.mkv', 'Foo.S01E02.mkv', 'Foo.S01E03.mkv',
        )),
        'find_main_videos',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.mediainfo.get_duration', return_value=main_video_duration),
        'get_duration',
    )

    return_value = ptp_tracker_jobs.autodetect_type('job_')
    assert return_value == exp_return_value

    exp_mock_calls = []
    if exp_duration_calls:
        exp_mock_calls.extend((
            call.find_main_videos(ptp_tracker_jobs.content_path),
            call.get_duration('Foo.S01E01.mkv'),
        ))
    assert mocks.mock_calls == exp_mock_calls


def test_imdb_job(ptp_tracker_jobs):
    assert ptp_tracker_jobs.imdb_job.no_id_ok is True


def test_tmdb_job(ptp_tracker_jobs, mocker):
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    assert ptp_tracker_jobs.tmdb_job.no_id_ok is True
    assert ptp_tracker_jobs.imdb_job in ptp_tracker_jobs.tmdb_job.prejobs
    assert ptp_tracker_jobs.tmdb_job.precondition == ptp_tracker_jobs.make_precondition.return_value
    assert ptp_tracker_jobs.make_precondition.call_args_list[-1] == call(
        'tmdb_job',
        precondition=ptp_tracker_jobs.no_imdb_id_available,
    )


@pytest.mark.parametrize(
    argnames='imdb_id, exp_no_imdb_id_available',
    argvalues=(
        ('', True),
        (None, True),
        ('tt123456', False),
    ),
    ids=lambda v: str(v),
)
def test_no_imdb_id_available(imdb_id, exp_no_imdb_id_available, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_id', PropertyMock(return_value=imdb_id))
    return_value = ptp_tracker_jobs.no_imdb_id_available()
    assert return_value is exp_no_imdb_id_available


def test_audio_languages_job(ptp_tracker_jobs, mocker):
    CustomJob_mock = mocker.patch('upsies.jobs.custom.CustomJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.audio_languages_job is CustomJob_mock.return_value
    assert CustomJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Audio Languages',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        worker=ptp_tracker_jobs.autodetect_audio_languages,
        no_output_is_ok=True,
        catch=(
            errors.ContentError,
        ),
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('audio-languages')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call('audio_languages_job')]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.parametrize(
    argnames='languages, exp_mock_calls',
    argvalues=(
        (
            ('en', 'es', 'ko'),
            [
                call.add_output('en'),
                call.add_output('es'),
                call.add_output('ko'),
            ],
        ),
        (
            ('en', '?', 'ko'),
            [
                call.add_output('en'),
                call.add_output('?'),
                call.add_output('ko'),
            ],
        ),
        (
            {},
            [],
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_audio_languages(languages, exp_mock_calls, ptp_tracker_jobs, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.audio.get_audio_languages', return_value=languages), 'get_audio_languages')
    mocker.patch.object(type(ptp_tracker_jobs), 'audio_languages_job', PropertyMock())
    mocks.attach_mock(ptp_tracker_jobs.audio_languages_job.add_output, 'add_output')

    return_value = await ptp_tracker_jobs.autodetect_audio_languages('ignored job')
    assert return_value is None
    assert mocks.mock_calls == [
        call.get_audio_languages(
            ptp_tracker_jobs.content_path,
            default='?',
            exclude_commentary=True,
        ),
        *exp_mock_calls,
    ]


def test_subtitle_languages_job(ptp_tracker_jobs, mocker):
    CustomJob_mock = mocker.patch('upsies.jobs.custom.CustomJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.subtitle_languages_job is CustomJob_mock.return_value
    assert CustomJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Subtitle Languages',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        worker=ptp_tracker_jobs.autodetect_subtitle_languages,
        catch=(
            ValueError,
        ),
        no_output_is_ok=True,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('subtitle-languages')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call('subtitle_languages_job')]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


class Subtitle:
    def __init__(self, language):
        self.language = language

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return self.language == other.language
        return NotImplemented

    def __hash__(self):
        return hash(self.language)

    def __str__(self):
        return self.language

@pytest.mark.parametrize(
    argnames='options, exp_user_subtitles, exp_user_subtitles_exception',
    argvalues=(
        pytest.param({}, (), None, id='No user subtitles (no key)'),
        pytest.param({'subtitles': None}, (), None, id='No user subtitles (None)'),
        pytest.param({'subtitles': ()}, (), None, id='No user subtitles (())'),
        pytest.param(
            {'subtitles': (Subtitle('sv'), Subtitle('no'), Subtitle('da'))},
            (Subtitle('sv'), Subtitle('no'), Subtitle('da')),
            None,
            id='Only known langauges',
        ),
        pytest.param(
            {'subtitles': (Subtitle('sv'), Subtitle('this'), Subtitle('da'), Subtitle('that'))},
            (),
            ValueError('Unsupported subtitles: this, that'),
            id='Multiple unknown langauges',
        ),
    ),
)
@pytest.mark.parametrize(
    argnames='autodetected_subtitles, exp_warn',
    argvalues=(
        pytest.param(
            (),
            False,
            id='No autodetected subtitles',
        ),
        pytest.param(
            (Subtitle('en'), Subtitle('es'), Subtitle('pt')),
            False,
            id='All subtitles have language tags',
        ),
        pytest.param(
            (Subtitle('en'), Subtitle('?'), Subtitle('pt')),
            True,
            id='Not all subtitles have language tags',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_subtitle_languages(
        options, exp_user_subtitles, exp_user_subtitles_exception,
        autodetected_subtitles, exp_warn,
        ptp_tracker_jobs, mocker,
):
    mocks = Mock()
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value=options))
    mocker.patch.object(type(ptp_tracker_jobs), 'subtitles', PropertyMock(return_value=autodetected_subtitles))
    mocker.patch.object(type(ptp_tracker_jobs), 'subtitle_languages_job', PropertyMock())
    mocks.attach_mock(ptp_tracker_jobs.subtitle_languages_job.add_output, 'add_output')
    mocks.attach_mock(ptp_tracker_jobs.subtitle_languages_job.warn, 'warn')

    if exp_user_subtitles_exception:
        with pytest.raises(type(exp_user_subtitles_exception), match=rf'^{re.escape(str(exp_user_subtitles_exception))}$'):
            await ptp_tracker_jobs.autodetect_subtitle_languages('ignored job')
        assert mocks.mock_calls == []
    else:
        return_value = await ptp_tracker_jobs.autodetect_subtitle_languages('ignored job')
        assert return_value is None

        exp_mock_calls = [
            call.add_output(subtitle)
            for subtitle in exp_user_subtitles + autodetected_subtitles
        ]
        if exp_warn:
            exp_mock_calls.append(
                call.warn(
                    "Some subtitle tracks don't have a language tag.\n"
                    'Please add any missing subtitle languages manually\n'
                    'on the website after uploading.'
                )
            )
        assert mocks.mock_calls == exp_mock_calls


def test_trumpable_job(ptp_tracker_jobs, mocker):
    CustomJob_mock = mocker.patch('upsies.jobs.custom.CustomJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(type(ptp_tracker_jobs), 'audio_languages_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'subtitle_languages_job', PropertyMock())

    assert ptp_tracker_jobs.trumpable_job is CustomJob_mock.return_value
    assert CustomJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Trumpable',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        prejobs=ptp_tracker_jobs.get_job_and_dependencies.return_value,
        worker=ptp_tracker_jobs.autodetect_trumpable,
        no_output_is_ok=True,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('trumpable')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call('trumpable_job')]
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [call(
        ptp_tracker_jobs.audio_languages_job,
        ptp_tracker_jobs.subtitle_languages_job,
    )]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.parametrize(
    argnames='options_hardcoded_subtitles, exp_hardcoded_subtitles_from_option',
    argvalues=(
        ({}, False),
        ({'hardcoded_subtitles': None}, False),
        ({'hardcoded_subtitles': False}, False),
        ({'hardcoded_subtitles': True}, True),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.parametrize(
    argnames=(
        'options_no_english_subtitles, trumpable_no_english_subtitles, trumpable_prompt,'
        'exp_no_english_subtitles, exp_hardcoded_subtitles_from_prompt'
    ),
    argvalues=(
        pytest.param(
            {'no_english_subtitles': True},
            AsyncMock(return_value=None),
            AsyncMock(return_value=None),
            True,
            False,
            id='User specified no_english_subtitles=True',
        ),
        pytest.param(
            {'no_english_subtitles': False},
            AsyncMock(return_value=None),
            AsyncMock(return_value=None),
            False,
            False,
            id='User specified no_english_subtitles=False',
        ),
        pytest.param(
            {'no_english_subtitles': None},
            AsyncMock(return_value=True),
            AsyncMock(return_value=True),
            False,
            True,
            id='Autodetected no_english_subtitles=True and user says hardcoded_subtitles=True',
        ),
        pytest.param(
            {},
            AsyncMock(return_value=True),
            AsyncMock(return_value=False),
            True,
            False,
            id='Autodetected no_english_subtitles=True and user says hardcoded_subtitles=False',
        ),
        pytest.param(
            {},
            AsyncMock(return_value=False),
            AsyncMock(return_value=None),
            False,
            False,
            id='Autodetected no_english_subtitles=False',
        ),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_autodetect_trumpable(
        options_hardcoded_subtitles, exp_hardcoded_subtitles_from_option,
        options_no_english_subtitles, trumpable_no_english_subtitles, trumpable_prompt,
        exp_no_english_subtitles, exp_hardcoded_subtitles_from_prompt,
        ptp_tracker_jobs, mocker,
):
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value={
        **options_hardcoded_subtitles,
        **options_no_english_subtitles,
    }))
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, 'trumpable_no_english_subtitles', trumpable_no_english_subtitles),
        'trumpable_no_english_subtitles',
    )
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, 'trumpable_prompt', trumpable_prompt),
        'trumpable_prompt',
    )

    exp_reasons = set()
    if exp_hardcoded_subtitles_from_option or exp_hardcoded_subtitles_from_prompt:
        exp_reasons.add(ptp.metadata.TrumpableReason.HARDCODED_SUBTITLES)
    if exp_no_english_subtitles:
        exp_reasons.add(ptp.metadata.TrumpableReason.NO_ENGLISH_SUBTITLES)

    exp_mock_calls = []
    # if options_no_english_subtitles.get('no_english_subtitles', None) is None:
    if options_no_english_subtitles.get('no_english_subtitles', None) is None:
        exp_mock_calls.append(call.trumpable_no_english_subtitles())
        if mocks.trumpable_no_english_subtitles.return_value:
            if exp_hardcoded_subtitles_from_option:
                exp_mock_calls.append(call.trumpable_prompt('Are the hardcoded subtitles English?'))
            else:
                exp_mock_calls.append(call.trumpable_prompt('Does this release have hardcoded English subtitles?'))

    reasons = await ptp_tracker_jobs.autodetect_trumpable('job_')
    assert reasons == exp_reasons
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize('en_tag', ('en', 'en (forced) [SRT]', 'en [SRT]'))
@pytest.mark.parametrize(
    argnames=(
        'audio_languages, subtitle_languages, trumpable_prompt_return_values,'
        'exp_mock_calls, exp_return_value'
    ),
    argvalues=(
        pytest.param((), (), (), [], False, id='Silent film (no audio tracks)'),
        pytest.param(
            ('fi', 'en', 'se'),
            (),
            (),
            [],
            False,
            id='English audio + No subtitles',
        ),
        pytest.param(
            ('fi', 'se'),
            ('es', 'en', 'it'),
            (),
            [],
            False,
            id='No English audio + English subtitles',
        ),
        pytest.param(
            ('fi', 'se'),
            ('es', 'it'),
            (),
            [],
            True,
            id='No English audio + No English subtitles',
        ),
        pytest.param(
            ('fi', '?', 'se'),
            ('es', 'it'),
            (False,),
            [
                call.trumpable_prompt(
                    'Does this release have an English audio track?\n'
                    '(Commentary and the like do not count.)'
                ),
            ],
            True,
            id='Unknown audio + No English subtitles + has_english_audio_track=False',
        ),
        pytest.param(
            ('fi', '?', 'se'),
            ('es', 'it'),
            (True,),
            [
                call.trumpable_prompt(
                    'Does this release have an English audio track?\n'
                    '(Commentary and the like do not count.)'
                ),
            ],
            False,
            id='Unknown audio + No English subtitles + has_english_audio_track=True',
        ),
        pytest.param(
            ('fi', 'se'),
            ('es', '?', 'it'),
            (False,),
            [
                call.trumpable_prompt(
                    'Does this release have English subtitles for the main audio track?'
                ),
            ],
            True,
            id='No English audio + Unknown subtitles + has_english_subtitle_track=False',
        ),
        pytest.param(
            ('fi', 'se'),
            ('es', '?', 'it'),
            (True,),
            [
                call.trumpable_prompt(
                    'Does this release have English subtitles for the main audio track?'
                ),
            ],
            False,
            id='No English audio + Unknown subtitles + has_english_subtitle_track=True',
        ),
    ),
)
@pytest.mark.asyncio
async def test_trumpable_no_english_subtitles(
        audio_languages, subtitle_languages, trumpable_prompt_return_values,
        exp_return_value, exp_mock_calls,
        en_tag,
        ptp_tracker_jobs, mocker,
):
    mocks = Mock()
    mocker.patch.object(type(ptp_tracker_jobs), 'audio_languages_job', PropertyMock(return_value=Mock(
        is_finished=True,
        output=tuple(lang.replace('en', en_tag) for lang in audio_languages),
    )))
    mocker.patch.object(type(ptp_tracker_jobs), 'subtitle_languages_job', PropertyMock(return_value=Mock(
        is_finished=True,
        output=tuple(lang.replace('en', en_tag) for lang in subtitle_languages),
    )))
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, 'trumpable_prompt', AsyncMock(side_effect=trumpable_prompt_return_values)),
        'trumpable_prompt',
    )

    return_value = await ptp_tracker_jobs.trumpable_no_english_subtitles()
    assert return_value is exp_return_value
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.asyncio
async def test_trumpable_prompt(ptp_tracker_jobs, mocker):
    mocks = Mock()
    mocker.patch.object(type(ptp_tracker_jobs), 'trumpable_job', PropertyMock(return_value=Mock(
        add_prompt=AsyncMock(return_value=('Maybe', 'True or False')),
    )))
    mocks.attach_mock(ptp_tracker_jobs.trumpable_job.add_prompt, 'add_prompt')
    mocks.attach_mock(mocker.patch('upsies.uis.prompts.RadioListPrompt'), 'RadioListPrompt')

    return_value = await ptp_tracker_jobs.trumpable_prompt('Is this it?')
    assert return_value == 'True or False'
    assert mocks.mock_calls == [
        call.RadioListPrompt(
            question='Is this it?',
            options=(
                ('Yes', True),
                ('No', False),
            ),
            focused=1,
        ),
        call.add_prompt(mocks.RadioListPrompt.return_value),
    ]


def test_description_job(ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'playlists_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'mediainfo_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'bdinfo_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'screenshots_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'upload_screenshots_job', PropertyMock())
    mocker.patch.object(ptp_tracker_jobs, 'generate_description')
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.description_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Description',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        prejobs=ptp_tracker_jobs.get_job_and_dependencies.return_value,
        text=ptp_tracker_jobs.generate_description,
        read_only=True,
        hidden=True,
        finish_on_success=True,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('description')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call('description_job')]
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [call(
        ptp_tracker_jobs.playlists_job,
        ptp_tracker_jobs.mediainfo_job,
        ptp_tracker_jobs.bdinfo_job,
        ptp_tracker_jobs.screenshots_job,
        ptp_tracker_jobs.upload_screenshots_job,
    )]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


def mock_video_info(video_filepath, mediainfos, bdinfos, screenshots):
    screenshot_url_base = 'https://' + video_filepath.replace('/', '.')
    return {video_filepath: {
        'mediainfos': tuple(f'<mediainfo {i} for {video_filepath}>' for i in range(1, mediainfos + 1)),
        'bdinfos': tuple(Mock(quick_summary=f'<bdinfo {i} for {video_filepath}>') for i in range(1, bdinfos + 1)),
        'screenshot_urls': tuple(f'{screenshot_url_base}.{i}.png' for i in range(1, screenshots + 1)),
    }}

@pytest.mark.parametrize(
    argnames='content_path, exp_original_release_name',
    argvalues=(
        ('path/to/Foo.2002.720p.x264-ASDF.mkv', 'Foo.2002.720p.x264-ASDF'),
        ('path/to/Foo.2002.720p.x264-ASDF', 'Foo.2002.720p.x264-ASDF'),
        ('Foo.2002.720p.x264-ASDF.mkv', 'Foo.2002.720p.x264-ASDF'),
        ('Foo.2002.720p.x264-ASDF', 'Foo.2002.720p.x264-ASDF'),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.parametrize(
    argnames='video_info, exp_description',
    argvalues=(
        pytest.param(
            {},
            '[size=4][b]{exp_original_release_name}[/b][/size]',
            id='No videos',
        ),
        pytest.param(
            {
                **mock_video_info('path/to/video1.mkv', mediainfos=0, bdinfos=0, screenshots=0),
                **mock_video_info('path/to/video2.mkv', mediainfos=0, bdinfos=0, screenshots=0),
                **mock_video_info('path/to/video3.mkv', mediainfos=0, bdinfos=0, screenshots=0),
            },
            '[size=4][b]{exp_original_release_name}[/b][/size]',
            id='no mediainfos, no bdinfos, no screenshots',
        ),
        pytest.param(
            {
                **mock_video_info('path/to/video1.mkv', mediainfos=0, bdinfos=0, screenshots=1),
                **mock_video_info('path/to/video2.mkv', mediainfos=0, bdinfos=0, screenshots=2),
                **mock_video_info('path/to/video3.mkv', mediainfos=0, bdinfos=0, screenshots=3),
            },
            (
                '[size=4][b]{exp_original_release_name}[/b][/size]'
                '\n\n'
                '[img=https://path.to.video1.mkv.1.png]\n'
                '\n[hr]\n'
                '[img=https://path.to.video2.mkv.1.png]\n'
                '[img=https://path.to.video2.mkv.2.png]\n'
                '\n[hr]\n'
                '[img=https://path.to.video3.mkv.1.png]\n'
                '[img=https://path.to.video3.mkv.2.png]\n'
                '[img=https://path.to.video3.mkv.3.png]'
            ),
            id='no mediainfos, no bdinfos, screenshots',
        ),
        pytest.param(
            {
                **mock_video_info('path/to/video1.mkv', mediainfos=0, bdinfos=1, screenshots=0),
                **mock_video_info('path/to/video2.mkv', mediainfos=0, bdinfos=2, screenshots=0),
                **mock_video_info('path/to/video3.mkv', mediainfos=0, bdinfos=3, screenshots=0),
            },
            (
                '[size=4][b]{exp_original_release_name}[/b][/size]'
                '\n\n'
                '[mediainfo]<bdinfo 1 for path/to/video1.mkv>[/mediainfo]\n'
                '\n[hr]\n'
                '[mediainfo]<bdinfo 1 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 2 for path/to/video2.mkv>[/mediainfo]\n'
                '\n[hr]\n'
                '[mediainfo]<bdinfo 1 for path/to/video3.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 2 for path/to/video3.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 3 for path/to/video3.mkv>[/mediainfo]'
            ),
            id='no mediainfos, bdinfos, no screenshots',
        ),
        pytest.param(
            {
                **mock_video_info('path/to/video1.mkv', mediainfos=0, bdinfos=1, screenshots=3),
                **mock_video_info('path/to/video2.mkv', mediainfos=0, bdinfos=2, screenshots=2),
                **mock_video_info('path/to/video3.mkv', mediainfos=0, bdinfos=3, screenshots=1),
            },
            (
                '[size=4][b]{exp_original_release_name}[/b][/size]'
                '\n\n'
                '[mediainfo]<bdinfo 1 for path/to/video1.mkv>[/mediainfo]'
                '[img=https://path.to.video1.mkv.1.png]\n'
                '[img=https://path.to.video1.mkv.2.png]\n'
                '[img=https://path.to.video1.mkv.3.png]\n'
                '\n[hr]\n'
                '[mediainfo]<bdinfo 1 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 2 for path/to/video2.mkv>[/mediainfo]'
                '[img=https://path.to.video2.mkv.1.png]\n'
                '[img=https://path.to.video2.mkv.2.png]\n'
                '\n[hr]\n'
                '[mediainfo]<bdinfo 1 for path/to/video3.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 2 for path/to/video3.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 3 for path/to/video3.mkv>[/mediainfo]'
                '[img=https://path.to.video3.mkv.1.png]'
            ),
            id='no mediainfos, bdinfos, screenshots',
        ),
        pytest.param(
            {
                **mock_video_info('path/to/video1.mkv', mediainfos=1, bdinfos=0, screenshots=0),
                **mock_video_info('path/to/video2.mkv', mediainfos=2, bdinfos=0, screenshots=0),
                **mock_video_info('path/to/video3.mkv', mediainfos=3, bdinfos=0, screenshots=0),
            },
            (
                '[size=4][b]{exp_original_release_name}[/b][/size]'
                '\n\n'
                '[mediainfo]<mediainfo 1 for path/to/video1.mkv>[/mediainfo]\n'
                '\n[hr]\n'
                '[mediainfo]<mediainfo 1 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 2 for path/to/video2.mkv>[/mediainfo]\n'
                '\n[hr]\n'
                '[mediainfo]<mediainfo 1 for path/to/video3.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 2 for path/to/video3.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 3 for path/to/video3.mkv>[/mediainfo]'
            ),
            id='mediainfos, no bdinfos, no screenshots',
        ),
        pytest.param(
            {
                **mock_video_info('path/to/video1.mkv', mediainfos=3, bdinfos=0, screenshots=1),
                **mock_video_info('path/to/video2.mkv', mediainfos=2, bdinfos=0, screenshots=2),
                **mock_video_info('path/to/video3.mkv', mediainfos=1, bdinfos=0, screenshots=3),
            },
            (
                '[size=4][b]{exp_original_release_name}[/b][/size]'
                '\n\n'
                '[mediainfo]<mediainfo 1 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 2 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 3 for path/to/video1.mkv>[/mediainfo]'
                '[img=https://path.to.video1.mkv.1.png]\n'
                '\n[hr]\n'
                '[mediainfo]<mediainfo 1 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 2 for path/to/video2.mkv>[/mediainfo]'
                '[img=https://path.to.video2.mkv.1.png]\n'
                '[img=https://path.to.video2.mkv.2.png]\n'
                '\n[hr]\n'
                '[mediainfo]<mediainfo 1 for path/to/video3.mkv>[/mediainfo]'
                '[img=https://path.to.video3.mkv.1.png]\n'
                '[img=https://path.to.video3.mkv.2.png]\n'
                '[img=https://path.to.video3.mkv.3.png]'
            ),
            id='mediainfos, no bdinfos, screenshots',
        ),
        pytest.param(
            {
                **mock_video_info('path/to/video1.mkv', mediainfos=2, bdinfos=3, screenshots=0),
                **mock_video_info('path/to/video2.mkv', mediainfos=3, bdinfos=2, screenshots=0),
                **mock_video_info('path/to/video3.mkv', mediainfos=1, bdinfos=1, screenshots=0),
            },
            (
                '[size=4][b]{exp_original_release_name}[/b][/size]'
                '\n\n'
                '[mediainfo]<mediainfo 1 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 1 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 2 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 2 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 3 for path/to/video1.mkv>[/mediainfo]\n'
                '\n[hr]\n'
                '[mediainfo]<mediainfo 1 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 1 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 2 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 2 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 3 for path/to/video2.mkv>[/mediainfo]\n'
                '\n[hr]\n'
                '[mediainfo]<mediainfo 1 for path/to/video3.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 1 for path/to/video3.mkv>[/mediainfo]'
            ),
            id='mediainfos, bdinfos, no screenshots',
        ),
        pytest.param(
            {
                **mock_video_info('path/to/video1.mkv', mediainfos=2, bdinfos=3, screenshots=1),
                **mock_video_info('path/to/video2.mkv', mediainfos=3, bdinfos=2, screenshots=2),
                **mock_video_info('path/to/video3.mkv', mediainfos=1, bdinfos=1, screenshots=3),
            },
            (
                '[size=4][b]{exp_original_release_name}[/b][/size]'
                '\n\n'
                '[mediainfo]<mediainfo 1 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 1 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 2 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 2 for path/to/video1.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 3 for path/to/video1.mkv>[/mediainfo]'
                '[img=https://path.to.video1.mkv.1.png]\n'
                '\n[hr]\n'
                '[mediainfo]<mediainfo 1 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 1 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 2 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 2 for path/to/video2.mkv>[/mediainfo]\n'
                '[mediainfo]<mediainfo 3 for path/to/video2.mkv>[/mediainfo]'
                '[img=https://path.to.video2.mkv.1.png]\n'
                '[img=https://path.to.video2.mkv.2.png]\n'
                '\n[hr]\n'
                '[mediainfo]<mediainfo 1 for path/to/video3.mkv>[/mediainfo]\n'
                '[mediainfo]<bdinfo 1 for path/to/video3.mkv>[/mediainfo]'
                '[img=https://path.to.video3.mkv.1.png]\n'
                '[img=https://path.to.video3.mkv.2.png]\n'
                '[img=https://path.to.video3.mkv.3.png]'
            ),
            id='mediainfos, bdinfos, screenshots',
        ),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_generate_description(
        content_path, exp_original_release_name,
        video_info, exp_description,
        ptp_tracker_jobs, mocker,
):
    mocker.patch.object(type(ptp_tracker_jobs), 'content_path', PropertyMock(return_value=content_path))
    mocker.patch.object(type(ptp_tracker_jobs), 'video_info', PropertyMock(return_value=video_info))

    description = await ptp_tracker_jobs.generate_description()
    exp_description = exp_description.format(
        exp_original_release_name=exp_original_release_name,
    )
    assert description == exp_description


def test_document_all_videos(ptp_tracker_jobs):
    assert ptp_tracker_jobs.document_all_videos is True


def test_screenshots_job(ptp_tracker_jobs, mocker):
    assert ptp_tracker_jobs.type_job in ptp_tracker_jobs.screenshots_job.prejobs


@pytest.mark.parametrize(
    argnames='options, type_job, exp_screenshots_count',
    argvalues=(
        pytest.param(
            {'screenshots_count': 123, 'screenshots_from_movie': 456, 'screenshots_from_episode': 789},
            Mock(wait_finished=AsyncMock(), output=('Whatever',)),
            123,
            id='Custom screenshots count',
        ),
        pytest.param(
            {'screenshots_count': None, 'screenshots_from_movie': 456, 'screenshots_from_episode': 789},
            Mock(wait_finished=AsyncMock(), output=('Not Miniseries',)),
            456,
            id='Screenshots for movie',
        ),
        pytest.param(
            {'screenshots_from_movie': 456, 'screenshots_from_episode': 789},
            Mock(wait_finished=AsyncMock(), output=('Miniseries',)),
            789,
            id='Screenshots for episode',
        ),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_screenshots_count(options, type_job, exp_screenshots_count, ptp_tracker_jobs, mocker):
    mocks = Mock()
    mocks.attach_mock(type_job.wait_finished, 'wait_finished')

    mocker.patch.object(type(ptp_tracker_jobs), 'type_job', PropertyMock(return_value=type_job))
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value=options))

    screenshots_count = ptp_tracker_jobs.screenshots_count
    if callable(screenshots_count):
        return_value = await screenshots_count()
        assert return_value == exp_screenshots_count
        assert mocks.mock_calls == [call.wait_finished()]
    else:
        assert screenshots_count == exp_screenshots_count
        assert mocks.mock_calls == []


def test_ptp_group_id_job(ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock())
    mocker.patch.object(ptp_tracker_jobs, 'get_ptp_group_id')
    CustomJob_mock = mocker.patch('upsies.jobs.custom.CustomJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.ptp_group_id_job is CustomJob_mock.return_value
    assert CustomJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='PTP Group ID',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        prejobs=ptp_tracker_jobs.get_job_and_dependencies.return_value,
        worker=ptp_tracker_jobs.get_ptp_group_id,
        catch=(
            errors.RequestError,
        ),
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('ptp-group-id')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call('ptp_group_id_job')]
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [call(
        ptp_tracker_jobs.imdb_job,
    )]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='group_id_from_ptp, group_id_from_user, exp_prompt, exp_info, exp_return_value',
    argvalues=(
        ('123', '456', None, '', '123'),
        (None, ' 456 ', True, 'Enter PTP group ID or nothing if this movie does not exist on PTP.', '456'),
        (None, '', True, 'Enter PTP group ID or nothing if this movie does not exist on PTP.', ''),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_get_ptp_group_id(
        group_id_from_ptp,
        group_id_from_user, exp_prompt, exp_info,
        exp_return_value,
        ptp_tracker_jobs, mocker,
):
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock(return_value=Mock(is_finished=True)))
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_id', PropertyMock(return_value='tt123456'))
    mocker.patch.object(ptp_tracker_jobs.tracker, 'get_ptp_group_id_by_imdb_id', AsyncMock(return_value=group_id_from_ptp))
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id_job', PropertyMock(return_value=Mock(
        info='',
        add_prompt=AsyncMock(return_value=group_id_from_user),
    )))
    TextPrompt_mock = mocker.patch('upsies.uis.prompts.TextPrompt')

    return_value = await ptp_tracker_jobs.get_ptp_group_id('ignored job')
    assert return_value == exp_return_value
    assert ptp_tracker_jobs.tracker.get_ptp_group_id_by_imdb_id.call_args_list == [call(ptp_tracker_jobs.imdb_id)]
    if exp_prompt:
        assert ptp_tracker_jobs.ptp_group_id_job.add_prompt.call_args_list == [call(TextPrompt_mock.return_value)]
        assert TextPrompt_mock.call_args_list == [call()]
    else:
        assert ptp_tracker_jobs.ptp_group_id_job.add_prompt.call_args_list == []
        assert TextPrompt_mock.call_args_list == []
    assert ptp_tracker_jobs.ptp_group_id_job.info == exp_info


@pytest.mark.parametrize(
    argnames='ptp_group_id_job, exp_ptp_group_id',
    argvalues=(
        (Mock(is_finished=False), None),
        (Mock(is_finished=True, output=[]), None),
        (Mock(is_finished=True, output=['123']), '123'),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_ptp_group_id(ptp_group_id_job, exp_ptp_group_id, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id_job', PropertyMock(return_value=ptp_group_id_job))
    ptp_group_id = ptp_tracker_jobs.ptp_group_id
    assert ptp_group_id == exp_ptp_group_id


@pytest.mark.parametrize(
    argnames='ptp_group_id_job, exp_ptp_group_does_not_exist',
    argvalues=(
        (Mock(is_finished=True, output=[]), True),
        (Mock(is_finished=True, output=['123']), False),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_ptp_group_does_not_exist(ptp_group_id_job, exp_ptp_group_does_not_exist, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id_job', PropertyMock(return_value=ptp_group_id_job))
    ptp_group_does_not_exist = ptp_tracker_jobs.ptp_group_does_not_exist()
    assert ptp_group_does_not_exist == exp_ptp_group_does_not_exist


@pytest.mark.parametrize(
    argnames='imdb_id, imdb_metadata, tmdb_id, tmdb_methods, exp_metadata, exp_calls',
    argvalues=(
        pytest.param(
            'tt123456',
            {
                'title': 'Foo',
                'plot': 'Something happens.',
                'year': '2012',
                'poster': 'http://poster.com/jpg',
                'countries': ('ug', 'co'),
            },
            None,
            {
                'title_original': Mock(return_value='Foo (tmdb)'),
                'summary': Mock(return_value='Something happens. (tmdb)'),
                'year': Mock(return_value='2013'),
                'poster_url': Mock(return_value='http://poster.tmdb/jpg'),
                'countries': Mock(return_value=('tm', 'db')),
            },
            {
                'title': 'Foo',
                'plot': 'Something happens.',
                'year': '2012',
                'poster': 'http://poster.com/jpg',
                'countries': ('ug', 'co'),
            },
            [
                call.get_movie_metadata('tt123456'),
            ],
            id='All IMDb values',
        ),
        pytest.param(
            None,
            {
                'title': '',
                'plot': '',
                'poster': '',
                'year': '',
                'countries': (),
            },
            'movie/123456',
            {
                'title_original': AsyncMock(return_value='Foo (tmdb)'),
                'summary': AsyncMock(return_value='Something happens. (tmdb)'),
                'year': AsyncMock(return_value='2013'),
                'poster_url': AsyncMock(return_value='http://poster.tmdb/jpg'),
                'countries': AsyncMock(return_value=('tm', 'db')),
            },
            {
                'title': 'Foo (tmdb)',
                'plot': 'Something happens. (tmdb)',
                'year': '2013',
                'poster': 'http://poster.tmdb/jpg',
                'countries': ('tm', 'db'),
            },
            [
                call.get_movie_metadata(None),
                call.title_original('movie/123456'),
                call.summary('movie/123456'),
                call.year('movie/123456'),
                call.poster_url('movie/123456'),
                call.countries('movie/123456'),
            ],
            id='All TMDb values',
        ),
        pytest.param(
            'tt123456',
            {
                'title': 'Foo',
                'plot': '',
                'year': '2012',
                'poster': '',
                'countries': ('ug', 'co'),
            },
            'movie/123456',
            {
                'title_original': AsyncMock(return_value='Foo (tmdb)'),
                'summary': AsyncMock(return_value='Something happens. (tmdb)'),
                'year': AsyncMock(return_value='2013'),
                'poster_url': AsyncMock(return_value='http://poster.tmdb/jpg'),
                'countries': AsyncMock(return_value=('tm', 'db')),
            },
            {
                'title': 'Foo',
                'plot': 'Something happens. (tmdb)',
                'year': '2012',
                'poster': 'http://poster.tmdb/jpg',
                'countries': ('ug', 'co'),
            },
            [
                call.get_movie_metadata('tt123456'),
                call.summary('movie/123456'),
                call.poster_url('movie/123456'),
            ],
            id='Mixed IMDb/TMDb values',
        ),
        pytest.param(
            '',
            {
                'title': '',
                'plot': '',
                'year': '',
                'poster': '',
                'countries': (),
            },
            '',
            {
                'title_original': AsyncMock(return_value='Foo (tmdb)'),
                'summary': AsyncMock(return_value='Something happens. (tmdb)'),
                'year': AsyncMock(return_value='2013'),
                'poster_url': AsyncMock(return_value='http://poster.tmdb/jpg'),
                'countries': AsyncMock(return_value=('tm', 'db')),
            },
            {
                'title': '',
                'plot': '',
                'year': '',
                'poster': '',
                'countries': (),
            },
            [
                call.get_movie_metadata(''),
            ],
            id='No IMDb values and no TMDb ID',
        ),
        pytest.param(
            'tt123456',
            {
                'title': 'Foo',
                'plot': 'Something happens.',
                'year': '',
                'poster': '',
                'countries': (),
            },
            'movie/123456',
            {
                'title_original': AsyncMock(side_effect=errors.RequestError('No response')),
                'summary': AsyncMock(return_value='Something happens. (tmdb)'),
                'year': AsyncMock(side_effect=errors.RequestError('No response')),
                'poster_url': AsyncMock(return_value='http://poster.tmdb/jpg'),
                'countries': AsyncMock(side_effect=errors.RequestError('No response')),
            },
            {
                'title': 'Foo',
                'plot': 'Something happens.',
                'year': '',
                'poster': 'http://poster.tmdb/jpg',
                'countries': (),
            },
            [
                call.get_movie_metadata('tt123456'),
                call.year('movie/123456'),
                call.poster_url('movie/123456'),
                call.countries('movie/123456'),
            ],
            id='RequestErrors are ignored',
        ),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_get_movie_metadata(imdb_id, imdb_metadata, tmdb_id, tmdb_methods, exp_metadata, exp_calls, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_id', PropertyMock(return_value=imdb_id))
    mocker.patch.object(type(ptp_tracker_jobs), 'tmdb_id', PropertyMock(return_value=tmdb_id))

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs.tracker, 'get_movie_metadata', AsyncMock(return_value=imdb_metadata)),
        'get_movie_metadata',
    )
    for method_name, mock_method in tmdb_methods.items():
        mocks.attach_mock(
            mocker.patch.object(ptp_tracker_jobs.tmdb, method_name, mock_method),
            method_name,
        )

    # Return value should be cached.
    for _ in range(3):
        metadata = await ptp_tracker_jobs.get_movie_metadata()
    assert metadata == exp_metadata
    assert mocks.mock_calls == exp_calls


def test_title_job(ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_does_not_exist')
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.title_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Title',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        prejobs=ptp_tracker_jobs.get_job_and_dependencies.return_value,
        text=ptp_tracker_jobs.fetch_title,
        warn_exceptions=(
            errors.RequestError,
        ),
        normalizer=ptp_tracker_jobs.normalize_title,
        validator=ptp_tracker_jobs.validate_title,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('title')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call(
        'title_job',
        precondition=ptp_tracker_jobs.ptp_group_does_not_exist,
    )]
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [call(
        ptp_tracker_jobs.ptp_group_id_job,
        ptp_tracker_jobs.imdb_job,
    )]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='imdb_job, metadata, exp_result',
    argvalues=(
        (Mock(is_finished=False), {}, AssertionError),
        (Mock(is_finished=True), {'title': 'The PTP Title'}, 'The PTP Title'),
        (Mock(is_finished=True), {'title': ''}, None),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_fetch_title(imdb_job, metadata, exp_result, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock(return_value=imdb_job))
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_id', PropertyMock(return_value='tt123456'))
    mocker.patch.object(type(ptp_tracker_jobs), 'title_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=Mock(
        title='The Release Name Title',
    )))

    async def get_movie_metadata(*args, **kwargs):
        assert ptp_tracker_jobs.title_job.text == 'The Release Name Title'
        return metadata

    mocker.patch.object(ptp_tracker_jobs, 'get_movie_metadata', AsyncMock(
        side_effect=get_movie_metadata,
    ))

    if isinstance(exp_result, type) and issubclass(exp_result, Exception):
        with pytest.raises(exp_result):
            await ptp_tracker_jobs.fetch_title()
    elif isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            await ptp_tracker_jobs.fetch_title()
    else:
        return_value = await ptp_tracker_jobs.fetch_title()
        assert return_value == exp_result
        assert ptp_tracker_jobs.get_movie_metadata.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='text, exp_title',
    argvalues=(
        ('The Title', 'The Title'),
        (' The Title', 'The Title'),
        (' The Title  ', 'The Title'),
        ('\nThe Title\t', 'The Title'),
    ),
    ids=lambda v: str(v),
)
def test_normalize_title(text, exp_title, ptp_tracker_jobs):
    return_value = ptp_tracker_jobs.normalize_title(text)
    assert return_value == exp_title


@pytest.mark.parametrize(
    argnames='text, exp_exception',
    argvalues=(
        ('', ValueError('Title must not be empty.')),
        ('not empty', None),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_validate_title(text, exp_exception, ptp_tracker_jobs):
    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            ptp_tracker_jobs.validate_title(text)
    else:
        return_value = ptp_tracker_jobs.validate_title(text)
        assert return_value is None


def test_year_job(ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_does_not_exist')
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock())
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.year_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Year',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        prejobs=ptp_tracker_jobs.get_job_and_dependencies.return_value,
        text=ptp_tracker_jobs.fetch_year,
        warn_exceptions=(
            errors.RequestError,
        ),
        normalizer=ptp_tracker_jobs.normalize_year,
        validator=ptp_tracker_jobs.validate_year,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('year')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call(
        'year_job',
        precondition=ptp_tracker_jobs.ptp_group_does_not_exist,
    )]
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [call(
        ptp_tracker_jobs.ptp_group_id_job,
        ptp_tracker_jobs.imdb_job,
    )]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='imdb_job, metadata, exp_result',
    argvalues=(
        (Mock(is_finished=False), {}, AssertionError),
        (Mock(is_finished=True), {'year': '2013'}, '2013'),
        (Mock(is_finished=True), {'year': ''}, None),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_fetch_year(imdb_job, metadata, exp_result, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock(return_value=imdb_job))
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_id', PropertyMock(return_value='tt123456'))
    mocker.patch.object(type(ptp_tracker_jobs), 'year_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=Mock(
        year='2012',
    )))

    async def get_movie_metadata(*args, **kwargs):
        assert ptp_tracker_jobs.year_job.text == '2012'
        return metadata

    mocker.patch.object(ptp_tracker_jobs, 'get_movie_metadata', AsyncMock(
        side_effect=get_movie_metadata,
    ))

    if isinstance(exp_result, type) and issubclass(exp_result, Exception):
        with pytest.raises(exp_result):
            await ptp_tracker_jobs.fetch_year()

    elif isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            await ptp_tracker_jobs.fetch_year()

    else:
        return_value = await ptp_tracker_jobs.fetch_year()
        assert return_value == exp_result
        assert ptp_tracker_jobs.get_movie_metadata.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='text, exp_year',
    argvalues=(
        ('2012', '2012'),
        (' 2012', '2012'),
        (' 2012  ', '2012'),
        ('\n2012\t', '2012'),
    ),
    ids=lambda v: str(v),
)
def test_normalize_year(text, exp_year, ptp_tracker_jobs):
    return_value = ptp_tracker_jobs.normalize_year(text)
    assert return_value == exp_year


@pytest.mark.parametrize(
    argnames='text, exp_exception',
    argvalues=(
        ('', ValueError('Year must not be empty.')),
        ('not empty', ValueError('Year is not a number.')),
        ('2012.3', ValueError('Year is not a number.')),
        ('1200', ValueError('Year is not reasonable.')),
        ('2012', None),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_validate_year(text, exp_exception, ptp_tracker_jobs):
    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            ptp_tracker_jobs.validate_year(text)
    else:
        return_value = ptp_tracker_jobs.validate_year(text)
        assert return_value is None


def test_edition_job(ptp_tracker_jobs, mocker):
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.edition_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Edition',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        text=ptp_tracker_jobs.autodetect_edition,
        finish_on_success=True,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('edition')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call('edition_job')]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.asyncio
async def test_autodetect_edition(ptp_tracker_jobs, mocker):
    mocker.patch('upsies.trackers.ptp.metadata.editions', {
        'theatrical': 'Theatrical Cut',
        'unrated': 'Unrated',
        '10b': '10-bit',
        'remux': 'Remux',
    })
    mocker.patch.object(ptp_tracker_jobs, 'autodetect_edition_map', {
        'theatrical': lambda self: False,
        'unrated': lambda self: True,
        '10b': lambda self: True,
        'remux': lambda self: True,
    })
    return_value = await ptp_tracker_jobs.autodetect_edition()
    assert return_value == 'Unrated / 10-bit / Remux'


@dataclasses.dataclass
class MockReleaseName:
    edition: str = ''
    source: str = ''
    hdr_format: str = ''
    audio_format: str = ''
    has_dual_audio: bool = False
    has_commentary: bool = False

def assert_edition_keys_for(ptp_tracker_jobs, exp_keys):
    keys = [
        key
        for key, is_edition in ptp_tracker_jobs.autodetect_edition_map.items()
        if is_edition(ptp_tracker_jobs)
    ]
    assert keys == exp_keys

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(edition=''), []),
        (MockReleaseName(edition='Criterion'), ['collection.criterion']),
        (MockReleaseName(edition='Criterion Collection'), ['collection.criterion']),
        (MockReleaseName(edition="foo Criterion bar"), ['collection.criterion']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__collection_criterion(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(edition=''), []),
        (MockReleaseName(edition="Director's Cut"), ['edition.dc']),
        (MockReleaseName(edition="foo Director's Cut bar"), ['edition.dc']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__edition_dc(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(edition=''), []),
        (MockReleaseName(edition="Extended Cut"), ['edition.extended']),
        (MockReleaseName(edition="foo Extended Cut bar"), ['edition.extended']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__edition_extended(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(edition=''), []),
        (MockReleaseName(edition="Theatrical Cut"), ['edition.theatrical']),
        (MockReleaseName(edition="foo Theatrical Cut bar"), ['edition.theatrical']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__edition_theatrical(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(edition=''), []),
        (MockReleaseName(edition="Uncut"), ['edition.uncut']),
        (MockReleaseName(edition="foo Uncut bar"), ['edition.uncut']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__edition_uncut(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(edition=''), []),
        (MockReleaseName(edition="Unrated"), ['edition.unrated']),
        (MockReleaseName(edition="foo Unrated bar"), ['edition.unrated']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__edition_unrated(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(source=''), []),
        (MockReleaseName(source='Remux'), ['feature.remux']),
        (MockReleaseName(source='foo Remux bar'), ['feature.remux']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_remux(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(edition=''), []),
        (MockReleaseName(edition='2in1'), ['feature.2in1']),
        (MockReleaseName(edition='foo 2in1 bar'), ['feature.2in1']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_2in1(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(edition=''), []),
        (MockReleaseName(edition='4k Restored'), ['feature.4krestoration']),
        (MockReleaseName(edition='foo 4k Restored bar'), ['feature.4krestoration']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_4krestoration(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(edition=''), []),
        (MockReleaseName(edition='4k Remastered'), ['feature.4kremaster']),
        (MockReleaseName(edition='foo 4k Remastered bar'), ['feature.4kremaster']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_4kremaster(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, bit_depth, exp_keys',
    argvalues=(
        (MockReleaseName(hdr_format=''), 8, []),
        (MockReleaseName(hdr_format=''), 10, ['feature.10bit']),
        (MockReleaseName(hdr_format='HDR10'), 10, ['feature.hdr10']),
        (MockReleaseName(hdr_format='HDR10+'), 10, ['feature.hdr10+']),
        (MockReleaseName(hdr_format='DV'), 10, ['feature.dolby_vision']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_10bit(release_name, bit_depth, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    bit_depth_mock = mocker.patch('upsies.utils.mediainfo.video.get_bit_depth', return_value=bit_depth)
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)
    assert bit_depth_mock.call_args_list == [call(ptp_tracker_jobs.content_path, default=None)]

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(audio_format=''), []),
        (MockReleaseName(audio_format='DTS:X'), ['feature.dtsx']),
        (MockReleaseName(audio_format='foo DTS:X bar'), ['feature.dtsx']),

    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_dtsx(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(audio_format=''), []),
        (MockReleaseName(audio_format='Atmos'), ['feature.dolby_atmos']),
        (MockReleaseName(audio_format='foo Atmos bar'), ['feature.dolby_atmos']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_dolby_atmos(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(hdr_format=''), []),
        (MockReleaseName(hdr_format='DV'), ['feature.dolby_vision']),
        (MockReleaseName(hdr_format='foo DV bar'), ['feature.dolby_vision']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_dolby_vision(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(hdr_format=''), []),
        (MockReleaseName(hdr_format='HDR10'), ['feature.hdr10']),
        (MockReleaseName(hdr_format='foo HDR10 bar'), ['feature.hdr10']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_hdr10(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(hdr_format=''), []),
        (MockReleaseName(hdr_format='HDR10+'), ['feature.hdr10+']),
        (MockReleaseName(hdr_format='foo HDR10+ bar'), ['feature.hdr10+']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_hdr10plus(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(has_dual_audio=False), []),
        (MockReleaseName(has_dual_audio=True), ['feature.dual_audio']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_dual_audio(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)

@pytest.mark.parametrize(
    argnames='release_name, exp_keys',
    argvalues=(
        (MockReleaseName(has_commentary=False), []),
        (MockReleaseName(has_commentary=True), ['feature.commentary']),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_edition_map__feature_commentary(release_name, exp_keys, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=release_name))
    assert_edition_keys_for(ptp_tracker_jobs, exp_keys)


def test_tags_job(ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_does_not_exist')
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock())
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.tags_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Tags',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        prejobs=ptp_tracker_jobs.get_job_and_dependencies.return_value,
        text=ptp_tracker_jobs.fetch_tags,
        warn_exceptions=(
            errors.RequestError,
        ),
        normalizer=ptp_tracker_jobs.normalize_tags,
        validator=ptp_tracker_jobs.validate_tags,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('tags')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call(
        'tags_job',
        precondition=ptp_tracker_jobs.ptp_group_does_not_exist,
    )]
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [call(
        ptp_tracker_jobs.ptp_group_id_job,
        ptp_tracker_jobs.imdb_job,
    )]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='imdb_job, exp_exception',
    argvalues=(
        (Mock(is_finished=True, output=['foo,bar,baz']), None),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_fetch_tags(imdb_job, exp_exception, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock(return_value=imdb_job))
    mocker.patch.object(ptp_tracker_jobs, 'get_movie_metadata', AsyncMock(
        return_value={'tags': ['foo', 'bar', 'baz']},
    ))
    return_value = await ptp_tracker_jobs.fetch_tags()
    assert return_value == 'foo, bar, baz'
    assert ptp_tracker_jobs.get_movie_metadata.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='text, exp_tags',
    argvalues=(
        pytest.param('foo,bar,baz', 'bar, baz, foo', id='Tags are sorted'),
        pytest.param('Foo,bAr,BAZ', 'bar, baz, foo', id='Tags are lowered'),
        pytest.param('foo,bar,baz,foo', 'bar, baz, foo', id='Tags are deduplicated'),
        pytest.param(',,,foo,,bar,baz,', 'bar, baz, foo', id='Empty tags are ignored'),
    ),
    ids=lambda v: str(v),
)
def test_normalize_tags(text, exp_tags, ptp_tracker_jobs):
    return_value = ptp_tracker_jobs.normalize_tags(text)
    assert return_value == exp_tags


@pytest.mark.parametrize(
    argnames='text, exp_exception',
    argvalues=(
        ('horror,awesome,drama', ValueError('Tag is not valid: awesome')),
        ('horror,drama', None),
        ('', ValueError('You must provide at least one tag.')),
        (', '.join(ptp.metadata.tags), ValueError('You provided too many tags.')),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_validate_tags(text, exp_exception, ptp_tracker_jobs):
    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            ptp_tracker_jobs.validate_tags(text)
    else:
        return_value = ptp_tracker_jobs.validate_tags(text)
        assert return_value is None


@pytest.mark.asyncio
async def test_poster_job(ptp_tracker_jobs, mocker):
    mocker.patch('upsies.trackers.base.jobs.TrackerJobsBase.poster_job', Mock(prejobs=('foo', 'bar')))
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock(return_value='<imdb job>'))
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id_job', PropertyMock(return_value='<ptp_group_id job>'))
    poster_job = ptp_tracker_jobs.poster_job
    assert poster_job.prejobs == (
        'foo',
        'bar',
        ptp_tracker_jobs.imdb_job,
        ptp_tracker_jobs.ptp_group_id_job,
    )


def test_make_poster_job_precondition(ptp_tracker_jobs, mocker):
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    precondition = ptp_tracker_jobs.make_poster_job_precondition()
    assert precondition is ptp_tracker_jobs.make_precondition.return_value
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call(
        'poster_job',
        precondition=ptp_tracker_jobs.ptp_group_does_not_exist,
    )]


@pytest.mark.parametrize(
    argnames='metadata, exp_return_value',
    argvalues=(
        ({'poster': ''}, None),
        (
            {'poster': 'http://myposter.jpg'},
            {
                'poster': 'http://myposter.jpg',
                'width': None,
                'height': None,
                'imghosts': (),
                'write_to': None,
            },
        ),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_get_poster_from_tracker(metadata, exp_return_value, ptp_tracker_jobs, mocker):
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock(return_value=Mock(wait_finished=AsyncMock()))),
        'wait_finished',
    )
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, 'get_movie_metadata', AsyncMock(return_value=metadata)),
        'get_movie_metadata',
    )
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_id', PropertyMock(return_value='tt0123'))

    return_value = await ptp_tracker_jobs.get_poster_from_tracker()
    assert return_value == exp_return_value
    assert mocks.mock_calls == [
        call.wait_finished(),
        call.get_movie_metadata(),
    ]


def test_plot_job(ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_does_not_exist')
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock())
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.plot_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Plot',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        prejobs=ptp_tracker_jobs.get_job_and_dependencies.return_value,
        text=ptp_tracker_jobs.fetch_plot,
        warn_exceptions=(
            errors.RequestError,
        ),
        normalizer=ptp_tracker_jobs.normalize_plot,
        validator=ptp_tracker_jobs.validate_plot,
        finish_on_success=True,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('plot')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call(
        'plot_job',
        precondition=ptp_tracker_jobs.ptp_group_does_not_exist,
    )]
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [call(
        ptp_tracker_jobs.ptp_group_id_job,
        ptp_tracker_jobs.imdb_job,
    )]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='imdb_job, exp_exception',
    argvalues=(
        (Mock(is_finished=True, output=['123']), None),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_fetch_plot(imdb_job, exp_exception, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock(return_value=imdb_job))
    mocker.patch.object(ptp_tracker_jobs, 'get_movie_metadata', AsyncMock(
        return_value={'plot': 'The plot.'},
    ))
    return_value = await ptp_tracker_jobs.fetch_plot()
    assert return_value == 'The plot.'
    assert ptp_tracker_jobs.get_movie_metadata.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='text, exp_plot',
    argvalues=(
        ('The plot.', 'The plot.'),
        (' The plot.', 'The plot.'),
        (' The plot.  ', 'The plot.'),
        ('\nThe plot.\t', 'The plot.'),
    ),
    ids=lambda v: str(v),
)
def test_normalize_plot(text, exp_plot, ptp_tracker_jobs):
    return_value = ptp_tracker_jobs.normalize_plot(text)
    assert return_value == exp_plot


@pytest.mark.parametrize(
    argnames='text, exp_exception',
    argvalues=(
        ('', ValueError('Plot must not be empty.')),
        ('not empty', None),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_validate_plot(text, exp_exception, ptp_tracker_jobs):
    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            ptp_tracker_jobs.validate_plot(text)
    else:
        return_value = ptp_tracker_jobs.validate_plot(text)
        assert return_value is None


def test_artists_job(ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id_job', PropertyMock())
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock())
    CustomJob_mock = mocker.patch('upsies.jobs.custom.CustomJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_and_dependencies')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ptp_tracker_jobs.artists_job is CustomJob_mock.return_value
    assert CustomJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Artists',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        prejobs=ptp_tracker_jobs.get_job_and_dependencies.return_value,
        worker=ptp_tracker_jobs._get_artists,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('artists')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call(
        'artists_job',
        precondition=ptp_tracker_jobs.artists_job_precondition,
    )]
    assert ptp_tracker_jobs.get_job_and_dependencies.call_args_list == [call(
        ptp_tracker_jobs.ptp_group_id_job,
        ptp_tracker_jobs.imdb_job,
    )]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='ptp_group_id, imdb_id, exp_return_value',
    argvalues=(
        ('', '', True),
        ('', 'tt456', False),
        ('123', '', False),
        ('123', 'tt456', False),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_artists_job_precondition(ptp_group_id, imdb_id, exp_return_value, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id', PropertyMock(return_value=ptp_group_id))
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_id', PropertyMock(return_value=imdb_id))
    return_value = ptp_tracker_jobs.artists_job_precondition()
    assert return_value is exp_return_value


@pytest.mark.parametrize(
    argnames='imdb_id, artists_found_on_tmdb',
    argvalues=(
        pytest.param(
            'tt123456',
            True,
            id='IMDb ID available, artists found on TMDb',
        ),
        pytest.param(
            'tt123456',
            False,
            id='IMDb ID available, no artists found on TMDb',
        ),
        pytest.param(
            None,
            True,
            id='IMDb ID not available, artists found on TMDb',
        ),
        pytest.param(
            None,
            False,
            id='IMDb ID not available, no artists found on TMDb',
        ),
    ),
)
@pytest.mark.asyncio
async def test__get_artists(imdb_id, artists_found_on_tmdb, ptp_tracker_jobs, mocker):
    mocks = Mock()
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_id', PropertyMock(return_value=imdb_id))
    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_job', PropertyMock(return_value=AsyncMock()))
    mocks.attach_mock(ptp_tracker_jobs.imdb_job, 'imdb_job')
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, '_get_artists_from_tmdb'),
        '_get_artists_from_tmdb',
    )
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, '_get_artists_from_user'),
        '_get_artists_from_user',
    )
    mocks.artists_job.output = ()

    async def _get_artists_from_tmdb(job):
        if artists_found_on_tmdb:
            job.output = ('artist1', 'artist2', 'artist3')

    ptp_tracker_jobs._get_artists_from_tmdb.side_effect = _get_artists_from_tmdb

    return_value = await ptp_tracker_jobs._get_artists(mocks.artists_job)
    assert return_value is None

    exp_mock_calls = [call.imdb_job.wait_finished()]
    if not imdb_id:
        exp_mock_calls.append(call._get_artists_from_tmdb(mocks.artists_job))
    if not imdb_id and artists_found_on_tmdb:
        exp_mock_calls.append(call._get_artists_from_user(mocks.artists_job, focused=-1))
    else:
        exp_mock_calls.append(call._get_artists_from_user(mocks.artists_job, focused=0))
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='artists_job, tmdb_id, directors, actors, exp_mock_calls',
    argvalues=(
        pytest.param(
            '<artists_job>',
            None,
            (),
            (),
            [
                call.tmdb_job.wait_finished(),
            ],
            id='No TMDb ID',
        ),

        pytest.param(
            '<artists_job>',
            'movie/123456',
            (
                utils.webdbs.common.Person('First Director'),
                utils.webdbs.common.Person('Second Director'),
            ),
            (
                utils.webdbs.common.Person('First Actor', role='She'),
                utils.webdbs.common.Person('Second Actor'),
            ),
            [
                call.tmdb_job.wait_finished(),
                call.tmdb.directors('movie/123456'),
                call.tmdb.cast('movie/123456'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.DIRECTOR, name='First Director', role=''),
                call._add_formatted_artist('<artists_job>', '<artist info: First Director: >'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.DIRECTOR, name='Second Director', role=''),
                call._add_formatted_artist('<artists_job>', '<artist info: Second Director: >'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.ACTOR, name='First Actor', role='She'),
                call._add_formatted_artist('<artists_job>', '<artist info: First Actor: She>'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.ACTOR, name='Second Actor', role=''),
                call._add_formatted_artist('<artists_job>', '<artist info: Second Actor: >'),

            ],
            id='User confirms all artists found on TMDb',
        ),

        pytest.param(
            '<artists_job>',
            'movie/123456',
            (
                utils.webdbs.common.Person('First Director'),
                utils.webdbs.common.Person('Second Director'),
            ),
            (
                utils.webdbs.common.Person('First Actor', role='She'),
                utils.webdbs.common.Person('Second Actor'),
                utils.webdbs.common.Person('Stupid Actor'),
                utils.webdbs.common.Person('Third Actor', role='He'),
            ),
            [
                call.tmdb_job.wait_finished(),
                call.tmdb.directors('movie/123456'),
                call.tmdb.cast('movie/123456'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.DIRECTOR, name='First Director', role=''),
                call._add_formatted_artist('<artists_job>', '<artist info: First Director: >'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.DIRECTOR, name='Second Director', role=''),
                call._add_formatted_artist('<artists_job>', '<artist info: Second Director: >'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.ACTOR, name='First Actor', role='She'),
                call._add_formatted_artist('<artists_job>', '<artist info: First Actor: She>'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.ACTOR, name='Second Actor', role=''),
                call._add_formatted_artist('<artists_job>', '<artist info: Second Actor: >'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.ACTOR, name='Stupid Actor', role=''),

            ],
            id='User stops entering artists',
        ),
    ),
)
@pytest.mark.asyncio
async def test__get_artists_from_tmdb(artists_job, tmdb_id, directors, actors, exp_mock_calls, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'tmdb_id', PropertyMock(return_value=tmdb_id))

    mocks = Mock()
    mocks.tmdb_job.wait_finished = AsyncMock()
    mocker.patch.object(type(ptp_tracker_jobs), 'tmdb_job', PropertyMock(return_value=mocks.tmdb_job))

    mocks.tmdb.directors = AsyncMock(return_value=directors)
    mocks.tmdb.cast = AsyncMock(return_value=actors)
    mocker.patch.object(type(ptp_tracker_jobs), 'tmdb', PropertyMock(return_value=mocks.tmdb))

    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, '_get_one_artist_from_user',
                            side_effect=lambda _, __, name, role: '' if 'Stupid' in name else f'<artist info: {name}: {role}>'),
        '_get_one_artist_from_user',
    )

    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, '_add_formatted_artist'),
        '_add_formatted_artist',
    )

    return_value = await ptp_tracker_jobs._get_artists_from_tmdb(artists_job)
    assert return_value is None
    for ec, ac in zip(exp_mock_calls, mocks.mock_calls):
        print('EXPECTED CALL:', repr(ec))
        print('  ACTUAL CALL:', repr(ac))
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='artists_job, focused, importances, artists, exp_mock_calls',
    argvalues=(
        pytest.param(
            '<artists_job>',
            '<focused>',
            (
                ptp.metadata.ArtistImportance.ACTOR,
                ptp.metadata.ArtistImportance.DIRECTOR,
                None,
            ),
            (
                'Mister Actor',
                'Missus Director',
                None,
            ),
            [
                call._get_artist_importance(focused='<focused>'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.ACTOR),
                call._add_formatted_artist('<artists_job>', 'Mister Actor'),
                call._get_artist_importance(focused='<focused>'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.DIRECTOR),
                call._add_formatted_artist('<artists_job>', 'Missus Director'),
                call._get_artist_importance(focused='<focused>'),
            ],
            id='User stops adding artists when prompted for importance',
        ),

        pytest.param(
            '<artists_job>',
            '<focused>',
            (
                ptp.metadata.ArtistImportance.ACTOR,
                ptp.metadata.ArtistImportance.DIRECTOR,
                ptp.metadata.ArtistImportance.WRITER,
                None,
            ),
            (
                'Mister Actor',
                'Missus Director',
                None,
            ),
            [
                call._get_artist_importance(focused='<focused>'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.ACTOR),
                call._add_formatted_artist('<artists_job>', 'Mister Actor'),
                call._get_artist_importance(focused='<focused>'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.DIRECTOR),
                call._add_formatted_artist('<artists_job>', 'Missus Director'),
                call._get_artist_importance(focused='<focused>'),
                call._get_one_artist_from_user('<artists_job>', ptp.metadata.ArtistImportance.WRITER),
            ],
            id='User stops adding artists when prompted for artist',
        ),
    ),
)
@pytest.mark.asyncio
async def test__get_artists_from_user(artists_job, focused, importances, artists, exp_mock_calls, ptp_tracker_jobs, mocker):
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, '_get_artist_importance'),
        '_get_artist_importance',
    )
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, '_get_one_artist_from_user'),
        '_get_one_artist_from_user',
    )
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, '_add_formatted_artist'),
        '_add_formatted_artist',
    )

    mocks._get_artist_importance.side_effect = importances
    mocks._get_one_artist_from_user.side_effect = artists
    return_value = await ptp_tracker_jobs._get_artists_from_user(artists_job, focused=focused)
    assert return_value is None
    for ec, ac in zip(exp_mock_calls, mocks.mock_calls):
        print('EXPECTED CALL:', repr(ec))
        print('  ACTUAL CALL:', repr(ac))
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='kwargs, prompt_responses, exp_mock_calls, exp_return_value',
    argvalues=(
        pytest.param(
            {'importance': ptp.metadata.ArtistImportance.ACTOR, 'name': 'jeffrey falcon', 'role': 'that guy'},
            {
                'name_and_ptpurl': [
                    ('Jeffrey Falcon', 'http://host/artist.php?id=123'),
                ],
                'role': [
                    'That Guy',
                ],
            },
            [
                call._get_artist_name_and_ptpurl(question='Actor name:', name='jeffrey falcon'),
                call.artists_job.clear_warnings(),
                call._get_artist_role(question='Jeffrey Falcon role (optional):', role='that guy')
            ],
            {
                'importance': ptp.metadata.ArtistImportance.ACTOR,
                'name': 'Jeffrey Falcon',
                'ptpurl': 'http://host/artist.php?id=123',
                'role': 'That Guy',
            },
            id='User accepts actor name and role',
        ),

        pytest.param(
            {'importance': ptp.metadata.ArtistImportance.DIRECTOR, 'name': 'lucas arts'},
            {
                'name_and_ptpurl': [
                    ('Lucas Arts', 'http://host/artist.php?id=456'),
                ],
                'role': [],
            },
            [
                call._get_artist_name_and_ptpurl(question='Director name:', name='lucas arts'),
                call.artists_job.clear_warnings(),
            ],
            {
                'importance': ptp.metadata.ArtistImportance.DIRECTOR,
                'name': 'Lucas Arts',
                'ptpurl': 'http://host/artist.php?id=456',
                'role': '',
            },
            id='User accepts non-actor name',
        ),

        pytest.param(
            {'importance': ptp.metadata.ArtistImportance.ACTOR, 'name': 'jeffrey hawk'},
            {
                'name_and_ptpurl': [
                    errors.RequestedNotFoundError('jeffrey hawk'),
                    ('Jeffrey Falcon', 'http://host/artist.php?id=123'),
                ],
                'role': [
                    'That Guy',
                ],
            },
            [
                call._get_artist_name_and_ptpurl(question='Actor name:', name='jeffrey hawk'),
                call.artists_job.warn(errors.RequestedNotFoundError('jeffrey hawk')),
                call._get_artist_name_and_ptpurl(question='Actor name:', name='jeffrey hawk'),
                call.artists_job.clear_warnings(),
                call._get_artist_role(question='Jeffrey Falcon role (optional):', role='')
            ],
            {
                'importance': ptp.metadata.ArtistImportance.ACTOR,
                'name': 'Jeffrey Falcon',
                'ptpurl': 'http://host/artist.php?id=123',
                'role': 'That Guy',
            },
            id='Requested name not found',
        ),

        pytest.param(
            {'importance': ptp.metadata.ArtistImportance.ACTOR},
            {
                'name_and_ptpurl': [
                    errors.RequestError('Service is down'),
                    ('Jeffrey Falcon', 'http://host/artist.php?id=123'),
                ],
                'role': [
                    '',
                ],
            },
            [
                call._get_artist_name_and_ptpurl(question='Actor name:', name=''),
                call.artists_job.warn(errors.RequestError('Service is down')),
                call._get_artist_name_and_ptpurl(question='Actor name:', name=''),
                call.artists_job.clear_warnings(),
                call._get_artist_role(question='Jeffrey Falcon role (optional):', role='')
            ],
            {
                'importance': ptp.metadata.ArtistImportance.ACTOR,
                'name': 'Jeffrey Falcon',
                'ptpurl': 'http://host/artist.php?id=123',
                'role': '',
            },
            id='Request for artist fails',
        ),

        pytest.param(
            {'importance': ptp.metadata.ArtistImportance.ACTOR},
            {
                'name_and_ptpurl': [
                    (None, None),
                ],
                'role': [
                    'That Guy',
                ],
            },
            [
                call._get_artist_name_and_ptpurl(question='Actor name:', name=''),
                call.artists_job.clear_warnings(),
            ],
            None,
            id='User does not enter artist',
        ),
    ),
)
@pytest.mark.asyncio
async def test__get_one_artist_from_user(kwargs, prompt_responses, exp_mock_calls, exp_return_value, ptp_tracker_jobs, mocker):
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, '_get_artist_name_and_ptpurl', side_effect=prompt_responses['name_and_ptpurl']),
        '_get_artist_name_and_ptpurl',
    )
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, '_get_artist_role', side_effect=prompt_responses['role']),
        '_get_artist_role',
    )

    return_value = await ptp_tracker_jobs._get_one_artist_from_user(mocks.artists_job, **kwargs)
    assert return_value == exp_return_value

    for ec, ac in zip(exp_mock_calls, mocks.mock_calls):
        print('EXPECTED CALL:', repr(ec))
        print('  ACTUAL CALL:', repr(ac))
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='prompt_responses, metadata, created_artist, exp_result, exp_mock_calls',
    argvalues=(
        pytest.param(
            [
                ' ',  # TextPrompt response
            ],
            {},  # get_artist_metadata() response
            {},  # create_artist() response
            (None, None),  # Expected return value
            [
                call.TextPrompt(question='Enter artist name:', text='Prefilled Name'),
                call.add_prompt('<mock TextPrompt 1>'),
            ],
            id='Artist name is empty',
        ),

        pytest.param(
            [
                ' jeffrey falcon ',  # "Enter artist name:" response
            ],
            {'name': 'Jeffrey Falcon', 'id': '123', 'url': 'http://host/artist.php?id=123'},  # get_artist_metadata() response
            {},  # create_artist() response
            ('Jeffrey Falcon', 'http://host/artist.php?id=123'),  # Expected return value
            [
                call.TextPrompt(question='Enter artist name:', text='Prefilled Name'),
                call.add_prompt('<mock TextPrompt 1>'),
                call.get_artist_metadata('jeffrey falcon'),
            ],
            id='Known artist',
        ),

        pytest.param(
            [
                ' jeffrey owl ',  # "Enter artist name:" response
                'No',  # "Are you sure?" response
                '_',  # "Are you really sure?" response
            ],
            errors.RequestedNotFoundError('jeffrey owl'),  # get_artist_metadata() response
            {},  # create_artist() response
            errors.RequestedNotFoundError('jeffrey owl'),  # Expected exception
            [
                call.TextPrompt(question='Enter artist name:', text='Prefilled Name'),
                call.add_prompt('<mock TextPrompt 1>'),
                call.get_artist_metadata('jeffrey owl'),
                call.RadioListPrompt(
                    question='Create new artist with the name "jeffrey owl"?',
                    options=('Yes', 'No'),
                    focused='No',
                ),
                call.add_prompt('<mock RadioListPrompt 1>'),
            ],
            id='Unknown artist, user responds "No"',
        ),

        pytest.param(
            [
                ' jeffrey owl ',  # "Enter artist name:" response
                'Yes',  # "Are you sure?" response
                'No',  # "Are you really sure?" response
            ],
            errors.RequestedNotFoundError('jeffrey owl'),  # get_artist_metadata() response
            {},  # create_artist() response
            errors.RequestedNotFoundError('jeffrey owl'),  # Expected exception
            [
                call.TextPrompt(question='Enter artist name:', text='Prefilled Name'),
                call.add_prompt('<mock TextPrompt 1>'),
                call.get_artist_metadata('jeffrey owl'),
                call.RadioListPrompt(
                    question='Create new artist with the name "jeffrey owl"?',
                    options=('Yes', 'No'),
                    focused='No',
                ),
                call.add_prompt('<mock RadioListPrompt 1>'),
                call.RadioListPrompt(
                    question='Are you sure "jeffrey owl" does not exist on PTP or IMDb?',
                    options=('Yes', 'No'),
                    focused='No',
                ),
                call.add_prompt('<mock RadioListPrompt 2>'),
            ],
            id='Unknown artist, user responds "Yes", "No"',
        ),

        pytest.param(
            [
                ' Jeffrey Owl ',  # "Enter artist name:" response
                'Yes',  # "Are you sure?" response
                'Yes',  # "Are you really sure?" response
            ],
            errors.RequestedNotFoundError('jeffrey owl'),  # get_artist_metadata() response
            {'name': 'Jeffrey Owl', 'id': '123', 'url': 'http://host/artist.php?id=123'},  # create_artist() response
            ('Jeffrey Owl', 'http://host/artist.php?id=123'),  # Expected return value
            [
                call.TextPrompt(question='Enter artist name:', text='Prefilled Name'),
                call.add_prompt('<mock TextPrompt 1>'),
                call.get_artist_metadata('Jeffrey Owl'),
                call.RadioListPrompt(
                    question='Create new artist with the name "Jeffrey Owl"?',
                    options=('Yes', 'No'),
                    focused='No',
                ),
                call.add_prompt('<mock RadioListPrompt 1>'),
                call.RadioListPrompt(
                    question='Are you sure "Jeffrey Owl" does not exist on PTP or IMDb?',
                    options=('Yes', 'No'),
                    focused='No',
                ),
                call.add_prompt('<mock RadioListPrompt 2>'),
                call.create_artist('Jeffrey Owl'),
            ],
            id='Unknown artist, user responds "Yes", "Yes"',
        ),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test__get_artist_name_and_ptpurl(
        prompt_responses, metadata, created_artist,
        exp_result, exp_mock_calls,
        ptp_tracker_jobs, mocker,
):
    def add_prompt(prompt):
        if 'TextPrompt' in prompt:
            assert ptp_tracker_jobs.artists_job.info == (
                'Enter artist name, IMDb link/ID, PTP link/ID '
                'or nothing to stop entering artists.'
            )
        else:
            assert ptp_tracker_jobs.artists_job.info == ''
        return prompt_responses.pop(0)

    def TextPrompt(*args, i=[0], **kwargs):
        i[0] += 1
        return f'<mock TextPrompt {i[0]}>'

    def RadioListPrompt(*args, i=[0], **kwargs):
        i[0] += 1
        return f'<mock RadioListPrompt {i[0]}>'

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.uis.prompts.TextPrompt', side_effect=TextPrompt),
        'TextPrompt',
    )
    mocks.attach_mock(
        mocker.patch('upsies.uis.prompts.RadioListPrompt', side_effect=RadioListPrompt),
        'RadioListPrompt',
    )
    mocker.patch.object(type(ptp_tracker_jobs), 'artists_job', PropertyMock(return_value=Mock(
        add_prompt=AsyncMock(side_effect=add_prompt),
        info=''
    )))
    mocks.attach_mock(ptp_tracker_jobs.artists_job.add_prompt, 'add_prompt')
    mocker.patch.object(type(ptp_tracker_jobs), 'tracker', PropertyMock(return_value=Mock(
        _artist_url='http://host/artist.php',
        get_artist_metadata=AsyncMock(**(
            {'side_effect': metadata}
            if isinstance(metadata, Exception) else
            {'return_value': metadata}
        )),
        create_artist=AsyncMock(return_value=created_artist),
    )))
    mocks.attach_mock(ptp_tracker_jobs.tracker.get_artist_metadata, 'get_artist_metadata')
    mocks.attach_mock(ptp_tracker_jobs.tracker.create_artist, 'create_artist')

    question = 'Enter artist name:'
    prefilled_name = 'Prefilled Name'
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            await ptp_tracker_jobs._get_artist_name_and_ptpurl(question=question, name=prefilled_name)
    else:
        return_value = await ptp_tracker_jobs._get_artist_name_and_ptpurl(question=question, name=prefilled_name)
        assert return_value == exp_result

    assert mocks.mock_calls == exp_mock_calls
    assert ptp_tracker_jobs.artists_job.info == ''


@pytest.mark.parametrize('focused', (None, Mock()))
@pytest.mark.asyncio
async def test__get_artist_importance(focused, ptp_tracker_jobs, mocker):
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.uis.prompts.RadioListPrompt'),
        'RadioListPrompt',
    )
    mocker.patch.object(type(ptp_tracker_jobs), 'artists_job', PropertyMock(return_value=Mock(
        add_prompt=AsyncMock(return_value=('<label>', '<enum>')),
    )))
    mocks.attach_mock(ptp_tracker_jobs.artists_job.add_prompt, 'add_prompt')

    if focused is not None:
        return_value = await ptp_tracker_jobs._get_artist_importance(focused=focused)
    else:
        return_value = await ptp_tracker_jobs._get_artist_importance()
    assert return_value == '<enum>'
    assert mocks.mock_calls == [
        call.RadioListPrompt(
            options=[
                ('Add actor', ptp.metadata.ArtistImportance.ACTOR),
                ('Add director', ptp.metadata.ArtistImportance.DIRECTOR),
                ('Add writer', ptp.metadata.ArtistImportance.WRITER),
                ('Add producer', ptp.metadata.ArtistImportance.PRODUCER),
                ('Add composer', ptp.metadata.ArtistImportance.COMPOSER),
                ('Add cinematographer', ptp.metadata.ArtistImportance.CINEMATOGRAPHER),
                ('Stop adding artists', None),
            ],
            focused=focused,
        ),
        call.add_prompt(mocks.RadioListPrompt.return_value),
    ]


@pytest.mark.parametrize(
    argnames='role, exp_return_value',
    argvalues=(
        (None, ''),
        ('   This Role ', 'THIS ROLE'),
    ),
)
@pytest.mark.asyncio
async def test__get_artist_role(role, exp_return_value, ptp_tracker_jobs, mocker):
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.uis.prompts.TextPrompt'),
        'TextPrompt',
    )
    mocker.patch.object(type(ptp_tracker_jobs), 'artists_job', PropertyMock(return_value=Mock(
        add_prompt=AsyncMock(return_value=role.upper() if role else ''),
    )))
    mocks.attach_mock(ptp_tracker_jobs.artists_job.add_prompt, 'add_prompt')

    if role is None:
        return_value = await ptp_tracker_jobs._get_artist_role(question='That Guy role:')
        exp_text = ''
    else:
        return_value = await ptp_tracker_jobs._get_artist_role(question='That Guy role:', role=role)
        exp_text = role

    assert return_value == exp_return_value
    assert mocks.mock_calls == [
        call.TextPrompt(question='That Guy role:', text=exp_text),
        call.add_prompt(mocks.TextPrompt.return_value),
    ]


@pytest.mark.parametrize(
    argnames='artist, exp_output',
    argvalues=(
        (
            {'importance': 'Gaffer', 'name': 'The Dude', 'role': 'Lé Düde', 'ptpurl': 'http://ptp.url/dude'},
            'Gaffer: The Dude | Lé Düde | http://ptp.url/dude',
        ),
        (
            {'importance': 'Gaffer', 'name': 'The Dude', 'role': '', 'ptpurl': 'http://ptp.url/dude'},
            'Gaffer: The Dude | http://ptp.url/dude',
        ),
        (
            {'importance': 'Gaffer', 'name': 'The Dude', 'ptpurl': 'http://ptp.url/dude'},
            'Gaffer: The Dude | http://ptp.url/dude',
        ),
    ),
    ids=lambda v: repr(v),
)
def test__add_formatted_artist(artist, exp_output, ptp_tracker_jobs):
    job = Mock()
    return_value = ptp_tracker_jobs._add_formatted_artist(job, artist)
    assert return_value is None
    assert job.mock_calls == [call.add_output(exp_output)]


@pytest.mark.parametrize(
    argnames='options, exp_ignore_cache',
    argvalues=(
        ({}, False),
        ({'source': ''}, False),
        ({'source': None}, False),
        ({'source': 'WEB-ray'}, True),
    ),
    ids=lambda v: repr(v),
)
def test_source_job(options, exp_ignore_cache, ptp_tracker_jobs, mocker):
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    mocker.patch.object(ptp_tracker_jobs, 'get_job_name')
    mocker.patch.object(ptp_tracker_jobs, 'make_precondition')
    mocker.patch.object(ptp_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value=options))

    assert ptp_tracker_jobs.source_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [call(
        name=ptp_tracker_jobs.get_job_name.return_value,
        label='Source',
        precondition=ptp_tracker_jobs.make_precondition.return_value,
        text=ptp_tracker_jobs.autodetect_source,
        normalizer=ptp_tracker_jobs.normalize_source,
        validator=ptp_tracker_jobs.validate_source,
        finish_on_success=True,
        common_job_arg='common job argument',
    )]
    assert ptp_tracker_jobs.get_job_name.call_args_list == [call('source')]
    assert ptp_tracker_jobs.make_precondition.call_args_list == [call('source_job')]
    assert ptp_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=exp_ignore_cache)]


@pytest.mark.parametrize(
    argnames='options, release_name_source, exp_source, exp_source_job_text',
    argvalues=(
        ({}, 'BluRay', 'Blu-ray', None),
        ({'source': 'dvd'}, 'Blu-ray', 'DVD', None),
        ({'source': ''}, 'DVDrip', 'DVD', None),
        ({'source': 'WEB'}, 'DVDRip', 'WEB', None),
        ({}, 'WEB-DL', 'WEB', None),
        ({'source': 'WEBDL'}, 'DVD', 'WEB', None),
        ({'source': None}, 'WEBRip', 'WEB', None),
        ({'source': 'Hd-Dvd'}, None, 'HD-DVD', None),
        ({'source': ''}, 'HDDVD', 'HD-DVD', None),
        ({'source': ''}, 'HD-TV', 'HDTV', None),
        ({'source': 'HDtv'}, 'Blu-ray', 'HDTV', None),
        ({}, 'TV', 'TV', None),
        ({'source': 'VHS'}, 'WEB', 'VHS', None),
        ({}, 'VHSRip', 'VHS', None),
        ({}, 'YerMom', None, 'YerMom'),
        ({'source': 'MeMom'}, 'YerMom', None, 'MeMom'),
    ),
    ids=lambda v: str(v),
)
def test_autodetect_source(options, release_name_source, exp_source, exp_source_job_text, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'release_name', PropertyMock(return_value=Mock(
        source=release_name_source,
    )))
    mocker.patch.object(type(ptp_tracker_jobs), 'source_job', PropertyMock(return_value=Mock(
        text='not set',
    )))
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value=options))

    return_value = ptp_tracker_jobs.autodetect_source()
    assert return_value == exp_source
    if exp_source_job_text is not None:
        assert ptp_tracker_jobs.source_job.text == exp_source_job_text
    else:
        assert ptp_tracker_jobs.source_job.text == 'not set'


@pytest.mark.parametrize(
    argnames='text, exp_source',
    argvalues=(
        ('bluray', 'Blu-ray'),
        ('BluRay', 'Blu-ray'),
        ('hd-dvd', 'HD-DVD'),
        ('hddvd', 'HD-DVD'),
        ('YerMom', 'YerMom'),
    ),
    ids=lambda v: str(v),
)
def test_normalize_source(text, exp_source, ptp_tracker_jobs):
    return_value = ptp_tracker_jobs.normalize_source(text)
    assert return_value == exp_source


@pytest.mark.parametrize(
    argnames='text, exp_exception',
    argvalues=(
        ('', ValueError('You must provide a source.')),
        ('UNKNOWN_SOURCE', ValueError('You must provide a source.')),
        ('KNOWN_SOURCE', ValueError('Source is not valid: KNOWN_SOURCE')),
        ('Blu-ray', None),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_validate_source(text, exp_exception, ptp_tracker_jobs, mocker):
    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            ptp_tracker_jobs.validate_source(text)
    else:
        return_value = ptp_tracker_jobs.validate_source(text)
        assert return_value is None


@pytest.mark.parametrize(
    argnames='ptp_group_id, post_data_common, post_data_add_movie_format, post_data_add_new_movie, exp_post_data',
    argvalues=(
        (
            None,
            {'common': 'data', 'more': 'common values'},
            {'add': 'format', 'common': 'data for adding movie format'},
            {'new': 'movie', 'common': 'data for adding new movie'},
            {'new': 'movie', 'common': 'data for adding new movie', 'more': 'common values'},
        ),
        (
            '123456',
            {'common': 'data', 'more': 'common values'},
            {'add': 'format', 'common': 'data for adding movie format'},
            {'new': 'movie', 'common': 'data for adding new movie'},
            {'add': 'format', 'common': 'data for adding movie format', 'more': 'common values'},
        ),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_post_data(
        ptp_group_id,
        post_data_common, post_data_add_movie_format, post_data_add_new_movie,
        exp_post_data,
        ptp_tracker_jobs, mocker,
):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id', PropertyMock(return_value=ptp_group_id))
    mocker.patch.object(type(ptp_tracker_jobs), '_post_data_common', PropertyMock(return_value=post_data_common))
    mocker.patch.object(type(ptp_tracker_jobs), '_post_data_add_movie_format', PropertyMock(return_value=post_data_add_movie_format))
    mocker.patch.object(type(ptp_tracker_jobs), '_post_data_add_new_movie', PropertyMock(return_value=post_data_add_new_movie))
    assert ptp_tracker_jobs.post_data == exp_post_data


@pytest.mark.parametrize('not_main_movie, exp_special', ((True, '1'), (False, None)))
@pytest.mark.parametrize('personal_rip, exp_internalrip', ((True, '1'), (False, None)))
@pytest.mark.parametrize('is_scene_release, exp_scene', ((True, '1'), (False, None)))
def test__post_data_common(
        not_main_movie, exp_special,
        personal_rip, exp_internalrip,
        is_scene_release, exp_scene,
        ptp_tracker_jobs, mock_job_attributes, mocker,
):
    mock_job_attributes(ptp_tracker_jobs)

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, 'get_job_attribute', side_effect=(
            '<type>',
            is_scene_release,
        )),
        'get_job_attribute',
    )
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, 'get_job_output', side_effect=(
            '<description>',
        )),
        'get_job_output',
    )
    mocker.patch.object(type(ptp_tracker_jobs), 'nfo_text', PropertyMock(return_value='<nfo text>'))
    mocker.patch.object(type(ptp_tracker_jobs), 'options', PropertyMock(return_value={
        'not_main_movie': not_main_movie,
        'personal_rip': personal_rip,
        'upload_token': '<upload token>',
    }))
    for name in ('source', 'codec', 'container', 'resolution', 'edition', 'subtitles', 'trumpable'):
        mocker.patch.object(
            type(ptp_tracker_jobs),
            f'_post_data_common_{name}',
            PropertyMock(return_value={name: 'post data'}),
        )

    exp_post_data = {
        'type': '<type>',
        'release_desc': '<description>',
        'special': exp_special,
        'internalrip': exp_internalrip,
        'scene': exp_scene,
        'nfo_text': '<nfo text>',
        'uploadtoken': '<upload token>',
        'source': 'post data',
        'codec': 'post data',
        'container': 'post data',
        'resolution': 'post data',
        'edition': 'post data',
        'subtitles': 'post data',
        'trumpable': 'post data',
    }

    assert ptp_tracker_jobs._post_data_common == exp_post_data

    assert mocks.mock_calls == [
        call.get_job_attribute(ptp_tracker_jobs.type_job, 'choice'),
        call.get_job_output(ptp_tracker_jobs.description_job, slice=0),
        call.get_job_attribute(ptp_tracker_jobs.scene_check_job, 'is_scene_release'),
    ]


def test__post_data_common_source(ptp_tracker_jobs, mocker):
    mocker.patch.object(ptp_tracker_jobs, 'get_job_output')
    assert ptp_tracker_jobs._post_data_common_source == {
        'source': 'Other',
        'other_source': ptp_tracker_jobs.get_job_output.return_value,
    }
    assert ptp_tracker_jobs.get_job_output.call_args_list == [
        call(ptp_tracker_jobs.source_job, slice=0),
    ]


def test__post_data_common_codec(ptp_tracker_jobs):
    assert ptp_tracker_jobs._post_data_common_codec == {
        'codec': '* Auto-detect',
        'other_codec': '',
    }


def test__post_data_common_container(ptp_tracker_jobs):
    assert ptp_tracker_jobs._post_data_common_container == {
        'container': '* Auto-detect',
        'other_container': ''
    }


def test__post_data_common_resolution(ptp_tracker_jobs):
    assert ptp_tracker_jobs._post_data_common_resolution == {
        'resolution': '* Auto-detect',
        'other_resolution': ''
    }


@pytest.mark.parametrize(
    argnames='subtitles_exist',
    argvalues=(
        pytest.param(True, id='Subtitles exist'),
        pytest.param(False, id='No subtitles exist'),
    ),
    ids=lambda v: repr(v),
)
def test__post_data_common_subtitles(subtitles_exist, ptp_tracker_jobs, mocker):
    if subtitles_exist:
        subtitles_job_output = (
            'es',
            'pt',
            'pt-BR',
            'asdf',
            'en [SRT]',
            'en (forced) [SRT]',
            'fr-GF [PGS]',
            'el [ASS]',
            'asdf-TW [SSA]',
        )
        exp_ptp_codes = [
            ptp.metadata.subtitles['es'],
            ptp.metadata.subtitles['pt'],
            ptp.metadata.subtitles['pt-BR'],
            ptp.metadata.subtitles['en'],
            ptp.metadata.subtitles['en (forced)'],
            ptp.metadata.subtitles['fr'],
            ptp.metadata.subtitles['el'],
        ]
    else:
        subtitles_job_output = ()
        exp_ptp_codes = [ptp.metadata.subtitles['No Subtitles']]

    mocker.patch.object(type(ptp_tracker_jobs), 'subtitle_languages_job', PropertyMock(return_value=Mock(
        output=subtitles_job_output,
    )))
    assert ptp_tracker_jobs._post_data_common_subtitles == {'subtitles[]': exp_ptp_codes}


@pytest.mark.parametrize(
    argnames='edition_text, exp_post_data',
    argvalues=(
        ('', {'remaster': None, 'remaster_title': None}),
        ('Superdupercut', {'remaster': 'on', 'remaster_title': 'Superdupercut'}),
    ),
    ids=lambda v: repr(v),
)
def test__post_data_common_edition(edition_text, exp_post_data, ptp_tracker_jobs, mocker):
    mocker.patch.object(ptp_tracker_jobs, 'get_job_output', return_value=edition_text)
    assert ptp_tracker_jobs._post_data_common_edition == exp_post_data
    assert ptp_tracker_jobs.get_job_output.call_args_list == [
        call(ptp_tracker_jobs.edition_job, slice=0),
    ]


@pytest.mark.parametrize(
    argnames='trumpable_job_output, exp_post_data',
    argvalues=(
        ((), {'trumpable[]': []}),
        (
            (
                str(ptp.metadata.TrumpableReason.HARDCODED_SUBTITLES),
            ),
            {
                'trumpable[]': [
                    ptp.metadata.TrumpableReason.HARDCODED_SUBTITLES.value,
                ],
            },
        ),
        (
            (
                str(ptp.metadata.TrumpableReason.NO_ENGLISH_SUBTITLES),
            ),
            {
                'trumpable[]': [
                    ptp.metadata.TrumpableReason.NO_ENGLISH_SUBTITLES.value,
                ],
            },
        ),
        (
            (
                str(ptp.metadata.TrumpableReason.HARDCODED_SUBTITLES),
                str(ptp.metadata.TrumpableReason.NO_ENGLISH_SUBTITLES),
            ),
            {
                'trumpable[]': [
                    ptp.metadata.TrumpableReason.HARDCODED_SUBTITLES.value,
                    ptp.metadata.TrumpableReason.NO_ENGLISH_SUBTITLES.value,
                ],
            },
        ),
    ),
    ids=lambda v: str(v),
)
def test__post_data_common_trumpable(trumpable_job_output, exp_post_data, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'trumpable_job', PropertyMock(return_value=Mock(
        output=trumpable_job_output,
    )))
    assert ptp_tracker_jobs._post_data_common_trumpable == exp_post_data


def test__post_data_add_movie_format(ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'ptp_group_id', PropertyMock(return_value='123456'))
    assert ptp_tracker_jobs._post_data_add_movie_format == {
        'groupid': ptp_tracker_jobs.ptp_group_id,
    }


def test__post_data_add_new_movie(ptp_tracker_jobs, mock_job_attributes, mocker):
    mock_job_attributes(ptp_tracker_jobs)

    mocker.patch.object(type(ptp_tracker_jobs), 'imdb_id', PropertyMock(return_value='tt123456'))

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs.tracker, 'normalize_imdb_id', side_effect=lambda id: f'<{id}>'),
        'normalize_imdb_id',
    )
    mocks.attach_mock(
        mocker.patch.object(ptp_tracker_jobs, 'get_job_output', side_effect=(
            '<title>',
            '<year>',
            '<plot>',
            '<tags>',
            '<poster>',
        )),
        'get_job_output',
    )

    mocker.patch.object(type(ptp_tracker_jobs), '_post_data_add_new_movie_artists', PropertyMock(return_value={
        'artistnames[]': ['Jeffrey Falcon', 'Jeffrey Owl', 'Jeffrey Pidgeon'],
        'artistids[]': ['123', '234', '345'],
        'importances[]': [1, 5, 5],
        'roles[]': ['', 'Buddy', ''],
    }))

    exp_post_data = {
        'imdb': f'<{ptp_tracker_jobs.imdb_id}>',
        'title': '<title>',
        'year': '<year>',
        'album_desc': '<plot>',
        'tags': '<tags>',
        'image': '<poster>',
        'artistnames[]': ['Jeffrey Falcon', 'Jeffrey Owl', 'Jeffrey Pidgeon'],
        'artistids[]': ['123', '234', '345'],
        'importances[]': [1, 5, 5],
        'roles[]': ['', 'Buddy', ''],
    }
    assert ptp_tracker_jobs._post_data_add_new_movie == exp_post_data
    assert mocks.mock_calls == [
        call.normalize_imdb_id(ptp_tracker_jobs.imdb_id),
        call.get_job_output(ptp_tracker_jobs.title_job, slice=0),
        call.get_job_output(ptp_tracker_jobs.year_job, slice=0),
        call.get_job_output(ptp_tracker_jobs.plot_job, slice=0),
        call.get_job_output(ptp_tracker_jobs.tags_job, slice=0),
        call.get_job_output(ptp_tracker_jobs.poster_job, slice=0),
    ]


@pytest.mark.parametrize(
    argnames='lines, exp_result',
    argvalues=(
        pytest.param(
            (),
            {},
            id='No artists',
        ),
        pytest.param(
            (
                'Director: Steven Spielbergo | https://passthepopcorn.me/artist.php?id=123',
                'Actor: Jeffrey Falcon | Buddy | https://passthepopcorn.me/artist.php?id=234',
                'Actor: Jeffrey Owl | https://passthepopcorn.me/artist.php?id=345',
                'Producer: Jeffrey Pidgeon | Dubby | https://passthepopcorn.me/artist.php?id=456',
                'Writer: Jeffrey Hawk | Yubbi | https://passthepopcorn.me/artist.php?id=567',
                'Composer: Jeffrey Vulture | https://passthepopcorn.me/artist.php?id=678',
                'Cinematographer: Jeffrey Tit | Hubby | https://passthepopcorn.me/artist.php?id=789',
            ),
            {
                'artistnames[]': [
                    'Steven Spielbergo',
                    'Jeffrey Falcon',
                    'Jeffrey Owl',
                    'Jeffrey Pidgeon',
                    'Jeffrey Hawk',
                    'Jeffrey Vulture',
                    'Jeffrey Tit',
                ],
                'artistids[]': [
                    '123',
                    '234',
                    '345',
                    '456',
                    '567',
                    '678',
                    '789',
                ],
                'importances[]': [1, 5, 5, 3, 2, 4, 6],
                'roles[]': [
                    '',
                    'Buddy',
                    '',
                    'Dubby',
                    'Yubbi',
                    '',
                    'Hubby',
                ],
            },
            id='Artists are assembled',
        ),
        pytest.param(
            (
                'Director: Steven Spielbergo | https://passthepopcorn.me/artist.php?id=123',
                'Producer: Jeffrey Pidgeon | Dubby',
                'Cinematographer: Jeffrey Tit | Hubby | https://passthepopcorn.me/artist.php?id=789',
            ),
            RuntimeError('Unexpected line: Producer: Jeffrey Pidgeon | Dubby'),
            id='Unexpected output: No URL',
        ),
        pytest.param(
            (
                'Jeffrey Tit | Hubby | https://passthepopcorn.me/artist.php?id=789',
            ),
            RuntimeError('Unexpected line: Jeffrey Tit | Hubby | https://passthepopcorn.me/artist.php?id=789'),
            id='Unexpected output: No importance',
        ),
    ),
    ids=lambda v: str(v),
)
def test__post_data_add_new_movie_artists(lines, exp_result, ptp_tracker_jobs, mocker):
    mocker.patch.object(type(ptp_tracker_jobs), 'artists_job', PropertyMock())
    mocker.patch.object(ptp_tracker_jobs, 'get_job_output', return_value=lines)

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            ptp_tracker_jobs._post_data_add_new_movie_artists
    else:
        assert ptp_tracker_jobs._post_data_add_new_movie_artists == exp_result

    assert ptp_tracker_jobs.get_job_output.call_args_list == [
        call(ptp_tracker_jobs.artists_job, default=()),
    ]
