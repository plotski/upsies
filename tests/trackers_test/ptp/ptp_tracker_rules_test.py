import re
from unittest.mock import Mock, PropertyMock

import pytest

from upsies import errors
from upsies.trackers.base import rules as base_rules
from upsies.trackers.ptp import rules as ptp_rules


def test_PtpBannedGroup_banned_groups():
    assert ptp_rules.PtpBannedGroup.banned_groups == {
        'aXXo',
        'BMDRu',
        'BRrip',
        'CM8',
        'CrEwSaDe',
        'CTFOH',
        'd3g',
        'DNL',
        'FaNGDiNG0',
        'HD2DVD',
        'HDT',
        'HDTime',
        'ION10',
        'iPlanet',
        'KiNGDOM',
        'LAMA',
        'mHD',
        'mSD',
        'NhaNc3',
        'nHD',
        'nikt0',
        'nSD',
        'OFT',
        'PRODJi',
        'SANTi',
        'SasukeducK',
        'SPiRiT',
        'STUTTERSHIT',
        'ViSION',
        'VXT',
        'WAF',
        'WORLD',
        'x0r',
        'YIFY',
    }


@pytest.mark.parametrize(
    argnames='release_name, exp_exception',
    argvalues=(
        # No encode from EVO.
        (Mock(group='EVO', source='WEB-DL', video_format=''), None),
        (Mock(group='EVO', source='BluRay', video_format=''), errors.BannedGroup('EVO', additional_info=('No encodes, only WEB-DL'))),
        (Mock(group='ASDF', source='BluRay', video_format=''), None),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_PtpBannedGroup__check_custom(release_name, exp_exception, mocker):
    assert issubclass(ptp_rules.PtpBannedGroup, base_rules.TrackerRuleBase)

    rule = ptp_rules.PtpBannedGroup(Mock())
    mocker.patch.object(type(rule), 'release_name', PropertyMock(return_value=release_name))

    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await rule._check()
    else:
        return_value = await rule._check()
        assert return_value is None
