import re
from unittest.mock import AsyncMock, Mock, call

import pytest

from upsies import errors
from upsies.trackers.base import rules


@pytest.fixture
def MyRule():

    class MyRule(rules.TrackerRuleBase):
        _check = AsyncMock()

    return MyRule


@pytest.fixture
def my_rule(MyRule):
    return MyRule(tracker_jobs=AsyncMock())


def test_TrackerRuleBase_tracker_jobs(MyRule):
    tracker_jobs = AsyncMock()
    rule = MyRule(tracker_jobs=tracker_jobs)
    assert rule.tracker_jobs is tracker_jobs


def test_TrackerRuleBase_release_name(my_rule):
    assert my_rule.release_name is my_rule.tracker_jobs.release_name


def test_TrackerRuleBase_tracker(my_rule):
    assert my_rule.tracker is my_rule.tracker_jobs.tracker


@pytest.mark.asyncio
async def test_TrackerRuleBase__wait_for_required_jobs(my_rule):
    type(my_rule).required_jobs = ('foo_job', 'bar_job', 'baz_job')
    await my_rule._wait_for_required_jobs()
    assert my_rule.tracker_jobs.mock_calls == [
        call.foo_job.wait_finished(),
        call.bar_job.wait_finished(),
        call.baz_job.wait_finished(),
    ]


@pytest.mark.asyncio
async def test_TrackerRuleBase_check(my_rule, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(my_rule, '_check'), '_check')
    mocks.attach_mock(mocker.patch.object(my_rule, '_wait_for_required_jobs'), '_wait_for_required_jobs')

    await my_rule.check()
    assert mocks.mock_calls == [
        call._wait_for_required_jobs(),
        call._check(),
    ]


def test_BannedGroup_required_jobs():
    rules.BannedGroup.required_jobs = ()


def test_BannedGroup_banned_groups():
    rules.BannedGroup.banned_groups = set()


@pytest.mark.parametrize(
    argnames='release_name_group, group, exp_return_value',
    argvalues=(
        ('FOO', 'FOO', True),
        ('FOO', 'FOOO', False),
        ('FOO', 'Foo', True),
        ('FOO', 'foo', True),
    ),
    ids=lambda v: repr(v),
)
def test_BannedGroup_is_group(release_name_group, group, exp_return_value):
    rule = rules.BannedGroup(tracker_jobs=Mock())
    rule.release_name.group = release_name_group
    return_value = rule.is_group(group)
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='banned_groups, is_group, exp_exception, exp_mock_calls',
    argvalues=(
        (
            ('FOO', 'BaR', 'BAZ', 'ASDF'),
            Mock(side_effect=(False, False, False, False)),
            None,
            [
                call._check_custom(),
                call.is_group('FOO'),
                call.is_group('BaR'),
                call.is_group('BAZ'),
                call.is_group('ASDF'),
            ],
        ),
        (
            ('FOO', 'BaR', 'BAZ', 'ASDF'),
            Mock(side_effect=(False, False, True, False)),
            errors.BannedGroup('BAZ'),
            [
                call._check_custom(),
                call.is_group('FOO'),
                call.is_group('BaR'),
                call.is_group('BAZ'),
            ],
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_BannedGroup_check(banned_groups, is_group, exp_exception, exp_mock_calls, mocker):
    rule = rules.BannedGroup(tracker_jobs=Mock())
    type(rule).banned_groups = banned_groups
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(rule, '_check_custom'), '_check_custom')
    mocks.attach_mock(mocker.patch.object(rule, 'is_group', is_group), 'is_group')

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await rule._check()
    else:
        return_value = await rule._check()
        assert return_value is None
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.asyncio
async def test_BannedGroup_check_custom():
    rule = rules.BannedGroup(tracker_jobs=Mock())
    # This should do nothing.
    return_value = await rule._check_custom()
    assert return_value is None


@pytest.mark.parametrize(
    argnames='resolution_int, exp_exception',
    argvalues=(
        (719, errors.RuleBroken('Not a HD release')),
        (720, None),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_HdOnly_check(resolution_int, exp_exception, mocker):
    rule = rules.HdOnly(tracker_jobs=Mock())
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_resolution_int', return_value=resolution_int), 'get_resolution_int')

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await rule._check()
    else:
        return_value = await rule._check()
        assert return_value is None
    assert mocks.mock_calls == [call.get_resolution_int(rule.release_name.path)]
