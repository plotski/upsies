import asyncio
import os
import re
import types
from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from upsies import __project_name__, errors
from upsies.trackers import base


def make_MockTracker(**kwargs):
    class MockTracker(base.TrackerBase):
        name = 'asdf'
        label = 'AsdF'
        torrent_source_field = '->!!!ASDF!!!<-'
        TrackerJobs = PropertyMock()
        TrackerConfig = PropertyMock()
        _login = AsyncMock()
        confirm_logged_in = AsyncMock()
        _logout = AsyncMock()
        get_announce_url = AsyncMock()
        upload = AsyncMock()

    return MockTracker(**kwargs)


@pytest.mark.parametrize(
    argnames='rules_attribute, exp_rules',
    argvalues=(
        (None, ()),
        (iter(('RuleClass1', 'RuleClass2', 'RuleClass3')), ('RuleClass1', 'RuleClass2', 'RuleClass3')),
        (
            Mock(spec=types.ModuleType, TrackerRuleBase_subclasses=('RuleClass1', 'RuleClass2', 'RuleClass3')),
            ('RuleClass1', 'RuleClass2', 'RuleClass3'),
        ),
        (123, TypeError('Invalid MyTracker.rules: 123')),
    ),
    ids=lambda v: repr(v),
)
def test_rules_attribute(rules_attribute, exp_rules, mocker):

    def subclasses(cls, modules):
        return tuple(
            TrackerRuleBase_subclass
            for module in modules
            for TrackerRuleBase_subclass in module.TrackerRuleBase_subclasses
        )

    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.subclasses', side_effect=subclasses), 'subclasses')

    if isinstance(exp_rules, Exception):
        with pytest.raises(type(exp_rules), match=rf'^{re.escape(str(exp_rules))}$'):
            class MyTracker(type(make_MockTracker())):
                rules = rules_attribute
    else:
        class MyTracker(type(make_MockTracker())):
            rules = rules_attribute
        tracker = MyTracker()
        assert tracker.rules == exp_rules


def test_signals():
    tracker = make_MockTracker()
    assert tracker.signal.id == f'{tracker.name}-tracker'
    assert tuple(tracker.signal.signals) == (
        'warning',
        'error',
        'exception',
        'logging_in', 'login_failed', 'logged_in',
        'logging_out', 'logout_failed', 'logged_out',
    )


def test_generate_setup_howto(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.trackers.base.tracker.Howto'), 'Howto')
    mocks.attach_mock(mocker.patch('upsies.utils.string.evaluate_fstring'), 'evaluate_fstring')
    tracker = make_MockTracker()
    return_value = type(tracker).generate_setup_howto()
    assert return_value is mocks.evaluate_fstring.return_value
    assert mocks.mock_calls == [
        call.Howto(tracker_cls=type(tracker)),
        call.evaluate_fstring(
            type(tracker).setup_howto_template,
            howto=mocks.Howto.return_value,
            tracker=type(tracker),
            executable=__project_name__,
        ),
    ]


def test_options():
    options = {'username': 'foo', 'password': 'bar'}
    tracker = make_MockTracker(options=options)
    assert tracker.options is options


def test_tasks():
    tracker = make_MockTracker()
    assert tracker._tasks == []
    assert tracker._tasks is tracker._tasks


@pytest.mark.parametrize('callback', (None, Mock()), ids=('without callback', 'with callback'))
@pytest.mark.asyncio
async def test_attach_task(callback, mocker):
    tracker = make_MockTracker()

    work = AsyncMock()
    if callback:
        task = tracker.attach_task(work(), callback=callback)
    else:
        task = tracker.attach_task(work())

    assert task in tracker._tasks

    await task

    assert task not in tracker._tasks
    if callback:
        assert callback.call_args_list == [call(task)]


@pytest.mark.asyncio
async def test_await_tasks(mocker):
    tracker = make_MockTracker()
    tasks = (
        asyncio.ensure_future(AsyncMock()()),
        asyncio.ensure_future(AsyncMock()()),
        asyncio.ensure_future(AsyncMock()()),
    )
    tracker._tasks.extend(tasks)

    for task in tasks:
        assert not task.done()

    await tracker.await_tasks()

    for task in tasks:
        assert task.done()


@pytest.mark.parametrize(
    argnames='is_logged_in, _login, confirm_logged_in, exp_mock_calls, exp_attributes, exp_exception',
    argvalues=(
        pytest.param(
            True,
            AsyncMock(),
            AsyncMock(),
            [],
            {'is_logged_in': True},
            None,
            id='Already logged in',
        ),
        pytest.param(
            False,
            AsyncMock(side_effect=errors.RequestError('Login failed')),
            AsyncMock(),
            [
                call.emit('logging_in'),
                call._login(custom='credentials'),
                call.emit('login_failed', errors.RequestError('Login failed')),
            ] * 3,
            {'is_logged_in': False},
            errors.RequestError('Login failed'),
            id='Login fails',
        ),
        pytest.param(
            False,
            AsyncMock(),
            AsyncMock(side_effect=errors.RequestError('Login confirmation failed')),
            [
                call.emit('logging_in'),
                call._login(custom='credentials'),
                call.confirm_logged_in(),
                call.emit('login_failed', errors.RequestError('Login confirmation failed')),
            ] * 3,
            {'is_logged_in': False},
            errors.RequestError('Login confirmation failed'),
            id='Login confirmation fails',
        ),
        pytest.param(
            False,
            AsyncMock(),
            AsyncMock(),
            [
                call.emit('logging_in'),
                call._login(custom='credentials'),
                call.confirm_logged_in(),
                call.emit('logged_in'),
            ],
            {'is_logged_in': True},
            None,
            id='Login succeeds',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_login(is_logged_in, _login, confirm_logged_in, exp_mock_calls, exp_attributes, exp_exception, mocker):
    tracker = make_MockTracker()

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tracker, '_login', _login), '_login')
    mocks.attach_mock(mocker.patch.object(tracker, 'confirm_logged_in', confirm_logged_in), 'confirm_logged_in')
    mocks.attach_mock(mocker.patch.object(tracker.signal, 'emit'), 'emit')
    tracker._is_logged_in = is_logged_in

    credentials = {'custom': 'credentials'}

    async def do():
        await asyncio.gather(
            tracker.login(**credentials),
            tracker.login(**credentials),
            tracker.login(**credentials),
        )

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await do()
    else:
        await do()

    assert mocks.mock_calls == exp_mock_calls
    for name, value in exp_attributes.items():
        assert getattr(tracker, name) == value


@pytest.mark.parametrize(
    argnames='cookies_filepath, confirm_logged_in, delete_cookies_filepath, exp_return_value, exp_mock_calls, exp_attributes',
    argvalues=(
        pytest.param(
            None,
            AsyncMock(),
            Mock(),
            None,
            [],
            {},
            id='No cookies_filepath',
        ),
        pytest.param(
            'path/to/cookies',
            AsyncMock(side_effect=errors.RequestError('Cannot confirm')),
            Mock(),
            False,
            [
                call.confirm_logged_in(),
                call.delete_cookies_filepath(),
                call.emit('login_failed', 'Session is no longer valid: Cannot confirm')
            ],
            {'is_logged_in': False},
            id='confirm_logged_in() raises RequestError',
        ),
        pytest.param(
            'path/to/cookies',
            AsyncMock(),
            Mock(),
            True,
            [
                call.confirm_logged_in(),
                call.emit('logged_in')
            ],
            {'is_logged_in': True},
            id='confirm_logged_in() does not raise',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_still_logged_in(
        cookies_filepath, confirm_logged_in, delete_cookies_filepath,
        exp_return_value, exp_mock_calls, exp_attributes,
        mocker,
):
    tracker = make_MockTracker()

    mocks = Mock()
    mocker.patch.object(type(tracker), 'cookies_filepath', PropertyMock(return_value=cookies_filepath))
    mocks.attach_mock(mocker.patch.object(tracker, 'confirm_logged_in', confirm_logged_in), 'confirm_logged_in')
    mocks.attach_mock(mocker.patch.object(tracker, 'delete_cookies_filepath', delete_cookies_filepath), 'delete_cookies_filepath')
    mocks.attach_mock(mocker.patch.object(tracker.signal, 'emit'), 'emit')

    return_value = await tracker.still_logged_in()
    assert return_value is exp_return_value
    assert mocks.mock_calls == exp_mock_calls
    for name, value in exp_attributes.items():
        if value is None:
            assert not hasattr(tracker, name)
        else:
            assert getattr(tracker, name) == value


@pytest.mark.parametrize(
    argnames='cookies_filepath, force, is_logged_in, _logout, exp_mock_calls, exp_attributes, exp_exception',
    argvalues=(
        pytest.param(
            'path/to/cookies', False, True,
            AsyncMock(),
            [],
            {'is_logged_in': True},
            None,
            id='Not logging out because: cookies_filepath specified, not forced',
        ),
        pytest.param(
            'path/to/cookies', True, False,
            AsyncMock(),
            [],
            {'is_logged_in': False},
            None,
            id='Not logging out because: cookies_filepath specified, forced, not logged in',
        ),
        pytest.param(
            None, False, False,
            AsyncMock(),
            [],
            {'is_logged_in': False},
            None,
            id='Not logging out because: no cookies_filepath specified, not forced, not logged in',
        ),
        pytest.param(
            None, False, True,
            AsyncMock(side_effect=errors.RequestError('You shall not log out!')),
            [
                call.emit('logging_out'),
                call._logout(),
                call.emit('logout_failed', errors.RequestError('You shall not log out!')),
                call.emit('logged_out'),
            ],
            {'is_logged_in': False},
            errors.RequestError('You shall not log out!'),
            id='Logging out: request fails',
        ),
        pytest.param(
            None, False, True,
            AsyncMock(),
            [
                call.emit('logging_out'),
                call._logout(),
                call.emit('logged_out'),
            ],
            {'is_logged_in': False},
            None,
            id='Logging out: request succeeds',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_logout(cookies_filepath, force, is_logged_in, _logout, exp_mock_calls, exp_attributes, exp_exception, mocker):
    tracker = make_MockTracker()

    mocks = Mock()
    mocker.patch.object(type(tracker), 'cookies_filepath', PropertyMock(return_value=cookies_filepath))
    mocks.attach_mock(mocker.patch.object(tracker, '_logout', _logout), '_logout')
    mocks.attach_mock(mocker.patch.object(tracker.signal, 'emit'), 'emit')
    tracker._is_logged_in = is_logged_in

    async def do():
        # Logout multiple times concurrently to test if session_lock works.
        await asyncio.gather(
            tracker.logout(),
            tracker.logout(),
            tracker.logout(),
        )

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await do()
    else:
        return_value = await do()
        assert return_value is None

    assert mocks.mock_calls == exp_mock_calls
    for name, value in exp_attributes.items():
        assert getattr(tracker, name) == value


@pytest.mark.parametrize(
    argnames='options, exp_return_value',
    argvalues=(
        ({}, None),
        ({'cookies_filepath': None}, None),
        ({'cookies_filepath': ''}, None),
        ({'cookies_filepath': 'relative/path/to/cookies'}, 'relative/path/to/cookies'),
        ({'cookies_filepath': '/absolute/path/to/cookies'}, '/absolute/path/to/cookies'),
        ({'cookies_filepath': '~/path/to/cookies'}, f'{os.environ["HOME"]}/path/to/cookies'),
    ),
    ids=lambda v: repr(v),
)
def test_cookies_filepath(options, exp_return_value, mocker):
    tracker = make_MockTracker()
    mocker.patch.object(type(tracker), 'options', PropertyMock(return_value=options))
    return_value = tracker.cookies_filepath
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='cookies_filepath, exists, permissions, exp_exception',
    argvalues=(
        (None, False, None, None),
        ('path/to/cookies', False, None, None),
        ('path/to/cookies', True, None, None),
        ('path/to/cookies', True, 0o000, OSError(r'Permission denied.*path/to/cookies')),
    ),
    ids=lambda v: repr(v),
)
def test_delete_cookies_filepath(cookies_filepath, exists, permissions, exp_exception, mocker, tmp_path):
    if cookies_filepath:
        cookies_filepath = tmp_path / cookies_filepath
        if exists:
            print('writing', cookies_filepath)
            cookies_filepath.parent.mkdir(parents=True, exist_ok=True)
            cookies_filepath.write_text('my cookies')
            if permissions is not None:
                print('restricting permissions', permissions, cookies_filepath)
                cookies_filepath.parent.chmod(permissions)
            else:
                print('not restricting permissions', cookies_filepath)
        else:
            print('not writing', cookies_filepath)

    tracker = make_MockTracker()
    mocker.patch.object(type(tracker), 'cookies_filepath', PropertyMock(return_value=(
        cookies_filepath
        if cookies_filepath is None else
        str(cookies_filepath)
    )))

    try:
        if exp_exception:
            with pytest.raises(type(exp_exception), match=rf'{exp_exception}'):
                tracker.delete_cookies_filepath()
        else:
            return_value = tracker.delete_cookies_filepath()
            assert return_value is None
        if cookies_filepath is not None:
            assert not os.path.exists(cookies_filepath)
    finally:
        if cookies_filepath and permissions is not None:
            cookies_filepath.parent.chmod(0o700)


def test_calculate_piece_size():
    tracker = make_MockTracker()
    assert type(tracker).calculate_piece_size is None


def test_calculate_piece_size_min_max():
    tracker = make_MockTracker()
    assert type(tracker).calculate_piece_size_min_max is None


def test_warn():
    tracker = make_MockTracker()
    cb = Mock()
    tracker.signal.register('warning', cb.handle_warning)
    tracker.warn('foo')
    assert cb.mock_calls == [call.handle_warning('foo')]


def test_error():
    tracker = make_MockTracker()
    cb = Mock()
    tracker.signal.register('error', cb.handle_error)
    tracker.error('foo')
    assert cb.mock_calls == [call.handle_error('foo')]


def test_exception():
    tracker = make_MockTracker()
    cb = Mock()
    tracker.signal.register('exception', cb.handle_exception)
    tracker.exception('foo')
    assert cb.mock_calls == [call.handle_exception('foo')]
