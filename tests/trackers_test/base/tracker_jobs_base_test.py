import builtins
import os
import pathlib
import re
import types
from unittest.mock import DEFAULT, AsyncMock, Mock, PropertyMock, call

import pytest

from upsies import __homepage__, __project_name__, errors, uis, utils
from upsies.trackers.base import TrackerJobsBase


@pytest.fixture
def tracker():
    tracker = Mock()
    tracker.configure_mock(
        name='AsdF',
        options={
            'announce' : 'http://foo.bar',
            'source'   : 'AsdF',
            'exclude'  : ('a', 'b'),
        },
    )
    return tracker


@pytest.fixture
def make_TestTrackerJobs(tracker):
    def make_TestTrackerJobs(**kwargs):
        class TestTrackerJobs(TrackerJobsBase):
            jobs_before_upload = PropertyMock()

        default_kwargs = {
            'content_path': '',
            'tracker': tracker,
            'reuse_torrent_path': '',
            'btclient': Mock(),
            'torrent_destination': '',
            'image_hosts': (Mock(),),
            'screenshots_optimization': 'my optimization level',
            'show_poster': 'maybe show poster',
            'common_job_args': {},
        }
        return TestTrackerJobs(**{**default_kwargs, **kwargs})

    return make_TestTrackerJobs


def test_arguments(make_TestTrackerJobs):
    kwargs = {
        'content_path': Mock(),
        'tracker': Mock(),
        'reuse_torrent_path': Mock(),
        'exclude_files': Mock(),
        'btclient': Mock(),
        'torrent_destination': Mock(),
        'screenshots_optimization': 'my optimization level',
        'image_hosts': (Mock(),),
        'options': {'mock': 'config'},
    }
    exp_attributes = {
        'content_path': kwargs['content_path'],
        'tracker': kwargs['tracker'],
        'reuse_torrent_path': kwargs['reuse_torrent_path'],
        'exclude_files': kwargs['exclude_files'],
        'btclient': kwargs['btclient'],
        'torrent_destination': kwargs['torrent_destination'],
        '_screenshots_optimization': kwargs['screenshots_optimization'],
        'image_hosts': kwargs['image_hosts'],
        'options': kwargs['options'],
    }
    tracker_jobs = make_TestTrackerJobs(**kwargs)
    for k, v in exp_attributes.items():
        assert getattr(tracker_jobs, k) is v


def test_is_bdmv_release(make_TestTrackerJobs, mocker):
    is_bluray_mock = mocker.patch('upsies.utils.disc.is_bluray')
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.is_bdmv_release is is_bluray_mock.return_value
    assert is_bluray_mock.call_args_list == [call(tracker_jobs.content_path, multidisc=True)]


def test_is_video_ts_release(make_TestTrackerJobs, mocker):
    is_dvd_mock = mocker.patch('upsies.utils.disc.is_dvd')
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.is_video_ts_release is is_dvd_mock.return_value
    assert is_dvd_mock.call_args_list == [call(tracker_jobs.content_path, multidisc=True)]


def test_is_disc_release(make_TestTrackerJobs, mocker):
    is_disc_mock = mocker.patch('upsies.utils.disc.is_disc')
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.is_disc_release is is_disc_mock.return_value
    assert is_disc_mock.call_args_list == [call(tracker_jobs.content_path, multidisc=True)]


@pytest.mark.parametrize(
    argnames='common_job_args, overload, exp_return_value',
    argvalues=(
        (
            {'home_directory': 'default/home', 'cache_directory': 'default/cache', 'ignore_cache': False},
            {'home_directory': 'overloaded/home'},
            {'home_directory': 'overloaded/home', 'cache_directory': 'default/cache', 'ignore_cache': False},
        ),
        (
            {'home_directory': 'default/home', 'cache_directory': 'default/cache', 'ignore_cache': False},
            {'cache_directory': 'overloaded/cache'},
            {'home_directory': 'default/home', 'cache_directory': 'overloaded/cache', 'ignore_cache': False},
        ),
        # `ignore_cache=False` can only be overloaded if `ignore_cache=True` was
        # not set globally.
        (
            {'home_directory': 'default/home', 'cache_directory': 'default/cache', 'ignore_cache': False},
            {'ignore_cache': True},
            {'home_directory': 'default/home', 'cache_directory': 'default/cache', 'ignore_cache': True},
        ),
        (
            {'home_directory': 'default/home', 'cache_directory': 'default/cache', 'ignore_cache': True},
            {'ignore_cache': False},
            {'home_directory': 'default/home', 'cache_directory': 'default/cache', 'ignore_cache': True},
        ),
        (
            {'home_directory': 'default/home', 'cache_directory': 'default/cache', 'ignore_cache': True},
            {'ignore_cache': True},
            {'home_directory': 'default/home', 'cache_directory': 'default/cache', 'ignore_cache': True},
        ),
    ),
    ids=lambda v: repr(v),
)
def test_common_job_args(common_job_args, overload, exp_return_value, make_TestTrackerJobs):
    tracker_jobs = make_TestTrackerJobs(common_job_args=common_job_args)
    return_value = tracker_jobs.common_job_args(**overload)
    assert return_value == exp_return_value


def test_imdb(make_TestTrackerJobs):
    tracker_jobs = make_TestTrackerJobs()
    assert isinstance(tracker_jobs.imdb, utils.webdbs.imdb.ImdbApi)

def test_tmdb(make_TestTrackerJobs):
    tracker_jobs = make_TestTrackerJobs()
    assert isinstance(tracker_jobs.tvmaze, utils.webdbs.tvmaze.TvmazeApi)

def test_tvmaze(make_TestTrackerJobs):
    tracker_jobs = make_TestTrackerJobs()
    assert isinstance(tracker_jobs.tvmaze, utils.webdbs.tvmaze.TvmazeApi)


def test_jobs_after_upload(make_TestTrackerJobs, mocker):
    logout_job_mock = mocker.patch('upsies.trackers.base.TrackerJobsBase.logout_job')
    add_torrent_job_mock = mocker.patch('upsies.trackers.base.TrackerJobsBase.add_torrent_job')
    copy_torrent_job_mock = mocker.patch('upsies.trackers.base.TrackerJobsBase.copy_torrent_job')
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.jobs_after_upload == (
        logout_job_mock,
        add_torrent_job_mock,
        copy_torrent_job_mock,
    )


def test_isolated_jobs(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.isolated_jobs == ()


class Job(str):
    def __new__(cls, name, *, prejobs=()):
        self = super().__new__(
            cls,
            f'Job({name!r}, '
            + 'prejobs=' + ', '.join(repr(j.name) for j in prejobs) + ', '
        )
        self.name = str(name)
        self.prejobs = tuple(prejobs)
        return self

@pytest.mark.parametrize(
    argnames='jobs, exp_job_names',
    argvalues=(
        pytest.param(
            [
                Job(name='a'),
                Job(name='b'),
                Job(name='c'),
            ],
            {'a', 'b', 'c'},
            id='No dependencies',
        ),

        pytest.param(
            [
                Job(name='a', prejobs=[
                    Job(name='a.a', prejobs=[Job(name='a.a.a')]),
                    Job(name='a.b', prejobs=[Job(name='a.b.a', prejobs=[
                        Job(name='a.b.a.a'),
                        Job(name='a.b.a.b'),
                    ])]),
                    Job(name='a.c', prejobs=[]),
                ]),
                Job(name='b', prejobs=[]),
                Job(name='c', prejobs=[
                    Job(name='c.a', prejobs=[Job(name='c.a.a')]),
                    Job(name='c.b', prejobs=[Job(name='c.b.a')]),
                ]),

            ],
            {
                'a',
                'a.a', 'a.a.a',
                'a.b', 'a.b.a', 'a.b.a.a', 'a.b.a.b',
                'a.c',

                'b',

                'c',
                'c.a', 'c.a.a',
                'c.b', 'c.b.a',
            },
            id='Dependencies from nested prejobs',
        ),
    ),
)
def test_get_job_and_dependencies(jobs, exp_job_names, make_TestTrackerJobs):
    tracker_jobs = make_TestTrackerJobs()
    required_jobs = tracker_jobs.get_job_and_dependencies(*jobs)
    assert {j.name for j in required_jobs} == exp_job_names


@pytest.mark.parametrize('isolated_jobs', ((), ('mock isolated job',)))
@pytest.mark.parametrize(
    argnames='jobs_before_upload, exp_submission_ok',
    argvalues=(
        ((), False),
        # All jobs exited successfully
        ((Mock(exit_code=0, is_enabled=True), Mock(exit_code=0, is_enabled=True), Mock(exit_code=0, is_enabled=True)), True),

        # Job exited with non-zero exit_code
        ((Mock(exit_code=0, is_enabled=True), Mock(exit_code=0, is_enabled=True), Mock(exit_code=1, is_enabled=True)), False),
        ((Mock(exit_code=0, is_enabled=True), Mock(exit_code=1, is_enabled=True), Mock(exit_code=0, is_enabled=True)), False),
        ((Mock(exit_code=1, is_enabled=True), Mock(exit_code=0, is_enabled=True), Mock(exit_code=0, is_enabled=True)), False),

        # Job is disabled
        ((Mock(exit_code=None, is_enabled=False), Mock(exit_code=0, is_enabled=True), Mock(exit_code=0, is_enabled=True)), True),
        ((Mock(exit_code=0, is_enabled=True), Mock(exit_code=None, is_enabled=False), Mock(exit_code=0, is_enabled=True)), True),
        ((Mock(exit_code=0, is_enabled=True), Mock(exit_code=0, is_enabled=False), Mock(exit_code=None, is_enabled=False)), True),
    ),
)
def test_submission_ok(isolated_jobs, jobs_before_upload, exp_submission_ok, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'jobs_before_upload', PropertyMock(return_value=jobs_before_upload))
    mocker.patch.object(type(tracker_jobs), 'isolated_jobs', PropertyMock(return_value=isolated_jobs))
    if isolated_jobs:
        assert tracker_jobs.submission_ok is False
    else:
        assert tracker_jobs.submission_ok == exp_submission_ok


@pytest.mark.parametrize(
    argnames='job_name, tracker_name, exp_name',
    argvalues=(
        ('myjob', 'mytracker', 'myjob.mytracker'),
        ('myjob.mytracker', 'mytracker', 'myjob.mytracker'),
    ),
)
def test_get_job_name(job_name, tracker_name, exp_name, tracker, make_TestTrackerJobs, mocker):
    tracker.name = tracker_name
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.get_job_name(job_name) == exp_name


@pytest.mark.parametrize(
    argnames='options, exp_prejobs',
    argvalues=(
        ({'announce_url': 'http:/foo'}, ()),
        ({}, ('login_job',)),
    ),
)
def test_create_torrent_job(options, exp_prejobs, make_TestTrackerJobs, tracker, mocker):
    CreateTorrentJob_mock = mocker.patch('upsies.jobs.torrent.CreateTorrentJob')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        reuse_torrent_path='path/to/existing.torrent',
        tracker=tracker,
        exclude_files=('a', 'b', 'c'),
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    mocker.patch.object(tracker_jobs, 'make_precondition')
    mocker.patch.object(type(tracker_jobs), 'options', PropertyMock(return_value=options))
    mocker.patch.object(type(tracker_jobs), 'login_job', PropertyMock())
    assert tracker_jobs.create_torrent_job is CreateTorrentJob_mock.return_value
    assert CreateTorrentJob_mock.call_args_list == [
        call(
            content_path='path/to/content',
            reuse_torrent_path='path/to/existing.torrent',
            tracker=tracker,
            exclude_files=('a', 'b', 'c'),
            precondition=tracker_jobs.make_precondition.return_value,
            prejobs=tuple(getattr(tracker_jobs, job_name) for job_name in exp_prejobs),
            home_directory='path/to/home',
            ignore_cache=False,
        ),
    ]
    assert tracker_jobs.make_precondition.call_args_list == [call('create_torrent_job')]

def test_create_torrent_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.torrent.CreateTorrentJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.create_torrent_job is tracker_jobs.create_torrent_job


@pytest.mark.parametrize(
    argnames='btclient, create_torrent_job, exp_add_torrent_job_is_None',
    argvalues=(
        (Mock(), Mock(), False),
        (None, Mock(), True),
        (Mock(), None, True),
    ),
)
def test_add_torrent_job(btclient, create_torrent_job, exp_add_torrent_job_is_None, make_TestTrackerJobs, mocker):
    AddTorrentJob_mock = mocker.patch('upsies.jobs.torrent.AddTorrentJob')
    tracker_jobs = make_TestTrackerJobs(
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
        btclient=btclient,
    )
    mocker.patch.object(type(tracker_jobs), 'create_torrent_job', PropertyMock(return_value=create_torrent_job))
    mocker.patch.object(tracker_jobs, 'make_precondition')
    if exp_add_torrent_job_is_None:
        assert tracker_jobs.add_torrent_job is None
        assert AddTorrentJob_mock.call_args_list == []
        assert tracker_jobs.make_precondition.call_args_list == []
    else:
        assert tracker_jobs.add_torrent_job is AddTorrentJob_mock.return_value
        assert AddTorrentJob_mock.call_args_list == [
            call(
                autostart=False,
                btclient=btclient,
                precondition=tracker_jobs.make_precondition.return_value,
                home_directory='path/to/home',
                ignore_cache=False,
            ),
        ]
        assert tracker_jobs.make_precondition.call_args_list == [call('add_torrent_job')]

def test_add_torrent_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.torrent.AddTorrentJob', side_effect=(Mock(), Mock()))
    mocker.patch('upsies.jobs.torrent.CreateTorrentJob')
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.add_torrent_job is tracker_jobs.add_torrent_job


@pytest.mark.parametrize(
    argnames='torrent_destination, create_torrent_job, exp_copy_torrent_job_is_None',
    argvalues=(
        ('some/path', Mock(), False),
        (None, Mock(), True),
        ('some/path', None, True),
    ),
)
def test_copy_torrent_job(
        torrent_destination, create_torrent_job, exp_copy_torrent_job_is_None,
        make_TestTrackerJobs, mocker,
):
    CopyTorrentJob_mock = mocker.patch('upsies.jobs.torrent.CopyTorrentJob')
    tracker_jobs = make_TestTrackerJobs(
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
        torrent_destination=torrent_destination,
    )
    mocker.patch.object(type(tracker_jobs), 'create_torrent_job', PropertyMock(return_value=create_torrent_job))
    mocker.patch.object(tracker_jobs, 'make_precondition')
    if exp_copy_torrent_job_is_None:
        assert tracker_jobs.copy_torrent_job is None
        assert CopyTorrentJob_mock.call_args_list == []
        assert tracker_jobs.make_precondition.call_args_list == []
    else:
        assert tracker_jobs.copy_torrent_job is CopyTorrentJob_mock.return_value
        assert CopyTorrentJob_mock.call_args_list == [
            call(
                autostart=False,
                destination=torrent_destination,
                precondition=tracker_jobs.make_precondition.return_value,
                home_directory='path/to/home',
                ignore_cache=False,
            ),
        ]
        assert tracker_jobs.make_precondition.call_args_list == [call('copy_torrent_job')]

def test_copy_torrent_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.torrent.CopyTorrentJob', side_effect=(Mock(), Mock()))
    mocker.patch('upsies.jobs.torrent.CreateTorrentJob')
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.copy_torrent_job is tracker_jobs.copy_torrent_job


def test_torrent_filepath(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'create_torrent_job', PropertyMock(
        return_value='<mock create_torrent_job>',
    ))
    mocker.patch.object(tracker_jobs, 'get_job_output', return_value='path/to/file.torrent')

    assert tracker_jobs.torrent_filepath == 'path/to/file.torrent'
    assert tracker_jobs.get_job_output.call_args_list == [
        call(tracker_jobs.create_torrent_job, slice=0),
    ]


def test_subtitles(make_TestTrackerJobs, mocker):
    get_subtitles_mock = mocker.patch('upsies.utils.mediainfo.text.get_subtitles')
    tracker_jobs = make_TestTrackerJobs()
    for _ in range(3):
        assert tracker_jobs.subtitles is get_subtitles_mock.return_value
    assert get_subtitles_mock.call_args_list == [
        call(tracker_jobs.content_path),
    ]


def test_release_name(make_TestTrackerJobs, mocker):
    ReleaseName_mock = mocker.patch('upsies.utils.release.ReleaseName')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
    )
    assert tracker_jobs.release_name is ReleaseName_mock.return_value
    assert ReleaseName_mock.call_args_list == [
        call(
            path='path/to/content',
            translate=tracker_jobs.release_name_translation,
            separator=tracker_jobs.release_name_separator,
            english_title_before_original=tracker_jobs.release_name_english_title_before_original,
        ),
    ]


def test_release_name_english_title_before_original(make_TestTrackerJobs):
    assert make_TestTrackerJobs().release_name_english_title_before_original is False


def test_release_name_job(make_TestTrackerJobs, mocker):
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )

    mocker.patch.object(type(tracker_jobs), 'release_name', PropertyMock())
    mocker.patch.object(tracker_jobs, 'get_job_name')
    mocker.patch.object(tracker_jobs, 'make_precondition')

    release_name_job = tracker_jobs.release_name_job
    assert release_name_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [
        call(
            name=tracker_jobs.get_job_name.return_value,
            label='Release Name',
            callbacks={
                'output': tracker_jobs.release_name.set_release_info,
            },
            precondition=tracker_jobs.make_precondition.return_value,
            validator=tracker_jobs.validate_release_name,
            home_directory='path/to/home',
            ignore_cache=False,
        ),
    ]
    assert tracker_jobs.make_precondition.call_args_list == [call('release_name_job')]

def test_release_name_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.dialog.TextFieldJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'release_name', PropertyMock())
    assert tracker_jobs.release_name_job is tracker_jobs.release_name_job


@pytest.mark.parametrize(
    argnames='text, exp_exception',
    argvalues=(
        ('', ValueError('Release name must not be empty.')),
        (' ', ValueError('Release name must not be empty.')),
        ('Foo 2012 UNKNOWN_YEAR BluRay-ASDF', ValueError('Replace "UNKNOWN_YEAR" with the proper year.')),
        ('Foo 2012 BluRay UNKNOWN_AUDIO_FORMAT-ASDF', ValueError('Replace "UNKNOWN_AUDIO_FORMAT" with the proper audio format.')),
        ('Foo.2012.UNKNOWN_RESOLUTION.BluRay-ASDF', ValueError('Replace "UNKNOWN_RESOLUTION" with the proper resolution.')),
        ('Foo 2012 BluRay-ASDF', None),
    ),
    ids=lambda v: repr(v),
)
def test_validate_release_name(text, exp_exception, make_TestTrackerJobs):
    jobs = make_TestTrackerJobs()

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            jobs.validate_release_name(text)
    else:
        assert jobs.validate_release_name(text) is None


@pytest.mark.parametrize('release_name_job_is_finished', (False, True))
@pytest.mark.asyncio
async def test_update_release_name_from(release_name_job_is_finished, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'release_name', PropertyMock(return_value=Mock(
        fetch_info=Mock(),
    )))
    mocker.patch.object(type(tracker_jobs), 'release_name_job', PropertyMock(return_value=Mock(
        is_finished=release_name_job_is_finished,
        fetch_text=AsyncMock(),
    )))
    mocks = Mock()
    mocks.attach_mock(tracker_jobs.release_name.fetch_info, 'fetch_info')
    mocks.attach_mock(tracker_jobs.release_name_job.fetch_text, 'fetch_text')

    webdb = Mock()
    await tracker_jobs.update_release_name_from(webdb, 'tt123456')
    if release_name_job_is_finished:
        assert mocks.mock_calls == []
    else:
        assert mocks.mock_calls == [
            call.fetch_info(webdb=webdb, webdb_id='tt123456'),
            call.fetch_text(
                coro=tracker_jobs.release_name.fetch_info.return_value,
                warn_exceptions=(errors.RequestError,),
                default=str(tracker_jobs.release_name),
            ),
        ]


def test_imdb_job(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    WebDbSearchJob_mock = mocker.patch('upsies.jobs.webdb.WebDbSearchJob')
    mocker.patch.object(tracker_jobs, 'make_precondition')
    imdb_job = tracker_jobs.imdb_job
    assert imdb_job is WebDbSearchJob_mock.return_value
    assert WebDbSearchJob_mock.call_args_list == [
        call(
            query='path/to/content',
            db=tracker_jobs.imdb,
            autodetect=tracker_jobs.autodetect_imdb_id,
            show_poster=tracker_jobs._show_poster,
            callbacks={
                'output': tracker_jobs._handle_imdb_id,
            },
            precondition=tracker_jobs.make_precondition.return_value,
            home_directory='path/to/home',
            ignore_cache=False,
        ),
    ]
    assert tracker_jobs.make_precondition.call_args_list == [call('imdb_job')]

def test_imdb_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.webdb.WebDbSearchJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.imdb_job is tracker_jobs.imdb_job


@pytest.mark.parametrize(
    argnames='options, lookup_result, exp_autodetected_id',
    argvalues=(
        ({}, '', None),
        ({'imdb': ''}, None, None),
        ({'imdb': 'tt123456'}, None, 'tt123456'),
        ({'imdb': 'tt123456'}, 'tt654321', 'tt123456'),
        ({}, 'tt654321', 'tt654321'),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_autodetect_imdb_id(options, lookup_result, exp_autodetected_id, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'options', PropertyMock(return_value=options))
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.utils.mediainfo.lookup', return_value=lookup_result),
        'lookup',
    )

    autodetected_id = await tracker_jobs.autodetect_imdb_id()
    assert autodetected_id == exp_autodetected_id

    exp_mock_calls = []
    if not options.get('imdb'):
        exp_mock_calls.append(call.lookup(
            path=tracker_jobs.content_path,
            keys=('General', 0, 'extra', 'IMDB'),
            default=None,
        ))
    assert mocks.mock_calls == exp_mock_calls


def test_handle_imdb_id(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    imdb_id = 'tt123456'
    imdb = Mock()

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tracker_jobs.tracker, 'attach_task'), 'attach_task')
    mocks.attach_mock(mocker.patch.object(tracker_jobs, '_propagate_webdb_info', Mock()), '_propagate_webdb_info')
    mocker.patch.object(type(tracker_jobs), 'imdb', PropertyMock(return_value=imdb))

    tracker_jobs._handle_imdb_id(imdb_id)
    assert mocks.mock_calls == [
        call._propagate_webdb_info(tracker_jobs.imdb, imdb_id),
        call.attach_task(tracker_jobs._propagate_webdb_info.return_value),
    ]


@pytest.mark.parametrize('selected, exp_id', (({}, None), ({'id': '123'}, '123')))
def test_imdb_id_property(selected, exp_id, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'imdb_job', PropertyMock(return_value=Mock(selected=selected)))
    return_value = tracker_jobs.imdb_id
    assert return_value == exp_id


def test_tmdb_job(make_TestTrackerJobs, mocker):
    WebDbSearchJob_mock = mocker.patch('upsies.jobs.webdb.WebDbSearchJob')
    webdb_mock = mocker.patch('upsies.utils.webdbs.webdb')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    mocker.patch.object(tracker_jobs, 'make_precondition')
    assert tracker_jobs.tmdb_job is WebDbSearchJob_mock.return_value
    assert WebDbSearchJob_mock.call_args_list == [
        call(
            query='path/to/content',
            db=webdb_mock.return_value,
            autodetect=tracker_jobs.autodetect_tmdb_id,
            show_poster=tracker_jobs._show_poster,
            callbacks={
                'output': tracker_jobs._handle_tmdb_id,
            },
            precondition=tracker_jobs.make_precondition.return_value,
            home_directory='path/to/home',
            ignore_cache=False,
        ),
    ]
    assert webdb_mock.call_args_list == [call('tmdb')]
    assert tracker_jobs.make_precondition.call_args_list == [call('tmdb_job')]


def test_tmdb_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.webdb.WebDbSearchJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.tmdb_job is tracker_jobs.tmdb_job


@pytest.mark.parametrize(
    argnames='options, lookup_result, exp_autodetected_id',
    argvalues=(
        ({}, '', None),
        ({'tmdb': ''}, None, None),
        ({'tmdb': 'movie/123456'}, None, 'movie/123456'),
        ({'tmdb': 'movie/123456'}, 'movie/654321', 'movie/123456'),
        ({}, 'movie/654321', 'movie/654321'),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_autodetect_tmdb_id(options, lookup_result, exp_autodetected_id, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'options', PropertyMock(return_value=options))
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.utils.mediainfo.lookup', return_value=lookup_result),
        'lookup',
    )

    autodetected_id = await tracker_jobs.autodetect_tmdb_id()
    assert autodetected_id == exp_autodetected_id

    exp_mock_calls = []
    if not options.get('tmdb'):
        exp_mock_calls.append(call.lookup(
            path=tracker_jobs.content_path,
            keys=('General', 0, 'extra', 'TMDB'),
            default=None,
        ))
    assert mocks.mock_calls == exp_mock_calls


def test_handle_tmdb_id(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    tmdb_id = 'movie/123456'
    tmdb = Mock()

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tracker_jobs.tracker, 'attach_task'), 'attach_task')
    mocks.attach_mock(mocker.patch.object(tracker_jobs, '_propagate_webdb_info', Mock()), '_propagate_webdb_info')
    mocker.patch.object(type(tracker_jobs), 'tmdb', PropertyMock(return_value=tmdb))

    tracker_jobs._handle_tmdb_id(tmdb_id)
    assert mocks.mock_calls == [
        # call._propagate_webdb_info(tracker_jobs.tmdb, tmdb_id),
        # call.attach_task(tracker_jobs._propagate_webdb_info.return_value),
    ]


@pytest.mark.parametrize('selected, exp_id', (({}, None), ({'id': '123'}, '123')))
def test_tmdb_id_property(selected, exp_id, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'tmdb_job', PropertyMock(return_value=Mock(selected=selected)))
    return_value = tracker_jobs.tmdb_id
    assert return_value == exp_id


def test_tvmaze_job(make_TestTrackerJobs, mocker):
    WebDbSearchJob_mock = mocker.patch('upsies.jobs.webdb.WebDbSearchJob')
    webdb_mock = mocker.patch('upsies.utils.webdbs.webdb')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    mocker.patch.object(tracker_jobs, 'make_precondition')
    assert tracker_jobs.tvmaze_job is WebDbSearchJob_mock.return_value
    assert WebDbSearchJob_mock.call_args_list == [
        call(
            query='path/to/content',
            db=webdb_mock.return_value,
            autodetect=tracker_jobs.autodetect_tvmaze_id,
            show_poster=tracker_jobs._show_poster,
            callbacks={
                'output': tracker_jobs._handle_tvmaze_id,
            },
            precondition=tracker_jobs.make_precondition.return_value,
            home_directory='path/to/home',
            ignore_cache=False,
        ),
    ]
    assert webdb_mock.call_args_list == [call('tvmaze')]
    assert tracker_jobs.make_precondition.call_args_list == [call('tvmaze_job')]

def test_tvmaze_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.webdb.WebDbSearchJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.tvmaze_job is tracker_jobs.tvmaze_job


@pytest.mark.parametrize(
    argnames='options, lookup_result, exp_autodetected_id',
    argvalues=(
        ({}, '', None),
        ({'tvmaze': ''}, None, None),
        ({'tvmaze': '123456'}, None, '123456'),
        ({'tvmaze': '123456'}, '654321', '123456'),
        ({}, '654321', '654321'),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_autodetect_tvmaze_id(options, lookup_result, exp_autodetected_id, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'options', PropertyMock(return_value=options))
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.utils.mediainfo.lookup', return_value=lookup_result),
        'lookup',
    )

    autodetected_id = await tracker_jobs.autodetect_tvmaze_id()
    assert autodetected_id == exp_autodetected_id

    exp_mock_calls = []
    if not options.get('tvmaze'):
        exp_mock_calls.append(call.lookup(
            path=tracker_jobs.content_path,
            keys=('General', 0, 'extra', 'TVMAZE'),
            default=None,
        ))
    assert mocks.mock_calls == exp_mock_calls


def test_handle_tvmaze_id(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    tvmaze_id = '123456'
    tvmaze = Mock()

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tracker_jobs.tracker, 'attach_task'), 'attach_task')
    mocks.attach_mock(mocker.patch.object(tracker_jobs, '_propagate_webdb_info', Mock()), '_propagate_webdb_info')
    mocker.patch.object(type(tracker_jobs), 'tvmaze', PropertyMock(return_value=tvmaze))

    tracker_jobs._handle_tvmaze_id(tvmaze_id)
    assert mocks.mock_calls == [
        call._propagate_webdb_info(tracker_jobs.tvmaze, tvmaze_id),
        call.attach_task(tracker_jobs._propagate_webdb_info.return_value),
    ]


@pytest.mark.parametrize('selected, exp_id', (({}, None), ({'id': '123'}, '123')))
def test_tvmaze_id_property(selected, exp_id, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'tvmaze_job', PropertyMock(return_value=Mock(selected=selected)))
    return_value = tracker_jobs.tvmaze_id
    assert return_value == exp_id


@pytest.mark.parametrize('target_webdb_jobs_exist', (True, False))
@pytest.mark.parametrize('english_title', ('', 'English Title'))
@pytest.mark.parametrize('origin_job_is_finished', (False, True))
@pytest.mark.asyncio
async def test__propagate_webdb_info(origin_job_is_finished, english_title, target_webdb_jobs_exist, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()

    mocks = Mock(
        update_release_name_from=AsyncMock(),
    )
    mocker.patch.object(tracker_jobs, 'update_release_name_from', mocks.update_release_name_from)
    mocker.patch('upsies.utils.webdbs.webdb_names', mocks.webdb_names)
    mocks.webdb_names.return_value = ('adb', 'bdb', 'cdb', 'ddb')

    mocker.patch('upsies.utils.webdbs.Query', mocks.Query)

    def make_db(name, type, title_english, title_original, year):
        return types.SimpleNamespace(
            name=name,
            type=AsyncMock(return_value=type),
            title_english=AsyncMock(return_value=title_english),
            title_original=AsyncMock(return_value=title_original),
            year=AsyncMock(return_value=year),
        )

    adb = make_db('adb', 'type from adb', english_title, 'original title from adb', 'year from adb')
    bdb = make_db('bdb', 'type from bdb', 'english title from bdb', 'original title from bdb', 'year from bdb')
    cdb = make_db('cdb', 'type from cdb', 'english title from cdb', 'original title from cdb', 'year from cdb')
    ddb = make_db('ddb', 'type from ddb', 'english title from ddb', 'original title from ddb', 'year from ddb')
    mocker.patch.object(type(tracker_jobs), 'adb', PropertyMock(return_value=adb), create=True)
    mocker.patch.object(type(tracker_jobs), 'bdb', PropertyMock(return_value=bdb), create=True)
    mocker.patch.object(type(tracker_jobs), 'cdb', PropertyMock(return_value=cdb), create=True)
    mocker.patch.object(type(tracker_jobs), 'ddb', PropertyMock(return_value=ddb), create=True)

    def make_db_job(name, *, search, is_enabled, is_finished):
        return types.SimpleNamespace(
            name=name,
            is_enabled=is_enabled,
            is_finished=is_finished,
            search=search,
            query=getattr(mocks, name[:name.index('-')]).query,
        )

    adb_job = make_db_job('adb-id', search=mocks.adb.search, is_enabled=True and target_webdb_jobs_exist, is_finished=origin_job_is_finished)
    bdb_job = make_db_job('bdb-id', search=mocks.bdb.search, is_enabled=False, is_finished=True)
    cdb_job = make_db_job('cdb-id', search=mocks.cdb.search, is_enabled=True and target_webdb_jobs_exist, is_finished=False)
    ddb_job = make_db_job('ddb-id', search=mocks.ddb.search, is_enabled=True and target_webdb_jobs_exist, is_finished=True)
    mocker.patch.object(type(tracker_jobs), 'adb_job', PropertyMock(return_value=adb_job), create=True)
    mocker.patch.object(type(tracker_jobs), 'bdb_job', PropertyMock(return_value=bdb_job), create=True)
    mocker.patch.object(type(tracker_jobs), 'cdb_job', PropertyMock(return_value=cdb_job), create=True)
    mocker.patch.object(type(tracker_jobs), 'ddb_job', PropertyMock(return_value=ddb_job), create=True)

    await tracker_jobs._propagate_webdb_info(tracker_jobs.adb, '123456')

    exp_title = english_title or 'original title from adb'
    if target_webdb_jobs_exist:
        assert mocks.mock_calls == [
            call.webdb_names(),
            call.Query(type='type from adb', title=exp_title, year='year from adb'),
            call.cdb.query.update(mocks.Query.return_value),
            call.update_release_name_from(tracker_jobs.adb, '123456'),
        ]
    else:
        assert mocks.mock_calls == [
            call.webdb_names(),
            call.update_release_name_from(tracker_jobs.adb, '123456'),
        ]


def test_screenshots_job(make_TestTrackerJobs, mocker):
    ScreenshotsJob_mock = mocker.patch('upsies.jobs.screenshots.ScreenshotsJob')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    mocker.patch.object(tracker_jobs, 'make_precondition')
    mocker.patch.object(type(tracker_jobs), 'screenshots_precreated', PropertyMock())
    mocker.patch.object(type(tracker_jobs), 'screenshots_count', PropertyMock())
    mocker.patch.object(type(tracker_jobs), 'document_all_videos', PropertyMock())
    mocker.patch.object(type(tracker_jobs), 'create_torrent_job', PropertyMock())
    assert tracker_jobs.screenshots_job is ScreenshotsJob_mock.return_value
    assert ScreenshotsJob_mock.call_args_list == [
        call(
            content_path='path/to/content',
            precreated=tracker_jobs.screenshots_precreated,
            exclude_files=tracker_jobs.exclude_files,
            count=tracker_jobs.screenshots_count,
            from_all_videos=tracker_jobs.document_all_videos,
            optimize=tracker_jobs._screenshots_optimization,
            precondition=tracker_jobs.make_precondition.return_value,
            home_directory='path/to/home',
            ignore_cache=False,
        ),
    ]
    assert tracker_jobs.make_precondition.call_args_list == [call('screenshots_job')]

def test_screenshots_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.screenshots.ScreenshotsJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'create_torrent_job', PropertyMock())
    assert tracker_jobs.screenshots_job is tracker_jobs.screenshots_job


@pytest.mark.parametrize(
    argnames='screenshots, exp_screenshots_precreated',
    argvalues=(
        (None, []),
        (('custom1.png', ('custom2.png', ['custom3.png'])), ['custom1.png', 'custom2.png', 'custom3.png']),
    ),
    ids=lambda v: repr(v),
)
def test_screenshots_precreated(screenshots, exp_screenshots_precreated, make_TestTrackerJobs, mocker):
    if screenshots is not None:
        options = {'screenshots': screenshots}
    else:
        options = {}
    tracker_jobs = make_TestTrackerJobs(options=options)
    assert tracker_jobs.screenshots_precreated == exp_screenshots_precreated


@pytest.mark.parametrize(
    argnames='screenshots_count, exp_screenshots_count',
    argvalues=(
        (None, None),
        (123, 123),
    ),
)
def test_screenshots_count(screenshots_count, exp_screenshots_count, make_TestTrackerJobs, mocker):
    if screenshots_count is not None:
        options = {'screenshots_count': screenshots_count}
    else:
        options = {}
    tracker_jobs = make_TestTrackerJobs(options=options)
    assert tracker_jobs.screenshots_count == exp_screenshots_count


def test_document_all_videos(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.document_all_videos is False


def test_image_host_config():
    assert TrackerJobsBase.image_host_config == {}


@pytest.mark.parametrize(
    argnames='image_hosts, screenshots_job, exp_upload_screenshots_job_is_None',
    argvalues=(
        (Mock(), Mock(), False),
        (None, Mock(), True),
        (Mock(), None, True),
    ),
)
def test_upload_screenshots_job(
        image_hosts, screenshots_job, exp_upload_screenshots_job_is_None,
        make_TestTrackerJobs, mocker,
):
    ImageHostJob_mock = mocker.patch('upsies.jobs.imghost.ImageHostJob')
    mocker.patch('upsies.jobs.screenshots.ScreenshotsJob')
    tracker_jobs = make_TestTrackerJobs(
        image_hosts=image_hosts,
        content_path='path/to/content',
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    mocker.patch.object(type(tracker_jobs), 'screenshots_job', PropertyMock(return_value=screenshots_job))
    mocker.patch.object(tracker_jobs, 'make_precondition')
    if exp_upload_screenshots_job_is_None:
        assert tracker_jobs.upload_screenshots_job is None
        assert ImageHostJob_mock.call_args_list == []
        assert tracker_jobs.make_precondition.call_args_list == []
    else:
        assert tracker_jobs.upload_screenshots_job is ImageHostJob_mock.return_value
        assert ImageHostJob_mock.call_args_list == [call(
            imghosts=image_hosts,
            precondition=tracker_jobs.make_precondition.return_value,
            home_directory='path/to/home',
            ignore_cache=False,
        )]
        assert tracker_jobs.make_precondition.call_args_list == [call('upload_screenshots_job')]

def test_upload_screenshots_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.imghost.ImageHostJob', side_effect=(Mock(), Mock()))
    mocker.patch('upsies.jobs.screenshots.ScreenshotsJob')
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.upload_screenshots_job is tracker_jobs.upload_screenshots_job


def test_poster_job(make_TestTrackerJobs, mocker):
    PosterJob_mock = mocker.patch('upsies.jobs.poster.PosterJob')
    tracker_jobs = make_TestTrackerJobs(
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    mocker.patch.object(tracker_jobs, 'make_poster_job_precondition')
    assert tracker_jobs.poster_job is PosterJob_mock.return_value
    assert PosterJob_mock.call_args_list == [
        call(
            precondition=tracker_jobs.make_poster_job_precondition.return_value,
            home_directory='path/to/home',
            ignore_cache=False,
            getter=tracker_jobs.get_poster,
            width=tracker_jobs.poster_max_width,
            height=tracker_jobs.poster_max_height,
            write_to=None,
            imghosts=tracker_jobs.image_hosts,
        ),
    ]
    assert tracker_jobs.make_poster_job_precondition.call_args_list == [call()]

def test_poster_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.poster.PosterJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.poster_job is tracker_jobs.poster_job


def test_make_poster_job_precondition(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(tracker_jobs, 'make_precondition')
    precondition = tracker_jobs.make_poster_job_precondition()
    assert precondition is tracker_jobs.make_precondition.return_value
    assert tracker_jobs.make_precondition.call_args_list == [call('poster_job')]


@pytest.mark.parametrize(
    argnames=(
        'get_poster_from_user, get_poster_from_tracker, get_poster_from_webdb,'
        'exp_return_value, exp_mock_calls'
    ),
    argvalues=(
        pytest.param(
            AsyncMock(return_value='http://poster_from_user.jpg'),
            AsyncMock(return_value='http://poster_from_tracker.jpg'),
            AsyncMock(return_value='http://poster_from_webdb.jpg'),
            'http://poster_from_user.jpg',
            [
                call.get_poster_from_user(),
            ],
            id='Poster from user',
        ),
        pytest.param(
            AsyncMock(return_value=None),
            AsyncMock(return_value='http://poster_from_tracker.jpg'),
            AsyncMock(return_value='http://poster_from_webdb.jpg'),
            'http://poster_from_tracker.jpg',
            [
                call.get_poster_from_user(),
                call.get_poster_from_tracker(),
            ],
            id='Poster from tracker',
        ),
        pytest.param(
            AsyncMock(return_value=None),
            AsyncMock(return_value=None),
            AsyncMock(return_value='http://poster_from_webdb.jpg'),
            'http://poster_from_webdb.jpg',
            [
                call.get_poster_from_user(),
                call.get_poster_from_tracker(),
                call.get_poster_from_webdb(),
            ],
            id='Poster from WebDB',
        ),
        pytest.param(
            AsyncMock(return_value=None),
            AsyncMock(return_value=None),
            AsyncMock(return_value=None),
            None,
            [
                call.get_poster_from_user(),
                call.get_poster_from_tracker(),
                call.get_poster_from_webdb(),
            ],
            id='No poster found',
        ),
    ),
)
@pytest.mark.asyncio
async def test_get_poster(
        get_poster_from_user, get_poster_from_tracker, get_poster_from_webdb,
        exp_return_value, exp_mock_calls,
        make_TestTrackerJobs, mocker,
):
    tracker_jobs = make_TestTrackerJobs()

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(tracker_jobs, 'get_poster_from_user', get_poster_from_user),
        'get_poster_from_user',
    )
    mocks.attach_mock(
        mocker.patch.object(tracker_jobs, 'get_poster_from_tracker', get_poster_from_tracker),
        'get_poster_from_tracker',
    )
    mocks.attach_mock(
        mocker.patch.object(tracker_jobs, 'get_poster_from_webdb', get_poster_from_webdb),
        'get_poster_from_webdb',
    )

    return_value = await tracker_jobs.get_poster()
    assert return_value == exp_return_value
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='options, exp_return_value',
    argvalues=(
        pytest.param(
            {'poster': 'http://host.local/poster.jpg'},
            'http://host.local/poster.jpg',
            id='User provided poster',
        ),
        pytest.param(
            {},
            None,
            id='User provided no poster',
        ),
    ),
)
@pytest.mark.asyncio
async def test_get_poster_from_user(options, exp_return_value, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'options', PropertyMock(return_value=options))
    return_value = await tracker_jobs.get_poster_from_user()
    assert return_value == exp_return_value


@pytest.mark.asyncio
async def test_get_poster_from_tracker(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    return_value = await tracker_jobs.get_poster_from_tracker()
    assert return_value is None


@pytest.fixture
def mocks_for_get_poster_from_webdb(make_TestTrackerJobs, mocker):
    jobs = make_TestTrackerJobs()
    mocks = Mock()
    mocks.poster_webdb_job.wait_finished = AsyncMock()
    mocks.poster_webdb.poster_url = AsyncMock()
    mocker.patch.object(type(jobs), 'poster_webdb_job', PropertyMock(return_value=mocks.poster_webdb_job))
    mocker.patch.object(type(jobs), 'poster_webdb', PropertyMock(return_value=mocks.poster_webdb))
    mocker.patch.object(type(jobs), 'release_name', PropertyMock(return_value=mocks.release_name))
    mocks.attach_mock(mocker.patch.object(jobs, 'get_job_output', return_value='mock output'), 'get_job_output')
    return jobs, mocks

@pytest.mark.asyncio
async def test_get_poster_from_webdb__no_poster_webdb_job(mocks_for_get_poster_from_webdb, mocker):
    tracker_jobs, mocks = mocks_for_get_poster_from_webdb
    mocker.patch.object(type(tracker_jobs), 'poster_webdb_job', PropertyMock(return_value=None))
    return_value = await tracker_jobs.get_poster_from_webdb()
    assert return_value is None
    assert mocks.mock_calls == []

@pytest.mark.asyncio
async def test_get_poster_from_webdb__no_poster_webdb_id(mocks_for_get_poster_from_webdb, mocker):
    tracker_jobs, mocks = mocks_for_get_poster_from_webdb
    mocks.get_job_output.return_value = None
    return_value = await tracker_jobs.get_poster_from_webdb()
    assert return_value is None
    assert mocks.mock_calls == [
        call.poster_webdb_job.wait_finished(),
        call.get_job_output(tracker_jobs.poster_webdb_job, slice=0, default=None),
    ]

@pytest.mark.asyncio
async def test_get_poster_from_webdb__webdb_cannot_find_poster(mocks_for_get_poster_from_webdb):
    tracker_jobs, mocks = mocks_for_get_poster_from_webdb
    mocks.poster_webdb.poster_url.return_value = ''
    return_value = await tracker_jobs.get_poster_from_webdb()
    assert return_value is None
    assert mocks.mock_calls == [
        call.poster_webdb_job.wait_finished(),
        call.get_job_output(tracker_jobs.poster_webdb_job, slice=0, default=None),
        call.poster_webdb.poster_url(
            tracker_jobs.get_job_output.return_value,
            season=tracker_jobs.release_name.only_season,
        ),
    ]

@pytest.mark.asyncio
async def test_get_poster_from_webdb__returns_poster(mocks_for_get_poster_from_webdb):
    tracker_jobs, mocks = mocks_for_get_poster_from_webdb
    mocks.poster_webdb.poster_url.return_value = 'http://webdb.local/poster.jpg'
    return_value = await tracker_jobs.get_poster_from_webdb()
    assert return_value == 'http://webdb.local/poster.jpg'
    assert mocks.mock_calls == [
        call.poster_webdb_job.wait_finished(),
        call.get_job_output(tracker_jobs.poster_webdb_job, slice=0, default=None),
        call.poster_webdb.poster_url(
            tracker_jobs.get_job_output.return_value,
            season=tracker_jobs.release_name.only_season,
        ),
    ]

@pytest.mark.asyncio
async def test_get_poster_from_webdb__ignores_RequestError(mocks_for_get_poster_from_webdb):
    tracker_jobs, mocks = mocks_for_get_poster_from_webdb
    mocks.poster_webdb.poster_url.side_effect = errors.RequestError('site is down')
    return_value = await tracker_jobs.get_poster_from_webdb()
    assert return_value is None
    assert mocks.mock_calls == [
        call.poster_webdb_job.wait_finished(),
        call.get_job_output(tracker_jobs.poster_webdb_job, slice=0, default=None),
        call.poster_webdb.poster_url(
            tracker_jobs.get_job_output.return_value,
            season=tracker_jobs.release_name.only_season,
        ),
    ]


def test_poster_webdb_job(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), '_poster_webdb_and_job', PropertyMock(
        return_value=('poster_webdb', 'poster_webdb_job'),
    ))
    assert tracker_jobs.poster_webdb_job == 'poster_webdb_job'


def test_poster_webdb(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), '_poster_webdb_and_job', PropertyMock(
        return_value=('poster_webdb', 'poster_webdb_job'),
    ))
    assert tracker_jobs.poster_webdb == 'poster_webdb'


@pytest.mark.parametrize(
    argnames='is_enabled, is_started, in_jobs_before_upload',
    argvalues=(
        (False, False, False),

        (False, False, True),
        (False, True, False),
        (True, False, False),

        (False, True, True),
        # (True, False, True),  # poster_job is enabled
        (True, True, False),

        (True, True, True),
    ),
)
def test__poster_webdb_and_job__when_poster_job_is_disabled(
        is_enabled, is_started, in_jobs_before_upload,
        make_TestTrackerJobs, mocker,
):
    tracker_jobs = make_TestTrackerJobs()

    mocker.patch.object(type(tracker_jobs), 'poster_job', PropertyMock(return_value=Mock(
        is_enabled=is_enabled,
        is_started=is_started,
    )))
    if in_jobs_before_upload:
        jobs_before_upload = ('foo', tracker_jobs.poster_job, 'bar')
    else:
        jobs_before_upload = ('foo', 'bar')
    mocker.patch.object(type(tracker_jobs), 'jobs_before_upload', PropertyMock(return_value=jobs_before_upload))

    assert tracker_jobs._poster_webdb_and_job == (None, None)

@pytest.mark.parametrize(
    argnames=(
        'tvmaze_is_enabled, tvmaze_in_jobs_before_upload, release_type, '
        'imdb_is_enabled, imdb_in_jobs_before_upload, '
        'tmdb_is_enabled, tmdb_in_jobs_before_upload, '
        'exp_result'
    ),
    argvalues=(
        pytest.param(
            True, True, utils.types.ReleaseType.season,  # TVmaze
            True, True,  # IMDb
            True, True,  # TMDb
            ('tvmaze', 'tvmaze_job'),
            id='tvmaze for season',
        ),
        pytest.param(
            True, True, utils.types.ReleaseType.episode,  # TVmaze
            True, True,  # IMDb
            True, True,  # TMDb
            ('tvmaze', 'tvmaze_job'),
            id='tvmaze for episode',
        ),
        pytest.param(
            True, True, utils.types.ReleaseType.movie,  # TVmaze
            True, True,  # IMDb
            True, True,  # TMDb
            ('imdb', 'imdb_job'),
            id='imdb for movie',
        ),
        pytest.param(
            True, False, utils.types.ReleaseType.series,  # TVmaze
            True, True,  # IMDb
            True, True,  # TMDb
            ('imdb', 'imdb_job'),
            id='imdb if tvmaze is not used',
        ),
        pytest.param(
            False, True, utils.types.ReleaseType.series,  # TVmaze
            True, True,  # IMDb
            True, True,  # TMDb
            ('imdb', 'imdb_job'),
            id='imdb if tvmaze is not enabled',
        ),
        pytest.param(
            False, False, utils.types.ReleaseType.movie,  # TVmaze
            True, False,  # IMDb
            True, True,  # TMDb
            ('tmdb', 'tmdb_job'),
            id='tmdb if imdb is not used',
        ),
        pytest.param(
            False, False, utils.types.ReleaseType.movie,  # TVmaze
            False, True,  # IMDb
            True, True,  # TMDb
            ('tmdb', 'tmdb_job'),
            id='tmdb if imdb is not enabled',
        ),
        pytest.param(
            True, False, utils.types.ReleaseType.movie,  # TVmaze
            True, False,  # IMDb
            True, False,  # TMDb
            (None, None),
            id='error if no webdb job is used',
        ),
        pytest.param(
            False, True, utils.types.ReleaseType.movie,  # TVmaze
            False, True,  # IMDb
            False, True,  # TMDb
            (None, None),
            id='error if no webdb job is enabled',
        ),
    ),
)
def test__poster_webdb_and_job_returns(
        tvmaze_is_enabled, tvmaze_in_jobs_before_upload, release_type,
        imdb_is_enabled, imdb_in_jobs_before_upload,
        tmdb_is_enabled, tmdb_in_jobs_before_upload,
        exp_result,
        make_TestTrackerJobs, mocker,
):
    tracker_jobs = make_TestTrackerJobs()

    mocker.patch.object(type(tracker_jobs), 'poster_job', PropertyMock(return_value=Mock(
        is_enabled=True,
        is_started=False,
    )))
    mocker.patch.object(type(tracker_jobs), 'imdb_job', PropertyMock(return_value=Mock(is_enabled=imdb_is_enabled)))
    mocker.patch.object(type(tracker_jobs), 'imdb', PropertyMock())
    mocker.patch.object(type(tracker_jobs), 'tmdb_job', PropertyMock(return_value=Mock(is_enabled=tmdb_is_enabled)))
    mocker.patch.object(type(tracker_jobs), 'tmdb', PropertyMock())
    mocker.patch.object(type(tracker_jobs), 'tvmaze_job', PropertyMock(return_value=Mock(is_enabled=tvmaze_is_enabled)))
    mocker.patch.object(type(tracker_jobs), 'tvmaze', PropertyMock())
    mocker.patch.object(type(tracker_jobs.release_name), 'type', PropertyMock(return_value=release_type))

    jobs_before_upload = ['foo', tracker_jobs.poster_job, 'bar']
    if imdb_in_jobs_before_upload:
        jobs_before_upload.insert(1, tracker_jobs.imdb_job)
    if tmdb_in_jobs_before_upload:
        jobs_before_upload.insert(1, tracker_jobs.tmdb_job)
    if tvmaze_in_jobs_before_upload:
        jobs_before_upload.insert(1, tracker_jobs.tvmaze_job)
    print(jobs_before_upload)
    mocker.patch.object(type(tracker_jobs), 'jobs_before_upload', PropertyMock(return_value=jobs_before_upload))

    assert tracker_jobs._poster_webdb_and_job == tuple(
        (
            getattr(tracker_jobs, name)
            if name else
            None
        )
        for name in exp_result
    )


def test_playlists_job(make_TestTrackerJobs, mocker):
    PlaylistsJob_mock = mocker.patch('upsies.jobs.playlists.PlaylistsJob')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    mocker.patch.object(tracker_jobs, 'make_precondition')
    assert tracker_jobs.playlists_job is PlaylistsJob_mock.return_value
    assert PlaylistsJob_mock.call_args_list == [
        call(
            content_path='path/to/content',
            select_multiple=tracker_jobs.document_all_videos,
            precondition=tracker_jobs.make_precondition.return_value,
            home_directory='path/to/home',
            ignore_cache=False,
        ),
    ]
    assert tracker_jobs.make_precondition.call_args_list == [call('playlists_job', precondition=tracker_jobs.playlists_job_precondition)]

def test_playlists_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.playlists.PlaylistsJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.playlists_job is tracker_jobs.playlists_job

def test_playlists_job_precondition(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    is_disc_mock = mocker.patch('upsies.utils.disc.is_disc')
    assert tracker_jobs.playlists_job_precondition() is is_disc_mock.return_value
    assert is_disc_mock.call_args_list == [call(tracker_jobs.content_path, multidisc=True)]


def test_mediainfo_job(make_TestTrackerJobs, mocker):
    MediainfoJob_mock = mocker.patch('upsies.jobs.mediainfo.MediainfoJob')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    mocker.patch.object(type(tracker_jobs), 'document_all_videos', PropertyMock(
        return_value='maybe document_all_videos?',
    ))
    mocker.patch.object(type(tracker_jobs), 'exclude_files', PropertyMock(
        return_value='exclude these files!',
    ))
    mocker.patch.object(tracker_jobs, 'make_precondition')
    assert tracker_jobs.mediainfo_job is MediainfoJob_mock.return_value
    assert MediainfoJob_mock.call_args_list == [
        call(
            content_path='path/to/content',
            from_all_videos='maybe document_all_videos?',
            exclude_files='exclude these files!',
            precondition=tracker_jobs.make_precondition.return_value,
            home_directory='path/to/home',
            ignore_cache=False,
        ),
    ]
    assert tracker_jobs.make_precondition.call_args_list == [call('mediainfo_job', precondition=tracker_jobs.mediainfo_job_precondition)]

def test_mediainfo_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.mediainfo.MediainfoJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.mediainfo_job is tracker_jobs.mediainfo_job

@pytest.mark.parametrize(
    argnames='is_bdmv_release, mediainfo_required_for_bdmv, exp_return_value',
    argvalues=(
        (False, False, True),
        (False, True, True),
        (True, False, False),
        (True, True, True),
    ),
    ids=lambda v: repr(v),
)
def test_mediainfo_job_precondition(is_bdmv_release, mediainfo_required_for_bdmv, exp_return_value, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'is_bdmv_release', PropertyMock(return_value=is_bdmv_release))
    mocker.patch.object(type(tracker_jobs), 'mediainfo_required_for_bdmv', PropertyMock(return_value=mediainfo_required_for_bdmv))
    return_value = tracker_jobs.mediainfo_job_precondition()
    assert return_value is exp_return_value

def test_mediainfo_required_for_bdmv():
    assert TrackerJobsBase.mediainfo_required_for_bdmv is False


def test_bdinfo_job(make_TestTrackerJobs, mocker):
    BdinfoJob_mock = mocker.patch('upsies.jobs.bdinfo.BdinfoJob')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        common_job_args={'home_directory': 'path/to/home', 'ignore_cache': False},
    )
    mocker.patch.object(tracker_jobs, 'make_precondition')
    assert tracker_jobs.bdinfo_job is BdinfoJob_mock.return_value
    assert BdinfoJob_mock.call_args_list == [call(
        precondition=tracker_jobs.make_precondition.return_value,
        **tracker_jobs.common_job_args(),
    )]
    assert tracker_jobs.make_precondition.call_args_list == [call('bdinfo_job', precondition=tracker_jobs.bdinfo_job_precondition)]

def test_bdinfo_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.bdinfo.BdinfoJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.bdinfo_job is tracker_jobs.bdinfo_job

def test_bdinfo_job_precondition(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    is_bdmv_release = Mock()
    mocker.patch.object(type(tracker_jobs), 'is_bdmv_release', PropertyMock(return_value=is_bdmv_release))
    assert tracker_jobs.bdinfo_job_precondition() is is_bdmv_release

@pytest.mark.parametrize(
    argnames='content_path, mediainfos_by_file, bdinfos_by_file, screenshots_by_file, urls_by_file, exp_video_info',
    argvalues=(
        pytest.param(
            'path/to/file.mkv',
            # mediainfos_by_file
            {
                'path/to/file.mkv': '<mediainfo for file.mkv>',
            },
            # bdinfos_by_file
            {},
            # screenshots_by_file
            {
                '': ('anywhere/custom.1.png', 'anywhere/custom.2.png'),
                'path/to/file.mkv': ('screenshots/file.1.png', 'screenshots/file.2.png'),
            },
            # urls_by_file
            {
                'anywhere/custom.1.png': 'https://custom.1.png',
                'anywhere/custom.2.png': 'https://custom.2.png',
                'screenshots/file.1.png': 'https://file.1.png',
                'screenshots/file.2.png': 'https://file.2.png',
            },
            # exp_video_info
            {
                '': {
                    'mediainfos': (),
                    'bdinfos': (),
                    'screenshot_urls': ('https://custom.1.png', 'https://custom.2.png'),
                },
                'file.mkv': {
                    'mediainfos': ('<mediainfo for file.mkv>',),
                    'bdinfos': (),
                    'screenshot_urls': ('https://file.1.png', 'https://file.2.png'),
                },
            },
            id='Single file',
        ),

        pytest.param(
            'path/to/content/',
            # mediainfos_by_file
            {
                'path/to/content/foo.mkv': '<mediainfo for foo.mkv>',
                'path/to/content/bar.mkv': '<mediainfo for bar.mkv>',
            },
            # bdinfos_by_file
            {},
            # screenshots_by_file
            {
                '': ('anywhere/custom.1.png', 'anywhere/custom.2.png'),
                'path/to/content/foo.mkv': ('screenshots/foo.1.png', 'screenshots/foo.2.png'),
                'path/to/content/bar.mkv': ('screenshots/bar.1.png', 'screenshots/bar.2.png', 'screenshots/bar.3.png'),
            },
            # urls_by_file
            {
                'anywhere/custom.1.png': 'https://custom.1.png',
                'anywhere/custom.2.png': 'https://custom.2.png',
                'screenshots/foo.1.png': 'https://foo.1.png',
                'screenshots/foo.2.png': 'https://foo.2.png',
                'screenshots/bar.1.png': 'https://bar.1.png',
                # Upload of bar.2.png failed or is missing for any other reason.
                # 'screenshots/bar.2.png': 'https://bar.2.png',
                'screenshots/bar.3.png': 'https://bar.3.png',
            },
            # exp_video_info
            {
                '': {
                    'mediainfos': (),
                    'bdinfos': (),
                    'screenshot_urls': ('https://custom.1.png', 'https://custom.2.png'),
                },
                'content/foo.mkv': {
                    'mediainfos': ('<mediainfo for foo.mkv>',),
                    'bdinfos': (),
                    'screenshot_urls': ('https://foo.1.png', 'https://foo.2.png'),
                },
                'content/bar.mkv': {
                    'mediainfos': ('<mediainfo for bar.mkv>',),
                    'bdinfos': (),
                    'screenshot_urls': ('https://bar.1.png', 'https://bar.3.png'),
                },
            },
            id='Multiple files',
        ),

        pytest.param(
            'path/to/content.dvd',
            # mediainfos_by_file
            {
                'path/to/content.dvd/disc1/VIDEO_TS/VTS_06_0.IFO': '<mediainfo for VTS_06_0.IFO>',
                'path/to/content.dvd/disc1/VIDEO_TS/VTS_06_1.VOB': '<mediainfo for VTS_06_1.VOB>',
                'path/to/content.dvd/disc1/VIDEO_TS/VTS_07_0.IFO': '<mediainfo for VTS_07_0.IFO>',
                'path/to/content.dvd/disc1/VIDEO_TS/VTS_07_2.VOB': '<mediainfo for VTS_07_2.VOB>',
                'path/to/content.dvd/disc2/VIDEO_TS/VTS_02_0.IFO': '<mediainfo for VTS_02_0.IFO>',
                'path/to/content.dvd/disc2/VIDEO_TS/VTS_02_8.VOB': '<mediainfo for VTS_02_8.VOB>',
            },
            # bdinfos_by_file
            {},
            # screenshots_by_file
            {
                '': ('anywhere/custom.1.png', 'anywhere/custom.2.png'),
                'path/to/content.dvd/disc1/VIDEO_TS/VTS_06_1.VOB': ('screenshots/VTS_06_1.VOB.1.jpg', 'screenshots/VTS_06_1.VOB.2.jpg'),
                'path/to/content.dvd/disc1/VIDEO_TS/VTS_07_2.VOB': ('screenshots/VTS_07_2.VOB.1.jpg', 'screenshots/VTS_07_2.VOB.2.jpg'),
                'path/to/content.dvd/disc2/VIDEO_TS/VTS_02_8.VOB': ('screenshots/VTS_02_8.VOB.1.jpg', 'screenshots/VTS_02_8.VOB.2.jpg'),
            },
            # urls_by_file
            {
                'anywhere/custom.1.png': 'https://custom.1.png',
                'anywhere/custom.2.png': 'https://custom.2.png',
                'screenshots/VTS_06_1.VOB.1.jpg': 'https://VTS_06_1.VOB.1.jpg',
                'screenshots/VTS_06_1.VOB.2.jpg': 'https://VTS_06_1.VOB.2.jpg',
                'screenshots/VTS_07_2.VOB.1.jpg': 'https://VTS_07_2.VOB.1.jpg',
                'screenshots/VTS_07_2.VOB.2.jpg': 'https://VTS_07_2.VOB.2.jpg',
                'screenshots/VTS_02_8.VOB.1.jpg': 'https://VTS_02_8.VOB.1.jpg',
                'screenshots/VTS_02_8.VOB.2.jpg': 'https://VTS_02_8.VOB.2.jpg',
            },
            # exp_video_info
            {
                '': {
                    'mediainfos': (),
                    'bdinfos': (),
                    'screenshot_urls': ('https://custom.1.png', 'https://custom.2.png'),
                },
                'content.dvd/disc1/VIDEO_TS/VTS_06': {
                    'mediainfos': (
                        '<mediainfo for VTS_06_0.IFO>',
                        '<mediainfo for VTS_06_1.VOB>',
                    ),
                    'bdinfos': (),
                    'screenshot_urls': ('https://VTS_06_1.VOB.1.jpg', 'https://VTS_06_1.VOB.2.jpg'),
                },
                'content.dvd/disc1/VIDEO_TS/VTS_07': {
                    'mediainfos': (
                        '<mediainfo for VTS_07_0.IFO>',
                        '<mediainfo for VTS_07_2.VOB>',
                    ),
                    'bdinfos': (),
                    'screenshot_urls': ('https://VTS_07_2.VOB.1.jpg', 'https://VTS_07_2.VOB.2.jpg'),
                },
                'content.dvd/disc2/VIDEO_TS/VTS_02': {
                    'mediainfos': (
                        '<mediainfo for VTS_02_0.IFO>',
                        '<mediainfo for VTS_02_8.VOB>',
                    ),
                    'bdinfos': (),
                    'screenshot_urls': ('https://VTS_02_8.VOB.1.jpg', 'https://VTS_02_8.VOB.2.jpg'),
                },
            },
            id='VIDEO_TS release',
        ),

        pytest.param(
            'path/to/content.bluray',
            # mediainfos_by_file
            {},
            # bdinfos_by_file
            {
                'path/to/content.bluray/disc1/BDMV/PLAYLIST/00003.mpls': '<bdinfo for 00003.mpls>',
                'path/to/content.bluray/disc1/BDMV/PLAYLIST/00006.mpls': '<bdinfo for 00006.mpls>',
                'path/to/content.bluray/disc2/BDMV/PLAYLIST/00009.mpls': '<bdinfo for 00009.mpls>',
            },
            # screenshots_by_file
            {
                '': ('anywhere/custom.1.png', 'anywhere/custom.2.png'),
                'path/to/content.bluray/disc1/BDMV/PLAYLIST/00003.mpls': ('screenshots/00003.1.png', 'screenshots/00003.2.png'),
                'path/to/content.bluray/disc1/BDMV/PLAYLIST/00006.mpls': ('screenshots/00006.1.png', 'screenshots/00006.2.png'),
                'path/to/content.bluray/disc2/BDMV/PLAYLIST/00009.mpls': ('screenshots/00009.1.png', 'screenshots/00009.2.png'),
            },
            # urls_by_file
            {
                'anywhere/custom.1.png': 'https://custom.1.png',
                'anywhere/custom.2.png': 'https://custom.2.png',
                'screenshots/00003.1.png': 'https://00003.1.png',
                'screenshots/00003.2.png': 'https://00003.2.png',
                'screenshots/00006.1.png': 'https://00006.1.png',
                'screenshots/00006.2.png': 'https://00006.2.png',
                'screenshots/00009.1.png': 'https://00009.1.png',
                'screenshots/00009.2.png': 'https://00009.2.png',
            },
            # exp_video_info
            {
                '': {
                    'mediainfos': (),
                    'bdinfos': (),
                    'screenshot_urls': ('https://custom.1.png', 'https://custom.2.png'),
                },
                'content.bluray/disc1/BDMV/PLAYLIST/00003.mpls': {
                    'mediainfos': (),
                    'bdinfos': ('<bdinfo for 00003.mpls>',),
                    'screenshot_urls': ('https://00003.1.png', 'https://00003.2.png'),
                },
                'content.bluray/disc1/BDMV/PLAYLIST/00006.mpls': {
                    'mediainfos': (),
                    'bdinfos': ('<bdinfo for 00006.mpls>',),
                    'screenshot_urls': ('https://00006.1.png', 'https://00006.2.png'),
                },
                'content.bluray/disc2/BDMV/PLAYLIST/00009.mpls': {
                    'mediainfos': (),
                    'bdinfos': ('<bdinfo for 00009.mpls>',),
                    'screenshot_urls': ('https://00009.1.png', 'https://00009.2.png'),
                },
            },
            id='BDMV release',
        ),
    ),
)
def test_video_info(
        content_path, mediainfos_by_file, bdinfos_by_file, screenshots_by_file, urls_by_file,
        exp_video_info,
        make_TestTrackerJobs, mocker,
):
    tracker_jobs = make_TestTrackerJobs(content_path=content_path)

    mocker.patch.object(type(tracker_jobs), 'mediainfo_job', PropertyMock(return_value=Mock(reports_by_file=mediainfos_by_file)))
    mocker.patch.object(type(tracker_jobs), 'bdinfo_job', PropertyMock(return_value=Mock(reports_by_file=bdinfos_by_file)))
    mocker.patch.object(type(tracker_jobs), 'screenshots_job', PropertyMock(return_value=Mock(screenshots_by_file=screenshots_by_file)))
    mocker.patch.object(type(tracker_jobs), 'upload_screenshots_job', PropertyMock(return_value=Mock(urls_by_file=urls_by_file)))
    mocker.patch.object(type(tracker_jobs), 'is_video_ts_release', PropertyMock(return_value=bool(content_path.endswith('.dvd'))))

    assert tracker_jobs.video_info == exp_video_info
    # Order is also important.
    assert tuple(tracker_jobs.video_info) == tuple(exp_video_info)


@pytest.mark.parametrize(
    argnames='options, common_job_args, exp_kwargs',
    argvalues=(
        ({}, {'ignore_cache': True}, {'ignore_cache': True, 'force': None}),
        ({}, {'ignore_cache': False}, {'ignore_cache': True, 'force': None}),
        ({'is_scene': None}, {'ignore_cache': True}, {'ignore_cache': True, 'force': None}),
        ({'is_scene': None}, {'ignore_cache': False}, {'ignore_cache': True, 'force': None}),
        ({'is_scene': True}, {'ignore_cache': True}, {'ignore_cache': True, 'force': True}),
        ({'is_scene': True}, {'ignore_cache': False}, {'ignore_cache': True, 'force': True}),
        ({'is_scene': False}, {'ignore_cache': True}, {'ignore_cache': True, 'force': False}),
        ({'is_scene': False}, {'ignore_cache': False}, {'ignore_cache': True, 'force': False}),
    ),
    ids=lambda v: repr(v),
)
def test_scene_check_job(options, common_job_args, exp_kwargs, make_TestTrackerJobs, mocker):
    SceneCheckJob_mock = mocker.patch('upsies.jobs.scene.SceneCheckJob')
    tracker_jobs = make_TestTrackerJobs(
        content_path='path/to/content',
        common_job_args=common_job_args,
    )
    mocker.patch.object(type(tracker_jobs), 'options', PropertyMock(return_value=options))
    mocker.patch.object(tracker_jobs, 'make_precondition')
    assert tracker_jobs.scene_check_job is SceneCheckJob_mock.return_value
    assert SceneCheckJob_mock.call_args_list == [call(
        content_path='path/to/content',
        precondition=tracker_jobs.make_precondition.return_value,
        **exp_kwargs,
    )]
    assert tracker_jobs.make_precondition.call_args_list == [call('scene_check_job')]

def test_scene_check_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.scene.SceneCheckJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.scene_check_job is tracker_jobs.scene_check_job


@pytest.mark.parametrize(
    argnames='options, exp_only_warn',
    argvalues=(
        ({}, False),
        ({'ignore_rules': False}, False),
        ({'ignore_rules': True}, True),
    ),
    ids=lambda v: repr(v),
)
def test_rules_job(options, exp_only_warn, make_TestTrackerJobs, mocker):
    RulesJob_mock = mocker.patch('upsies.jobs.rules.RulesJob')
    tracker_jobs = make_TestTrackerJobs(options=options)
    mocker.patch.object(tracker_jobs, 'make_precondition')
    assert tracker_jobs.rules_job is RulesJob_mock.return_value
    assert RulesJob_mock.call_args_list == [call(
        precondition=tracker_jobs.make_precondition.return_value,
        tracker_jobs=tracker_jobs,
        only_warn=exp_only_warn,
        **tracker_jobs.common_job_args(),
    )]
    assert tracker_jobs.make_precondition.call_args_list == [call('rules_job', precondition=tracker_jobs.rules_job_precondition)]

def test_rules_job_is_singleton(make_TestTrackerJobs, mocker):
    mocker.patch('upsies.jobs.rules.RulesJob', side_effect=(Mock(), Mock()))
    tracker_jobs = make_TestTrackerJobs()
    assert tracker_jobs.rules_job is tracker_jobs.rules_job

@pytest.mark.parametrize(
    argnames='rules, exp_return_value',
    argvalues=(
        ((), False),
        (('rule1', 'rule2', 'rule3'), True),
    ),
    ids=lambda v: repr(v),
)
def test_rules_job_precondition(rules, exp_return_value, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    tracker_jobs.tracker.rules = rules
    assert tracker_jobs.rules_job_precondition() is exp_return_value


def test_nfo_job(make_TestTrackerJobs, mocker):
    CustomJob_mock = mocker.patch('upsies.jobs.custom.CustomJob')
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(tracker_jobs, 'get_job_name')
    mocker.patch.object(tracker_jobs, 'make_precondition')
    mocker.patch.object(tracker_jobs, 'common_job_args')
    assert tracker_jobs.nfo_job is CustomJob_mock.return_value

    assert CustomJob_mock.call_args_list == [call(
        name=tracker_jobs.get_job_name.return_value,
        label='NFO File',
        precondition=tracker_jobs.make_precondition.return_value,
        worker=tracker_jobs.read_nfo_worker,
        no_output_is_ok=True,
        catch=(
            errors.ContentError,
        ),
        hidden=True,
        **tracker_jobs.common_job_args.return_value,
    )]
    assert tracker_jobs.get_job_name.call_args_list == [call('nfo')]
    assert tracker_jobs.make_precondition.call_args_list == [call('nfo_job')]
    assert tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.asyncio
async def test_read_nfo_worker(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(tracker_jobs, 'read_nfo')
    return_value = await tracker_jobs.read_nfo_worker('<ignored job>')
    assert return_value is tracker_jobs.read_nfo.return_value
    assert tracker_jobs.read_nfo.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='nfo_job_output, exp_return_value',
    argvalues=(
        ((), None),
        (('<the nfo text>',), '<the nfo text>'),
    ),
    ids=lambda v: repr(v),
)
def test_nfo_text(nfo_job_output, exp_return_value, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'nfo_job', PropertyMock(return_value=Mock(
        output=nfo_job_output,
    )))
    assert tracker_jobs.nfo_text == exp_return_value


def test_login_job(make_TestTrackerJobs, mocker):
    CustomJob_mock = mocker.patch('upsies.jobs.custom.CustomJob')
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(tracker_jobs, 'get_job_name')
    mocker.patch.object(tracker_jobs, 'make_precondition')
    mocker.patch.object(tracker_jobs, 'common_job_args')
    assert tracker_jobs.login_job is CustomJob_mock.return_value

    assert CustomJob_mock.call_args_list == [call(
        name=tracker_jobs.get_job_name.return_value,
        label='Login',
        precondition=tracker_jobs.make_precondition.return_value,
        worker=tracker_jobs.perform_login,
        no_output_is_ok=True,
        catch=(
            errors.RequestError,
        ),
        **tracker_jobs.common_job_args.return_value,
    )]
    assert tracker_jobs.get_job_name.call_args_list == [call('login')]
    assert tracker_jobs.make_precondition.call_args_list == [call('login_job')]
    assert tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.parametrize(
    argnames='still_logged_in, login, add_prompt, exp_mock_calls',
    argvalues=(
        pytest.param(
            AsyncMock(return_value=False),
            AsyncMock(side_effect=None),
            AsyncMock(return_value='123456'),
            [
                call.login_job.indicate_activity(True),
                call.tracker.still_logged_in(),
                call.login_job.indicate_activity(False),

                call.login_job.indicate_activity(True),
                call.tracker.login(),
                call.login_job.indicate_activity(False),
            ],
            id='No 2FA required',
        ),

        pytest.param(
            AsyncMock(return_value=True),
            AsyncMock(side_effect=None),
            AsyncMock(return_value='123456'),
            [
                call.login_job.indicate_activity(True),
                call.tracker.still_logged_in(),
                call.login_job.indicate_activity(False),
            ],
            id='Still logged in',
        ),

        pytest.param(
            AsyncMock(return_value=False),
            AsyncMock(side_effect=(
                errors.TfaRequired('Where is your second factor?'),
                None,
            )),
            AsyncMock(return_value='123456'),
            [
                call.login_job.indicate_activity(True),
                call.tracker.still_logged_in(),
                call.login_job.indicate_activity(False),

                call.login_job.indicate_activity(True),
                call.tracker.login(),
                call.login_job.indicate_activity(False),
                call.login_job.add_prompt(uis.prompts.TextPrompt(question='Enter 2FA one-time password:')),

                call.login_job.indicate_activity(True),
                call.tracker.login(tfa_otp='123456'),
                call.login_job.indicate_activity(False),
            ],
            id='2FA provided after asking once',
        ),

        pytest.param(
            AsyncMock(return_value=False),
            AsyncMock(side_effect=(
                errors.TfaRequired('Where is your second factor?'),
                errors.TfaRequired('Your second factor sucks!'),
                errors.TfaRequired('Your second factor sucks!'),
                None,
            )),
            AsyncMock(side_effect=('123', '456', '789')),
            [
                call.login_job.indicate_activity(True),
                call.tracker.still_logged_in(),
                call.login_job.indicate_activity(False),

                call.login_job.indicate_activity(True),
                call.tracker.login(),
                call.login_job.indicate_activity(False),
                call.login_job.add_prompt(uis.prompts.TextPrompt(question='Enter 2FA one-time password:')),

                call.login_job.indicate_activity(True),
                call.tracker.login(tfa_otp='123'),
                call.login_job.indicate_activity(False),
                call.login_job.add_prompt(uis.prompts.TextPrompt(question='Enter 2FA one-time password:')),

                call.login_job.indicate_activity(True),
                call.tracker.login(tfa_otp='456'),
                call.login_job.indicate_activity(False),
                call.login_job.add_prompt(uis.prompts.TextPrompt(question='Enter 2FA one-time password:')),

                call.login_job.indicate_activity(True),
                call.tracker.login(tfa_otp='789'),
                call.login_job.indicate_activity(False),
            ],
            id='2FA provided after asking multiple times',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_perform_login(still_logged_in, login, add_prompt, exp_mock_calls, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'tracker', PropertyMock(return_value=Mock(
        still_logged_in=still_logged_in,
        login=login,
    )))

    def assert_job_is_not_hidden(*args, **kwargs):
        assert login_job.hidden is False
        return DEFAULT

    login_job = Mock()
    login_job.add_prompt = add_prompt
    login_job.indicate_activity.side_effect = assert_job_is_not_hidden
    login_job.tracker.login.side_effect = assert_job_is_not_hidden
    login_job.tracker.still_logged_in.side_effect = assert_job_is_not_hidden

    mocks = Mock()
    mocks.attach_mock(login_job, 'login_job')
    mocks.attach_mock(tracker_jobs.tracker, 'tracker')

    return_value = await tracker_jobs.perform_login(login_job)
    assert return_value is None
    assert login_job.hidden is True
    assert mocks.mock_calls == exp_mock_calls


def test_logout_job(make_TestTrackerJobs, mocker):
    CustomJob_mock = mocker.patch('upsies.jobs.custom.CustomJob')
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(tracker_jobs, 'get_job_name')
    mocker.patch.object(tracker_jobs, 'make_precondition')
    mocker.patch.object(tracker_jobs, 'common_job_args')
    assert tracker_jobs.logout_job is CustomJob_mock.return_value

    assert CustomJob_mock.call_args_list == [call(
        name=tracker_jobs.get_job_name.return_value,
        label='Logout',
        precondition=tracker_jobs.make_precondition.return_value,
        worker=tracker_jobs.perform_logout,
        guaranteed=True,
        no_output_is_ok=True,
        catch=(
            errors.RequestError,
        ),
        **tracker_jobs.common_job_args.return_value,
    )]
    assert tracker_jobs.get_job_name.call_args_list == [call('logout')]
    assert tracker_jobs.make_precondition.call_args_list == [call('logout_job')]
    assert tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.parametrize(
    argnames='exception',
    argvalues=(
        None,
        errors.RequestError('You shall not logout!'),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_perform_logout(exception, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'tracker', PropertyMock(return_value=AsyncMock()))

    def assert_job_is_hidden(*args, **kwargs):
        assert logout_job.hidden is True
        return DEFAULT

    def assert_job_is_not_hidden(*args, **kwargs):
        assert logout_job.hidden is False
        return DEFAULT

    logout_job = Mock()
    logout_job.hidden = False
    logout_job.indicate_activity.side_effect = assert_job_is_not_hidden
    logout_job.tracker.logout.side_effect = assert_job_is_not_hidden

    mocks = Mock()
    mocker.patch.object(logout_job, 'wait_for', AsyncMock())
    mocks.attach_mock(logout_job, 'logout_job')
    mocks.attach_mock(tracker_jobs.tracker, 'tracker')

    assert logout_job.hidden is False
    return_value = await tracker_jobs.perform_logout(logout_job)
    assert return_value is None
    assert mocks.mock_calls == [
        call.logout_job.wait_for('submit', 'finished'),
        call.logout_job.indicate_activity(True),
        call.tracker.logout(),
        call.logout_job.indicate_activity(False),
    ]
    assert logout_job.hidden is True


@pytest.mark.parametrize(
    argnames='job_attr, jobs_before_upload, jobs_after_upload, isolated_jobs, custom_precondition, exp_return_value',
    argvalues=(
        ('foo_job', (), (), (), None, False),
        ('foo_job', (), (), (), Mock(return_value=True), False),
        ('foo_job', (), (), (), Mock(return_value=False), False),

        ('foo_job', ('foo_job', 'bar_job'), ('baz_job',), (), None, True),
        ('foo_job', ('foo_job', 'bar_job'), ('baz_job',), (), Mock(return_value=True), True),
        ('foo_job', ('foo_job', 'bar_job'), ('baz_job',), (), Mock(return_value=False), False),

        ('bar_job', ('foo_job', 'bar_job'), ('baz_job',), (), None, True),
        ('bar_job', ('foo_job', 'bar_job'), ('baz_job',), (), Mock(return_value=True), True),
        ('bar_job', ('foo_job', 'bar_job'), ('baz_job',), (), Mock(return_value=False), False),

        ('baz_job', ('foo_job', 'bar_job'), ('baz_job',), (), None, True),
        ('baz_job', ('foo_job', 'bar_job'), ('baz_job',), (), Mock(return_value=True), True),
        ('baz_job', ('foo_job', 'bar_job'), ('baz_job',), (), Mock(return_value=False), False),

        ('baz_job', ('foo_job',), ('bar_job',), (), None, False),
        ('baz_job', ('foo_job',), ('bar_job',), (), Mock(return_value=True), False),
        ('baz_job', ('foo_job',), ('bar_job',), (), Mock(return_value=False), False),

        ('bar_job', (), ('bar_job',), (), None, True),
        ('bar_job', (), ('bar_job',), (), Mock(return_value=True), True),
        ('bar_job', (), ('bar_job',), (), Mock(return_value=False), False),

        ('foo_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job',), None, True),
        ('foo_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job',), Mock(return_value=True), True),
        ('foo_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job',), Mock(return_value=False), False),

        ('bar_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job',), None, False),
        ('bar_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job',), Mock(return_value=True), False),
        ('bar_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job',), Mock(return_value=False), False),

        ('bar_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job', 'bar_job'), None, True),
        ('bar_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job', 'bar_job'), Mock(return_value=True), True),
        ('bar_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job', 'bar_job'), Mock(return_value=False), False),

        ('baz_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job', 'bar_job'), None, False),
        ('baz_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job', 'bar_job'), Mock(return_value=True), False),
        ('baz_job', ('foo_job', 'bar_job'), ('baz_job',), ('foo_job', 'bar_job'), Mock(return_value=False), False),
    ),
    ids=lambda v: str(v),
)
def test_make_precondition(job_attr, jobs_before_upload, jobs_after_upload, isolated_jobs, custom_precondition,
                           exp_return_value,
                           make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    for attr in ('foo_job', 'bar_job', 'baz_job'):
        mocker.patch.object(type(tracker_jobs), attr, PropertyMock(), create=True)
    mocker.patch.object(type(tracker_jobs), 'jobs_before_upload', PropertyMock(
        return_value=[getattr(tracker_jobs, attr) for attr in jobs_before_upload],
    ))
    mocker.patch.object(type(tracker_jobs), 'jobs_after_upload', PropertyMock(
        return_value=[getattr(tracker_jobs, attr) for attr in jobs_after_upload],
    ))
    mocker.patch.object(type(tracker_jobs), 'isolated_jobs', PropertyMock(
        return_value=[getattr(tracker_jobs, attr) for attr in isolated_jobs],
    ))
    precondition = tracker_jobs.make_precondition(job_attr, precondition=custom_precondition)
    return_value = precondition()
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='job, slice, default, exp_result',
    argvalues=(
        (
            Mock(is_finished=True, output=('foo', 'bar', 'baz')),
            None,
            TrackerJobsBase._NO_DEFAULT,
            ('foo', 'bar', 'baz'),
        ),
        (
            Mock(is_finished=True, output=('foo', 'bar', 'baz')),
            0,
            'ignored default',
            'foo',
        ),
        (
            Mock(is_finished=True, output=('foo', 'bar', 'baz')),
            1,
            TrackerJobsBase._NO_DEFAULT,
            'bar',
        ),
        (
            Mock(is_finished=True, output=('foo', 'bar', 'baz')),
            2,
            'ignored default',
            'baz',
        ),
        (
            Mock(is_finished=True, output=('foo', 'bar', 'baz')),
            builtins.slice(1, 3),
            TrackerJobsBase._NO_DEFAULT,
            ('bar', 'baz'),
        ),

        # Job is not Finished
        (
            Mock(is_finished=False, output=('foo', 'bar', 'baz')),
            None,
            TrackerJobsBase._NO_DEFAULT,
            RuntimeError('Cannot get output from unfinished job: asdf'),
        ),
        (
            Mock(is_finished=False, output=('foo', 'bar', 'baz')),
            None,
            'my default',
            'my default',
        ),
        (
            Mock(is_finished=False, output=('foo', 'bar', 'baz')),
            None,
            None,
            None,
        ),

        # Slicing output fails with IndexError
        (
            Mock(is_finished=True, output=('foo', 'bar', 'baz')),
            10,
            TrackerJobsBase._NO_DEFAULT,
            RuntimeError("Job finished with insufficient output: asdf: ('foo', 'bar', 'baz')"),
        ),
        (
            Mock(is_finished=True, output=('foo', 'bar', 'baz')),
            10,
            'my default',
            'my default',
        ),
        (
            Mock(is_finished=True, output=('foo', 'bar', 'baz')),
            10,
            None,
            None,
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_get_job_output(job, slice, default, exp_result, make_TestTrackerJobs, mocker):
    job.configure_mock(name='asdf')
    tracker_jobs = make_TestTrackerJobs()
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            tracker_jobs.get_job_output(job, slice, default=default)
    else:
        output = tracker_jobs.get_job_output(job, slice, default=default)
        assert output == exp_result


@pytest.mark.parametrize(
    argnames='job, attribute, default, exp_result',
    argvalues=(
        (
            Mock(is_finished=True, foo='bar'),
            'foo',
            TrackerJobsBase._NO_DEFAULT,
            'bar',
        ),
        (
            Mock(is_finished=True, foo='bar'),
            'foo',
            'ignored default',
            'bar',
        ),
        (
            Mock(is_finished=False, foo='bar'),
            'foo',
            TrackerJobsBase._NO_DEFAULT,
            RuntimeError('Cannot get attribute from unfinished job: asdf'),
        ),
        (
            Mock(is_finished=False, foo='bar'),
            'foo',
            'my default',
            'my default',
        ),
        (
            Mock(is_finished=True, foo='bar', spec=()),
            'no_such_attribute',
            TrackerJobsBase._NO_DEFAULT,
            AttributeError('no_such_attribute'),
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_get_job_attribute(job, attribute, default, exp_result, make_TestTrackerJobs, mocker):
    job.configure_mock(name='asdf')
    tracker_jobs = make_TestTrackerJobs()
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'{re.escape(str(exp_result))}'):
            tracker_jobs.get_job_attribute(job, attribute, default=default)
    else:
        value = tracker_jobs.get_job_attribute(job, attribute, default=default)
        assert value == exp_result


@pytest.mark.parametrize(
    argnames='cwd, file_path, content_path, exp_result',
    argvalues=(
        pytest.param(
            '{tmp_path}/cwd',
            '{tmp_path}/content/file.mkv',
            '{tmp_path}/content/file.mkv',
            'file.mkv',
            id='absolute file_path is identical to absolute content_path',
        ),
        pytest.param(
            '{tmp_path}/cwd',
            '{tmp_path}/content/file.mkv',
            '../content/file.mkv',
            'file.mkv',
            id='absolute file_path is identical to relative content_path',
        ),
        pytest.param(
            '{tmp_path}/cwd',
            '../content/file.mkv',
            '{tmp_path}/content/file.mkv',
            'file.mkv',
            id='relative file_path is identical to absolute content_path',
        ),
        pytest.param(
            '{tmp_path}/cwd',
            '../content/file.mkv',
            '../content/file.mkv',
            'file.mkv',
            id='relative file_path is identical to relative content_path',
        ),

        pytest.param(
            '{tmp_path}/cwd',
            '{tmp_path}/content/foo/bar/baz/file.mkv',
            '{tmp_path}/content',
            'content/foo/bar/baz/file.mkv',
            id='absolute file_path is subpath of absolute content_path',
        ),
        pytest.param(
            '{tmp_path}/cwd',
            '{tmp_path}/content/foo/bar/baz/file.mkv',
            '../content/',
            'content/foo/bar/baz/file.mkv',
            id='absolute file_path is subpath of relative content_path',
        ),
        pytest.param(
            '{tmp_path}/cwd',
            '../content//foo/bar/baz/file.mkv',
            '{tmp_path}/content/',
            'content/foo/bar/baz/file.mkv',
            id='relative file_path is subpath of absolute content_path',
        ),
        pytest.param(
            '{tmp_path}/cwd',
            '../content//foo/bar/baz/file.mkv',
            '../content',
            'content/foo/bar/baz/file.mkv',
            id='relative file_path is subpath of relative content_path',
        ),

        pytest.param(
            '{tmp_path}/cwd',
            '{tmp_path}/this/file.mkv',
            '{tmp_path}/content',
            ValueError("'{tmp_path}/this/file.mkv' is not a subpath of '{tmp_path}/content'"),
            id='absolute file_path is not subpath of absolute content_path',
        ),
        pytest.param(
            '{tmp_path}/cwd',
            '{tmp_path}/this/file.mkv',
            '../content/',
            ValueError("'{tmp_path}/this/file.mkv' is not a subpath of '{tmp_path}/content'"),
            id='absolute file_path is not subpath of relative content_path',
        ),
        pytest.param(
            '{tmp_path}/cwd',
            '../this//file.mkv',
            '{tmp_path}/content/',
            ValueError("'{tmp_path}/this/file.mkv' is not a subpath of '{tmp_path}/content'"),
            id='relative file_path is not subpath of absolute content_path',
        ),
        pytest.param(
            '{tmp_path}/cwd',
            '../this/file.mkv',
            '../content',
            ValueError("'{tmp_path}/this/file.mkv' is not a subpath of '{tmp_path}/content'"),
            id='relative file_path is not subpath of relative content_path',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_relative_file_path(cwd, file_path, content_path, exp_result, make_TestTrackerJobs, mocker, tmp_path):
    cwd = pathlib.Path(cwd.format(tmp_path=tmp_path))
    orig_cwd = os.getcwd()
    cwd.mkdir(parents=True, exist_ok=True)
    os.chdir(cwd)
    try:
        file_path = pathlib.Path(file_path.format(tmp_path=tmp_path))
        content_path = pathlib.Path(content_path.format(tmp_path=tmp_path))

        print('____________ cwd:', cwd, '--', os.path.abspath(cwd))
        print('______ file_path:', file_path, '--', os.path.abspath(file_path))
        print('___ content_path:', content_path, '--', os.path.abspath(content_path))

        tracker_jobs = make_TestTrackerJobs(content_path=str(content_path))
        if isinstance(exp_result, Exception):
            exp_msg = str(exp_result).format(tmp_path=tmp_path)
            with pytest.raises(type(exp_result), match=rf'^{re.escape(exp_msg)}$'):
                tracker_jobs.get_relative_file_path(str(file_path))
        else:
            relative_path = tracker_jobs.get_relative_file_path(str(file_path))
            assert relative_path == exp_result

    finally:
        os.chdir(orig_cwd)


class ImageUrl(str):
    def __new__(cls, *args, thumbnail=True, **kwargs):
        self = super().__new__(cls, *args, **kwargs)
        self._thumbnail = thumbnail
        return self

    @property
    def thumbnail_url(self):
        if self._thumbnail:
            return f'thumb_{self}'

@pytest.mark.parametrize(
    argnames='screenshots, columns, horizontal_spacer, vertical_spacer, exp_bbcode',
    argvalues=(
        (
            (ImageUrl('a.png'), ImageUrl('b.png'), ImageUrl('c.png')),
            1, ' | ', '\n-\n',
            (
                '[url=a.png][img]thumb_a.png[/img][/url]'
                '\n-\n'
                '[url=b.png][img]thumb_b.png[/img][/url]'
                '\n-\n'
                '[url=c.png][img]thumb_c.png[/img][/url]'
            ),
        ),
        (
            (ImageUrl('a.png'), ImageUrl('b.png'), ImageUrl('c.png')),
            2, ' | ', '\n-\n',
            (
                '[url=a.png][img]thumb_a.png[/img][/url] | [url=b.png][img]thumb_b.png[/img][/url]'
                '\n-\n'
                '[url=c.png][img]thumb_c.png[/img][/url]'
            ),
        ),
        (
            (ImageUrl('a.png'), ImageUrl('b.png'), ImageUrl('c.png')),
            3, '|', '-',
            (
                '[url=a.png][img]thumb_a.png[/img][/url]|[url=b.png][img]thumb_b.png[/img][/url]|[url=c.png][img]thumb_c.png[/img][/url]'
            ),
        ),
        (
            (ImageUrl('a.png'), ImageUrl('b.png'), ImageUrl('c.png'), ImageUrl('d.png')),
            2, '.', ':',
            (
                '[url=a.png][img]thumb_a.png[/img][/url].[url=b.png][img]thumb_b.png[/img][/url]'
                ':'
                '[url=c.png][img]thumb_c.png[/img][/url].[url=d.png][img]thumb_d.png[/img][/url]'
            ),
        ),
        (
            (ImageUrl('a.png'), ImageUrl('b.png'), ImageUrl('c.png'), ImageUrl('d.png'), ImageUrl('e.png')),
            2, '.', ':',
            (
                '[url=a.png][img]thumb_a.png[/img][/url].[url=b.png][img]thumb_b.png[/img][/url]'
                ':'
                '[url=c.png][img]thumb_c.png[/img][/url].[url=d.png][img]thumb_d.png[/img][/url]'
                ':'
                '[url=e.png][img]thumb_e.png[/img][/url]'
            ),
        ),
        (
            (ImageUrl('a.png'), ImageUrl('b.png'), ImageUrl('c.png'), ImageUrl('d.png'), ImageUrl('e.png')),
            3, '.', ':',
            (
                '[url=a.png][img]thumb_a.png[/img][/url].[url=b.png][img]thumb_b.png[/img][/url].[url=c.png][img]thumb_c.png[/img][/url]'
                ':'
                '[url=d.png][img]thumb_d.png[/img][/url].[url=e.png][img]thumb_e.png[/img][/url]'
            ),
        ),
        (
            (ImageUrl('a.png'), ImageUrl('b.png'), ImageUrl('c.png'), ImageUrl('d.png'), ImageUrl('e.png'), ImageUrl('f.png')),
            2, '.', ':',
            (
                '[url=a.png][img]thumb_a.png[/img][/url].[url=b.png][img]thumb_b.png[/img][/url]'
                ':'
                '[url=c.png][img]thumb_c.png[/img][/url].[url=d.png][img]thumb_d.png[/img][/url]'
                ':'
                '[url=e.png][img]thumb_e.png[/img][/url].[url=f.png][img]thumb_f.png[/img][/url]'
            ),
        ),
        (
            (ImageUrl('a.png'), ImageUrl('b.png'), ImageUrl('c.png'), ImageUrl('d.png'), ImageUrl('e.png'), ImageUrl('f.png')),
            3, '.', ':',
            (
                '[url=a.png][img]thumb_a.png[/img][/url].[url=b.png][img]thumb_b.png[/img][/url].[url=c.png][img]thumb_c.png[/img][/url]'
                ':'
                '[url=d.png][img]thumb_d.png[/img][/url].[url=e.png][img]thumb_e.png[/img][/url].[url=f.png][img]thumb_f.png[/img][/url]'
            ),
        ),
    ),
)
def test_make_screenshots_grid(
        screenshots, columns, horizontal_spacer, vertical_spacer, exp_bbcode,
        make_TestTrackerJobs, mocker,
):
    tracker_jobs = make_TestTrackerJobs()
    bbcode = tracker_jobs.make_screenshots_grid(
        screenshots,
        columns=columns,
        horizontal_spacer=horizontal_spacer,
        vertical_spacer=vertical_spacer,
    )
    assert bbcode == exp_bbcode

def test_make_screenshots_grid_without_thumbnail_url(make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    screenshots = (ImageUrl('a.png'), ImageUrl('b.png', thumbnail=False), ImageUrl('c.png'))
    with pytest.raises(RuntimeError, match=r'^No thumbnail for b\.png$'):
        tracker_jobs.make_screenshots_grid(screenshots)


@pytest.mark.parametrize(
    argnames='options, exp_path',
    argvalues=(
        ({}, '<CONTENT_PATH>'),
        ({'nfo': 'my/custom/file.nfo'}, 'my/custom/file.nfo'),
    ),
    ids=lambda v: str(v),
)
def test_read_nfo(options, exp_path, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'options', PropertyMock(return_value=options))
    read_nfo_mock = mocker.patch('upsies.utils.string.read_nfo')
    strip = Mock()

    nfo = tracker_jobs.read_nfo(strip=strip)
    assert nfo is read_nfo_mock.return_value

    exp_path = exp_path.replace('<CONTENT_PATH>', tracker_jobs.content_path)
    assert read_nfo_mock.call_args_list == [call(exp_path, strip=strip)]


@pytest.mark.parametrize(
    argnames='options, exp_return_value',
    argvalues=(
        (
            {},
            (
                '[align=right][size=1]'
                f'Shared with [url={__homepage__}]{__project_name__}[/url]'
                '[/size][/align]'
            )
        ),
        (
            {'only_description': False},
            (
                '[align=right][size=1]'
                f'Shared with [url={__homepage__}]{__project_name__}[/url]'
                '[/size][/align]'
            )
        ),
        (
            {'only_description': True},
            '',
        ),
    ),
    ids=lambda v: str(v),
)
def test_promotion_bbcode(options, exp_return_value, make_TestTrackerJobs, mocker):
    tracker_jobs = make_TestTrackerJobs()
    mocker.patch.object(type(tracker_jobs), 'options', PropertyMock(return_value=options))
    return_value = tracker_jobs.promotion_bbcode
    assert return_value == exp_return_value
