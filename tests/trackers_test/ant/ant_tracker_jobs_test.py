import re
from unittest.mock import Mock, PropertyMock, call

import pytest

from upsies import errors
from upsies.trackers import ant


@pytest.fixture
def tracker():
    tracker = Mock()
    tracker.name = 'ant'
    return tracker


@pytest.fixture
def ant_tracker_jobs(tracker, tmp_path, mocker):
    content_path = tmp_path / 'Foo 2000 1080p BluRay x264-ASDF'

    ant_tracker_jobs = ant.jobs.AntTrackerJobs(
        content_path=str(content_path),
        tracker=tracker,
        image_hosts=(),
        btclient=Mock(),
        torrent_destination=str(tmp_path / 'destination'),
        common_job_args={
            'home_directory': str(tmp_path / 'home_directory'),
            'ignore_cache': True,
        },
        options=None,
    )

    return ant_tracker_jobs


@pytest.fixture
def mock_job_attributes(ant_tracker_jobs, mocker):
    job_attrs = (
        # Interactive jobs
        'playlists_job',
        'tmdb_job',
        'source_job',
        'scene_check_job',

        # Background jobs
        'create_torrent_job',
        'group_job',
        'mediainfo_job',
        'bdinfo_job',
        'resolution_job',
        'flags_job',
        'anonymous_job',
        'description_job',
        'rules_job',
    )
    for job_attr in job_attrs:
        mocker.patch.object(
            type(ant_tracker_jobs),
            job_attr,
            PropertyMock(return_value=Mock(attr=job_attr, prejobs=())),
        )


def test_jobs_before_upload_items(ant_tracker_jobs, mock_job_attributes, mocker):
    assert tuple(job.attr for job in ant_tracker_jobs.jobs_before_upload) == (
        # Interactive jobs
        'playlists_job',
        'tmdb_job',
        'source_job',
        'scene_check_job',

        # Background jobs
        'create_torrent_job',
        'group_job',
        'mediainfo_job',
        'bdinfo_job',
        'resolution_job',
        'flags_job',
        'anonymous_job',
        'description_job',
        'rules_job',
    )


def test_source_job(ant_tracker_jobs, mocker):
    ChoiceJob_mock = mocker.patch('upsies.jobs.dialog.ChoiceJob')
    mocker.patch.object(ant_tracker_jobs, 'get_job_name')
    mocker.patch.object(ant_tracker_jobs, 'make_precondition')
    mocker.patch.object(ant_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ant_tracker_jobs.source_job is ChoiceJob_mock.return_value
    assert ChoiceJob_mock.call_args_list == [call(
        name=ant_tracker_jobs.get_job_name.return_value,
        label='Source',
        precondition=ant_tracker_jobs.make_precondition.return_value,
        options=(
            ('Blu-ray', 'Blu-ray'),
            ('DVD', 'DVD'),
            ('WEB', 'WEB'),
            ('HD-DVD', 'HD-DVD'),
            ('HDTV', 'HDTV'),
            ('VHS', 'VHS'),
            ('TV', 'TV'),
            ('LaserDisc', 'LaserDisc'),
            ('Unknown', 'Unknown'),
        ),
        autodetect=ant_tracker_jobs.autodetect_source,
        autofinish=True,
        common_job_arg='common job argument',
    )]
    assert ant_tracker_jobs.get_job_name.call_args_list == [call('source')]
    assert ant_tracker_jobs.make_precondition.call_args_list == [call('source_job')]
    assert ant_tracker_jobs.common_job_args.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='release_name, exp_return_value',
    argvalues=(
        ({'source': 'BluRay Remux'}, 'Blu-ray'),
        ({'source': 'HD-DVD'}, 'HD-DVD'),
        ({'source': 'HDTV'}, 'HDTV'),
        ({'source': 'DVDRip'}, 'DVD'),
        ({'source': 'DVD5'}, 'DVD'),
        ({'source': 'DVD9'}, 'DVD'),
        ({'source': 'WEB-DL'}, 'WEB'),
        ({'source': 'WEBRip'}, 'WEB'),
        ({'source': 'VHS'}, 'VHS'),
        ({'source': 'VHSRip'}, 'VHS'),
        ({'source': 'TV'}, 'TV'),
        ({'source': 'TVRip'}, 'TV'),
        ({'source': 'IDUNNO'}, None),
        ({'source': 'UNKNOWN'}, None),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_source(release_name, exp_return_value, ant_tracker_jobs, mocker):
    mocker.patch.object(type(ant_tracker_jobs), 'release_name', Mock(**release_name))
    return_value = await ant_tracker_jobs.autodetect_source('ignored job instance')
    assert return_value == exp_return_value


def test_group_job(ant_tracker_jobs, mocker):
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    mocker.patch.object(ant_tracker_jobs, 'get_job_name')
    mocker.patch.object(ant_tracker_jobs, 'make_precondition')
    mocker.patch.object(ant_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ant_tracker_jobs.group_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [call(
        name=ant_tracker_jobs.get_job_name.return_value,
        label='Group',
        precondition=ant_tracker_jobs.make_precondition.return_value,
        text=ant_tracker_jobs.autodetect_group,
        validator=ant_tracker_jobs.validate_group,
        finish_on_success=True,
        common_job_arg='common job argument',
    )]
    assert ant_tracker_jobs.get_job_name.call_args_list == [call('group')]
    assert ant_tracker_jobs.make_precondition.call_args_list == [call('group_job')]
    assert ant_tracker_jobs.common_job_args.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='group, exp_return_value, exp_mock_calls',
    argvalues=(
        pytest.param(
            'ASDF123',
            'ASDF123',
            [],
            id='Valid group name',
        ),
        pytest.param(
            'ASDF-123',
            None,
            [call.set_text('ASDF-123')],
            id='Invalid group name',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_group(group, exp_return_value, exp_mock_calls, ant_tracker_jobs, mocker):
    mocker.patch.object(ant_tracker_jobs, 'release_name', Mock(group=group))
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(ant_tracker_jobs.group_job, 'set_text'), 'set_text')

    return_value = await ant_tracker_jobs.autodetect_group()
    assert return_value == exp_return_value
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='group, exp_exception',
    argvalues=(
        pytest.param(
            'ASDF123',
            None,
            id='Valid group name',
        ),
        pytest.param(
            'ASDF:123',
            ValueError('Release group must only contain alphanumeric characters (a-z, A-Z, 0-9)'),
            id='Invalid group name',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_validate_group(group, exp_exception, ant_tracker_jobs):
    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            ant_tracker_jobs.validate_group(group)
    else:
        return_value = ant_tracker_jobs.validate_group(group)
        assert return_value is None


def test_resolution_job(ant_tracker_jobs, mocker):
    ChoiceJob_mock = mocker.patch('upsies.jobs.dialog.ChoiceJob')
    mocker.patch.object(ant_tracker_jobs, 'get_job_name')
    mocker.patch.object(ant_tracker_jobs, 'make_precondition')
    mocker.patch.object(ant_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ant_tracker_jobs.resolution_job is ChoiceJob_mock.return_value
    assert ChoiceJob_mock.call_args_list == [call(
        name=ant_tracker_jobs.get_job_name.return_value,
        label='Resolution',
        precondition=ant_tracker_jobs.make_precondition.return_value,
        options=(
            'SD',
            '720p',
            '1080i',
            '1080p',
            '2160p',
        ),
        autodetect=ant_tracker_jobs.autodetect_resolution,
        autofinish=True,
        common_job_arg='common job argument',
    )]
    assert ant_tracker_jobs.get_job_name.call_args_list == [call('resolution')]
    assert ant_tracker_jobs.make_precondition.call_args_list == [call('resolution_job')]
    assert ant_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.parametrize(
    argnames='content_path, resolution_int, scan_type, exp_return_value, exp_mock_calls',
    argvalues=(
        ('path/to/content', 3000, None, '2160p', [call.get_resolution_int('path/to/content')]),
        ('path/to/content', 2160, None, '2160p', [call.get_resolution_int('path/to/content')]),
        ('path/to/content', 2159, 'i', '1080i', [call.get_resolution_int('path/to/content'), call.get_scan_type('path/to/content')]),
        ('path/to/content', 1080, 'p', '1080p', [call.get_resolution_int('path/to/content'), call.get_scan_type('path/to/content')]),
        ('path/to/content', 1079, None, '720p', [call.get_resolution_int('path/to/content')]),
        ('path/to/content', 720, None, '720p', [call.get_resolution_int('path/to/content')]),
        ('path/to/content', 719, None, 'SD', [call.get_resolution_int('path/to/content')]),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_resolution(content_path, resolution_int, scan_type, exp_return_value, exp_mock_calls, ant_tracker_jobs, mocker):
    mocker.patch.object(type(ant_tracker_jobs), 'content_path', PropertyMock(return_value=content_path))
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_resolution_int', return_value=resolution_int), 'get_resolution_int')
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_scan_type', return_value=scan_type), 'get_scan_type')

    return_value = await ant_tracker_jobs.autodetect_resolution('job_')
    assert return_value == exp_return_value
    assert mocks.mock_calls == exp_mock_calls


def test_flags_job(ant_tracker_jobs, mocker):
    CustomJob_mock = mocker.patch('upsies.jobs.custom.CustomJob')
    mocker.patch.object(ant_tracker_jobs, 'get_job_name')
    mocker.patch.object(ant_tracker_jobs, 'make_precondition')
    mocker.patch.object(ant_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ant_tracker_jobs.flags_job is CustomJob_mock.return_value
    assert CustomJob_mock.call_args_list == [call(
        name=ant_tracker_jobs.get_job_name.return_value,
        label='Flags',
        precondition=ant_tracker_jobs.make_precondition.return_value,
        worker=ant_tracker_jobs.autodetect_flags,
        no_output_is_ok=True,
        common_job_arg='common job argument',
    )]
    assert ant_tracker_jobs.get_job_name.call_args_list == [call('flags')]
    assert ant_tracker_jobs.make_precondition.call_args_list == [call('flags_job')]
    assert ant_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.parametrize(
    argnames='attrs, exp_flags',
    argvalues=(
        ({}, []),
        ({'edition': ["Director's Cut"]}, ['Directors']),
        ({'edition': ['Extended Cut']}, ['Extended']),
        ({'edition': ['Uncut']}, ['Uncut']),
        ({'edition': ['Unrated']}, ['Unrated']),
        ({'edition': ['Criterion Collection']}, ['Criterion']),
        ({'edition': ['IMAX']}, ['IMAX']),
        ({'edition': ['4k Remastered']}, ['4KRemaster']),
        ({'edition': ['Dual Audio']}, ['DualAudio']),
        ({'source': 'BluRay Remux'}, ['Remux']),
        ({'source': 'DVD Remux'}, ['Remux']),
        ({'hdr_formats': ('DV')}, ['DV']),
        ({'hdr_formats': ('HDR')}, []),
        ({'hdr_formats': ('HDR10')}, ['HDR10']),
        ({'hdr_formats': ('HDR10+')}, ['HDR10']),
        ({'audio_format': 'DDP Atmos'}, ['Atmos']),
        ({'has_commentary': False}, []),
        ({'has_commentary': True}, ['Commentary']),
        (
            {
                'edition': ["Director's Cut", 'Extended Cut', '4k Remastered'],
                'source': 'WEB-DL Remux',
                'hdr_formats': ('DV', 'HDR10+'),
                'audio_format': 'TrueHD Atmos',
                'has_commentary': True,
            },
            [
                "Directors", 'Extended', '4KRemaster',
                'Remux',
                'DV', 'HDR10',
                'Atmos',
                'Commentary',
            ],
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_flags(attrs, exp_flags, ant_tracker_jobs, mocker):
    mocker.patch.object(type(ant_tracker_jobs), 'release_name', PropertyMock(return_value=Mock(
        edition=attrs.get('edition', []),
        source=attrs.get('source', ''),
        audio_format=attrs.get('audio_format', ''),
        has_commentary=attrs.get('has_commentary', False),
    )))
    mocker.patch('upsies.utils.mediainfo.video.get_hdr_formats', return_value=attrs.get('hdr_formats', ()))

    return_value = await ant_tracker_jobs.autodetect_flags('job_')
    assert return_value == exp_flags


def test_anonymous_job(ant_tracker_jobs, mocker):
    ChoiceJob_mock = mocker.patch('upsies.jobs.dialog.ChoiceJob')
    mocker.patch.object(ant_tracker_jobs, 'get_job_name')
    mocker.patch.object(ant_tracker_jobs, 'make_precondition')
    mocker.patch.object(ant_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})

    assert ant_tracker_jobs.anonymous_job is ChoiceJob_mock.return_value
    assert ChoiceJob_mock.call_args_list == [call(
        name=ant_tracker_jobs.get_job_name.return_value,
        label='Anonymous',
        precondition=ant_tracker_jobs.make_precondition.return_value,
        options=(
            ('No', False),
            ('Yes', True),
        ),
        autodetect=ant_tracker_jobs.autodetect_anonymous,
        autofinish=True,
        common_job_arg='common job argument',
    )]
    assert ant_tracker_jobs.get_job_name.call_args_list == [call('anonymous')]
    assert ant_tracker_jobs.make_precondition.call_args_list == [call('anonymous_job')]
    assert ant_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.parametrize(
    argnames='options, exp_return_value',
    argvalues=(
        ({'anonymous': True}, True),
        ({'anonymous': False}, False),
        ({}, False),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_autodetect_anonymous(options, exp_return_value, ant_tracker_jobs, mocker):
    mocker.patch.object(type(ant_tracker_jobs), 'options', PropertyMock(return_value=options))
    return_value = await ant_tracker_jobs.autodetect_anonymous('ignored job instance')
    assert return_value is exp_return_value


def test_description_job(ant_tracker_jobs, mocker):
    TextFieldJob_mock = mocker.patch('upsies.jobs.dialog.TextFieldJob')
    mocker.patch.object(ant_tracker_jobs, 'get_job_name')
    mocker.patch.object(ant_tracker_jobs, 'make_precondition')
    mocker.patch.object(ant_tracker_jobs, 'common_job_args', return_value={'common_job_arg': 'common job argument'})
    mocker.patch.object(type(ant_tracker_jobs), 'mediainfo_job', PropertyMock())
    mocker.patch.object(type(ant_tracker_jobs), 'bdinfo_job', PropertyMock())

    assert ant_tracker_jobs.description_job is TextFieldJob_mock.return_value
    assert TextFieldJob_mock.call_args_list == [call(
        name=ant_tracker_jobs.get_job_name.return_value,
        label='Description',
        precondition=ant_tracker_jobs.make_precondition.return_value,
        prejobs=(
            ant_tracker_jobs.mediainfo_job,
            ant_tracker_jobs.bdinfo_job,
        ),
        text=ant_tracker_jobs.generate_description,
        error_exceptions=(
            errors.ContentError,
        ),
        hidden=True,
        finish_on_success=True,
        read_only=True,
        common_job_arg='common job argument',
    )]
    assert ant_tracker_jobs.get_job_name.call_args_list == [call('description')]
    assert ant_tracker_jobs.make_precondition.call_args_list == [call('description_job')]
    assert ant_tracker_jobs.common_job_args.call_args_list == [call(ignore_cache=True)]


@pytest.mark.parametrize(
    argnames='nfo, exp_nfo_included',
    argvalues=(
        (None, False),
        ('', False),
        ('<nfo>', False),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='bdinfo, exp_bdinfo_included',
    argvalues=(
        (None, False),
        ('', False),
        ('<bdinfo report>', True),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='mediainfo, exp_mediainfo_included',
    argvalues=(
        (None, False),
        ('', False),
        ('<mediainfo report>', True),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='promotion_bbcode, exp_promotion_included',
    argvalues=(
        ('<promotion>', True),
        ('', False),
    ),
    ids=lambda v: repr(v),
)
def test_generate_description(
        nfo, exp_nfo_included,
        mediainfo, exp_mediainfo_included,
        bdinfo, exp_bdinfo_included,
        promotion_bbcode, exp_promotion_included,
        ant_tracker_jobs, mocker,
):
    exp_description = []

    mocker.patch.object(ant_tracker_jobs, 'generate_description_nfo', return_value=nfo)
    if exp_nfo_included and isinstance(nfo, str):
        exp_description.append(nfo)

    mocker.patch.object(ant_tracker_jobs, 'generate_description_mediainfo', return_value=mediainfo)
    if exp_mediainfo_included:
        exp_description.append(mediainfo)

    mocker.patch.object(ant_tracker_jobs, 'generate_description_bdinfo', return_value=bdinfo)
    if exp_bdinfo_included:
        exp_description.append(bdinfo)

    mocker.patch.object(type(ant_tracker_jobs), 'promotion_bbcode', PropertyMock(return_value=promotion_bbcode))
    if exp_promotion_included:
        if exp_description:
            exp_description.append('\n' + promotion_bbcode)
        else:
            exp_description.append(promotion_bbcode)

    return_value = ant_tracker_jobs.generate_description()
    assert return_value == '\n'.join(exp_description)
    assert ant_tracker_jobs.generate_description_nfo.call_args_list == []
    assert ant_tracker_jobs.generate_description_bdinfo.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='bdinfo_job_is_enabled, reports_by_file, exp_return_value',
    argvalues=(
        (False, (), None),
        (
            True,
            {},
            None,
        ),
        (
            True,
            {
                '00003.mpls': Mock(quick_summary='<bdinfo report for 00003.mpls>'),
                '00006.mpls': Mock(quick_summary='<bdinfo report for 00006.mpls>'),
                '00009.mpls': Mock(quick_summary='<bdinfo report for 00009.mpls>'),
            },
            (
                '[spoiler=relative/path/of/00003.mpls]<bdinfo report for 00003.mpls>[/spoiler]\n'
                '[spoiler=relative/path/of/00006.mpls]<bdinfo report for 00006.mpls>[/spoiler]\n'
                '[spoiler=relative/path/of/00009.mpls]<bdinfo report for 00009.mpls>[/spoiler]'
            )
        ),
    ),
    ids=lambda v: repr(v),
)
def test_generate_description_bdinfo(bdinfo_job_is_enabled, reports_by_file, exp_return_value, ant_tracker_jobs, mocker):
    mocker.patch.object(type(ant_tracker_jobs), 'bdinfo_job', PropertyMock(return_value=Mock(
        is_enabled=bdinfo_job_is_enabled,
        reports_by_file=reports_by_file,
    )))
    mocker.patch.object(ant_tracker_jobs, 'get_relative_file_path', side_effect=lambda file: f'relative/path/of/{file}')
    return_value = ant_tracker_jobs.generate_description_bdinfo()
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='mediainfo_job_is_enabled, reports_by_file, exp_return_value',
    argvalues=(
        (False, (), None),
        (
            True,
            {},
            None,
        ),
        (
            True,
            {
                'foo.mkv': '<mediainfo report for foo.mkv>',
            },
            None,
        ),
        (
            True,
            {
                'foo.mkv': '<mediainfo report for foo.mkv>',
                'bar.mkv': '<mediainfo report for bar.mkv>',
            },
            (
                '[spoiler=relative/path/of/foo.mkv]<mediainfo report for foo.mkv>[/spoiler]\n'
                '[spoiler=relative/path/of/bar.mkv]<mediainfo report for bar.mkv>[/spoiler]'
            )
        ),
    ),
    ids=lambda v: repr(v),
)
def test_generate_description_mediainfo(mediainfo_job_is_enabled, reports_by_file, exp_return_value, ant_tracker_jobs, mocker):
    mocker.patch.object(type(ant_tracker_jobs), 'mediainfo_job', PropertyMock(return_value=Mock(
        is_enabled=mediainfo_job_is_enabled,
        reports_by_file=reports_by_file,
    )))
    mocker.patch.object(ant_tracker_jobs, 'get_relative_file_path', side_effect=lambda file: f'relative/path/of/{file}')
    return_value = ant_tracker_jobs.generate_description_mediainfo()
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='nfo, exp_return_value',
    argvalues=(
        ('', None),
        ('<nfo>', '[spoiler=NFO][pre]<nfo>[/pre][/spoiler]'),
    ),
    ids=lambda v: repr(v),
)
def test_generate_description_nfo(nfo, exp_return_value, ant_tracker_jobs, mocker):
    mocker.patch.object(ant_tracker_jobs, 'read_nfo', return_value=nfo)

    return_value = ant_tracker_jobs.generate_description_nfo()
    assert return_value == exp_return_value
    assert ant_tracker_jobs.read_nfo.call_args_list == [call(strip=True)]


def test_mediainfo_required_for_bdmv(ant_tracker_jobs, mocker):
    assert ant_tracker_jobs.mediainfo_required_for_bdmv is True


@pytest.mark.parametrize('anonymous, exp_anonymous_post_data', (
    (False, {'anonymous': None}),
    (True, {'anonymous': '1'}),
))
@pytest.mark.parametrize('description, exp_description_post_data', (
    ('[description]', {'release_desc': '[description]'}),
    ('', {'release_desc': None}),
))
@pytest.mark.parametrize('is_scene, exp_is_scene_post_data', (
    (False, {'censored': None}),
    (True, {'censored': '1'}),
))
async def test_post_data(
        anonymous, exp_anonymous_post_data,
        description, exp_description_post_data,
        is_scene, exp_is_scene_post_data,
        ant_tracker_jobs, mocker,
):
    mocker.patch.object(type(ant_tracker_jobs), 'tmdb_job', PropertyMock())
    mocker.patch.object(type(ant_tracker_jobs), 'mediainfo_job', PropertyMock())
    mocker.patch.object(type(ant_tracker_jobs), 'description_job', PropertyMock())
    mocker.patch.object(type(ant_tracker_jobs), 'resolution_job', PropertyMock())
    mocker.patch.object(type(ant_tracker_jobs), 'flags_job', PropertyMock())
    mocker.patch.object(type(ant_tracker_jobs), 'scene_check_job', PropertyMock())
    mocker.patch.object(type(ant_tracker_jobs), 'anonymous_job', PropertyMock())
    mocker.patch.object(type(ant_tracker_jobs), '_post_data_release_group', PropertyMock(return_value={
        'release_group': 'info',
    }))
    mocker.patch.object(ant_tracker_jobs, '_tracker', Mock(
        apikey='d34db33f',
    ))
    mocker.patch.object(ant_tracker_jobs, 'get_job_output', side_effect=(
        'movie/123456',
        '[mediainfo]',
        description,
        ('Uncut', 'Commentary'),
    ))
    mocker.patch.object(ant_tracker_jobs, 'get_job_attribute', side_effect=(
        is_scene,
        anonymous,
        'Blu-DL',
        '9000p',
    ))
    options = {}
    if anonymous is not None:
        options['anonymous'] = anonymous
    mocker.patch.object(type(ant_tracker_jobs), 'options', PropertyMock(return_value=options))

    exp_post_data = {
        'api_key': 'd34db33f',
        'action': 'upload',
        'tmdbid': '123456',
        'mediainfo': '[mediainfo]',
        'flags[]': ('Uncut', 'Commentary'),
        'release_group': 'info',
        'media': 'Blu-DL',
        'ressel': '9000p',
    }
    exp_post_data.update(exp_is_scene_post_data)
    exp_post_data.update(exp_anonymous_post_data)
    exp_post_data.update(exp_description_post_data)

    assert ant_tracker_jobs.post_data == exp_post_data
    assert ant_tracker_jobs.get_job_output.call_args_list == [
        call(ant_tracker_jobs.tmdb_job, slice=0),
        call(ant_tracker_jobs.mediainfo_job, slice=0),
        call(ant_tracker_jobs.description_job, slice=0),
        call(ant_tracker_jobs.flags_job),
    ]
    assert ant_tracker_jobs.get_job_attribute.call_args_list == [
        call(ant_tracker_jobs.scene_check_job, 'is_scene_release'),
        call(ant_tracker_jobs.anonymous_job, 'choice'),
        call(ant_tracker_jobs.source_job, 'choice'),
        call(ant_tracker_jobs.resolution_job, 'choice'),
    ]


@pytest.mark.parametrize(
    argnames='group, exp_return_value',
    argvalues=(
        ('NOGROUP', {'noreleasegroup': 'on'}),
        ('ASDF', {'releasegroup': 'ASDF'}),
    ),
    ids=lambda v: repr(v),
)
async def test__post_data_release_group(group, exp_return_value, ant_tracker_jobs, mocker):
    mocker.patch.object(ant_tracker_jobs, 'get_job_output', return_value=group)
    assert ant_tracker_jobs._post_data_release_group == exp_return_value


async def test_post_files(ant_tracker_jobs, mocker):
    mocker.patch.object(type(ant_tracker_jobs), 'torrent_filepath', PropertyMock(
        return_value='path/to/torrent',
    ))

    assert ant_tracker_jobs.post_files == {
        'file_input': {
            'file': 'path/to/torrent',
            'mimetype': 'application/x-bittorrent',
        },
    }
