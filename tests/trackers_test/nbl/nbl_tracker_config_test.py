import base64

import pytest

from upsies import utils
from upsies.trackers import nbl


@pytest.fixture
def tracker_config():
    return nbl.config.NblTrackerConfig()


def test_NblTrackerConfig_defaults(tracker_config):
    assert set(tracker_config) == {
        'anonymous',
        'upload_url',
        'announce_url',
        'apikey',
        'randomize_infohash',
        'exclude',

        # Inherited from TrackerConfigBase
        'add_to',
        'copy_to',
    }


def test_NblTrackerConfig_defaults_anonymous(tracker_config):
    assert tracker_config['anonymous'] == utils.types.Bool('no')
    assert tracker_config['anonymous'].description == 'Whether your username is displayed on your uploads.'


def test_NblTrackerConfig_defaults_base_url(tracker_config):
    assert tracker_config['upload_url'] == base64.b64decode('aHR0cHM6Ly9uZWJ1bGFuY2UuaW8vdXBsb2FkLnBocA==').decode('ascii')


def test_NblTrackerConfig_defaults_password(tracker_config):
    assert tracker_config['apikey'] == ''
    assert tracker_config['apikey'].description == (
        'Your personal private API key.\n'
        'Get it from the website: <USERNAME> -> Settings -> API keys'
    )


def test_NblTrackerConfig_defaults_announce_url(tracker_config):
    assert tracker_config['announce_url'] == ''
    assert tracker_config['announce_url'].description == (
        'The complete announce URL with your private passkey.\n'
        'Get it from the website: Shows -> Upload -> Your personal announce URL'
    )


def test_NblTrackerConfig_defaults_exclude(tracker_config):
    assert tracker_config['exclude'] == ()


def test_NblTrackerConfig_arguments(tracker_config):
    exp_argument_definitions = {
        'submit': {
            ('--anonymous', '--an'),
            ('--tvmaze', '--tv'),
        },
    }
    assert set(tracker_config.argument_definitions) == set(exp_argument_definitions)
    for command, exp_arguments in exp_argument_definitions.items():
        assert set(tracker_config.argument_definitions[command]) == exp_arguments


def test_NblTrackerConfig_argument_definitions_submit_tvmaze(tracker_config):
    assert tracker_config.argument_definitions['submit'][('--tvmaze', '--tv')] == {
        'help': 'TVmaze ID or URL',
        'type': utils.argtypes.webdb_id('tvmaze'),
    }


def test_NblTrackerConfig_argument_definitions_submit_anonymous(tracker_config):
    assert tracker_config.argument_definitions['submit'][('--anonymous', '--an')] == {
        'help': 'Hide your username for this submission',
        'action': 'store_true',
        # This must be `None` so it doesn't override the "anonymous"
        # value from the config file. See CommandBase.get_options().
        'default': None,
    }
