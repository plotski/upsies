import re
from unittest.mock import Mock, PropertyMock

import pytest

from upsies import errors
from upsies.trackers.base import rules as base_rules
from upsies.trackers.uhd import rules as uhd_rules


def test_UhdHdOnly():
    assert issubclass(uhd_rules.UhdHdOnly, base_rules.HdOnly)


@pytest.mark.parametrize(
    argnames='has_dual_audio, exp_exception',
    argvalues=(
        (False, None),
        (True, errors.RuleBroken('No English dubs except for animated content')),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_UhdNoEnglishDubs(has_dual_audio, exp_exception, mocker):
    assert issubclass(uhd_rules.UhdNoEnglishDubs, base_rules.TrackerRuleBase)

    rule = uhd_rules.UhdNoEnglishDubs(Mock())
    mocker.patch.object(type(rule), 'release_name', PropertyMock(return_value=Mock(has_dual_audio=has_dual_audio)))
    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            await rule._check()
    else:
        return_value = await rule._check()
        assert return_value is None
