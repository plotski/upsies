import asyncio
import re
from unittest.mock import AsyncMock, Mock, call

import pytest

from upsies.uis.tui.tui import TUI


class Job:
    def __init__(self, name, **kwargs):
        self.name = name
        for k, v in kwargs.items():
            setattr(self, k, v)

    def __repr__(self):
        return f'<{type(self).__name__} {self.name!r}>'


class JobObjects:
    def __init__(self, job, widget, container):
        self.job = job
        self.widget = widget
        self.container = container

    def __repr__(self):
        return f'<{type(self).__name__} {self.job.name!r}>'


class JobContainer:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f'<{type(self).__name__} {self.name!r}>'


@pytest.fixture
def tui(event_loop):
    tui = TUI()
    assert tui._loop is event_loop
    return tui


def test_uncaught_exception(tui, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tui, '_jobrunner'), '_jobrunner')

    counter = 0

    def bad_callback(task):
        nonlocal counter
        counter += 1
        raise RuntimeError(f'bad, bad, bad: {counter}')

    async def with_running_loop():
        for _ in range(3):
            task = asyncio.create_task(AsyncMock()())
            task.add_done_callback(bad_callback)
            await task

    tui._loop.run_until_complete(with_running_loop())

    assert type(tui._unhandled_exception) is RuntimeError
    assert str(tui._unhandled_exception) == 'bad, bad, bad: 1'
    assert mocks.mock_calls == [
        call._jobrunner.terminate(reason="Unhandled exception: RuntimeError('bad, bad, bad: 1')"),
    ]


def test__add_jobs(tui, mocker):
    jobs = {
        'a': Job(name='a'),
        'b': Job(name='b'),
        'c': Job(name='c'),
    }

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tui, '_add_job'), '_add_job')
    mocks.attach_mock(mocker.patch.object(tui, '_update_jobs_container'), '_update_jobs_container')
    mocks.attach_mock(mocker.patch.object(tui, '_connect_jobs'), '_connect_jobs')

    tui._add_jobs(tuple(jobs.values()))

    assert mocks.mock_calls == [
        call._add_job(jobs['a']),
        call._add_job(jobs['b']),
        call._add_job(jobs['c']),
        call._update_jobs_container(),
        call._connect_jobs(tuple(jobs.values())),
    ]


def test__add_job(tui, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tui, '_jobrunner'), '_jobrunner')
    mocks.attach_mock(mocker.patch('upsies.uis.tui.jobwidgets.JobWidget'), 'JobWidget')
    mocks.attach_mock(mocker.patch('upsies.uis.tui.tui.to_container'), 'to_container')

    job = Job(name='a')
    tui._add_job(job)
    assert tui._widgets['a'] is mocks.JobWidget.return_value
    assert tui._containers['a'] is mocks.to_container.return_value
    assert mocks.mock_calls == [
        call._jobrunner.add(job),
        call.JobWidget(job, tui._app),
        call.to_container(mocks.JobWidget.return_value),
    ]


def test__connect_jobs(tui):
    mocks = Mock()
    jobs = [
        mocks.foo,
        mocks.bar,
        mocks.baz,
    ]
    tui._connect_jobs(jobs)
    assert mocks.mock_calls == [
        call.foo.signal.register('finished', tui._handle_job_finished),
        call.foo.signal.register('refresh_ui', tui._refresh),
        call.bar.signal.register('finished', tui._handle_job_finished),
        call.bar.signal.register('refresh_ui', tui._refresh),
        call.baz.signal.register('finished', tui._handle_job_finished),
        call.baz.signal.register('refresh_ui', tui._refresh),
    ]


@pytest.mark.parametrize(
    argnames='job, exp_exception, exp_mock_calls',
    argvalues=(
        pytest.param(
            Job(name='my_job', exit_code=1, is_finished=False),
            AssertionError('my_job is actually not finished'),
            [],
            id='Finished job is actually not finished',
        ),
        pytest.param(
            Job(name='my_job', exit_code=0, is_finished=True),
            None,
            [call._refresh()],
            id='Finished job succeeded',
        ),
        pytest.param(
            Job(name='my_job', exit_code=1, is_finished=True, raised='MyException'),
            None,
            [
                call._refresh(),
                call._jobrunner.terminate(reason="Job failed: my_job: 'MyException'"),
            ],
            id='Finished job failed',
        ),
    ),
)
def test__handle_job_finished(job, exp_exception, exp_mock_calls, tui, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tui, '_refresh'), '_refresh')
    mocks.attach_mock(mocker.patch.object(tui, '_jobrunner'), '_jobrunner')

    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            tui._handle_job_finished(job)
    else:
        tui._handle_job_finished(job)
    assert mocks.mock_calls == exp_mock_calls


def test__refresh(tui, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tui, '_jobrunner'), '_jobrunner')
    mocks.attach_mock(mocker.patch.object(tui, '_update_jobs_container'), '_update_jobs_container')
    mocks.attach_mock(mocker.patch.object(tui, '_app'), '_app')

    tui._refresh()

    assert mocks.mock_calls == [
        call._jobrunner.start_more_jobs(),
        call._update_jobs_container(),
        call._app.invalidate(),
    ]


@pytest.mark.parametrize(
    argnames='jobs, focused_job_name, exp_return_value',
    argvalues=(
        ({}, None, None),
        ({'my_job': '<my_job instance>'}, 'my_job', '<my_job instance>'),
    ),
)
def test__focused_job(jobs, focused_job_name, exp_return_value, tui, mocker):
    mocker.patch.object(tui, '_focused_job_name', focused_job_name)
    mocker.patch.object(tui, '_jobrunner', jobs)

    assert tui._focused_job == exp_return_value


def test__update_jobs_container(tui, mocker):
    objects = {
        # Interactive jobs
        'ai': JobObjects(job=Job('ai', is_started=False, is_finished=False), widget=Mock(is_interactive=True), container=JobContainer('aic')),
        'bi': JobObjects(job=Job('bi', is_started=True, is_finished=False), widget=Mock(is_interactive=True), container=JobContainer('bic')),
        'ci': JobObjects(job=Job('ci', is_started=True, is_finished=True), widget=Mock(is_interactive=True), container=JobContainer('cic')),
        'di': JobObjects(job=Job('di', is_started=True, is_finished=False), widget=Mock(is_interactive=True), container=JobContainer('dic')),

        # Background jobs
        '1b': JobObjects(job=Job('1b', is_started=False, is_finished=False), widget=Mock(is_interactive=False), container=JobContainer('1bc')),
        '2b': JobObjects(job=Job('2b', is_started=True, is_finished=False), widget=Mock(is_interactive=False), container=JobContainer('2bc')),
        '3b': JobObjects(job=Job('3b', is_started=True, is_finished=True), widget=Mock(is_interactive=False), container=JobContainer('3bc')),
    }
    jobs_container_id = id(tui._jobs_container)

    def get_job(self, name):
        return objects[name].job

    # tui._layout.focus() may raise ValueError, which should be ignored.
    def layout_focus(container):
        if container in (
                objects['bi'].container,
                objects['2b'].container,
        ):
            raise ValueError(f'Unfocusable container: {container!r}')

    mocker.patch.object(tui._layout, 'focus', side_effect=layout_focus)

    def assert_jobs_container(*children, focused):
        tui._update_jobs_container()

        exp_children = [objects[child].container for child in children]
        assert tui._jobs_container.children == exp_children

        # The new sequence must not replace the old one, it must be updated in place.
        assert id(tui._jobs_container) == jobs_container_id

        if focused:
            focused = objects[focused]
            assert tui._focused_job_name == focused.job.name
            assert tui._layout.focus.call_args_list == [call(focused.container)]
        else:
            assert tui._focused_job_name is None
            assert tui._layout.focus.call_args_list == []
        tui._layout.focus.reset_mock()

    # No enabled jobs must be handled gracefully.
    mocker.patch.object(tui, '_jobrunner', Mock(enabled_jobs=()))
    assert_jobs_container(focused=None)

    # Add mock jobs.
    mocker.patch.object(tui, '_jobrunner', Mock(
        enabled_jobs=tuple(obj.job for obj in objects.values()),
        __getitem__=get_job,
    ))
    mocker.patch.object(tui, '_widgets', {job_name: obj.widget for job_name, obj in objects.items()})
    mocker.patch.object(tui, '_containers', {job_name: obj.container for job_name, obj in objects.items()})

    # All finished and all background jobs are listed.
    # First interactive, started and unfinished job is focused.
    assert_jobs_container('bi', 'ci', '1b', '2b', '3b', focused='bi')

    # Finishing background job should have no effect
    objects['2b'].job.is_finished = True
    assert_jobs_container('bi', 'ci', '1b', '2b', '3b', focused='bi')

    # Finishing focused job should add and focus the next interactive job (di). Finished job bi is
    # still displayed. ci is also displayed because it was finished from the start, we just didn't
    # notice until bi finished.
    objects['bi'].job.is_finished = True
    assert_jobs_container('bi', 'ci', 'di', '1b', '2b', '3b', focused='di')

    # Starting previously unstarted interactive job should add it, but it must not steal focus.
    objects['ai'].job.is_finished = True
    assert_jobs_container('ai', 'bi', 'ci', 'di', '1b', '2b', '3b', focused='di')

    # Starting and finishing remaining background jobs should have no effect.
    objects['1b'].job.is_started = True
    assert_jobs_container('ai', 'bi', 'ci', 'di', '1b', '2b', '3b', focused='di')
    objects['1b'].job.is_finished = True
    assert_jobs_container('ai', 'bi', 'ci', 'di', '1b', '2b', '3b', focused='di')

    # Finishing focused job should unfocus if there is no interactive, started and unfinished job.
    objects['di'].job.is_finished = True
    assert_jobs_container('ai', 'bi', 'ci', 'di', '1b', '2b', '3b', focused=None)


def test_run(tui, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tui, '_loop'), '_loop')
    mocks.attach_mock(mocker.patch.object(tui, 'run_async', Mock()), 'run_async')
    jobs = ('job1', 'job2', 'job3')
    tui.run(jobs)
    assert mocks.mock_calls == [
        call.run_async(jobs),
        call._loop.run_until_complete(tui.run_async.return_value),
    ]


@pytest.mark.asyncio
async def test_run_async(tui, mocker):
    jobs = ('job1', 'job2', 'job3')

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(tui, '_app', Mock(run_async=AsyncMock())), '_app')
    mocks.attach_mock(mocker.patch.object(tui, '_add_jobs'), '_add_jobs')
    mocks.attach_mock(mocker.patch.object(tui, '_jobrunner', Mock(wait=AsyncMock(), all_jobs=jobs)), '_jobrunner')
    mocks.attach_mock(mocker.patch.object(tui, '_maybe_raise_exception'), '_maybe_raise_exception')
    mocks.attach_mock(mocker.patch.object(tui, '_get_exit_code'), '_get_exit_code')

    return_value = await tui.run_async(jobs)
    assert return_value is tui._get_exit_code.return_value

    assert mocks.mock_calls == [
        call._app.run_async(set_exception_handler=False),
        call._add_jobs(jobs),
        call._jobrunner.start_more_jobs(),
        call._jobrunner.wait(),
        call._maybe_raise_exception(),
        call._get_exit_code(),
    ]


@pytest.mark.parametrize(
    argnames='unhandled_exception, jobrunner_exceptions, exp_exception',
    argvalues=(
        (
            ValueError('application borked'),
            (TypeError('job1 freaked out'), TypeError('job2 went crazy')),
            ValueError('application borked'),
        ),
        (
            None,
            (TypeError('job1 freaked out'), TypeError('job2 went crazy')),
            TypeError('job1 freaked out'),
        ),
        (
            None,
            (),
            None,

        ),
    ),
)
def test__maybe_raise_exception(unhandled_exception, jobrunner_exceptions, exp_exception, tui, mocker):
    mocker.patch.object(tui, '_unhandled_exception', unhandled_exception)
    mocker.patch.object(tui, '_jobrunner', Mock(exceptions=jobrunner_exceptions))

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            tui._maybe_raise_exception()
    else:
        return_value = tui._maybe_raise_exception()
        assert return_value is None


@pytest.mark.parametrize(
    argnames='jobs, exp_return_value',
    argvalues=(
        (
            (Job('a', exit_code=1), Job('b', exit_code=None), Job('b', exit_code=3), Job('b', exit_code=4)),
            1,
        ),
        (
            (Job('a', exit_code=0), Job('b', exit_code=None), Job('b', exit_code=3), Job('b', exit_code=4)),
            3,
        ),
        (
            (Job('a', exit_code=0), Job('b', exit_code=0), Job('b', exit_code=None), Job('b', exit_code=4)),
            4,
        ),
        (
            (Job('a', exit_code=0), Job('b', exit_code=0), Job('b', exit_code=None), Job('b', exit_code=0)),
            0,
        ),
        (
            (Job('a', exit_code=None), Job('b', exit_code=None), Job('b', exit_code=None), Job('b', exit_code=None)),
            0,
        ),
    ),
    ids=lambda v: repr(v),
)
def test__get_exit_code(jobs, exp_return_value, tui, mocker):
    mocker.patch.object(tui, '_jobrunner', Mock(all_jobs=jobs))
    return_value = tui._get_exit_code()
    assert return_value == exp_return_value
