import sys
from unittest.mock import Mock

import pytest

from upsies.uis.tui import utils


@pytest.mark.parametrize(
    argnames='stdin_isatty, stdout_isatty, stderr_isatty',
    argvalues=(
        pytest.param(False, False, False, id='stdin_isatty=False, stdout_isatty=False, stderr_isatty=False'),
        pytest.param(False, False, True, id='stdin_isatty=False, stdout_isatty=False, stderr_isatty=True'),
        pytest.param(False, True, False, id='stdin_isatty=False, stdout_isatty=True, stderr_isatty=False'),
        pytest.param(False, True, True, id='stdin_isatty=False, stdout_isatty=True, stderr_isatty=True'),
        pytest.param(True, False, False, id='stdin_isatty=True, stdout_isatty=False, stderr_isatty=False'),
        pytest.param(True, False, True, id='stdin_isatty=True, stdout_isatty=False, stderr_isatty=True'),
        pytest.param(True, True, False, id='stdin_isatty=True, stdout_isatty=True, stderr_isatty=False'),
        pytest.param(True, True, True, id='stdin_isatty=True, stdout_isatty=True, stderr_isatty=True'),
    ),
)
@pytest.mark.parametrize(
    argnames='stdin, stdout, stderr',
    argvalues=(
        pytest.param(None, None, None, id='stdin=no, stdout=no, stderr=no'),
        pytest.param(None, None, Mock(name='stderr'), id='stdin=no, stdout=no, stderr=yes'),
        pytest.param(None, Mock(name='stdout'), None, id='stdin=no, stdout=yes, stderr=no'),
        pytest.param(None, Mock(name='stdout'), Mock(name='stderr'), id='stdin=no, stdout=yes, stderr=yes'),
        pytest.param(Mock(name='stdin'), None, None, id='stdin=yes, stdout=no, stderr=no'),
        pytest.param(Mock(name='stdin'), None, Mock(name='stderr'), id='stdin=yes, stdout=no, stderr=yes'),
        pytest.param(Mock(name='stdin'), Mock(name='stdout'), None, id='stdin=yes, stdout=yes, stderr=no'),
        pytest.param(Mock(name='stdin'), Mock(name='stdout'), Mock(name='stderr'), id='stdin=yes, stdout=yes, stderr=yes'),
    ),
)
def test_is_tty(stdin, stdout, stderr, stdin_isatty, stdout_isatty, stderr_isatty, mocker):
    if stdin:
        stdin.isatty = lambda: stdin_isatty
    if stdout:
        stdout.isatty = lambda: stdout_isatty
    if stderr:
        stderr.isatty = lambda: stderr_isatty
    mocker.patch('sys.stdin', stdin)
    mocker.patch('sys.stdout', stdout)
    mocker.patch('sys.stderr', stderr)

    if not stdin or not stdin.isatty():
        sys.__stdout__.write('False because no stdin usable\n')
        exp_is_tty = False
    elif (stdout and stdout.isatty()) or (stderr and stderr.isatty()):
        sys.__stdout__.write('True because stdout or stderr is usable\n')
        exp_is_tty = True
    else:
        sys.__stdout__.write('False because default\n')
        exp_is_tty = False

    assert utils.is_tty() is exp_is_tty
