from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from upsies import errors
from upsies.jobs.torrent import AddTorrentJob


@pytest.fixture
def btclient():
    class MockBtClient:
        name = 'mocksy'
        label = 'Mocksy'
        add_torrent = AsyncMock()

        def __repr__(self):
            return '<BtClient instance>'

    return MockBtClient()


@pytest.fixture
async def make_AddTorrentJob(tmp_path, btclient):
    def make_AddTorrentJob(torrent_files=()):
        return AddTorrentJob(
            home_directory=tmp_path,
            cache_directory=tmp_path,
            ignore_cache=False,
            btclient=btclient,
            torrent_files=torrent_files,
        )
    return make_AddTorrentJob


def test_cache_id(make_AddTorrentJob):
    job = make_AddTorrentJob()
    assert job.cache_id is None


@pytest.mark.parametrize('is_started, exp_hidden', (
    (False, True),
    (True, False),
))
def test_hidden(is_started, exp_hidden, make_AddTorrentJob, mocker):
    job = make_AddTorrentJob()
    mocker.patch.object(type(job), 'is_started', PropertyMock(return_value=is_started))
    assert job.hidden is exp_hidden


@pytest.mark.parametrize(
    argnames='signal, args, exp_info',
    argvalues=(
        ('adding', ('path/to.torrent',), 'Adding to.torrent'),
        ('added', ('ignored argument',), ''),
        ('finished', ('ignored argument',), ''),
        ('error', ('ignored argument',), ''),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_info_property_is_set(signal, args, exp_info, make_AddTorrentJob):
    job = make_AddTorrentJob()
    job.info = 'foo!'
    assert job.info == 'foo!'
    job.signal.emit(signal, *args)
    assert job.info == exp_info


class AsyncIter:
    def __init__(self, *items):
        self.items = items

    async def __aiter__(self):
        for item in self.items:
            yield item

@pytest.mark.parametrize(
    argnames='torrent_files, exp_torrent_files_mock_calls',
    argvalues=(
        pytest.param(
            (),
            [],
            id='no torrent_files provided',
        ),
        pytest.param(
            ('1.torrent', '2.torrent', '3.torrent'),
            [
                call._add_torrent('1.torrent'),
                call._add_torrent('2.torrent'),
                call._add_torrent('3.torrent'),
            ],
            id='torrent_files provided',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='mocked, exp_torrent_job_mock_calls',
    argvalues=(
        pytest.param(
            {
                'siblings': PropertyMock(return_value={}),
                'receive_all': Mock(return_value=AsyncIter(
                    ('path/to/first.torrent',),
                    ('path/to/second.torrent',),
                    ('path/to/third.torrent',),
                )),
                '_add_torrent': AsyncMock(),
            },
            [],
            id='torrent job does not exists',
        ),
        pytest.param(
            {
                'siblings': PropertyMock(return_value={'torrent': '<CreateTorrentJob>'}),
                'receive_all': Mock(return_value=AsyncIter(
                    ('path/to/first.torrent',),
                    ('path/to/second.torrent',),
                    ('path/to/third.torrent',),
                )),
                '_add_torrent': AsyncMock(),
            },
            [
                call.receive_all('torrent', 'output', only_posargs=True),
                call._add_torrent('path/to/first.torrent'),
                call._add_torrent('path/to/second.torrent'),
                call._add_torrent('path/to/third.torrent'),
            ],
            id='torrent job exists',
        ),
    ),
    ids=lambda v: repr(v),
)
async def test_run(
        torrent_files, exp_torrent_files_mock_calls,
        mocked, exp_torrent_job_mock_calls,
        make_AddTorrentJob, mocker,
):
    job = make_AddTorrentJob(torrent_files=torrent_files)

    mocks = Mock()
    for attr, mock in mocked.items():
        if isinstance(mock, PropertyMock):
            mocker.patch.object(type(job), attr, mock)
        else:
            mocks.attach_mock(mocker.patch.object(job, attr, mock), attr)

    return_value = await job.run()
    assert return_value is None
    assert mocks.mock_calls == exp_torrent_files_mock_calls + exp_torrent_job_mock_calls


@pytest.mark.parametrize(
    argnames='torrent_path, add_torrent_result, exp_calls',
    argvalues=(
        pytest.param(
            'my.torrent',
            errors.TorrentAddError('bad torrent'),
            [
                call.emit('adding', 'my.torrent'),
                call.add_torrent('my.torrent'),
                call.error(errors.TorrentAddError('bad torrent')),
            ],
            id='Adding fails',
        ),
        pytest.param(
            'my.torrent',
            'd34db33f',
            [
                call.emit('adding', 'my.torrent'),
                call.add_torrent('my.torrent'),
                call.add_output('d34db33f'),
                call.emit('added', 'd34db33f'),
            ],
            id='Adding succeeds',
        ),
    ),
)
@pytest.mark.asyncio
async def test__add_torrent(torrent_path, add_torrent_result, exp_calls, make_AddTorrentJob, mocker):
    job = make_AddTorrentJob()

    mocks = Mock()
    mocks.attach_mock(
        (
            mocker.patch.object(job._btclient, 'add_torrent', side_effect=add_torrent_result)
            if isinstance(add_torrent_result, Exception) else
            mocker.patch.object(job._btclient, 'add_torrent', return_value=add_torrent_result)
        ),
        'add_torrent',
    )
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')

    await job._add_torrent(torrent_path)
    assert mocks.mock_calls == exp_calls
