from unittest.mock import AsyncMock, Mock, PropertyMock, call, patch

import pytest

from upsies import errors
from upsies.jobs.submit import SubmitJob
from upsies.trackers import TrackerBase, TrackerJobsBase


@pytest.fixture
def make_SubmitJob(tracker_jobs, tmp_path):
    def make_SubmitJob(**kwargs):
        return SubmitJob(
            home_directory=tmp_path,
            cache_directory=tmp_path,
            tracker_jobs=tracker_jobs,
            **kwargs,
        )

    return make_SubmitJob


@pytest.fixture
def job(make_SubmitJob):
    return make_SubmitJob()


@pytest.fixture
def tracker():
    class TestTracker(TrackerBase):
        name = 'test'
        label = 'TeST'
        torrent_source_field = 'TEST'
        TrackerConfig = 'tracker config class'
        TrackerJobs = 'tracker jobs class'
        login = AsyncMock()
        logout = AsyncMock()
        _login = AsyncMock()
        confirm_logged_in = AsyncMock()
        _logout = AsyncMock()
        is_logged_in = PropertyMock()
        get_announce_url = AsyncMock()
        upload = AsyncMock()

    return TestTracker()

@pytest.fixture
def tracker_jobs(tracker):
    kwargs = {
        'content_path': 'content/path',
        'tracker': tracker,
        'btclient': Mock(),
        'torrent_destination': 'torrent/destination',
        'image_hosts': (Mock(),),
        'common_job_args': {},
    }

    class TestTrackerJobs(TrackerJobsBase):
        def __init__(self, **kw):
            super().__init__(**{**kwargs, **kw})

        jobs_before_upload = PropertyMock()
        jobs_after_upload = PropertyMock()
        submission_ok = PropertyMock(return_value=False)

    return TestTrackerJobs()


def test_cache_id(job):
    assert job.cache_id is None


def test_initialize(job, tracker_jobs, mocker):
    assert job._tracker_jobs is tracker_jobs
    assert job._tracker is tracker_jobs.tracker

    assert 'submitting' in job.signal.signals
    assert 'submitted' in job.signal.signals

    assert job.warn in job._tracker.signal.signals['warning']
    assert job.error in job._tracker.signal.signals['error']
    assert job.exception in job._tracker.signal.signals['exception']


@pytest.mark.parametrize(
    argnames='submission_ok, exp_mock_calls',
    argvalues=(
        (True, [
            call._wait_for_jobs_before_upload(),
            call._submit(),
            call._start_jobs_after_upload(),
        ]),
        (False, [
            call._wait_for_jobs_before_upload(),
            call._start_jobs_after_upload(),
        ]),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_run(submission_ok, exp_mock_calls, job, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, '_wait_for_jobs_before_upload'), '_wait_for_jobs_before_upload')
    mocks.attach_mock(mocker.patch.object(job, '_start_jobs_after_upload'), '_start_jobs_after_upload')
    mocks.attach_mock(mocker.patch.object(job, '_submit'), '_submit')
    mocker.patch.object(job, '_tracker_jobs', Mock(submission_ok=submission_ok))

    return_value = await job.run()
    assert return_value is None
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='upload_result, exp_mock_calls',
    argvalues=(
        (
            None,
            [
                call.emit('submitting'),
                call._tracker.upload('<tracker_jobs>'),
                call.emit('submitted', None),
            ],
        ),
        (
            'http://web.site/torrent.php?id=123',
            [
                call.emit('submitting'),
                call._tracker.upload('<tracker_jobs>'),
                call.add_output('http://web.site/torrent.php?id=123'),
                call.emit('submitted', 'http://web.site/torrent.php?id=123'),
            ],
        ),
        (
            errors.RequestError('Upload rejected'),
            [
                call.emit('submitting'),
                call._tracker.upload('<tracker_jobs>'),
                call.error(errors.RequestError('Upload rejected')),
            ],
        ),

    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test__submit(upload_result, exp_mock_calls, job, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')
    mocker.patch.object(job, '_tracker_jobs', '<tracker_jobs>')
    if isinstance(upload_result, Exception):
        mocks._tracker.upload = AsyncMock(side_effect=upload_result)
    else:
        mocks._tracker.upload = AsyncMock(return_value=upload_result)
    mocker.patch.object(job, '_tracker', mocks._tracker)

    return_value = await job._submit()
    assert return_value is None
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.asyncio
async def test__wait_for_jobs_before_upload(job, mocker):
    JobRunner_mock = mocker.patch('upsies.jobs.submit.JobRunner', Mock(return_value=Mock(wait=AsyncMock())))

    return_value = await job._wait_for_jobs_before_upload()
    assert return_value is None

    assert JobRunner_mock.call_args_list == [call(job._tracker_jobs.jobs_before_upload, id='submit')]
    exp_jobrunner = JobRunner_mock.return_value
    assert exp_jobrunner.mock_calls == [call.wait()]


@pytest.mark.asyncio
async def test__start_jobs_after_upload(job, mocker):
    class Job(Mock):
        def __init__(self, name, **kwargs):
            super().__init__(start=Mock(), **kwargs)
            self.configure_mock(name=name)
            self.__bool__ = lambda self: True

    jobs_after_upload = (
        Job('job1', is_started=False, is_enabled=False),
        Job('job2', is_started=False, is_enabled=True),
        None,
        Job('job3', is_started=True, is_enabled=False),
        Job('job4', is_started=True, is_enabled=True),
        None,
        Job('job5', is_started=False, is_enabled=False),
        Job('job6', is_started=False, is_enabled=True),
        None,
        Job('job7', is_started=True, is_enabled=False),
        Job('job8', is_started=True, is_enabled=True),
    )
    mocker.patch.object(job, '_tracker_jobs', Mock(jobs_after_upload=jobs_after_upload))
    mocks = Mock()
    for j in jobs_after_upload:
        if j:
            mocks.attach_mock(j, j.name)

    return_value = await job._start_jobs_after_upload()
    assert return_value is None

    assert mocks.mock_calls == [
        call.job2.start(),
        call.job6.start(),
    ]


def test__enabled_jobs_before_upload(job, mocker):
    jobs_before_upload = {
        'job1': Mock(is_enabled=False),
        'job2': None,
        'job3': Mock(is_enabled=True),
        'job4': Mock(is_enabled=False),
        'job5': None,
        'job6': Mock(is_enabled=True),
        'job7': Mock(is_enabled=False),
        'job8': None,
        'job9': Mock(is_enabled=True),
    }
    mocker.patch.object(job, '_tracker_jobs', Mock(jobs_before_upload=tuple(jobs_before_upload.values())))

    assert job._enabled_jobs_before_upload == (
        jobs_before_upload['job3'],
        jobs_before_upload['job6'],
        jobs_before_upload['job9'],
    )


@pytest.mark.parametrize('submission_ok, exp_hidden', ((False, True), (True, False)))
def test_hidden(submission_ok, exp_hidden, job, mocker):
    mocker.patch.object(type(job._tracker_jobs), 'submission_ok', PropertyMock(return_value=submission_ok))
    assert job.hidden is exp_hidden


@pytest.mark.parametrize(
    argnames='submission_ok, enabled_jobs_before_upload, exp_main_job',
    argvalues=(
        (True, (), '<super>'),
        (False, (Mock(is_finished=True), Mock(is_finished=False), Mock(is_finished=True)), '<super>'),
        (True, (Mock(is_finished=True, id='a'), Mock(is_finished=True, id='b'), Mock(is_finished=True, id='c')), '<super>'),
        (False, (Mock(is_finished=True, id='a'), Mock(is_finished=True, id='b'), Mock(is_finished=True, id='c')), 'c'),
    ),
)
def test__main_job(submission_ok, enabled_jobs_before_upload, exp_main_job, job, mocker):
    mocker.patch.object(type(job), '_tracker_jobs', PropertyMock(return_value=Mock(submission_ok=submission_ok)), create=True)
    mocker.patch.object(type(job), '_enabled_jobs_before_upload', PropertyMock(return_value=enabled_jobs_before_upload))
    if exp_main_job == '<super>':
        with patch('upsies.jobs.submit.super') as super_mock:
            assert job._main_job is super_mock.return_value
        assert super_mock.call_args_list == [call()]

    else:
        assert job._main_job.id == exp_main_job


def test_output(job, mocker):
    mocker.patch.object(type(job), '_main_job', PropertyMock())
    assert job.output is job._main_job.output


def test_exit_code(job, mocker):
    mocker.patch.object(type(job), '_main_job', PropertyMock())
    assert job.exit_code is job._main_job.exit_code
