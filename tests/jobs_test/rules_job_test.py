import asyncio
import re
import time
from unittest.mock import AsyncMock, Mock, call

import pytest

from upsies import errors
from upsies.jobs import rules


@pytest.fixture
def job(tmp_path):
    return rules.RulesJob(
        home_directory=tmp_path,
        cache_directory=tmp_path,
        tracker_jobs=Mock(),
    )


def test_RulesJob_name():
    assert rules.RulesJob.name == 'rules'


def test_RulesJob_label():
    assert rules.RulesJob.label == 'Rules'


def test_RulesJob_cache_id(job):
    assert job.cache_id is None


def test_RulesJob_tracker(job):
    assert job.tracker is job._tracker_jobs.tracker


def test_RulesJob_tracker_jobs(job):
    assert job.tracker_jobs is job._tracker_jobs


def test_RulesJob_release_name(job):
    assert job.release_name is job._tracker_jobs.release_name


def test_RulesJob_initialize():
    job = rules.RulesJob(
        tracker_jobs='mock_tracker_jobs',
        only_warn='maybe?',
    )
    assert job._tracker_jobs == 'mock_tracker_jobs'
    assert job._only_warn == 'maybe?'
    assert 'checking' in job.signal.signals
    assert 'checked' in job.signal.signals


@pytest.mark.asyncio
async def test_RulesJob_run(job, mocker):

    async def check_rule(rule):
        delay = int(rule[-1]) * 0.01
        await asyncio.sleep(delay)

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'check_rule', side_effect=check_rule), 'check_rule')
    job.tracker.rules = ('TrackerRule1', 'TrackerRule2', 'TrackerRule3')

    # If each rule was checked in series, we should get a delay of 1 + 2 + 3, but we want concurrent
    # execution and a delay of 3 + some small overhead.
    start = time.monotonic()
    return_value = await job.run()
    stop = time.monotonic()
    elapsed = stop - start

    assert return_value is None
    assert elapsed < 0.03 + 0.01
    assert mocks.mock_calls == [
        call.check_rule('TrackerRule1'),
        call.check_rule('TrackerRule2'),
        call.check_rule('TrackerRule3'),
    ]


@pytest.mark.parametrize(
    argnames='exception, only_warn, exp_mock_calls',
    argvalues=(
        (None, False, [call.check()]),
        (None, True, [call.check()]),
        (errors.RuleBroken('nope'), False, [
            call.check(),
            call.error(errors.RuleBroken('nope')),
        ]),
        (errors.RuleBroken('nope'), True, [
            call.check(),
            call.warn(errors.RuleBroken('nope')),
        ]),
        (TypeError('unexpected exception'), False, [call.check()]),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_RulesJob_check_rile(exception, only_warn, exp_mock_calls, job, mocker):
    TrackerRule = Mock(__name__='TrackerRule')
    TrackerRule.return_value.check = AsyncMock(side_effect=exception)
    mocks = Mock()
    mocks.attach_mock(TrackerRule, 'TrackerRule')
    mocks.attach_mock(TrackerRule.return_value.check, 'check')
    mocks.attach_mock(mocker.patch.object(job, 'warn'), 'warn')
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')
    job._only_warn = only_warn

    if exception is not None and not isinstance(exception, errors.UpsiesError):
        with pytest.raises(type(exception), match=rf'^{re.escape(str(exception))}$'):
            await job.check_rule(TrackerRule)
    else:
        return_value = await job.check_rule(TrackerRule)
        assert return_value is None

    exp_mock_calls = [
        call.TrackerRule(job.tracker_jobs),
        call.emit('checking', TrackerRule.return_value),
        *exp_mock_calls,
    ]
    if exception is None or isinstance(exception, errors.UpsiesError):
        exp_mock_calls.append(call.emit('checked', TrackerRule.return_value))
    assert mocks.mock_calls == exp_mock_calls
