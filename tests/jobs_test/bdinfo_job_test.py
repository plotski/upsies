import errno
import itertools
import os
import pickle
import re
import sys
from unittest.mock import DEFAULT, AsyncMock, Mock, PropertyMock, call

import pytest

from upsies import errors, utils
from upsies.jobs import bdinfo


def make_job(tmp_path, *args, **kwargs):
    return bdinfo.BdinfoJob(
        *args,
        **{
            'home_directory': tmp_path,
            'cache_directory': tmp_path,
            **kwargs,
        },
    )

@pytest.fixture
def job(tmp_path, mocker):
    return make_job(tmp_path)


def test_BdinfoJob_name(job):
    assert job.name == 'bdinfo'


def test_BdinfoJob_label(job):
    assert job.label == 'BDInfo'


def test_BdinfoJob_cache_id(job):
    assert job.cache_id is None


def test_BdinfoJob__DEFAULT_FORMAT(job):
    assert job._DEFAULT_FORMAT == '{BDINFO}'


def test_BdinfoJob_initialize(tmp_path, mocker):
    job = make_job(
        tmp_path,
        summary='<summary>',
        format='<format>',
    )
    assert job._summary == '<summary>'
    assert job._format == '<format>'
    assert job._bdinfo_reports == []
    assert job._reports_by_file == {}
    assert job._bdinfo_process is None

    assert 'bdinfo_progress' in job.signal.signals
    assert 'bdinfo_report' in job.signal.signals
    assert job.signal.signals['bdinfo_report'] == [job._store_bdinfo_report]
    assert job.signal.signals['finished'] == [job._hide_job]


def test__hide_job(job):
    assert job.hidden is False
    job._hide_job(job)
    assert job.hidden is True


@pytest.mark.asyncio
async def test_BdinfoJob_run(job, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'receive_all'), 'receive_all')
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.DaemonProcess', return_value=Mock(join=AsyncMock())), 'DaemonProcess')
    mocks.attach_mock(mocks.DaemonProcess.return_value.start, 'DaemonProcess_start')
    mocks.attach_mock(mocks.DaemonProcess.return_value.join, 'DaemonProcess_join')
    mocks.attach_mock(mocks.DaemonProcess.return_value.send, 'DaemonProcess_send')
    mocks.attach_mock(mocker.patch.object(job, '_add_bdinfo_reports'), '_add_bdinfo_reports')
    mocks.receive_all.return_value.__aiter__.return_value = (
        ('path/to/disc1', ('playlist1', 'playlist1', 'playlist1')),
        ('path/to/disc2', ('playlist2', 'playlist3', 'playlist4')),
    )

    await job.run()

    assert mocks.mock_calls == [
        call.DaemonProcess(
            target=bdinfo._bdinfo_process,
            kwargs={
                'cache_directory': job.cache_directory,
                'ignore_cache': job.ignore_cache,
            },
            info_callback=job._handle_info,
            error_callback=job._handle_error,
        ),
        call.DaemonProcess_start(),
        call.receive_all('playlists', 'playlists_selected', only_posargs=True),
        call.receive_all().__aiter__(),
        call.DaemonProcess_send(utils.daemon.MsgType.info, {'playlists_selected': ('path/to/disc1', ('playlist1', 'playlist1', 'playlist1'))}),
        call.DaemonProcess_send(utils.daemon.MsgType.info, {'playlists_selected': ('path/to/disc2', ('playlist2', 'playlist3', 'playlist4'))}),
        call.DaemonProcess_send(utils.daemon.MsgType.info, {'playlists_selected': bdinfo._ALL_PLAYLIST_SELECTIONS_MADE}),
        call.DaemonProcess_join(),
        call._add_bdinfo_reports(),
    ]


@pytest.mark.parametrize(
    argnames='summary, format, reports, exp_exception, exp_mock_calls',
    argvalues=(
        pytest.param(
            None,
            '[bdinfo]{BDINFO}[/bdinfo]',
            {
                'bdinfo_reports': ('<report 1>', '<report 2>', '<report 3>'),
                'full_summaries': ('<full summary 1>', '<full summary 2>', '<full summary 3>'),
                'quick_summaries': ('<quick summary 1>', '<quick summary 2>', '<quick summary 3>'),
            },
            None,
            [
                call.add_output('[bdinfo]<report 1>[/bdinfo]'),
                call.add_output('[bdinfo]<report 2>[/bdinfo]'),
                call.add_output('[bdinfo]<report 3>[/bdinfo]'),
            ],
            id='Full reports',
        ),
        pytest.param(
            'full',
            '[bdinfo]{BDINFO}[/bdinfo]',
            {
                'bdinfo_reports': ('<report 1>', '<report 2>', '<report 3>'),
                'full_summaries': ('<full summary 1>', '<full summary 2>', '<full summary 3>'),
                'quick_summaries': ('<quick summary 1>', '<quick summary 2>', '<quick summary 3>'),
            },
            None,
            [
                call.add_output('[bdinfo]<full summary 1>[/bdinfo]'),
                call.add_output('[bdinfo]<full summary 2>[/bdinfo]'),
                call.add_output('[bdinfo]<full summary 3>[/bdinfo]'),
            ],
            id='Full summaries',
        ),
        pytest.param(
            'quick',
            '[bdinfo]{BDINFO}[/bdinfo]',
            {
                'bdinfo_reports': ('<report 1>', '<report 2>', '<report 3>'),
                'full_summaries': ('<full summary 1>', '<full summary 2>', '<full summary 3>'),
                'quick_summaries': ('<quick summary 1>', '<quick summary 2>', '<quick summary 3>'),
            },
            None,
            [
                call.add_output('[bdinfo]<quick summary 1>[/bdinfo]'),
                call.add_output('[bdinfo]<quick summary 2>[/bdinfo]'),
                call.add_output('[bdinfo]<quick summary 3>[/bdinfo]'),
            ],
            id='Quick summaries',
        ),
        pytest.param(
            'foo',
            '[bdinfo]{BDINFO}[/bdinfo]',
            {
                'bdinfo_reports': ('<report 1>', '<report 2>', '<report 3>'),
                'full_summaries': ('<full summary 1>', '<full summary 2>', '<full summary 3>'),
                'quick_summaries': ('<quick summary 1>', '<quick summary 2>', '<quick summary 3>'),
            },
            RuntimeError("Unexpected summary value: 'foo'"),
            [],
            id='Unknown summary',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_BdinfoJob__add_bdinfo_reports(summary, format, reports, exp_exception, exp_mock_calls, job, mocker):
    job._summary = summary
    job._format = format
    for name, value in reports.items():
        mocker.patch.object(type(job), name, PropertyMock(return_value=value))

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            job._add_bdinfo_reports()
    else:
        return_value = job._add_bdinfo_reports()
        assert return_value is None

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='reason, exp_parent_terminate_calls',
    argvalues=(
        (None, [call.parent_terminate(reason=None)]),
        ('Because!', [call.parent_terminate(reason='Because!')]),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='bdinfo_process, exp_bdinfo_process_stop_calls',
    argvalues=(
        (None, []),
        (Mock(), [call.bdinfo_process_stop()]),
    ),
    ids=lambda v: repr(v),
)
def test_BdinfoJob_terminate(
        reason, exp_parent_terminate_calls,
        bdinfo_process, exp_bdinfo_process_stop_calls,
        job, mocker,
):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.jobs.base.JobBase.terminate'), 'parent_terminate')
    if bdinfo_process:
        mocks.attach_mock(bdinfo_process.stop, 'bdinfo_process_stop')
        bdinfo_process.stop.reset_mock()
    job._bdinfo_process = bdinfo_process

    if reason is None:
        job.terminate()
    else:
        job.terminate(reason=reason)

    exp_mock_calls = exp_bdinfo_process_stop_calls + exp_parent_terminate_calls
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='is_finished, info, exp_exception, exp_mock_calls',
    argvalues=(
        (
            True,
            {'bdinfo_progress': '<bdinfo progress>'},
            None,
            [],
        ),
        (
            False,
            {'bdinfo_progress': '<bdinfo progress>'},
            None,
            [call.emit('bdinfo_progress', '<bdinfo progress>')],
        ),
        (
            False,
            {'bdinfo_report': '<bdinfo report>'},
            None,
            [call.emit('bdinfo_report', '<bdinfo report>')],
        ),
        (
            False,
            {'foo': 'bar'},
            RuntimeError("Unexpected info: {'foo': 'bar'}"),
            [],
        ),
    ),
    ids=lambda v: repr(v),
)
def test_BdinfoJob__handle_info(is_finished, info, exp_exception, exp_mock_calls, job, mocker):
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=is_finished))
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            job._handle_info(info)
    else:
        return_value = job._handle_info(info)
        assert return_value is None

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='is_finished, error, exp_mock_calls',
    argvalues=(
        (
            True,
            errors.DependencyError('bdinfo is not installed'),
            [],
        ),
        (
            False,
            errors.DependencyError('bdinfo is not installed'),
            [
                call.error(errors.DependencyError('bdinfo is not installed')),
            ],
        ),
        (
            False,
            errors.UpsiesError('The programmer did an oopsie!'),
            [
                call.exception(errors.UpsiesError('The programmer did an oopsie!')),
            ],
        ),
    ),
    ids=lambda v: repr(v),
)
def test_BdinfoJob__handle_error(is_finished, error, exp_mock_calls, job, mocker):
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=is_finished))
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')
    mocks.attach_mock(mocker.patch.object(job, 'exception'), 'exception')
    job._handle_error(error)
    assert mocks.mock_calls == exp_mock_calls


def test_BdinfoJob_store_bdinfo_report(job):
    reports = (
        Mock(name='3', playlist=Mock(filepath='path/to/BDMV/PLAYLIST/00003.mpls')),
        Mock(name='9', playlist=Mock(filepath='path/to/BDMV/PLAYLIST/00009.mpls')),
        Mock(name='12', playlist=Mock(filepath='path/to/BDMV/PLAYLIST/00012.mpls')),
    )

    return_value = job._store_bdinfo_report(reports[0])
    assert return_value is None
    assert job._bdinfo_reports == [reports[0]]
    assert job._reports_by_file == {
        'path/to/BDMV/PLAYLIST/00003.mpls': reports[0],
    }

    return_value = job._store_bdinfo_report(reports[1])
    assert return_value is None
    assert job._bdinfo_reports == [reports[0], reports[1]]
    assert job._reports_by_file == {
        'path/to/BDMV/PLAYLIST/00003.mpls': reports[0],
        'path/to/BDMV/PLAYLIST/00009.mpls': reports[1],
    }

    return_value = job._store_bdinfo_report(reports[2])
    assert return_value is None
    assert job._bdinfo_reports == [reports[0], reports[1], reports[2]]
    assert job._reports_by_file == {
        'path/to/BDMV/PLAYLIST/00003.mpls': reports[0],
        'path/to/BDMV/PLAYLIST/00009.mpls': reports[1],
        'path/to/BDMV/PLAYLIST/00012.mpls': reports[2],
    }


def test_BdinfoJob_bdinfo_reports(job):
    job._bdinfo_reports.extend(('foo', 'bar', 'baz'))
    assert job.bdinfo_reports == ('foo', 'bar', 'baz')


def test_MediainfoJob_reports_by_file(job):
    job._reports_by_file = {
        'path/to/BDMV/PLAYLIST/00003.mpls': '<bdinfo for 00003.mpls>',
        'path/to/BDMV/PLAYLIST/00009.mpls': '<bdinfo for 00009.mpls>',
        'path/to/BDMV/PLAYLIST/000012.mpls': '<bdinfo for 000012.mpls>',
    }
    reports_by_file = job.reports_by_file
    assert reports_by_file == {
        'path/to/BDMV/PLAYLIST/00003.mpls': '<bdinfo for 00003.mpls>',
        'path/to/BDMV/PLAYLIST/00009.mpls': '<bdinfo for 00009.mpls>',
        'path/to/BDMV/PLAYLIST/000012.mpls': '<bdinfo for 000012.mpls>',
    }
    reports_by_file.clear()
    assert job.reports_by_file == {
        'path/to/BDMV/PLAYLIST/00003.mpls': '<bdinfo for 00003.mpls>',
        'path/to/BDMV/PLAYLIST/00009.mpls': '<bdinfo for 00009.mpls>',
        'path/to/BDMV/PLAYLIST/000012.mpls': '<bdinfo for 000012.mpls>',
    }


def test_BdinfoJob_full_summaries(job, mocker):
    mocker.patch.object(type(job), 'bdinfo_reports', PropertyMock(return_value=(
        Mock(full_summary='<full summary 1>'),
        Mock(full_summary='<full summary 2>'),
        Mock(full_summary='<full summary 3>'),
    )))
    assert job.full_summaries == (
        '<full summary 1>',
        '<full summary 2>',
        '<full summary 3>',
    )


def test_BdinfoJob_quick_summaries(job, mocker):
    mocker.patch.object(type(job), 'bdinfo_reports', PropertyMock(return_value=(
        Mock(quick_summary='<quick summary 1>'),
        Mock(quick_summary='<quick summary 2>'),
        Mock(quick_summary='<quick summary 3>'),
    )))
    assert job.quick_summaries == (
        '<quick summary 1>',
        '<quick summary 2>',
        '<quick summary 3>',
    )


@pytest.mark.parametrize(
    argnames='kwargs, exp_attributes',
    argvalues=(
        pytest.param(
            {'playlist': '<My Playlist>'},
            {'playlist': '<My Playlist>', 'percent': 0, 'time_elapsed': 0, 'time_remaining': 0},
            id='playlist',
        ),
        pytest.param(
            {'playlist': '<My Playlist>', 'percent': '34'},
            {'playlist': '<My Playlist>', 'percent': 34, 'time_elapsed': 0, 'time_remaining': 0},
            id='playlist, percent',
        ),
        pytest.param(
            {'playlist': '<My Playlist>', 'percent': '34', 'time_elapsed': '123'},
            {'playlist': '<My Playlist>', 'percent': 34, 'time_elapsed': 123, 'time_remaining': 0},
            id='playlist, percent, time_elapsed',
        ),
        pytest.param(
            {'playlist': '<My Playlist>', 'percent': '34', 'time_elapsed': '123', 'time_remaining': '456'},
            {'playlist': '<My Playlist>', 'percent': 34, 'time_elapsed': 123, 'time_remaining': 456},
            id='playlist, percent, time_elapsed, time_remaining',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_BdinfoProgress(kwargs, exp_attributes):
    progress = bdinfo.BdinfoProgress(**kwargs)
    assert exp_attributes, exp_attributes
    for name, value in exp_attributes.items():
        assert progress[name] == value
        assert getattr(progress, name) == value
    assert isinstance(progress.time_elapsed, utils.types.Timestamp), type(progress.time_elapsed)
    assert isinstance(progress.time_remaining, utils.types.Timestamp), type(progress.time_remaining)


@pytest.fixture
def bdinfo_report_text(data_dir):
    bdinfo_report_text = {}
    with open(os.path.join(data_dir, 'video', 'bdinfo_report_complete.txt'), 'r') as f:
        bdinfo_report_text['complete'] = f.read()
    with open(os.path.join(data_dir, 'video', 'bdinfo_report_full_summary.txt'), 'r') as f:
        bdinfo_report_text['full_summary'] = f.read()
    with open(os.path.join(data_dir, 'video', 'bdinfo_report_quick_summary.txt'), 'r') as f:
        bdinfo_report_text['quick_summary'] = f.read()
    return bdinfo_report_text


def test_BdinfoReport_is_str(bdinfo_report_text):
    playlist = Mock()
    report = bdinfo.BdinfoReport(bdinfo_report_text['complete'], playlist)
    assert report == bdinfo_report_text['complete']
    assert isinstance(report, str)


def test_BdinfoReport_is_picklable(bdinfo_report_text):
    playlist = utils.disc.Playlist(
        items=('00001.m2ts', '00002.m2ts', '00003.m2ts'),
        filepath='path/to/disc/00012.mpls',
        discpath='path/to/disc',
        duration=123,
        is_main=True,
    )
    report1 = bdinfo.BdinfoReport(bdinfo_report_text['complete'], playlist)
    dumped_report = pickle.dumps(report1)
    report2 = pickle.loads(dumped_report)
    assert report1 == report2
    assert report1.playlist == report2.playlist


@pytest.mark.parametrize(
    argnames='report_text_butcher, exp_full_summary_is_None',
    argvalues=(
        (lambda original_report: original_report, False),
        (lambda original_report: original_report.replace('DISC', 'DUSK'), True),
    ),
    ids=lambda v: repr(v),
)
def test_BdinfoReport_full_summary(report_text_butcher, exp_full_summary_is_None, bdinfo_report_text, mocker):
    playlist = Mock()
    report = bdinfo.BdinfoReport(report_text_butcher(bdinfo_report_text['complete']), playlist)
    if exp_full_summary_is_None:
        assert report.full_summary is None
    else:
        assert report.full_summary == bdinfo_report_text['full_summary'].strip()


@pytest.mark.parametrize(
    argnames='report_text_butcher, exp_quick_summary_is_None',
    argvalues=(
        (lambda original_report: original_report, False),
        (lambda original_report: original_report.replace('QUICK', 'QUACK'), True),
    ),
    ids=lambda v: repr(v),
)
def test_BdinfoReport_quick_summary(report_text_butcher, exp_quick_summary_is_None, bdinfo_report_text, mocker):
    playlist = Mock()
    report = bdinfo.BdinfoReport(report_text_butcher(bdinfo_report_text['complete']), playlist)
    if exp_quick_summary_is_None:
        assert report.quick_summary is None
    else:
        assert report.quick_summary == bdinfo_report_text['quick_summary'].strip()


def test_BdinfoReport___repr__(bdinfo_report_text, mocker):
    playlist = Mock()
    report = bdinfo.BdinfoReport(bdinfo_report_text['complete'], playlist)
    assert repr(report) == f'<{type(report).__name__} {playlist!r}>'


@pytest.fixture
def _bdinfo_process_mocks(mocker):
    mocks = Mock()
    mocks.output_queue = Mock()
    mocks.input_queue = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.utils.daemon.read_input_queue_key', side_effect=()),
        'read_input_queue_key',
    )
    mocks.attach_mock(
        mocker.patch('upsies.jobs.bdinfo._get_bdinfo', side_effect=lambda *args, playlist, **kwargs: f'<report for {playlist}>'),
        '_get_bdinfo',
    )
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.maybe_terminate'), 'maybe_terminate')
    mocks.attach_mock(mocker.patch('upsies.jobs.bdinfo.BdinfoReport', side_effect=lambda text, pls: f'{text} from {pls}'), 'BdinfoReport')
    return mocks

def test__bdinfo_process_is_terminated_by__ALL_PLAYLIST_SELECTIONS_MADE(_bdinfo_process_mocks, mocker, tmp_path):
    mocks = _bdinfo_process_mocks
    mocks.read_input_queue_key.side_effect = (
        ('path/to/disc1', ('00001.mpls', '00002.mpls')),
        ('path/to/disc2', ('00010.mpls', '00020.mpls', '00030.mpls')),
        bdinfo._ALL_PLAYLIST_SELECTIONS_MADE,
    )

    cache_directory = 'path/to/cache'
    ignore_cache = '<maybe ignore cache>'
    bdinfo._bdinfo_process(mocks.output_queue, mocks.input_queue, cache_directory=cache_directory, ignore_cache=ignore_cache)

    exp_mock_calls = [
        # First batch of playlists.
        call.read_input_queue_key(mocks.input_queue, 'playlists_selected'),
        call._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist='00001.mpls', cache_directory=cache_directory, ignore_cache=ignore_cache),
        call.BdinfoReport('<report for 00001.mpls>', '00001.mpls'),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_report': '<report for 00001.mpls> from 00001.mpls'})),
        call.maybe_terminate(mocks.input_queue),

        call._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist='00002.mpls', cache_directory=cache_directory, ignore_cache=ignore_cache),
        call.BdinfoReport('<report for 00002.mpls>', '00002.mpls'),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_report': '<report for 00002.mpls> from 00002.mpls'})),
        call.maybe_terminate(mocks.input_queue),

        # Second batch of playlists.
        call.read_input_queue_key(mocks.input_queue, 'playlists_selected'),
        call._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist='00010.mpls', cache_directory=cache_directory, ignore_cache=ignore_cache),
        call.BdinfoReport('<report for 00010.mpls>', '00010.mpls'),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_report': '<report for 00010.mpls> from 00010.mpls'})),
        call.maybe_terminate(mocks.input_queue),

        call._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist='00020.mpls', cache_directory=cache_directory, ignore_cache=ignore_cache),
        call.BdinfoReport('<report for 00020.mpls>', '00020.mpls'),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_report': '<report for 00020.mpls> from 00020.mpls'})),
        call.maybe_terminate(mocks.input_queue),

        call._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist='00030.mpls', cache_directory=cache_directory, ignore_cache=ignore_cache),
        call.BdinfoReport('<report for 00030.mpls>', '00030.mpls'),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_report': '<report for 00030.mpls> from 00030.mpls'})),
        call.maybe_terminate(mocks.input_queue),

        # Read terminator (_ALL_PLAYLIST_SELECTIONS_MADE).
        call.read_input_queue_key(mocks.input_queue, 'playlists_selected'),
    ]
    for got_call, exp_call in itertools.zip_longest(mocks.mock_calls, exp_mock_calls):
        print('Exp:', exp_call)
        print('Got:', got_call)
        if exp_call != got_call:
            break
    assert mocks.mock_calls == exp_mock_calls

def test__bdinfo_process_is_terminated_by__maybe_terminate(_bdinfo_process_mocks, mocker, tmp_path):
    mocks = _bdinfo_process_mocks
    mocks.read_input_queue_key.side_effect = (
        ('path/to/disc1', ('00001.mpls', '00002.mpls')),
        ('path/to/disc2', ('00010.mpls', '00020.mpls', '00030.mpls')),
        bdinfo._ALL_PLAYLIST_SELECTIONS_MADE,
    )
    mocks.maybe_terminate.side_effect = (
        None,
        None,
        None,
        errors.DaemonProcessTerminated('Bye'),
    )

    cache_directory = 'path/to/cache'
    ignore_cache = '<maybe ignore cache>'
    with pytest.raises(errors.DaemonProcessTerminated, match=r'^Bye'):
        bdinfo._bdinfo_process(mocks.output_queue, mocks.input_queue, cache_directory=cache_directory, ignore_cache=ignore_cache)

    exp_mock_calls = [
        # First batch of playlists.
        call.read_input_queue_key(mocks.input_queue, 'playlists_selected'),
        call._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist='00001.mpls', cache_directory=cache_directory, ignore_cache=ignore_cache),
        call.BdinfoReport('<report for 00001.mpls>', '00001.mpls'),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_report': '<report for 00001.mpls> from 00001.mpls'})),
        call.maybe_terminate(mocks.input_queue),

        call._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist='00002.mpls', cache_directory=cache_directory, ignore_cache=ignore_cache),
        call.BdinfoReport('<report for 00002.mpls>', '00002.mpls'),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_report': '<report for 00002.mpls> from 00002.mpls'})),
        call.maybe_terminate(mocks.input_queue),

        # Second batch of playlists.
        call.read_input_queue_key(mocks.input_queue, 'playlists_selected'),
        call._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist='00010.mpls', cache_directory=cache_directory, ignore_cache=ignore_cache),
        call.BdinfoReport('<report for 00010.mpls>', '00010.mpls'),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_report': '<report for 00010.mpls> from 00010.mpls'})),
        call.maybe_terminate(mocks.input_queue),

        call._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist='00020.mpls', cache_directory=cache_directory, ignore_cache=ignore_cache),
        call.BdinfoReport('<report for 00020.mpls>', '00020.mpls'),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_report': '<report for 00020.mpls> from 00020.mpls'})),
        call.maybe_terminate(mocks.input_queue),
    ]
    for got_call, exp_call in itertools.zip_longest(mocks.mock_calls, exp_mock_calls):
        print('Exp:', exp_call)
        print('Got:', got_call)
        if exp_call != got_call:
            break
    assert mocks.mock_calls == exp_mock_calls


@pytest.fixture
def _get_bdinfo_mocks(mocker, tmp_path):
    mocks = Mock()
    mocks.output_queue = Mock()
    mocks.input_queue = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_bluray', return_value=True), 'is_bluray')
    mocks.attach_mock(mocker.patch('upsies.jobs.bdinfo._get_cache_filepath'), '_get_cache_filepath')
    mocks.attach_mock(mocker.patch('upsies.jobs.bdinfo._get_bdinfo_from_cache'), '_get_bdinfo_from_cache')
    mocks.attach_mock(mocker.patch('upsies.jobs.bdinfo._get_bdinfo_from_bdinfo'), '_get_bdinfo_from_bdinfo')
    mocks.attach_mock(mocker.patch('upsies.jobs.bdinfo._write_bdinfo_to_cache'), '_write_bdinfo_to_cache')
    return mocks

def test__get_bdinfo_gets_invalid_playlist(_get_bdinfo_mocks, mocker, tmp_path):
    mocks = _get_bdinfo_mocks
    mocks.is_bluray.return_value = False
    return_value = bdinfo._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist=mocks.playlist, cache_directory='path/to/cache', ignore_cache='<maybe>')
    assert return_value is None
    assert mocks.mock_calls == [
        call.is_bluray(mocks.playlist.discpath),
        call.output_queue.put((utils.daemon.MsgType.error, errors.ContentError(f'Not a Blu-ray disc path: {mocks.playlist.discpath}'))),
    ]

def test__get_bdinfo_gets_bdinfo_from_cache(_get_bdinfo_mocks, mocker, tmp_path):
    mocks = _get_bdinfo_mocks
    mocks.is_bluray.return_value = True
    mocks._get_cache_filepath.return_value = 'path/to/cache/foo.bdinfo'
    mocks._get_bdinfo_from_cache.return_value = '<my cache bdinfo>'
    mocks._get_bdinfo_from_bdinfo.return_value = '<my fresh bdinfo>'
    cache_directory = 'path/to/cache'
    ignore_cache = False
    return_value = bdinfo._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist=mocks.playlist, cache_directory=cache_directory, ignore_cache=ignore_cache)
    assert return_value is mocks._get_bdinfo_from_cache.return_value
    assert mocks.mock_calls == [
        call.is_bluray(mocks.playlist.discpath),
        call._get_cache_filepath(mocks.playlist, cache_directory),
        call._get_bdinfo_from_cache(mocks._get_cache_filepath.return_value),
    ]

@pytest.mark.parametrize(
    argnames='ignore_cache, cached_bdinfo',
    argvalues=(
        pytest.param(True, '<my cached bdinfo>', id='cache is ignored'),
        pytest.param(False, None, id='reading cache failed'),
    ),
)
def test__get_bdinfo_gets_bdinfo_from_bdinfo(ignore_cache, cached_bdinfo, _get_bdinfo_mocks, mocker, tmp_path):
    mocks = _get_bdinfo_mocks
    mocks.is_bluray.return_value = True
    mocks._get_cache_filepath.return_value = 'path/to/cache/foo.bdinfo'
    mocks._get_bdinfo_from_cache.return_value = cached_bdinfo
    mocks._get_bdinfo_from_bdinfo.return_value = '<my fresh bdinfo>'
    cache_directory = 'path/to/cache'
    return_value = bdinfo._get_bdinfo(mocks.output_queue, mocks.input_queue, playlist=mocks.playlist, cache_directory=cache_directory, ignore_cache=ignore_cache)
    assert return_value is mocks._get_bdinfo_from_bdinfo.return_value
    assert mocks.mock_calls == [
        call.is_bluray(mocks.playlist.discpath),
        call._get_cache_filepath(mocks.playlist, cache_directory),
    ] + (
        # _get_bdinfo_from_cache() is only called if ignore_cache == False.
        []
        if ignore_cache else
        [call._get_bdinfo_from_cache(mocks._get_cache_filepath.return_value)]
    ) + [
        call._get_bdinfo_from_bdinfo(mocks.output_queue, mocks.input_queue, mocks.playlist),
        call._write_bdinfo_to_cache(mocks._get_bdinfo_from_bdinfo.return_value, mocks._get_cache_filepath.return_value),
    ]


@pytest.fixture
def _get_bdinfo_from_bdinfo_mocks(mocker, tmp_path):
    mocks = Mock()
    mocks.output_queue = Mock()
    mocks.input_queue = Mock()
    mocks.attach_mock(mocker.patch('tempfile.TemporaryDirectory'), 'TemporaryDirectory')
    mocks.attach_mock(mocker.patch('upsies.jobs.bdinfo._generate_bdinfo'), '_generate_bdinfo')
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.maybe_terminate'), 'maybe_terminate')
    mocks.attach_mock(
        mocker.patch('upsies.jobs.bdinfo._find_bdinfo_in_directory'),
        '_find_bdinfo_in_directory',
    )

    mocks.bdinfo_directory = tmp_path / 'bdinfo_directory'
    mocks.bdinfo_directory.mkdir(parents=True, exist_ok=True)
    mocks.TemporaryDirectory.return_value.__enter__.return_value = mocks.bdinfo_directory

    return mocks

def test__get_bdinfo_from_bdinfo_sets_bdinfo_directory_permissions(_get_bdinfo_from_bdinfo_mocks):
    mocks = _get_bdinfo_from_bdinfo_mocks

    def check_bdinfo_directory_permissions(*args, bdinfo_directory, **kwargs):
        permission_mask = os.stat(bdinfo_directory).st_mode & 0o777
        print(bdinfo_directory, 'permissions:', oct(permission_mask))
        assert permission_mask == 0o777
        return DEFAULT

    mocks._generate_bdinfo.side_effect = check_bdinfo_directory_permissions
    bdinfo._get_bdinfo_from_bdinfo(mocks.output_queue, mocks.input_queue, playlist=mocks.playlist)
    assert call._generate_bdinfo(
        playlist=mocks.playlist,
        bdinfo_directory=mocks.bdinfo_directory,
    ) in mocks.mock_calls

def test__get_bdinfo_from_bdinfo_provides_progress_reports(_get_bdinfo_from_bdinfo_mocks, mocker, tmp_path):
    mocks = _get_bdinfo_from_bdinfo_mocks
    mocks._generate_bdinfo.return_value = iter((
        {'progress': 1},
        {'progress': 30},
        {'progress': 90},
        {'progress': 100},
    ))

    return_value = bdinfo._get_bdinfo_from_bdinfo(mocks.output_queue, mocks.input_queue, playlist=mocks.playlist)
    assert return_value is mocks._find_bdinfo_in_directory.return_value
    exp_mock_calls = [
        call.TemporaryDirectory(),
        call.TemporaryDirectory().__enter__(),
        call._generate_bdinfo(playlist=mocks.playlist, bdinfo_directory=mocks.bdinfo_directory),
        call.maybe_terminate(mocks.input_queue),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_progress': {'progress': 1}})),
        call.maybe_terminate(mocks.input_queue),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_progress': {'progress': 30}})),
        call.maybe_terminate(mocks.input_queue),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_progress': {'progress': 90}})),
        call.maybe_terminate(mocks.input_queue),
        call.output_queue.put((utils.daemon.MsgType.info, {'bdinfo_progress': {'progress': 100}})),
        call._find_bdinfo_in_directory(mocks.bdinfo_directory),
        call.TemporaryDirectory().__exit__(None, None, None),
    ]
    for got_call, exp_call in itertools.zip_longest(mocks.mock_calls, exp_mock_calls):
        print('Exp:', exp_call)
        print('Got:', got_call)
        if exp_call != got_call:
            break
    assert mocks.mock_calls == exp_mock_calls


def test__get_cache_filepath():
    playlist = Mock(label='foo', size=123.4)
    cache_directory = 'path/to/cache'
    return_value = bdinfo._get_cache_filepath(playlist, cache_directory)
    assert return_value == 'path/to/cache/foo.123.bdinfo'


@pytest.mark.parametrize(
    argnames='cache_filepath, open_exception, exp_return_value',
    argvalues=(
        (
            'path/to/cache/foo.bdinfo',
            None,
            '<content of path/to/cache/foo.bdinfo>',
        ),
        (
            'path/to/cache/foo.bdinfo',
            OSError(errno.EACCES, 'Permission denied'),
            None,
        ),
    ),
    ids=lambda v: repr(v),
)
def test__get_bdinfo_from_cache(cache_filepath, open_exception, exp_return_value, mocker):
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('builtins.open', mocker.mock_open(read_data=f'<content of {cache_filepath}>')),
        'open',
    )
    if open_exception:
        mocks.open.side_effect = open_exception

    return_value = bdinfo._get_bdinfo_from_cache(cache_filepath)
    assert return_value == exp_return_value

    if open_exception:
        assert mocks.mock_calls == [
            call.open(cache_filepath, 'r'),
        ]
    else:
        assert mocks.mock_calls == [
            call.open(cache_filepath, 'r'),
            call.open().__enter__(),
            call.open().read(),
            call.open().__exit__(None, None, None),
        ] + (
            # TODO: Remove this when Python 3.12 is no longer supported.
            # Python >= 3.13 calls close() while previous versions don't.
            [call.open().close()]
            if sys.version_info >= (3, 13, 0) else
            []
        )


@pytest.mark.parametrize(
    argnames='bdinfo_report, cache_filepath, open_exception, exp_return_value',
    argvalues=(
        (
            '<my bdinfo report>',
            'path/to/cache/foo.bdinfo',
            None,
            '<content of path/to/cache/foo.bdinfo>',
        ),
        (
            '<my bdinfo report>',
            'path/to/cache/foo.bdinfo',
            OSError(errno.EACCES, 'Permission denied'),
            None,
        ),
    ),
    ids=lambda v: repr(v),
)
def test__write_bdinfo_to_cache(bdinfo_report, cache_filepath, open_exception, exp_return_value, mocker):
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('builtins.open', mocker.mock_open()),
        'open',
    )
    if open_exception:
        mocks.open.side_effect = open_exception

    return_value = bdinfo._write_bdinfo_to_cache(bdinfo_report, cache_filepath)
    assert return_value is None

    if open_exception:
        assert mocks.mock_calls == [
            call.open(cache_filepath, 'w'),
        ]
    else:
        assert mocks.mock_calls == [
            call.open(cache_filepath, 'w'),
            call.open().__enter__(),
            call.open().write(bdinfo_report),
            call.open().__exit__(None, None, None),
        ] + (
            # TODO: Remove this when Python 3.12 is no longer supported.
            # Python >= 3.13 calls close() while previous versions don't.
            [call.open().close()]
            if sys.version_info >= (3, 13, 0) else
            []
        )


@pytest.fixture
def _generate_bdinfo_mocks(mocker):
    mocks = Mock()
    mocks.playlist = Mock(filename='00003.mpls', discpath='path/to/disc')
    mocks.bdinfo_directory = 'path/to/bdinfo_directory'
    mocks.attach_mock(mocker.patch('upsies.utils.subproc.run'), 'run')
    mocks.process = mocks.run.return_value
    mocks.process.stdout = ()
    mocks.process.stderr = ()
    return mocks

def test__generate_bdinfo_succeeds(_generate_bdinfo_mocks):
    mocks = _generate_bdinfo_mocks
    mocks.run.return_value.stdout = (
        'Please wait while we scan the disc...\n',
        '00004.mpls\n',
        'Preparing to analyze the following:\n',
        '00004.MPLS --> 00010.M2TS + 00004.M2TS\n',
        '\n',
        '                File           Elapsed      Remaining\n',
        'Scanning   1% - 00010.M2TS     00:02:23  |  01:19:06\r',
        'Scanning  17% - 00010.M2TS     00:05:56  |  01:16:46\r',
        'Scanning  33% - 00004.M2TS     00:12:58  |  00:58:23\r',
        'Scanning  50% - 00004.M2TS     00:16:09  |  00:57:33\r',
        'Scanning  66% - 00004.M2TS     00:32:15  |  00:46:16\r',
        'Scanning  82% - 00004.M2TS     00:45:15  |  00:45:32\r',
        'Scanning  98% - 00004.M2TS     00:46:39  |  00:37:17\r',
        'Scanning  99% - 00004.M2TS     00:47:00  |  00:31:48\r',
        '\n',
        'Scan completed successfully.\n',
    )

    exp_progress_reports = [
        bdinfo.BdinfoProgress(percent=0, time_elapsed=0, time_remaining=0, playlist=mocks.playlist),
        bdinfo.BdinfoProgress(percent=1, time_elapsed=143, time_remaining=4746, playlist=mocks.playlist),
        bdinfo.BdinfoProgress(percent=17, time_elapsed=356, time_remaining=4606, playlist=mocks.playlist),
        bdinfo.BdinfoProgress(percent=33, time_elapsed=778, time_remaining=3503, playlist=mocks.playlist),
        bdinfo.BdinfoProgress(percent=50, time_elapsed=969, time_remaining=3453, playlist=mocks.playlist),
        bdinfo.BdinfoProgress(percent=66, time_elapsed=1935, time_remaining=2776, playlist=mocks.playlist),
        bdinfo.BdinfoProgress(percent=82, time_elapsed=2715, time_remaining=2732, playlist=mocks.playlist),
        bdinfo.BdinfoProgress(percent=98, time_elapsed=2799, time_remaining=2237, playlist=mocks.playlist),
        bdinfo.BdinfoProgress(percent=99, time_elapsed=2820, time_remaining=1908, playlist=mocks.playlist),
        bdinfo.BdinfoProgress(percent=100, time_elapsed=2820, time_remaining=0, playlist=mocks.playlist),
    ]

    for progress in bdinfo._generate_bdinfo(
            playlist=mocks.playlist,
            bdinfo_directory=mocks.bdinfo_directory,
    ):
        exp_progress_report = exp_progress_reports.pop(0)
        assert progress == exp_progress_report
        assert isinstance(progress.time_elapsed, utils.types.Timestamp), progress.time_elapsed
        assert isinstance(progress.time_remaining, utils.types.Timestamp), progress.time_remaining

    assert mocks.mock_calls == [
        call.run(
            argv=(
                'bdinfo',
                '--mpls=' + mocks.playlist.filename,
                mocks.playlist.discpath,
                mocks.bdinfo_directory,
            ),
            communicate=True,
        ),
        call.run().terminate(),
    ]

def test__generate_bdinfo_handles_bdinfo_stderr(_generate_bdinfo_mocks):
    mocks = _generate_bdinfo_mocks
    mocks.run.return_value.stderr = (
        'Failed to create report!',
        'Report directory is not writable or whatever.',
    )

    exp_msg = (
        'Command failed: '
        + str((
            'bdinfo',
            '--mpls=' + mocks.playlist.filename,
            mocks.playlist.discpath,
            mocks.bdinfo_directory,
        ))
        + ':\n'
        + 'Failed to create report!\n'
        + 'Report directory is not writable or whatever.'
    )
    with pytest.raises(RuntimeError, match=rf'^{re.escape(str(exp_msg))}'):
        tuple(
            bdinfo._generate_bdinfo(
                playlist=mocks.playlist,
                bdinfo_directory=mocks.bdinfo_directory,
            )
        )

    assert mocks.mock_calls == [
        call.run(
            argv=(
                'bdinfo',
                '--mpls=' + mocks.playlist.filename,
                mocks.playlist.discpath,
                mocks.bdinfo_directory,
            ),
            communicate=True,
        ),
        call.run().terminate(),
    ]


@pytest.mark.parametrize(
    argnames='bdinfo_directory, existing_files, exp_result',
    argvalues=(
        pytest.param(
            'path/to/bdinfo_directory',
            {},
            RuntimeError('No BDInfo report found in temporary BDInfo report directory: {tmp_path}/path/to/bdinfo_directory'),
            id='Less than one file found',
        ),
        pytest.param(
            'path/to/bdinfo_directory',
            {
                'path/to/bdinfo_directory/1.mkv': 'Foo.',
                'path/to/bdinfo_directory/2.jpg': 'Bar.'},
            RuntimeError("Unexpected files in temporary BDInfo report directory: {tmp_path}/path/to/bdinfo_directory: ['1.mkv', '2.jpg']"),
            id='More than one file found',
        ),
        pytest.param(
            'path/to/bdinfo_directory',
            {'path/to/bdinfo_directory/report.txt': 'This is the BDInfo report.'},
            'This is the BDInfo report.',
            id='One file found',
        ),
    ),
    ids=lambda v: repr(v),
)
def test__find_bdinfo_in_directory(bdinfo_directory, existing_files, exp_result, tmp_path):
    bdinfo_directory = tmp_path / bdinfo_directory
    bdinfo_directory.mkdir(parents=True, exist_ok=True)
    for relpath, content in existing_files.items():
        abspath = tmp_path / relpath
        abspath.parent.mkdir(parents=True, exist_ok=True)
        abspath.write_text(content)

    if isinstance(exp_result, Exception):
        exp_result = type(exp_result)(str(exp_result).format(tmp_path=tmp_path))
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            bdinfo._find_bdinfo_in_directory(bdinfo_directory)
    else:
        return_value = bdinfo._find_bdinfo_in_directory(bdinfo_directory)
        assert return_value == exp_result
