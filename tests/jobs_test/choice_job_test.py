import re
import sys
from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from upsies import errors
from upsies.jobs import dialog


@pytest.fixture
def make_ChoiceJob(tmp_path):
    def make_ChoiceJob(**kwargs):
        return dialog.ChoiceJob(home_directory=tmp_path, cache_directory=tmp_path, **kwargs)
    return make_ChoiceJob


def test_name_property(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    assert job.name == 'foo'


def test_label_property(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    assert job.label == 'Foo'


def test_question_property(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'), question='Pick a number:')
    assert job.question == 'Pick a number:'
    job.question = 'Peeck ay numbeur:'
    assert job.question == 'Peeck ay numbeur:'
    job.question = None
    assert job.question is None
    job.question = ''
    assert job.question == ''


def test_options_getter(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    assert job.options is job._options
    assert job._options == [('1', '1'), ('2', '2'), ('3', '3')]

def test_options_setter_with_strings(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    job.options = ('a', 'b', 'c')
    assert job._options == [('a', 'a'), ('b', 'b'), ('c', 'c')]

@pytest.mark.parametrize('invalid_sequence', (['b'], ['b', 2, 'foo']))
def test_options_setter_with_invalid_sequence(invalid_sequence, make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    with pytest.raises(ValueError, match=(
            r'^Option must be 2-tuple, '
            rf'not {re.escape(str(invalid_sequence))}$'
    )):
        job.options = (['a', 1], invalid_sequence, ['c', 3])
    assert job._options == [('1', '1'), ('2', '2'), ('3', '3')]

def test_options_setter_with_valid_sequence(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    job.options = (['a', 1], ['b', 2], ['c', 3])
    assert job._options == [('a', 1), ('b', 2), ('c', 3)]

def test_options_setter_with_invalid_option(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    with pytest.raises(ValueError, match=r'^Option must be 2-tuple, not None$'):
        job.options = (['a', 1], None, ['c', 3])
    assert job._options == [('1', '1'), ('2', '2'), ('3', '3')]

def test_options_setter_with_too_few_options(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    with pytest.raises(ValueError, match=r"^There must be at least 2 options: \['a'\]$"):
        job.options = ['a']
    assert job._options == [('1', '1'), ('2', '2'), ('3', '3')]

@pytest.mark.parametrize('focused_option', (('2', 2), '2', 2))
def test_options_setter_preserves_focus_if_possible(focused_option, make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=(('1', 1), ('2', 2), ('3', 3)))
    job.focused = ('2', 2)
    assert job.focused == ('2', 2)
    job.options = (['a', 0], ['b', 1], ['c', 2], ['d', 3], ['e', 4])
    assert job.focused == ('c', 2)

@pytest.mark.parametrize('focused_option', (('2', 2), '2', 2))
def test_options_setter_defaults_focus_to_first_option(focused_option, make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=(('1', 1), ('2', 2), ('3', 3)))
    job.focused = ('3', 3)
    assert job.focused == ('3', 3)
    job.options = (['0', 0], ['1', 1], ['2', 2])
    assert job.focused == ('0', 0)

def test_options_setter_emits_dialog_updated_signal(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=(('1', 1), ('2', 2), ('3', 3)))
    cb = Mock()
    job.signal.register('dialog_updated', cb)
    job.options = (['0', 0], ['1', 1], ['2', 2])
    assert cb.call_args_list == [call()]

def test_options_change_emits_dialog_updated_signal(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=(('1', 1), ('2', 2), ('3', 3)))
    cb = Mock()
    job.signal.register('dialog_updated', cb)
    job.options.append(['3', 3])
    assert cb.call_args_list == [call()]


def test_multichoice(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=(('1', 1), ('2', 2), ('3', 3)))
    mock = Mock()
    job._multichoice = mock
    assert job.multichoice is mock


@pytest.mark.parametrize(
    argnames='options, thing, exp_result',
    argvalues=(
        # None
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], None, None),

        # option
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], ('Foo', 10), 0),
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], ('Bar', 20), 1),
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], ('Baz', 30), 2),

        # Index
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], -1, 0),
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], 0, 0),
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], 1, 1),
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], 2, 2),
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], 3, 2),

        # Label
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], 'Foo', 0),
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], 'Bar', 1),
        ([('Foo', 10), ('Bar', 20), ('Baz', 30)], 'Baz', 2),

        # Value
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], 1.1, 0),
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], 2.2, 1),
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], 3.3, 2),

        # Regular expression match against label
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], re.compile(r'(?i:foo)'), 0),
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], re.compile(r'r$'), 1),
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], re.compile(r'^[Bb]az$'), 2),

        # Regular expression match against value
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], re.compile(r'\d\.1'), 0),
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], re.compile(r'2+$'), 1),
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], re.compile(r'3\.?3'), 2),

        # No matching option found
        ([('Foo', 1.1), ('Bar', 2.2), ('Baz', 3.3)], 'foo', ValueError("No such option: 'foo'")),
    ),
    ids=lambda v: str(v),
)
def test_get_index(options, thing, exp_result, make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=options)

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            job.get_index(thing)
    else:
        index = job.get_index(thing)
        assert index == exp_result

@pytest.mark.parametrize(
    argnames='options, index, focused_index, exp_return_value',
    argvalues=(
        ([('a', 1), ('b', 2), ('c', 3)], 0, 2, ('a', 1)),
        ([('a', 1), ('b', 2), ('c', 3)], 1, 1, ('b', 2)),
        ([('a', 1), ('b', 2), ('c', 3)], 2, 0, ('c', 3)),
        ([('a', 1), ('b', 2), ('c', 3)], None, 2, ('c', 3)),
        ([('a', 1), ('b', 2), ('c', 3)], None, 1, ('b', 2)),
        ([('a', 1), ('b', 2), ('c', 3)], None, 0, ('a', 1)),
    ),
    ids=lambda v: repr(v),
)
def test_get_option(options, index, focused_index, exp_return_value, make_ChoiceJob, mocker):
    job = make_ChoiceJob(name='foo', label='Foo', options=options)
    mocker.patch.object(job, 'get_index', return_value=index)
    mocker.patch.object(type(job), 'focused_index', PropertyMock(return_value=focused_index))

    return_value = job.get_option('something')
    assert return_value == exp_return_value
    assert job.get_index.call_args_list == [call('something')]


def test_focused_index(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    assert job.focused_index is job._focused_index
    delattr(job, '_focused_index')
    assert job.focused_index is None


@pytest.mark.parametrize(
    argnames='options, focused_index, exp_focused',
    argvalues=(
        ((('1', 1), ('2', 2), ('3', 3)), None, None),
        ((('1', 1), ('2', 2), ('3', 3)), 0, ('1', 1)),
        ((('1', 1), ('2', 2), ('3', 3)), 1, ('2', 2)),
        ((('1', 1), ('2', 2), ('3', 3)), 2, ('3', 3)),
    ),
)
def test_focused_getter(options, focused_index, exp_focused, make_ChoiceJob, mocker):
    job = make_ChoiceJob(name='foo', label='Foo', options=options)
    mocker.patch.object(type(job), 'focused_index', PropertyMock(return_value=focused_index))
    assert job.focused == exp_focused

@pytest.mark.parametrize(
    argnames='get_index_return_value, exp_focused_index',
    argvalues=(
        (None, 0),
        (0, 0),
        (123, 123),
    ),
)
def test_focused_setter(get_index_return_value, exp_focused_index, make_ChoiceJob, mocker):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'get_index', return_value=get_index_return_value), 'get_index')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    job.focused = 'something'
    assert job.focused_index == exp_focused_index
    assert mocks.mock_calls == [
        call.get_index('something'),
        call.emit('dialog_updated'),
    ]


@pytest.mark.parametrize(
    argnames='multichoice, options, autodetected_indexes, exp_autodetected',
    argvalues=(
        (False, (('1', 1), ('2', 2), ('3', 3)), (), None),
        (False, (('1', 1), ('2', 2), ('3', 3)), (0,), ('1', 1)),
        (False, (('1', 1), ('2', 2), ('3', 3)), (1,), ('2', 2)),
        (False, (('1', 1), ('2', 2), ('3', 3)), (2,), ('3', 3)),
        (True, (('1', 1), ('2', 2), ('3', 3)), (), ()),
        (True, (('1', 1), ('2', 2), ('3', 3)), (0,), (('1', 1),)),
        (True, (('1', 1), ('2', 2), ('3', 3)), (1,), (('2', 2),)),
        (True, (('1', 1), ('2', 2), ('3', 3)), (2,), (('3', 3),)),
        (True, (('1', 1), ('2', 2), ('3', 3)), (0, 2), (('1', 1), ('3', 3))),
    ),
)
def test_autodetected_getter(multichoice, options, autodetected_indexes, exp_autodetected, make_ChoiceJob, mocker):
    job = make_ChoiceJob(name='foo', label='Foo', options=options, multichoice=multichoice)
    mocker.patch.object(type(job), 'autodetected_indexes', PropertyMock(return_value=autodetected_indexes))
    assert job.autodetected == exp_autodetected

@pytest.mark.parametrize(
    argnames='multichoice, autodetected, get_index, exp_mock_calls, exp_return_value',
    argvalues=(
        pytest.param(
            False,
            'foo',
            Mock(side_effect=(None,)),
            [
                call.get_index('foo'),
                call.emit('dialog_updated'),
            ],
            (),
            id='multichoice=False, autodetected is string, get_index() returns None',
        ),

        pytest.param(
            False,
            'foo',
            Mock(side_effect=(1, 2, 3)),
            [
                call.get_index('foo'),
                call.emit('dialog_updated'),
            ],
            (1,),
            id='multichoice=False, autodetected is string, get_index() returns index',
        ),

        pytest.param(
            False,
            '',
            Mock(side_effect=(1, 2, 3)),
            [
                call.get_index(''),
                call.emit('dialog_updated'),
            ],
            (1,),
            id="multichoice=False, autodetected='' (~False), get_index() returns index",
        ),

        pytest.param(
            False,
            ('foo', 'bar', 'baz'),
            Mock(side_effect=(None, None, None)),
            [
                call.get_index(('foo', 'bar', 'baz')),
                call.emit('dialog_updated'),
            ],
            (),
            id='multichoice=False, autodetected is sequence, get_index() returns None',
        ),

        pytest.param(
            False,
            ('foo', 'bar', 'baz'),
            Mock(side_effect=(1, 2, 3)),
            [
                call.get_index(('foo', 'bar', 'baz')),
                call.emit('dialog_updated'),
            ],
            (1,),
            id='multichoice=False, autodetected is sequence, get_index() returns index',
        ),

        pytest.param(
            True,
            'foo',
            Mock(side_effect=(None,)),
            [
                call.get_index('foo'),
                call.emit('dialog_updated'),
            ],
            (),
            id='multichoice=True, autodetected is string, get_index() returns None',
        ),

        pytest.param(
            True,
            'foo',
            Mock(side_effect=(1, 2, 3)),
            [
                call.get_index('foo'),
                call.emit('dialog_updated'),
            ],
            (1,),
            id='multichoice=True, autodetected is string, get_index() returns index',
        ),

        pytest.param(
            True,
            ('foo', 'bar', 'baz'),
            Mock(side_effect=(None, None, None)),
            [
                call.get_index('foo'),
                call.get_index('bar'),
                call.get_index('baz'),
                call.emit('dialog_updated'),
            ],
            (),
            id='multichoice=True, autodetected is sequence, get_index() returns None',
        ),

        pytest.param(
            True,
            ('foo', 'bar', 'baz'),
            Mock(side_effect=(1, 2, 3)),
            [
                call.get_index('foo'),
                call.get_index('bar'),
                call.get_index('baz'),
                call.emit('dialog_updated'),
            ],
            (1, 2, 3),
            id='multichoice=True, autodetected is sequence, get_index() returns index',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_autodetected_setter(multichoice, autodetected, get_index, exp_mock_calls, exp_return_value, make_ChoiceJob, mocker):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'), multichoice=multichoice)

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'get_index', get_index), 'get_index')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    print('!!!', repr(autodetected))
    job.autodetected = autodetected
    assert job.autodetected_indexes == exp_return_value
    assert mocks.mock_calls == exp_mock_calls


def test_autodetected_indexes(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=('1', '2', '3'))
    mock = ['mock', 'indexes']
    job._autodetected_indexes = mock
    assert job.autodetected_indexes == ('mock', 'indexes')
    delattr(job, '_autodetected_indexes')
    assert job.autodetected_indexes == ()


@pytest.mark.parametrize(
    argnames='multichoice, _choice, exp_return_value',
    argvalues=(
        (False, None, None),
        (False, 'foo', 'foo'),
        (True, None, ()),
        (True, (1, 2, 3), (1, 2, 3)),
    ),
    ids=lambda v: str(v),
)
def test_choice_property(multichoice, _choice, exp_return_value, make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=(('a', 1), ('b', 2), ('c', 3)), multichoice=multichoice)
    if _choice is not None:
        job._choice = _choice
    assert job.choice == exp_return_value


@pytest.mark.parametrize(
    argnames='multichoice, _choice, choice, exp_exception, exp_choice, exp_mock_calls',
    argvalues=(
        (False, 'mychoice', 'newchoice', RuntimeError('foo: Choice was already made: mychoice'), 'mychoice', []),
        (False, None, 'newchoice', None, 'value:newchoice', [call('newchoice')]),
        (True, None, 'newchoice', None, ('value:newchoice',), [call('newchoice')]),
        (True, None, ('newchoice1', 'newchoice2'), None, ('value:newchoice1', 'value:newchoice2'), [
            call('newchoice1'),
            call('newchoice2'),
        ]),
    ),
    ids=lambda v: str(v),
)
def test__set_choice(multichoice, _choice, choice, exp_exception, exp_choice, exp_mock_calls, make_ChoiceJob, mocker):
    job = make_ChoiceJob(name='foo', label='Foo', options=('ignored', 'options'), multichoice=multichoice)
    if _choice is not None:
        job._choice = _choice

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(job, 'get_option', side_effect=lambda choice: (f'label:{choice}', f'value:{choice}')),
        'get_option',
    )

    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            job._set_choice(choice)
    else:
        job._set_choice(choice)
        assert job._choice == exp_choice
        assert job.get_option.call_args_list == exp_mock_calls


# ValueError class for which instances are equal if their error are equal.
class MyValueError(ValueError, errors.UpsiesError):
    pass

@pytest.mark.parametrize(
    argnames='multichoice, thing, validate, exp_mock_calls',
    argvalues=(
        (False, 'the thing', None, [
            call.get_option('the thing'),
            call.add_output('label:the thing'),
            call.finalize(),
        ]),
        (False, 'the thing', Mock(), [
            call.get_option('the thing'),
            call.validate((('label:the thing', 'value:the thing'),)),
            call.add_output('label:the thing'),
            call.finalize(),
        ]),
        (False, 'the thing', Mock(side_effect=MyValueError('no such thing!')), [
            call.get_option('the thing'),
            call.validate((('label:the thing', 'value:the thing'),)),
            call.warn(MyValueError('no such thing!')),
        ]),
        (True, 'the thing', None, [
            call.get_option('the thing'),
            call.add_output('label:the thing'),
            call.finalize(),
        ]),
        (True, ('the thing', 'the other thing'), Mock(), [
            call.get_option('the thing'),
            call.get_option('the other thing'),
            call.validate((('label:the thing', 'value:the thing'), ('label:the other thing', 'value:the other thing'))),
            call.add_output('label:the thing'),
            call.add_output('label:the other thing'),
            call.finalize(),
        ]),
        (True, ('the thing', 'the other thing'), Mock(side_effect=MyValueError('no such thing!')), [
            call.get_option('the thing'),
            call.get_option('the other thing'),
            call.validate((('label:the thing', 'value:the thing'), ('label:the other thing', 'value:the other thing'))),
            call.warn(MyValueError('no such thing!')),
        ]),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_make_choice(multichoice, thing, validate, exp_mock_calls, make_ChoiceJob, mocker):
    job = make_ChoiceJob(name='foo', label='Foo', options=('ignored', 'options'),
                         multichoice=multichoice, validate=validate)

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch.object(job, 'get_option', side_effect=lambda v: (f'label:{v}', f'value:{v}')),
        'get_option',
    )
    if validate:
        mocks.attach_mock(validate, 'validate')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job, 'warn'), 'warn')
    mocks.attach_mock(mocker.patch.object(job, 'finalize'), 'finalize')

    job.make_choice(thing)
    assert mocks.mock_calls == exp_mock_calls


def test_set_label(make_ChoiceJob):
    job = make_ChoiceJob(name='foo', label='Foo', options=(('a', 1), ('b', 2), ('c', 3)))
    cb = Mock()
    job.signal.register('dialog_updated', cb)

    job.set_label('a', 'the a')
    assert job.options == [('the a', 1), ('b', 2), ('c', 3)]
    assert cb.call_args_list == [call()]

    job.set_label(2, 'the b')
    assert job.options == [('the a', 1), ('the b', 2), ('c', 3)]
    assert cb.call_args_list == [call(), call()]

    job.set_label(('c', 3), 'the c')
    assert job.options == [('the a', 1), ('the b', 2), ('the c', 3)]
    assert cb.call_args_list == [call(), call(), call()]

    job.set_label('d', 'the d')
    assert job.options == [('the a', 1), ('the b', 2), ('the c', 3)]
    assert cb.call_args_list == [call(), call(), call()]


@pytest.mark.parametrize(
    argnames='multichoice, autodetected, focused, exp_multichoice, exp_autodetected, exp_focused',
    argvalues=(
        (False, None, None, False, None, ('a', 'a')),
        (True, None, None, True, (), ('a', 'a')),
        (False, 'b', None, False, ('b', 'b'), ('b', 'b')),
        (True, ('b',), None, True, (('b', 'b'),), ('b', 'b')),
        (False, 'b', 'c', False, ('b', 'b'), ('c', 'c')),
        (True, 'b', 'c', True, (('b', 'b'),), ('c', 'c')),
    ),
    ids=lambda v: repr(v),
)
def test_initialize_sets_properties(
        multichoice, autodetected, focused,
        exp_autodetected, exp_focused, exp_multichoice,
        make_ChoiceJob, mocker,
):
    autodetect = Mock()
    job = make_ChoiceJob(
        name='foo',
        label='Foo',
        options=('a', 'b', 'c'),
        question='Wat?',
        multichoice=multichoice,
        autodetect=autodetect,
        autofinish=True,
        autodetected=autodetected,
        focused=focused,
    )
    assert job._name == 'foo'
    assert job._label == 'Foo'
    assert job._autodetect is autodetect
    assert job._autofinish is True
    assert job.options == [('a', 'a'), ('b', 'b'), ('c', 'c')]
    assert job.question == 'Wat?'
    assert job.autodetected == exp_autodetected
    assert job.focused == exp_focused
    assert job.multichoice == exp_multichoice

@pytest.mark.asyncio
async def test_initialize_sets_choice_property_when_output_is_cached(make_ChoiceJob):
    job1 = make_ChoiceJob(name='foo', label='Foo', options=(('a', 1), ('b', 2), ('c', 3)), ignore_cache=False)
    job1.start()
    job1.make_choice('b')
    await job1.wait_finished()
    assert job1.is_finished
    assert job1.output == ('b',)
    assert job1.choice == 2

    # Running the same job again should finish it immediately and set `choice`
    # as well as `output`
    job2 = make_ChoiceJob(name='foo', label='Foo', options=(('a', 1), ('b', 2), ('c', 3)), ignore_cache=False)
    job2.start()
    await job2.wait_finished()
    assert job2.is_finished
    assert job2.output == ('b',)
    assert job2.choice == 2


@pytest.mark.parametrize(
    argnames='attributes, autodetected, exp_autodetected, exp_focused, exp_mock_calls',
    argvalues=(
        pytest.param(
            {'_autodetect': False, '_multichoice': False, '_autofinish': False},
            ('b', 'c'),
            None,
            ('a', 0),
            [
                call.signal.emit('autodetecting'),
                call.signal.emit('autodetected'),
                call.finalization(),
            ],
            id="autodetect=False, multichoice=False, autofinish=False, autodetected=('b', 'c')",
        ),

        pytest.param(
            {'_autodetect': False, '_multichoice': False, '_autofinish': True},
            ('b', 'c'),
            None,
            ('a', 0),
            [
                call.signal.emit('autodetecting'),
                call.signal.emit('autodetected'),
                call.finalization(),
            ],
            id="autodetect=False, multichoice=False, autofinish=True, autodetected=('b', 'c')",
        ),

        pytest.param(
            {'_autodetect': False, '_multichoice': True, '_autofinish': False},
            ('b', 'c'),
            (),
            ('a', 0),
            [
                call.signal.emit('autodetecting'),
                call.signal.emit('autodetected'),
                call.finalization(),
            ],
            id="autodetect=False, multichoice=True, autofinish=False, autodetected=('b', 'c')",
        ),

        pytest.param(
            {'_autodetect': True, '_multichoice': False, '_autofinish': False},
            'b',
            ('b', 1),
            ('b', 1),
            [
                call.signal.emit('autodetecting'),
                call._call_autodetect(),
                call.signal.emit('dialog_updated'),
                call.signal.emit('dialog_updated'),
                call.signal.emit('autodetected'),
                call.finalization(),
            ],
            id="autodetect=True, multichoice=False, autofinish=False, autodetected='b'",
        ),

        pytest.param(
            {'_autodetect': False, '_multichoice': True, '_autofinish': True},
            ('b', 'c'),
            (),
            ('a', 0),
            [
                call.signal.emit('autodetecting'),
                call.signal.emit('autodetected'),
                call.finalization(),
            ],
            id="autodetect=False, multichoice=True, autofinish=True, autodetected=('b', 'c')",
        ),

        pytest.param(
            {'_autodetect': True, '_multichoice': False, '_autofinish': True},
            'c',
            ('c', 2),
            ('c', 2),
            [
                call.signal.emit('autodetecting'),
                call._call_autodetect(),
                call.signal.emit('dialog_updated'),
                call.signal.emit('dialog_updated'),
                call.signal.emit('autodetected'),
                call.make_choice(('c', 2)),
                call.finalization(),
            ],
            id="autodetect=True, multichoice=False, autofinish=True, autodetected='c'",
        ),

        pytest.param(
            {'_autodetect': True, '_multichoice': True, '_autofinish': False},
            ('c', 'b'),
            (('c', 2), ('b', 1)),
            ('c', 2),
            [
                call.signal.emit('autodetecting'),
                call._call_autodetect(),
                call.signal.emit('dialog_updated'),
                call.signal.emit('dialog_updated'),
                call.signal.emit('autodetected'),
                call.finalization(),
            ],
            id="autodetect=True, multichoice=True, autofinish=False, autodetected=('c', 'b')",
        ),

        pytest.param(
            {'_autodetect': True, '_multichoice': True, '_autofinish': True},
            ('b', 'c'),
            (('b', 1), ('c', 2)),
            ('b', 1),
            [
                call.signal.emit('autodetecting'),
                call._call_autodetect(),
                call.signal.emit('dialog_updated'),
                call.signal.emit('dialog_updated'),
                call.signal.emit('autodetected'),
                call.make_choice((('b', 1), ('c', 2))),
                call.finalization(),
            ],
            id="autodetect=True, multichoice=True, autofinish=True, autodetected=('b', 'c')",
        ),

        pytest.param(
            {'_autodetect': True, '_multichoice': True, '_autofinish': True},
            (),
            (),
            ('a', 0),
            [
                call.signal.emit('autodetecting'),
                call._call_autodetect(),
                call.signal.emit('dialog_updated'),
                call.signal.emit('dialog_updated'),
                call.signal.emit('autodetected'),
                call.finalization(),
            ],
            id="autodetect=True, multichoice=True, autofinish=True, autodetected=()",
        ),

        pytest.param(
            {'_autodetect': True, '_multichoice': False, '_autofinish': True},
            None,
            None,
            ('a', 0),
            [
                call.signal.emit('autodetecting'),
                call._call_autodetect(),
                call.signal.emit('autodetected'),
                call.finalization(),
            ],
            id="autodetect=True, multichoice=False, autofinish=True, autodetected=None",
        ),

        pytest.param(
            {'_autodetect': True, '_multichoice': False, '_autofinish': True},
            0,
            ('a', 0),
            ('a', 0),
            [
                call.signal.emit('autodetecting'),
                call._call_autodetect(),
                call.signal.emit('dialog_updated'),
                call.signal.emit('dialog_updated'),
                call.signal.emit('autodetected'),
                call.make_choice(('a', 0)),
                call.finalization(),
            ],
            id="autodetect=True, multichoice=False, autofinish=True, autodetected=0",
        ),

        pytest.param(
            {'_autodetect': True, '_multichoice': True, '_autofinish': True},
            0,
            (('a', 0),),
            ('a', 0),
            [
                call.signal.emit('autodetecting'),
                call._call_autodetect(),
                call.signal.emit('dialog_updated'),
                call.signal.emit('dialog_updated'),
                call.signal.emit('autodetected'),
                call.make_choice((('a', 0),)),
                call.finalization(),
            ],
            id="autodetect=True, multichoice=True, autofinish=True, autodetected=0",
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_run(attributes, autodetected, exp_autodetected, exp_focused, exp_mock_calls, make_ChoiceJob, mocker):
    job = make_ChoiceJob(name='foo', label='Foo', options=(('a', 0), ('b', 1), ('c', 2)))
    for name, value in attributes.items():
        setattr(job, name, value)

    mocks = Mock()
    mocker.patch.object(type(job), 'signal', PropertyMock())
    mocks.attach_mock(job.signal, 'signal')
    mocks.attach_mock(
        mocker.patch.object(job, '_call_autodetect', return_value=autodetected),
        '_call_autodetect',
    )
    mocks.attach_mock(mocker.patch.object(job, 'make_choice'), 'make_choice')
    mocks.attach_mock(mocker.patch.object(job, 'finalization', AsyncMock()), 'finalization')

    await job.run()

    assert job.autodetected == exp_autodetected
    assert job.focused == exp_focused
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='autodetect, exp_result',
    argvalues=(
        (None, None),
        (AsyncMock(return_value='autodetected'), 'autodetected'),
        (Mock(return_value='autodetected'), 'autodetected'),
        ('wat', RuntimeError("Bad autodetect value: 'wat'")),
        (0, RuntimeError("Bad autodetect value: 0")),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test__call_autodetect(autodetect, exp_result, make_ChoiceJob, mocker):
    # TODO: Remove when Python 3.9 is no longer supported.
    # inspect.iscoroutinefunction() returns False for AsynMock() instances in
    # Python 3.9.
    if sys.version_info < (3, 10, 0):
        if isinstance(autodetect, AsyncMock):
            async def autodetect_proper(job, _return_value=autodetect.return_value):
                return _return_value

            autodetect = autodetect_proper

    job = make_ChoiceJob(
        name='foo',
        label='Foo',
        options=('a', 'b', 'c'),
        autodetect=autodetect,
    )

    if isinstance(exp_result, BaseException):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            await job._call_autodetect()
    else:
        return_value = await job._call_autodetect()
        assert return_value == exp_result
