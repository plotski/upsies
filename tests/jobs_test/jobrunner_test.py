from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from upsies.jobs import jobrunner


class Job(Mock):
    def __init__(self, name=None, **kwargs):
        super().__init__(**kwargs)
        self.configure_mock(name=name)


@pytest.mark.parametrize(
    argnames='id, exp_id',
    argvalues=(
        ('myjobrunner', 'myjobrunner'),
        (None, 'anonymous'),
        (123, '123'),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='jobs, exp_add_calls',
    argvalues=(
        (
            None,
            [call.add()],
        ),
        (
            ('foo', 'bar', 'baz'),
            [call.add('foo', 'bar', 'baz')],
        ),
    ),
    ids=lambda v: repr(v),
)
def test_JobRunner___init__(jobs, exp_add_calls, id, exp_id, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.jobs.jobrunner.JobRunner.add'), 'add')

    kwargs = {}
    if id is not None:
        kwargs['id'] = id
    if jobs is not None:
        kwargs['jobs'] = jobs

    jr = jobrunner.JobRunner(**kwargs)
    assert jr._id == exp_id
    assert mocks.mock_calls == exp_add_calls


def test_JobRunner___getitem__():
    jobs = {'foo': Job('foo'), 'bar': Job('bar'), 'baz': Job('baz')}
    jr = jobrunner.JobRunner(jobs=jobs.values())
    assert jr['foo'] is jobs['foo']
    assert jr['bar'] is jobs['bar']
    assert jr['baz'] is jobs['baz']


def test_JobRunner___iter__():
    jobs = {'foo': Job('foo'), 'bar': Job('bar'), 'baz': Job('baz')}
    jr = jobrunner.JobRunner(jobs=jobs.values())
    assert tuple(jr) == tuple(jobs)


def test_JobRunner___len__():
    jobs = {'foo': Job('foo'), 'bar': Job('bar'), 'baz': Job('baz')}
    jr = jobrunner.JobRunner(jobs=jobs.values())
    assert len(jr) == len(jobs)


def test_JobRunner_add_unique_jobs():
    jobs = {'foo': Job('foo'), 'bar': Job('bar'), 'baz': Job('baz')}
    jr = jobrunner.JobRunner()
    for job in jobs.values():
        jr.add(job)
    assert jr._jobs == jobs


def test_JobRunner_add_the_same_job_twice():
    jobs = {'foo': Job('foo'), 'bar': Job('bar'), 'baz': Job('baz')}
    jr = jobrunner.JobRunner()
    for job in jobs.values():
        jr.add(job)
    jr.add(jobs['bar'])
    assert jr._jobs == jobs


def test_JobRunner_add_different_job_with_duplicate_name():
    jobs = {'foo': Job('foo'), 'bar': Job('bar'), 'baz': Job('baz')}
    jr = jobrunner.JobRunner()
    for job in jobs.values():
        jr.add(job)
    with pytest.raises(RuntimeError, match=r"^Conflicting job name: 'foo'$"):
        jr.add(Job('foo'))
    assert jr._jobs == jobs


def test_JobRunner_all_jobs():
    jobs = {
        'foo': Job('foo', is_enabled=False),
        'bar': Job('bar', is_enabled=True),
        'baz': Job('baz', is_enabled=False),
    }
    jr = jobrunner.JobRunner(jobs.values())
    assert jr.all_jobs == tuple(jobs.values())


def test_JobRunner_enabled_jobs():
    jobs = {
        'foo': Job('foo', is_enabled=True),
        'bar': Job('bar', is_enabled=False),
        'baz': Job('baz', is_enabled=True),
    }
    jr = jobrunner.JobRunner(jobs.values())
    assert jr.enabled_jobs == (jobs['foo'], jobs['baz'])


def test_JobRunner_start_more_jobs(mocker):
    mocks = Mock()
    jobs = {
        'a': Job('a', is_started=False, is_terminated=False, autostart=False, is_enabled=False, start=mocks.a.start),
        'b': Job('b', is_started=False, is_terminated=False, autostart=False, is_enabled=True, start=mocks.b.start),
        'c': Job('c', is_started=False, is_terminated=False, autostart=True, is_enabled=False, start=mocks.c.start),
        'd': Job('d', is_started=False, is_terminated=False, autostart=True, is_enabled=True, start=mocks.d.start),
        'e': Job('e', is_started=False, is_terminated=True, autostart=False, is_enabled=False, start=mocks.e.start),
        'f': Job('f', is_started=False, is_terminated=True, autostart=False, is_enabled=True, start=mocks.f.start),
        'g': Job('g', is_started=False, is_terminated=True, autostart=True, is_enabled=False, start=mocks.g.start),
        'h': Job('h', is_started=False, is_terminated=True, autostart=True, is_enabled=True, start=mocks.h.start),
        'i': Job('i', is_started=True, is_terminated=False, autostart=False, is_enabled=False, start=mocks.i.start),
        'j': Job('j', is_started=True, is_terminated=False, autostart=False, is_enabled=True, start=mocks.j.start),
        'k': Job('k', is_started=True, is_terminated=False, autostart=True, is_enabled=False, start=mocks.k.start),
        'l': Job('l', is_started=True, is_terminated=False, autostart=True, is_enabled=True, start=mocks.l.start),
        'm': Job('m', is_started=True, is_terminated=True, autostart=False, is_enabled=False, start=mocks.m.start),
        'n': Job('n', is_started=True, is_terminated=True, autostart=False, is_enabled=True, start=mocks.n.start),
        'o': Job('o', is_started=True, is_terminated=True, autostart=True, is_enabled=False, start=mocks.o.start),
        'p': Job('p', is_started=True, is_terminated=True, autostart=True, is_enabled=True, start=mocks.p.start),
    }

    jr = jobrunner.JobRunner(jobs.values())
    jr.start_more_jobs()
    assert mocks.mock_calls == [
        call.d.start(),
    ]


@pytest.mark.asyncio
async def test_JobRunner_wait(mocker):
    mocks = AsyncMock()
    jobs = {
        'a': Job('a', is_started=True, is_finished=True, wait_finished=mocks.a.wait_finished),    # Already finished.
        'b': Job('b', is_started=True, is_finished=False, wait_finished=mocks.b.wait_finished),   # Started but not finished.
        'c': Job('c', is_started=False, is_finished=False, wait_finished=mocks.c.wait_finished),  # Started by b_wait_finished().
        'd': Job('d', is_started=False, is_finished=False, wait_finished=mocks.d.wait_finished),  # Started by c_wait_finished().
        'e': Job('e', is_started=False, is_finished=False, wait_finished=mocks.e.wait_finished),  # Never started.
    }

    import logging
    _log = logging.getLogger(__name__)

    def b_wait_finished():
        _log.debug('Finishing job b, starting job c')
        jr['b'].is_finished = True  # Finish this job.
        jr['c'].is_started = True   # Start next job.

    mocks.b.wait_finished.side_effect = b_wait_finished

    def c_wait_finished():
        _log.debug('Finishing job c, starting job d')
        jr['c'].is_finished = True  # Finish this job.
        jr['d'].is_started = True   # Start next job.

    mocks.c.wait_finished.side_effect = c_wait_finished

    def d_wait_finished():
        _log.debug('Finishing job d')
        jr['d'].is_finished = True  # Finish this job.

    mocks.d.wait_finished.side_effect = d_wait_finished

    jr = jobrunner.JobRunner(jobs.values())
    await jr.wait()
    assert mocks.mock_calls == [
        call.b.wait_finished(),
        call.c.wait_finished(),
        call.d.wait_finished(),
    ]


@pytest.mark.parametrize(
    argnames='reason, exp_reason',
    argvalues=(
        (None, None),
        ('Because', 'Because'),
    ),
)
def test_JobRunner_terminate(reason, exp_reason):
    mocks = Mock()
    jobs = {
        'a': Job('a', terminate=mocks.a.terminate),
        'b': Job('b', terminate=mocks.b.terminate),
        'c': Job('c', terminate=mocks.c.terminate),
    }

    jr = jobrunner.JobRunner(jobs.values())
    if reason is None:
        jr.terminate()
    else:
        jr.terminate(reason=reason)

    assert mocks.mock_calls == [
        call.a.terminate(reason=exp_reason),
        call.b.terminate(reason=exp_reason),
        call.c.terminate(reason=exp_reason),
    ]


def test_JobRunner_all_jobs_finished(mocker):
    jr = jobrunner.JobRunner()
    mocker.patch.object(type(jr), 'enabled_jobs', PropertyMock(return_value=(
        Job('a', is_finished=False),
        Job('b', is_finished=False),
        Job('c', is_finished=False),
    )))
    assert jr.all_jobs_finished is False
    jr.enabled_jobs[1].is_finished = True
    assert jr.all_jobs_finished is False
    jr.enabled_jobs[0].is_finished = True
    assert jr.all_jobs_finished is False
    jr.enabled_jobs[2].is_finished = True
    assert jr.all_jobs_finished is True


def test_JobRunner_exceptions(mocker):
    jr = jobrunner.JobRunner()
    mocker.patch.object(type(jr), 'all_jobs', PropertyMock(return_value=(
        Job('a', raised='foo'),
        Job('b', raised=None),
        Job('c', raised='baz'),
    )))
    assert jr.exceptions == ('foo', 'baz')
