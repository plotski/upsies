import errno
import re
import sys
from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from upsies import errors, utils
from upsies.jobs import screenshots
from upsies.utils.daemon import MsgType


@pytest.fixture
def job(tmp_path, mocker):
    DaemonProcess_mock = Mock(
        return_value=Mock(
            join=AsyncMock(),
        ),
    )
    mocker.patch('upsies.utils.daemon.DaemonProcess', DaemonProcess_mock)
    return screenshots.ScreenshotsJob(
        home_directory=str(tmp_path / 'home'),
        cache_directory=str(tmp_path / 'cache'),
        ignore_cache=False,
        content_path='some/path',
        exclude_files=('exclude', 'these', 'files'),
        timestamps=(120,),
        count=2,
    )


def test_ScreenshotsJob_name():
    assert screenshots.ScreenshotsJob.name == 'screenshots'


def test_ScreenshotsJob_label():
    assert screenshots.ScreenshotsJob.label == 'Screenshots'


def test_ScreenshotsJob_cache_id(job):
    assert job.cache_id is None


def get_count_sync():
    return 123

async def get_count_async():
    return 456

@pytest.mark.parametrize(
    argnames='count, exp_count, exp_count_callable',
    argvalues=(
        pytest.param(123, 123, None, id='count is number'),
        pytest.param(get_count_sync, 0, get_count_sync, id='count is synchronous callable'),
        pytest.param(get_count_async, 0, get_count_async, id='count is synchronous callable'),
    ),
    ids=lambda v: repr(v),
)
def test_ScreenshotsJob_initialize(count, exp_count, exp_count_callable, tmp_path):
    job = screenshots.ScreenshotsJob(
        home_directory=tmp_path,
        cache_directory=tmp_path,
        ignore_cache=False,
        content_path='some/path',
        precreated=('custom1.png', 'custom2.png'),
        exclude_files=['foo', 'bar', 'baz'],
        timestamps=(120,),
        count=count,
        from_all_videos='<from_all_videos>',
        optimize='<optimize>',
    )
    assert job._content_path == 'some/path'
    assert job._precreated == ('custom1.png', 'custom2.png')
    assert job._exclude_files == ['foo', 'bar', 'baz']
    assert job._screenshots_created == 0
    assert job._screenshots_total == -1
    assert job._timestamps == (120,)
    assert job._count == exp_count
    assert job._count_callable == exp_count_callable
    assert job._from_all_videos == '<from_all_videos>'
    assert job._optimize == '<optimize>'
    assert job._screenshots_process is None
    assert job._optimize_process is None
    assert 'screenshots_total' in job.signal.signals

    assert job.output == ()
    assert job.errors == ()
    assert not job.is_finished
    assert job.exit_code is None


@pytest.mark.parametrize(
    argnames='is_disc, optimize, exp_calls',
    argvalues=(
        (
            True,
            None,
            [
                call._execute_screenshots_process(),
                call.is_disc('some/path', multidisc=True),
                call._process_playlists(),
                call._screenshots_process.join(),
            ],
        ),
        (
            False,
            None,
            [
                call._execute_screenshots_process(),
                call.is_disc('some/path', multidisc=True),
                call._process_file_or_directory(),
                call._screenshots_process.join(),
            ],
        ),
        (
            True,
            '<optimize_process>',
            [
                call._execute_screenshots_process(),
                call._execute_optimize_process(),
                call.is_disc('some/path', multidisc=True),
                call._process_playlists(),
                call._screenshots_process.join(),
                call._optimize_process.join(),
            ],
        ),
        (
            False,
            '<optimize_process>',
            [
                call._execute_screenshots_process(),
                call._execute_optimize_process(),
                call.is_disc('some/path', multidisc=True),
                call._process_file_or_directory(),
                call._screenshots_process.join(),
                call._optimize_process.join(),
            ],
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_ScreenshotsJob_run(is_disc, optimize, exp_calls, job, mocker):
    mocks = Mock(
        _optimize_process=Mock(join=AsyncMock()),
        _screenshots_process=Mock(join=AsyncMock()),
    )
    mocks.attach_mock(
        mocker.patch.object(job, '_execute_screenshots_process'),
        '_execute_screenshots_process',
    )
    mocks.attach_mock(
        mocker.patch.object(job, '_execute_optimize_process'),
        '_execute_optimize_process',
    )
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_disc', return_value=is_disc), 'is_disc')
    mocks.attach_mock(mocker.patch.object(job, '_process_playlists'), '_process_playlists')
    mocks.attach_mock(mocker.patch.object(job, '_process_file_or_directory'), '_process_file_or_directory')
    mocker.patch.object(type(job), '_optimize', PropertyMock(return_value=optimize), create=True)
    job._screenshots_process = mocks._screenshots_process
    if optimize not in ('none', None):
        job._optimize_process = mocks._optimize_process

    await job.run()

    assert mocks.mock_calls == exp_calls


@pytest.mark.parametrize(
    argnames='count, count_callable, exp_count',
    argvalues=(
        pytest.param(123, None, 123, id='count is not callable'),
        pytest.param(123, Mock(return_value=456), 456, id='count is synchronous callable'),
        pytest.param(123, AsyncMock(return_value=789), 789, id='count is asynchronous callable'),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_ScreenshotsJob__execute_screenshots_process(count, count_callable, exp_count, job, mocker):
    # TODO: Remove this when Python 3.9 is no longer supported.
    if sys.version_info < (3, 10):
        pytest.skip(reason="AsyncMock doesn't behave in Python <= 3.9")

    job._count = count
    job._count_callable = count_callable
    DaemonProcess_mock = mocker.patch('upsies.utils.daemon.DaemonProcess')
    await job._execute_screenshots_process()
    assert job._screenshots_process is DaemonProcess_mock.return_value
    assert DaemonProcess_mock.call_args_list == [call(
        name='_screenshots_process',
        target=screenshots._screenshots_process,
        kwargs={
            'precreated': job._precreated,
            'exclude_files': job._exclude_files,
            'timestamps': job._timestamps,
            'count': exp_count,
            'from_all_videos': job._from_all_videos,
            'output_dir': job.cache_directory,
            'overwrite': job.ignore_cache,
        },
        info_callback=job._handle_info,
        error_callback=job._handle_error,
    )]
    assert job._screenshots_process.start.call_args_list == [call()]
    assert job._count == exp_count


@pytest.mark.parametrize(
    argnames='optimize, exp_ignore_dependency_error',
    argvalues=(
        ('default', True),
        ('custom', False),
    ),
    ids=lambda v: repr(v),
)
def test_ScreenshotsJob__execute_optimize_process(optimize, exp_ignore_dependency_error, job, mocker):
    mocker.patch.object(type(job), 'ignore_cache', PropertyMock(Return_value='mock ignore_cache'))
    mocker.patch.object(job, '_optimize', optimize)
    DaemonProcess_mock = mocker.patch('upsies.utils.daemon.DaemonProcess')
    job._execute_optimize_process()
    assert job._optimize_process is DaemonProcess_mock.return_value
    assert DaemonProcess_mock.call_args_list == [call(
        name='_optimize_process',
        target=screenshots._optimize_process,
        kwargs={
            'level': job._optimize,
            'overwrite': job.ignore_cache,
            'ignore_dependency_error': exp_ignore_dependency_error,
            'cache_directory': job.cache_directory,
        },
        info_callback=job._handle_info,
        error_callback=job._handle_error,
    )]
    assert job._optimize_process.start.call_args_list == [call()]


@pytest.mark.asyncio
async def test_ScreenshotsJob__process_playlists(job, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'receive_all'), 'receive_all')
    mocks.receive_all.return_value.__aiter__.return_value = (
        ('path/to/disc1', ('playlist1.1', 'playlist1.2', 'playlist1.3')),
        ('path/to/disc2', ('playlist2.1', 'playlist2.2', 'playlist2.3')),
    )
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process'), '_screenshots_process')

    return_value = await job._process_playlists()
    assert return_value is None

    assert mocks.mock_calls == [
        call.receive_all('playlists', 'playlists_selected', only_posargs=True),
        call.receive_all().__aiter__(),
        call._screenshots_process.send(utils.daemon.MsgType.info, {'source': 'playlist1.1'}),
        call._screenshots_process.send(utils.daemon.MsgType.info, {'source': 'playlist1.2'}),
        call._screenshots_process.send(utils.daemon.MsgType.info, {'source': 'playlist1.3'}),
        call._screenshots_process.send(utils.daemon.MsgType.info, {'source': 'playlist2.1'}),
        call._screenshots_process.send(utils.daemon.MsgType.info, {'source': 'playlist2.2'}),
        call._screenshots_process.send(utils.daemon.MsgType.info, {'source': 'playlist2.3'}),
        call._screenshots_process.send(utils.daemon.MsgType.info, {'source': screenshots._ALL_SOURCES_SELECTED}),
    ]


@pytest.mark.asyncio
async def test_ScreenshotsJob__process_file_or_directory(job, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process'), '_screenshots_process')

    return_value = await job._process_file_or_directory()
    assert return_value is None

    assert mocks.mock_calls == [
        call._screenshots_process.send(utils.daemon.MsgType.info, {'source': job._content_path}),
        call._screenshots_process.send(utils.daemon.MsgType.info, {'source': screenshots._ALL_SOURCES_SELECTED}),
    ]


def test_ScreenshotsJob__handle_info_when_job_is_finished(job, mocker):
    mocker.patch('upsies.utils.fs.file_size', return_value=123)
    mocks = Mock()
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=True))
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process').send, '_screenshots_process_send')
    mocks.attach_mock(mocker.patch.object(job, '_optimize_process').send, '_optimize_process_send')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    job._handle_info({})

    assert mocks.mock_calls == []
    assert job.screenshots_total == -1

def test_ScreenshotsJob__handle_info_gets_screenshots_total(job, mocker):
    mocker.patch('upsies.utils.fs.file_size', return_value=123)
    mocks = Mock()
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=False))
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process').send, '_screenshots_process_send')
    mocks.attach_mock(mocker.patch.object(job, '_optimize_process').send, '_optimize_process_send')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    job._handle_info({'screenshots_total': 24})

    assert mocks.mock_calls == [
        call.emit('screenshots_total', 24),
    ]
    assert job.screenshots_total == 24

@pytest.mark.parametrize('file_size', (123, None))
def test_ScreenshotsJob__handle_info_gets_screenshot_filepath_with_optimization_enabled(file_size, job, mocker):
    mocker.patch('upsies.utils.fs.file_size', return_value=file_size)
    mocks = Mock()
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=False))
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process').send, '_screenshots_process_send')
    mocks.attach_mock(mocker.patch.object(job, '_optimize_process').send, '_optimize_process_send')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    job._handle_info({
        'screenshot_filepath': 'path/to/screenshot1.jpg',
        'video_filepath': 'path/to/release/file.mkv',
        'source': 'path/to/release/',
    })
    assert mocks.mock_calls == [
        call._optimize_process_send(MsgType.info, {
            'screenshot_filepath': 'path/to/screenshot1.jpg',
            'video_filepath': 'path/to/release/file.mkv',
            'source': 'path/to/release/',
        }),
    ]
    assert job.screenshots_total == -1

@pytest.mark.parametrize('is_final_screenshot', (True, False), ids=('Is final screenshot', 'Is not final screenshot'))
@pytest.mark.parametrize('file_size', (123, None))
def test_ScreenshotsJob__handle_info_gets_screenshot_filepath_with_optimization_disabled(file_size, is_final_screenshot, job, mocker):
    mocker.patch('upsies.utils.fs.file_size', return_value=file_size)
    mocks = Mock()
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=False))
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process').send, '_screenshots_process_send')
    mocker.patch.object(job, '_optimize_process', None)
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    mocker.patch.object(type(job), 'screenshots_created', PropertyMock(return_value=123))
    mocker.patch.object(type(job), 'screenshots_total', PropertyMock(return_value=(123 if is_final_screenshot else 124)))

    job._handle_info({
        'screenshot_filepath': 'path/to/screenshot1.jpg',
        'video_filepath': 'path/to/release/file.mkv',
        'source': 'path/to/release/',
    })
    exp_mock_calls = [
        call.add_output('path/to/screenshot1.jpg', 'path/to/release/file.mkv', 'path/to/release/'),
    ]
    assert mocks.mock_calls == exp_mock_calls

@pytest.mark.parametrize('is_final_screenshot', (True, False), ids=('Is final screenshot', 'Is not final screenshot'))
@pytest.mark.parametrize('file_size', (123, None))
def test_ScreenshotsJob__handle_info_gets_optimized_screenshot_filepath(file_size, is_final_screenshot, job, mocker):
    mocker.patch('upsies.utils.fs.file_size', return_value=file_size)
    mocks = Mock()
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=False))
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process').send, '_screenshots_process_send')
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process').stop, '_screenshots_process_stop')
    mocks.attach_mock(mocker.patch.object(job, '_optimize_process').stop, '_optimize_process_stop')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    mocker.patch.object(type(job), 'screenshots_created', PropertyMock(return_value=123))
    mocker.patch.object(type(job), 'screenshots_total', PropertyMock(return_value=(123 if is_final_screenshot else 124)))

    job._handle_info({
        'optimized_screenshot_filepath': 'path/to/optimized_screenshot1.jpg',
        'video_filepath': 'path/to/release/file.mkv',
        'source': 'path/to/release/',
    })
    exp_mock_calls = [
        call.add_output('path/to/optimized_screenshot1.jpg', 'path/to/release/file.mkv', 'path/to/release/'),
    ]
    if is_final_screenshot:
        exp_mock_calls.append(call._optimize_process_stop())
    assert mocks.mock_calls == exp_mock_calls

def test_ScreenshotsJob__handle_info_gets_invalid_info(job, mocker):
    mocks = Mock()
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=False))
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process').send, '_screenshots_process_send')
    mocks.attach_mock(mocker.patch.object(job, '_screenshots_process').stop, '_screenshots_process_stop')
    mocks.attach_mock(mocker.patch.object(job, '_optimize_process').stop, '_optimize_process_stop')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    with pytest.raises(RuntimeError, match=r'Unexpected info: ' + re.escape(str({'foo': 'bar'}))):
        job._handle_info({'foo': 'bar'})
    assert mocks.mock_calls == []


@pytest.mark.parametrize(
    argnames='is_finished, error, exp_exception, exp_mock_calls',
    argvalues=(
        (True, errors.ScreenshotError('Foo!'), None, []),
        (False, errors.ScreenshotError('Foo!'), None, [call.error(errors.ScreenshotError('Foo!'))]),
        (False, errors.ImageOptimizeError('Bar!'), None, [call.error(errors.ImageOptimizeError('Bar!'))]),
        (False, errors.DependencyError('Baz!'), None, [call.error(errors.DependencyError('Baz!'))]),
        (False, ValueError('wat?'), ValueError('wat?'), []),
        (False, 'Huh...', None, [call.error('Huh...')]),
    ),
    ids=lambda v: repr(v),
)
def test_ScreenshotsJob__handle_error(is_finished, error, exp_exception, exp_mock_calls, job, mocker):
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=is_finished))
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')
    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$$'):
            job._handle_error(error)
    else:
        return_value = job._handle_error(error)
        assert return_value is None
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='reason, exp_parent_terminate_calls',
    argvalues=(
        (None, [call.parent_terminate(reason=None)]),
        ('Because!', [call.parent_terminate(reason='Because!')]),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='screenshots_process, exp_screenshots_process_stop_calls',
    argvalues=(
        (None, []),
        (Mock(name='screenshots_process'), [call.screenshots_process_stop()]),
    ),
)
@pytest.mark.parametrize(
    argnames='optimize_process, exp_optimize_process_stop_calls',
    argvalues=(
        (None, []),
        (Mock(name='optimize_process'), [call.optimize_process_stop()]),
    ),
)
def test_ScreenshotsJob_terminate(
        reason, exp_parent_terminate_calls,
        screenshots_process, exp_screenshots_process_stop_calls,
        optimize_process, exp_optimize_process_stop_calls,
        job, mocker,
):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.jobs.base.JobBase.terminate'), 'parent_terminate')

    if screenshots_process:
        mocks.attach_mock(screenshots_process.stop, 'screenshots_process_stop')
        screenshots_process.stop.reset_mock()
    job._screenshots_process = screenshots_process

    print(repr(optimize_process))
    if optimize_process:
        mocks.attach_mock(optimize_process.stop, 'optimize_process_stop')
        optimize_process.stop.reset_mock()
    job._optimize_process = optimize_process

    if reason is None:
        job.terminate()
    else:
        job.terminate(reason=reason)

    exp_mock_calls = (
        exp_screenshots_process_stop_calls
        + exp_optimize_process_stop_calls
        + exp_parent_terminate_calls
    )
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='is_finished, screenshots_total, output, exp_exit_code',
    argvalues=(
        pytest.param(
            False, -1, (), None,
            id='Not finished',
        ),
        pytest.param(
            True, -1, ('screenshot1.png', 'screenshot2.png', 'screenshot3.png'), 0,
            id='screenshots_total was never sent and we have output',
        ),
        pytest.param(
            True, -1, (), 1,
            id='screenshots_total was never sent and we have no output',
        ),
        pytest.param(
            True, 3, ('screenshot1.png', 'screenshot2.png', 'screenshot3.png'), 0,
            id='screenshots_total was sent and len(output) matches',
        ),
        pytest.param(
            True, 3, ('screenshot1.png', 'screenshot2.png'), 1,
            id='screenshots_total was sent and len(output) does not match',
        ),
    ),
)
@pytest.mark.asyncio
async def test_ScreenshotsJob_exit_code(is_finished, screenshots_total, output, exp_exit_code, job, mocker):
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=is_finished))
    mocker.patch.object(type(job), 'screenshots_total', PropertyMock(return_value=screenshots_total))
    mocker.patch.object(type(job), 'output', PropertyMock(return_value=output))
    assert job.exit_code == exp_exit_code


def test_ScreenshotsJob_exclude_files(job, mocker):
    mock_exclude_files = Mock()
    job.exclude_files = mock_exclude_files
    assert job.exclude_files is mock_exclude_files

    mocker.patch.object(type(job), 'is_started', PropertyMock(return_value=True))
    with pytest.raises(RuntimeError, match=rf'^Do not call exclude_files\(\) after job is started: {job.name}$'):
        job.exclude_files = Mock()
    assert job.exclude_files is mock_exclude_files


def test_ScreenshotsJob_from_all_videos(job, mocker):
    mock_from_all_videos = Mock()
    job.from_all_videos = mock_from_all_videos
    assert job.from_all_videos is mock_from_all_videos

    mocker.patch.object(type(job), 'is_started', PropertyMock(return_value=True))
    with pytest.raises(RuntimeError, match=rf'^Do not call from_all_videos\(\) after job is started: {job.name}$'):
        job.from_all_videos = Mock()
    assert job.from_all_videos is mock_from_all_videos


def test_ScreenshotsJob_count(job, mocker):
    mock_count = Mock()
    job.count = mock_count
    assert job.count is mock_count

    mocker.patch.object(type(job), 'is_started', PropertyMock(return_value=True))
    with pytest.raises(RuntimeError, match=rf'^Do not call count\(\) after job is started: {job.name}$'):
        job.count = Mock()
    assert job.count is mock_count


def test_ScreenshotsJob_timestamps(job, mocker):
    mock_timestamps = Mock()
    job.timestamps = mock_timestamps
    assert job.timestamps is mock_timestamps

    mocker.patch.object(type(job), 'is_started', PropertyMock(return_value=True))
    with pytest.raises(RuntimeError, match=rf'^Do not call timestamps\(\) after job is started: {job.name}$'):
        job.timestamps = Mock()
    assert job.timestamps is mock_timestamps


def test_ScreenshotsJob_screenshots_total(job):
    assert job.screenshots_total is job._screenshots_total


def test_ScreenshotsJob_screenshots_created(job):
    assert job.screenshots_created is job._screenshots_created


def test_ScreenshotsJob_screenshots_by_file(job):
    job._screenshots_by_file = {
        'path/to/foo.mkv': ['screenshot1', 'screenshot2'],
    }

    screenshots_by_file = job.screenshots_by_file
    assert screenshots_by_file == {
        'path/to/foo.mkv': ('screenshot1', 'screenshot2'),
    }

    screenshots_by_file['path/to/foo.mkv'] = 'This is not the original dict.'
    assert job.screenshots_by_file == {
        'path/to/foo.mkv': ('screenshot1', 'screenshot2'),
    }

    screenshots_by_file['path/to/bar.mkv'] = 'screenshot5'
    assert job.screenshots_by_file == {
        'path/to/foo.mkv': ('screenshot1', 'screenshot2'),
    }

    del screenshots_by_file['path/to/foo.mkv']
    assert job.screenshots_by_file == {
        'path/to/foo.mkv': ('screenshot1', 'screenshot2'),
    }


@pytest.fixture
def ScreenshotsJob_add_output_mocks(job, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('shutil.copy2', side_effect=lambda src, dst: f'{src} -> {dst}'), 'copy2')
    mocks.attach_mock(mocker.patch('upsies.jobs.base.JobBase.add_output'), 'parent_add_output')
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')
    return mocks

def test_ScreenshotsJob_add_output__source_is_playlist(job, ScreenshotsJob_add_output_mocks):
    mocks = ScreenshotsJob_add_output_mocks

    playlist = utils.disc.Playlist(filepath='path/to/release/BDMV/PLAYLIST/00030.mpls', items=())
    job.add_output(
        screenshot_filepath='screenshots/1.png',
        video_filepath='path/to/release/BDMV/STREAM/00003.m2ts',
        source=playlist,
    )

    assert mocks.mock_calls == [
        call.copy2('screenshots/1.png', job.home_directory),
        call.parent_add_output(f'screenshots/1.png -> {job.home_directory}'),
    ]
    assert job.screenshots_created == 1
    assert job.screenshots_by_file == {
        'path/to/release/BDMV/PLAYLIST/00030.mpls': (f'screenshots/1.png -> {job.home_directory}',),
    }

def test_ScreenshotsJob_add_output__cache_and_home_are_same(job, ScreenshotsJob_add_output_mocks, mocker):
    mocks = ScreenshotsJob_add_output_mocks
    mocker.patch.object(type(job), 'home_directory', PropertyMock(return_value=job.cache_directory))

    job.add_output(
        screenshot_filepath='screenshots/1.png',
        video_filepath='path/to/release/foo.mkv',
        source='path/to/release',
    )

    assert mocks.mock_calls == [
        call.parent_add_output('screenshots/1.png'),
    ]
    assert job.screenshots_created == 1
    assert job.screenshots_by_file == {
        'path/to/release/foo.mkv': ('screenshots/1.png',),
    }

def test_ScreenshotsJob_add_output__copy_fails(job, ScreenshotsJob_add_output_mocks, mocker):
    mocks = ScreenshotsJob_add_output_mocks
    mocks.copy2.side_effect = OSError(errno.EACCES, 'Permission denied')

    job.add_output(
        screenshot_filepath='screenshots/1.png',
        video_filepath='path/to/release/foo.mkv',
        source='path/to/release',
    )

    assert mocks.mock_calls == [
        call.copy2('screenshots/1.png', job.home_directory),
        call.error(f'Permission denied: {job.home_directory}'),
    ]
    assert job.screenshots_created == 0
    assert job.screenshots_by_file == {}


@pytest.fixture
def _screenshots_process_mocks(mocker):
    args = {
        'output_queue': Mock(),
        'input_queue': Mock(),
        'precreated': ('path/to/custom1.png', 'path/to/custom2.png', 'path/to/custom3.png'),
        'exclude_files': ('files', 'to', 'exclude'),
        'timestamps': ['1:00:00', '2:00:00', '3:00:00'],
        'count': 123,
        'from_all_videos': False,
        'output_dir': 'path/to/my/screenshots',
        'overwrite': 'my overwrite value',
    }
    mocks = Mock()
    mocks.attach_mock(args['output_queue'], 'output_queue')
    mocks.attach_mock(args['input_queue'], 'input_queue')
    mocks.attach_mock(mocker.patch('upsies.jobs.screenshots._get_screenshot_infos'), '_get_screenshot_infos')
    mocks.attach_mock(mocker.patch('upsies.jobs.screenshots._make_screenshot'), '_make_screenshot')
    return args, mocks

def test__screenshots_process__no_exceptions(_screenshots_process_mocks):
    args, mocks = _screenshots_process_mocks
    screenshot_infos = (
        # source, video_filepath, screenshot_filename_base, timestamp
        ('first/source', 'first/source/video.mkv', 'video', 123),
        ('first/source', 'first/source/video.mkv', 'video', 456),
        ('second/source.mkv', 'second/source.mkv', 'source', 789),
    )
    mocks._get_screenshot_infos.return_value = tuple(screenshot_infos)
    screenshots_made = [
        'first/source/video.123.png',
        'first/source/video.456.png',
        'second/source.789.png',
    ]
    mocks._make_screenshot.side_effect = tuple(screenshots_made)

    return_value = screenshots._screenshots_process(**args)
    assert return_value is None

    exp_mock_calls = [
        call._get_screenshot_infos(
            input_queue=mocks.input_queue,
            custom_timestamps=args['timestamps'],
            count=args['count'],
            exclude_files=args['exclude_files'],
            from_all_videos=args['from_all_videos'],
        ),
        call.output_queue.put((MsgType.info, {'screenshots_total': len(args['precreated']) + len(screenshot_infos)})),
    ]

    exp_mock_calls.extend(
        call.output_queue.put((MsgType.info, {
            'screenshot_filepath': screenshot_filepath,
            'video_filepath': '',
            'source': '',
        }))
        for screenshot_filepath in args['precreated']
    )

    for i, (_source, video_filepath, screenshot_filename_base, timestmap) in enumerate(screenshot_infos):
        exp_mock_calls.extend((
            call._make_screenshot(
                video_filepath=video_filepath,
                screenshot_filename_base=screenshot_filename_base,
                timestamp=timestmap,
                output_dir=args['output_dir'],
                overwrite=args['overwrite'],
            ),
            call.output_queue.put((MsgType.info, {
                'screenshot_filepath': screenshots_made[i],
                'video_filepath': video_filepath,
                'source': screenshot_infos[i][0],
            }))
        ))

    assert mocks.mock_calls == exp_mock_calls

def test__screenshots_process__exception_from__make_screenshot(_screenshots_process_mocks):
    args, mocks = _screenshots_process_mocks
    screenshot_infos = (
        # source, video_filepath, screenshot_filename_base, timestamp
        ('first/source', 'first/source/video.mkv', 'video', 123),
        ('first/source', 'first/source/video.mkv', 'video', 456),
        ('second/source.mkv', 'second/source.mkv', 'source', 789),
    )
    mocks._get_screenshot_infos.return_value = tuple(screenshot_infos)
    mocks._make_screenshot.side_effect = (
        'first/source/video.123.png',
        errors.ScreenshotError('Creating screenshot failed'),
        errors.ScreenshotError('Creating screenshot failed again'),
    )

    return_value = screenshots._screenshots_process(**args)
    assert return_value is None

    exp_mock_calls = [
        call._get_screenshot_infos(
            input_queue=mocks.input_queue,
            custom_timestamps=args['timestamps'],
            count=args['count'],
            exclude_files=args['exclude_files'],
            from_all_videos=args['from_all_videos'],
        ),
        call.output_queue.put((MsgType.info, {'screenshots_total': len(args['precreated']) + len(screenshot_infos)})),
    ]

    exp_mock_calls.extend(
        call.output_queue.put((MsgType.info, {
            'screenshot_filepath': screenshot_filepath,
            'video_filepath': '',
            'source': '',
        }))
        for screenshot_filepath in args['precreated']
    )
    exp_mock_calls.extend((
        call._make_screenshot(
            video_filepath='first/source/video.mkv', screenshot_filename_base='video', timestamp=123,
            output_dir=args['output_dir'], overwrite=args['overwrite'],
        ),
        call.output_queue.put((MsgType.info, {
            'screenshot_filepath': 'first/source/video.123.png',
            'video_filepath': 'first/source/video.mkv',
            'source': 'first/source',
        }))
    ))
    exp_mock_calls.extend((
        call._make_screenshot(
            video_filepath='first/source/video.mkv', screenshot_filename_base='video', timestamp=456,
            output_dir=args['output_dir'], overwrite=args['overwrite'],
        ),
        call.output_queue.put((MsgType.error, errors.ScreenshotError('Creating screenshot failed')))
    ))

    assert mocks.mock_calls == exp_mock_calls


def test__get_screenshot_infos(mocker):
    custom_timestamps = ('1:00:00', '2:00:00', '3:00:00')
    count = '<count>'
    exclude_files = ('*X*', '*.y', '.*.z')
    from_all_videos = '<from_all_videos>'

    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.read_input_queue_key'), 'read_input_queue_key')
    mocks.attach_mock(mocker.patch('upsies.jobs.screenshots._get_video_infos'), '_get_video_infos')
    mocks.attach_mock(mocker.patch('upsies.jobs.screenshots._validate_timestamps'), '_validate_timestamps')
    mocks.read_input_queue_key.side_effect = (
        'path/to/foo.mkv',
        'path/to/bar',
        screenshots._ALL_SOURCES_SELECTED,
    )
    mocks._get_video_infos.side_effect = (
        (
            ('path/to/foo.mkv', 'path/to/foo.mkv', 'foo'),
        ),
        (
            ('path/to/bar', 'path/to/bar/bar1.mkv', 'bar1'),
            ('path/to/bar', 'path/to/bar/bar2.mkv', 'bar2'),
        ),
    )
    mocks._validate_timestamps.side_effect = (
        # path/to/foo.mkv
        (1 * 60 * 60 + 1, 1 * 60 * 60 + 2, 1 * 60 * 60 + 3),
        # path/to/bar/bar1.mkv
        (2 * 60 * 60 + 10,),
        # path/to/bar/bar2.mkv
        (2 * 60 * 60 + 20,),
    )

    return_value = screenshots._get_screenshot_infos(
        input_queue=mocks.input_queue,
        custom_timestamps=custom_timestamps,
        count=count,
        exclude_files=exclude_files,
        from_all_videos=from_all_videos,
    )
    assert return_value == (
        ('path/to/foo.mkv', 'path/to/foo.mkv', 'foo', 1 * 60 * 60 + 1),
        ('path/to/foo.mkv', 'path/to/foo.mkv', 'foo', 1 * 60 * 60 + 2),
        ('path/to/foo.mkv', 'path/to/foo.mkv', 'foo', 1 * 60 * 60 + 3),
        ('path/to/bar', 'path/to/bar/bar1.mkv', 'bar1', 2 * 60 * 60 + 10),
        ('path/to/bar', 'path/to/bar/bar2.mkv', 'bar2', 2 * 60 * 60 + 20),
    )

    assert mocks.mock_calls == [
        call.read_input_queue_key(mocks.input_queue, 'source'),
        call._get_video_infos('path/to/foo.mkv', exclude_files, from_all_videos),
        call._validate_timestamps(video_filepath='path/to/foo.mkv', timestamps=custom_timestamps, count='<count>'),
        call.read_input_queue_key(mocks.input_queue, 'source'),
        call._get_video_infos('path/to/bar', exclude_files, from_all_videos),
        call._validate_timestamps(video_filepath='path/to/bar/bar1.mkv', timestamps=custom_timestamps, count='<count>'),
        call._validate_timestamps(video_filepath='path/to/bar/bar2.mkv', timestamps=custom_timestamps, count='<count>'),
        call.read_input_queue_key(mocks.input_queue, 'source'),
    ]


@pytest.mark.parametrize(
    argnames='source, exp_video_filepaths_from, exp_mock_calls',
    argvalues=(
        (
            utils.disc.Playlist(items=('foo', 'bar', 'baz'), filepath='path/to/my.pls', duration=123),
            'playlist',
            [call._get_video_infos_from_playlist(utils.disc.Playlist(items=('foo', 'bar', 'baz'), filepath='path/to/my.pls', duration=123))],
        ),
        (
            'path/to/foo.mkv',
            'file_or_directory',
            [call._get_video_infos_from_file_or_directory('path/to/foo.mkv', '<exclude_files>', '<from_all_videos>')],
        ),

    ),
    ids=lambda v: repr(v),
)
def test__get_video_infos(source, exp_video_filepaths_from, exp_mock_calls, mocker):
    exclude_files = '<exclude_files>'
    from_all_videos = '<from_all_videos>'

    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.jobs.screenshots._get_video_infos_from_playlist'), '_get_video_infos_from_playlist')
    mocks.attach_mock(mocker.patch('upsies.jobs.screenshots._get_video_infos_from_file_or_directory'), '_get_video_infos_from_file_or_directory')

    return_value = screenshots._get_video_infos(source, exclude_files, from_all_videos)
    if exp_video_filepaths_from == 'playlist':
        assert return_value is mocks._get_video_infos_from_playlist.return_value
    elif exp_video_filepaths_from == 'file_or_directory':
        assert return_value is mocks._get_video_infos_from_file_or_directory.return_value
    else:
        raise RuntimeError(f'Unexpected exp_video_filepaths_from value: {exp_video_filepaths_from!r}')

    assert mocks.mock_calls == exp_mock_calls


def test__get_video_infos_from_playlist__with_nondvd_playlist(mocker):
    playlist = Mock(discname='MyDisc', filename='00001.mpls', largest_item='path/to/01234.m2ts')
    return_value = screenshots._get_video_infos_from_playlist(playlist)
    assert return_value == (
        (playlist, 'path/to/01234.m2ts', 'MyDisc.00001.01234'),
    )

def test__get_video_infos_from_playlist__with_dvd_playlist(mocker):
    playlist = Mock(discname='MyDisc', filename='VTS_03_00.IFO', largest_item='path/to/VTS_03_06.VOB')
    return_value = screenshots._get_video_infos_from_playlist(playlist)
    assert return_value == (
        (playlist, 'path/to/VTS_03_06.VOB', 'MyDisc.VTS_03_06'),
    )


@pytest.mark.parametrize(
    argnames='content_path, video_filepaths, from_all_videos, exp_return_value',
    argvalues=(
        pytest.param(
            'path/to/content',
            ('path/to/content/foo.mkv', 'path/to/content/bar.mkv', 'path/to/content/baz.mkv'),
            True,
            (
                ('path/to/content', 'path/to/content/foo.mkv', 'foo'),
                ('path/to/content', 'path/to/content/bar.mkv', 'bar'),
                ('path/to/content', 'path/to/content/baz.mkv', 'baz'),
            ),
            id='From all videos',
        ),
        pytest.param(
            'path/to/content',
            ('path/to/content/foo.mkv', 'path/to/content/bar.mkv', 'path/to/content/baz.mkv'),
            False,
            (
                ('path/to/content', 'path/to/content/foo.mkv', 'foo'),
            ),
            id='From first video',
        ),
    ),
    ids=lambda v: repr(v),
)
def test__get_video_infos_from_file_or_directory(content_path, video_filepaths, from_all_videos, exp_return_value, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.fs.find_main_videos', return_value=video_filepaths), 'find_main_videos')

    exclude_files = ('*.x', '*.y', '*.z')
    return_value = screenshots._get_video_infos_from_file_or_directory(content_path, exclude_files, from_all_videos)
    assert return_value == exp_return_value
    assert mocks.mock_calls == [call.find_main_videos(content_path, exclude_files)]


@pytest.mark.parametrize(
    argnames='overwrite, screenshot_filepath_exists, exp_screenshot_called',
    argvalues=(
        pytest.param(False, False, True, id='overwrite=False, screenshot does not exist'),
        pytest.param(False, True, False, id='overwrite=False, screenshot does exist'),
        pytest.param(True, False, True, id='overwrite=True, screenshot does not exist'),
        pytest.param(True, True, True, id='overwrite=True, screenshot does exist'),
    ),
    ids=lambda v: repr(v),
)
def test__make_screenshot(overwrite, screenshot_filepath_exists, exp_screenshot_called, mocker, tmp_path):
    video_filepath = 'path/to/my_video.mkv'
    screenshot_filename_base = 'my_video'
    timestamp = '0:12:34'
    output_dir = tmp_path / 'path/to/output/directory'
    exp_screenshot_filepath = output_dir / (screenshot_filename_base + f'.{timestamp}.png')
    if screenshot_filepath_exists:
        exp_screenshot_filepath.parent.mkdir(parents=True, exist_ok=True)
        exp_screenshot_filepath.write_text('screenshot data')

    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.image.screenshot', return_value=str(exp_screenshot_filepath)), 'screenshot')

    return_value = screenshots._make_screenshot(
        video_filepath=video_filepath,
        screenshot_filename_base=screenshot_filename_base,
        timestamp=timestamp,
        output_dir=output_dir,
        overwrite=overwrite,
    )
    assert return_value == mocks.screenshot.return_value
    if exp_screenshot_called:
        assert mocks.mock_calls == [call.screenshot(
            video_file='path/to/my_video.mkv',
            screenshot_file=str(output_dir / 'my_video.0:12:34.png'),
            timestamp='0:12:34',
        )]
    else:
        assert mocks.mock_calls == []


@pytest.mark.parametrize(
    argnames='duration, count, timestamps, exp_result',
    argvalues=(
        # Duration too short
        (-1, 0, [], errors.ContentError('Video duration is too short: -1s')),
        (0, 0, [], errors.ContentError('Video duration is too short: 0s')),
        # Invalid custom timestamp
        (60, 0, [20, 'foo', '0:00:30'], errors.ContentError("Invalid timestamp: 'foo'")),
        # Very short duration
        (1, 0, [], ('0:00:00',)),
        (9, 0, [], ('0:00:04', '0:00:06')),
        (9, 3, [], ('0:00:02', '0:00:04', '0:00:06')),
        (9, 12, [], ('0:00:01', '0:00:02', '0:00:03', '0:00:04', '0:00:05', '0:00:06', '0:00:07', '0:00:08')),
        (60, 0, [], ('0:00:30', '0:00:45')),
        (60, 3, [], ('0:00:15', '0:00:30', '0:00:45')),
        (60, 6, [], ('0:00:15', '0:00:22', '0:00:30', '0:00:37', '0:00:45', '0:00:52')),
        # Normal duration with specific count
        (300, 1, [], ('0:02:30',)),
        (300, 2, [], ('0:02:30', '0:03:45')),
        (300, 3, [], ('0:01:15', '0:02:30', '0:03:45')),
        (300, 4, [], ('0:01:15', '0:02:30', '0:03:45', '0:04:22')),
        # Normal duration with specific timestamps
        (300, 0, [60], ('0:01:00',)),
        (300, 0, [60, 60], ('0:01:00',)),
        (300, 0, [60, '180'], ('0:01:00', '0:03:00')),
        (300, 0, [60, '180', '0:181'], ('0:01:00', '0:03:00', '0:03:01')),
        (300, 0, [60, '180', '002:01:181'], ('0:01:00', '0:03:00', '0:05:00')),
        (86400, 0, [60, '180', '002:01:181'], ('0:01:00', '0:03:00', '2:04:01')),
        # Normal duration with specific timestamps and specific count
        (300, 3, [0], ('0:00:00', '0:02:30', '0:03:45')),
        (300, 3, [300], ('0:02:30', '0:03:45', '0:05:00')),
        (300, 3, [0, 301], ('0:00:00', '0:02:30', '0:05:00')),
        (300, 1, [60], ('0:01:00',)),
        (300, 2, [60], ('0:01:00', '0:03:00')),
        (300, 3, [60], ('0:01:00', '0:02:00', '0:03:00')),
        (300, 3, [60, 180], ('0:01:00', '0:03:00', '0:04:00')),
        # Timestamps are returned sorted
        (300, 3, [60, 180, 120, 90], ('0:01:00', '0:01:30', '0:02:00', '0:03:00')),
        # Timestamps are deduplicated
        (300, 4, [60, '0:60', 90, '1:00'], ('0:01:00', '0:01:30', '0:03:14', '0:04:07')),
    ),
    ids=lambda v: repr(v),
)
def test__validate_timestamps(duration, count, timestamps, exp_result, mocker):
    duration_mock = mocker.patch('upsies.utils.mediainfo.get_duration', return_value=duration)

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            screenshots._validate_timestamps(
                video_filepath='foo.mkv',
                timestamps=timestamps,
                count=count,
            )
    else:
        return_value = screenshots._validate_timestamps(
            video_filepath='foo.mkv',
            timestamps=timestamps,
            count=count,
        )
        assert return_value == tuple(
            utils.types.Timestamp(string)
            for string in exp_result
        )
    assert duration_mock.call_args_list == [call('foo.mkv')]


def test__optimize_process(mocker):
    level = '<level>'
    overwrite = '<overwrite>'
    ignore_dependency_error = '<ignore_dependency_error>'
    cache_directory = '<cache_directory>'
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.jobs.screenshots._optimize_screenshot', return_value=None), '_optimize_screenshot')
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.read_input_queue_until_empty'), 'read_input_queue_until_empty')
    mocks.read_input_queue_until_empty.side_effect = (
        (),
        (
            (MsgType.info, {'screenshot_filepath': 'path/to/1.png', 'video_filepath': 'path/to/video.1.mkv', 'source': 'path/to'}),
        ),
        (), (),
        (
            (MsgType.info, {'screenshot_filepath': 'path/to/2.png', 'video_filepath': 'path/to/video.2.mkv', 'source': 'path/to'}),
            (MsgType.info, {'screenshot_filepath': 'path/to/3.png', 'video_filepath': 'path/to/video.2.mkv', 'source': 'path/to'}),
            (MsgType.info, {'screenshot_filepath': 'path/to/4.png', 'video_filepath': 'path/to/video.3.mkv', 'source': 'path/to'}),
        ),
        (), (), (),
        (
            (MsgType.info, {'screenshot_filepath': 'path/to/5.png', 'video_filepath': 'path/to/video.3.mkv', 'source': 'path/to'}),
            (MsgType.info, {'screenshot_filepath': 'path/to/6.png', 'video_filepath': 'path/to/video.4.mkv', 'source': 'path/to'}),
        ),
        (), (), (), (),
        errors.DaemonProcessTerminated('done'),
    )

    with pytest.raises(errors.DaemonProcessTerminated, match=r'^done$'):
        screenshots._optimize_process(
            mocks.output_queue, mocks.input_queue,
            level=level,
            overwrite=overwrite,
            ignore_dependency_error=ignore_dependency_error,
            cache_directory=cache_directory,
        )

    def _optimize_screenshot_call(screenshot_filepath, video_filepath, source):
        return call._optimize_screenshot(
            output_queue=mocks.output_queue,
            screenshot_filepath=screenshot_filepath,
            video_filepath=video_filepath,
            source=source,
            level=level,
            overwrite=overwrite,
            ignore_dependency_error=ignore_dependency_error,
            cache_directory=cache_directory,
        )

    exp_mock_calls = [
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.

        call.read_input_queue_until_empty(mocks.input_queue),
        _optimize_screenshot_call('path/to/1.png', 'path/to/video.1.mkv', 'path/to'),
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.

        call.read_input_queue_until_empty(mocks.input_queue),
        _optimize_screenshot_call('path/to/2.png', 'path/to/video.2.mkv', 'path/to'),
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.
        _optimize_screenshot_call('path/to/3.png', 'path/to/video.2.mkv', 'path/to'),
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.
        _optimize_screenshot_call('path/to/4.png', 'path/to/video.3.mkv', 'path/to'),
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.

        call.read_input_queue_until_empty(mocks.input_queue),
        _optimize_screenshot_call('path/to/5.png', 'path/to/video.3.mkv', 'path/to'),
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.
        _optimize_screenshot_call('path/to/6.png', 'path/to/video.4.mkv', 'path/to'),
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.
        call.read_input_queue_until_empty(mocks.input_queue),  # Queue is empty.

        call.read_input_queue_until_empty(mocks.input_queue),  # Queue contains terminator.
    ]
    for got_call, exp_call in zip(mocks.mock_calls, exp_mock_calls):
        print('GOT:', got_call)
        print('EXP:', exp_call)
    assert mocks.mock_calls == exp_mock_calls


def run_optimize_screenshot_test(
        *,
        output_queue, screenshot_filepath, video_filepath, source, level, overwrite, ignore_dependency_error, cache_directory,
        optimize_exception,
        exp_mock_calls, exp_exception,
        mocker,
):
    mocks = Mock()
    mocks.attach_mock(output_queue, 'output_queue')

    def optimize_mock(screenshot_filepath, *args, **kwargs):
        if optimize_exception:
            raise optimize_exception
        else:
            print('OPTIMIZING', repr(screenshot_filepath))
            return f'optimized:{screenshot_filepath}'

    mocks.attach_mock(
        mocker.patch('upsies.utils.image.optimize', side_effect=optimize_mock),
        'optimize',
    )

    def run():
        return screenshots._optimize_screenshot(
            output_queue=output_queue,
            screenshot_filepath=screenshot_filepath,
            video_filepath=video_filepath,
            source=source,
            level=level,
            overwrite=overwrite,
            ignore_dependency_error=ignore_dependency_error,
            cache_directory=cache_directory,
        )

    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            run()
    else:
        return_value = run()
        assert return_value is None

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    'ignore_dependency_error',
    (True, False),
    ids=('ignore_dependency_error=True', 'ignore_dependency_error=False'),
)
@pytest.mark.parametrize(
    argnames='overwrite, output_file_exists, exp_optimized',
    argvalues=(
        (False, False, True),
        (False, True, False),
        (True, False, True),
        (True, True, True),
    ),
    ids=lambda v: repr(v),
)
def test__optimize_screenshot_with_existing_output_file(
        ignore_dependency_error,
        overwrite, output_file_exists, exp_optimized,
        mocker, tmp_path,
):
    output_queue = Mock()
    screenshot_filepath = tmp_path / 'screenshot.png'
    video_filepath = tmp_path / 'video.mkv'
    source = video_filepath.parent
    cache_directory = tmp_path / 'path/to/cache'
    level = 'my level'
    exp_output_file = cache_directory / f'screenshot.optimized={level}.png'
    if output_file_exists:
        exp_output_file.parent.mkdir(parents=True, exist_ok=True)
        exp_output_file.write_bytes(b'mock image data')

    if exp_optimized:
        exp_mock_calls = [
            call.optimize(
                str(screenshot_filepath),
                level=level,
                output_file=str(exp_output_file),
            ),
            call.output_queue.put((MsgType.info, {
                'optimized_screenshot_filepath': f'optimized:{screenshot_filepath}',
                'video_filepath': video_filepath,
                'source': str(source),
            })),
        ]
    else:
        exp_mock_calls = [
            call.output_queue.put((MsgType.info, {
                'optimized_screenshot_filepath': str(exp_output_file),
                'video_filepath': video_filepath,
                'source': str(source),
            })),
        ]

    run_optimize_screenshot_test(
        output_queue=output_queue,
        screenshot_filepath=str(screenshot_filepath),
        video_filepath=video_filepath,
        source=str(source),
        level=level,
        overwrite=overwrite,
        ignore_dependency_error=ignore_dependency_error,
        cache_directory=cache_directory,
        optimize_exception=None,
        exp_mock_calls=exp_mock_calls,
        exp_exception=None,
        mocker=mocker,
    )


@pytest.mark.parametrize(
    'ignore_dependency_error',
    (True, False),
    ids=('ignore_dependency_error=True', 'ignore_dependency_error=False'),
)
def test__optimize_screenshot_with_bad_screenshot(ignore_dependency_error, mocker, tmp_path):
    output_queue = Mock()
    screenshot_filepath = tmp_path / 'screenshot.png'
    video_filepath = tmp_path / 'video.mkv'
    source = video_filepath.parent
    cache_directory = tmp_path / 'path/to/cache'
    level = 'my level'
    overwrite = True
    exp_output_file = cache_directory / f'screenshot.optimized={level}.png'

    exp_mock_calls = [
        call.optimize(
            str(screenshot_filepath),
            level=level,
            output_file=str(exp_output_file),
        ),
        call.output_queue.put((MsgType.error, errors.ImageOptimizeError('bad image'))),
    ]

    run_optimize_screenshot_test(
        output_queue=output_queue,
        screenshot_filepath=str(screenshot_filepath),
        video_filepath=video_filepath,
        source=str(source),
        level=level,
        overwrite=overwrite,
        ignore_dependency_error=ignore_dependency_error,
        cache_directory=cache_directory,
        optimize_exception=errors.ImageOptimizeError('bad image'),
        exp_mock_calls=exp_mock_calls,
        exp_exception=None,
        mocker=mocker,
    )


@pytest.mark.parametrize(
    'ignore_dependency_error',
    (True, False),
    ids=('ignore_dependency_error=True', 'ignore_dependency_error=False'),
)
def test__optimize_screenshot_with_missing_dependency(ignore_dependency_error, mocker, tmp_path):
    output_queue = Mock()
    screenshot_filepath = tmp_path / 'screenshot.png'
    video_filepath = tmp_path / 'video.mkv'
    source = video_filepath.parent
    cache_directory = tmp_path / 'path/to/cache'
    level = 'my level'
    overwrite = True
    exp_output_file = cache_directory / f'screenshot.optimized={level}.png'

    exp_mock_calls = [
        call.optimize(
            str(screenshot_filepath),
            level=level,
            output_file=str(exp_output_file),
        ),
    ]
    if ignore_dependency_error:
        exp_mock_calls.append(
            call.output_queue.put((MsgType.info, {
                'optimized_screenshot_filepath': str(screenshot_filepath),
                'video_filepath': video_filepath,
                'source': str(source),
            }))
        )
        exp_exception = None
    else:
        exp_exception = errors.DependencyError('missing dependency')

    run_optimize_screenshot_test(
        output_queue=output_queue,
        screenshot_filepath=str(screenshot_filepath),
        video_filepath=video_filepath,
        source=str(source),
        level=level,
        overwrite=overwrite,
        ignore_dependency_error=ignore_dependency_error,
        cache_directory=cache_directory,
        optimize_exception=errors.DependencyError('missing dependency'),
        exp_mock_calls=exp_mock_calls,
        exp_exception=exp_exception,
        mocker=mocker,
    )


@pytest.mark.parametrize(
    argnames='screenshot_filepath, cache_directory, exp_output_file, exp_optimized_screenshot_filepath',
    argvalues=(
        (
            'path/to/cache/file.png',
            'path/to/cache/',
            'path/to/cache/file.optimized=my level.png',
            'optimized:{tmp_path}/path/to/cache/file.png',
        ),
        (
            'path/to/cache/foo/bar/baz/file.png',
            'path/to/cache/',
            'path/to/cache/foo/bar/baz/file.optimized=my level.png',
            'optimized:{tmp_path}/path/to/cache/foo/bar/baz/file.png',
        ),
        (
            'path/to/foo/bar/baz/file.png',
            'path/to/cache/',
            'path/to/cache/file.optimized=my level.png',
            'optimized:{tmp_path}/path/to/foo/bar/baz/file.png',
        ),
    ),
    ids=lambda v: repr(v),
)
def test__optimize_screenshot_ensures_output_in_cache_directory(
        screenshot_filepath, cache_directory, exp_output_file, exp_optimized_screenshot_filepath,
        mocker, tmp_path,
):
    output_queue = Mock()
    cache_directory = tmp_path / cache_directory
    screenshot_filepath = tmp_path / screenshot_filepath
    video_filepath = tmp_path / 'video.mkv'
    source = video_filepath.parent
    level = 'my level'
    overwrite = True

    print('screenshot_filepath:', str(screenshot_filepath))
    print('cache_directory:', str(cache_directory))
    print('exp_output_file:', str(exp_output_file))

    exp_mock_calls = [
        call.optimize(
            str(screenshot_filepath),
            level=level,
            output_file=str(tmp_path / exp_output_file),
        ),
        call.output_queue.put((MsgType.info, {
            'optimized_screenshot_filepath': exp_optimized_screenshot_filepath.format(tmp_path=tmp_path),
            'video_filepath': video_filepath,
            'source': str(source),
        }))
    ]

    run_optimize_screenshot_test(
        output_queue=output_queue,
        screenshot_filepath=str(screenshot_filepath),
        video_filepath=video_filepath,
        source=str(source),
        level=level,
        overwrite=overwrite,
        ignore_dependency_error=False,
        cache_directory=cache_directory,
        optimize_exception=None,
        exp_mock_calls=exp_mock_calls,
        exp_exception=None,
        mocker=mocker,
    )


@pytest.mark.parametrize(
    'ignore_dependency_error',
    (True, False),
    ids=('ignore_dependency_error=True', 'ignore_dependency_error=False'),
)
def test__optimize_screenshot_succeeds(ignore_dependency_error, mocker, tmp_path):
    output_queue = Mock()
    screenshot_filepath = tmp_path / 'screenshot.png'
    video_filepath = tmp_path / 'video.mkv'
    source = video_filepath.parent
    cache_directory = tmp_path / 'path/to/cache'
    level = 'my level'
    overwrite = True
    exp_output_file = cache_directory / f'screenshot.optimized={level}.png'

    exp_mock_calls = [
        call.optimize(
            str(screenshot_filepath),
            level=level,
            output_file=str(exp_output_file),
        ),
        call.output_queue.put((MsgType.info, {
            'optimized_screenshot_filepath': f'optimized:{screenshot_filepath}',
            'video_filepath': video_filepath,
            'source': str(source),
        }))
    ]

    run_optimize_screenshot_test(
        output_queue=output_queue,
        screenshot_filepath=str(screenshot_filepath),
        video_filepath=video_filepath,
        source=str(source),
        level=level,
        overwrite=overwrite,
        ignore_dependency_error=ignore_dependency_error,
        cache_directory=cache_directory,
        optimize_exception=None,
        exp_mock_calls=exp_mock_calls,
        exp_exception=None,
        mocker=mocker,
    )
