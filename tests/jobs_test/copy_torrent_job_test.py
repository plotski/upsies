import os
from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from upsies.jobs.torrent import CopyTorrentJob


@pytest.fixture
async def make_CopyTorrentJob(tmp_path):
    def make_CopyTorrentJob(destination, torrent_files=()):
        return CopyTorrentJob(
            home_directory=tmp_path,
            cache_directory=tmp_path,
            ignore_cache=False,
            destination=destination,
            torrent_files=torrent_files,
        )
    return make_CopyTorrentJob


def test_cache_id(make_CopyTorrentJob, tmp_path):
    job = make_CopyTorrentJob(destination=tmp_path)
    assert job.cache_id is None


@pytest.mark.parametrize('is_started, exp_hidden', (
    (False, True),
    (True, False),
))
def test_hidden(is_started, exp_hidden, make_CopyTorrentJob, tmp_path, mocker):
    job = make_CopyTorrentJob(destination=tmp_path)
    mocker.patch.object(type(job), 'is_started', PropertyMock(return_value=is_started))
    assert job.hidden is exp_hidden


class AsyncIter:
    def __init__(self, *items):
        self.items = items

    async def __aiter__(self):
        for item in self.items:
            yield item

@pytest.mark.parametrize(
    argnames='torrent_files, exp_torrent_files_mock_calls',
    argvalues=(
        pytest.param(
            (),
            [],
            id='no torrent_files provided',
        ),
        pytest.param(
            ('1.torrent', '2.torrent', '3.torrent'),
            [
                call._copy_torrent('1.torrent'),
                call._copy_torrent('2.torrent'),
                call._copy_torrent('3.torrent'),
            ],
            id='torrent_files provided',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='mocked, exp_torrent_job_mock_calls',
    argvalues=(
        pytest.param(
            {
                'siblings': PropertyMock(return_value={}),
                'receive_all': Mock(return_value=AsyncIter(
                    ('path/to/first.torrent',),
                    ('path/to/second.torrent',),
                    ('path/to/third.torrent',),
                )),
                '_copy_torrent': AsyncMock(),
            },
            [],
            id='torrent job does not exists',
        ),
        pytest.param(
            {
                'siblings': PropertyMock(return_value={'torrent': '<CreateTorrentJob>'}),
                'receive_all': Mock(return_value=AsyncIter(
                    ('path/to/first.torrent',),
                    ('path/to/second.torrent',),
                    ('path/to/third.torrent',),
                )),
                '_copy_torrent': AsyncMock(),
            },
            [
                call.receive_all('torrent', 'output', only_posargs=True),
                call._copy_torrent('path/to/first.torrent'),
                call._copy_torrent('path/to/second.torrent'),
                call._copy_torrent('path/to/third.torrent'),
            ],
            id='torrent job exists',
        ),
    ),
    ids=lambda v: repr(v),
)
async def test_run(
        torrent_files, exp_torrent_files_mock_calls,
        mocked, exp_torrent_job_mock_calls,
        make_CopyTorrentJob, mocker, tmp_path,
):
    job = make_CopyTorrentJob(destination=tmp_path, torrent_files=torrent_files)

    mocks = Mock()
    for attr, mock in mocked.items():
        if isinstance(mock, PropertyMock):
            mocker.patch.object(type(job), attr, mock)
        else:
            mocks.attach_mock(mocker.patch.object(job, attr, mock), attr)

    return_value = await job.run()
    assert return_value is None
    assert mocks.mock_calls == exp_torrent_files_mock_calls + exp_torrent_job_mock_calls


@pytest.mark.asyncio
async def test__copy_torrent_call_order(make_CopyTorrentJob, mocker, tmp_path):
    source = tmp_path / 'foo.torrent'
    source.write_bytes(b'content')

    job = make_CopyTorrentJob(destination=tmp_path)

    mocks = Mock()
    job.signal.register('copying', mocks.copying)
    job.signal.register('copied', mocks.copied)
    mocks.attach_mock(mocker.patch('shutil.copy2', return_value=str(tmp_path / source.name)), 'copy2')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')

    await job._copy_torrent(str(source))

    assert mocks.mock_calls == [
        call.copying(str(source)),
        call.copy2(str(source), str(tmp_path)),
        call.add_output(str(source)),
        call.copied(mocks.copy2.return_value),
    ]

@pytest.mark.asyncio
async def test__copy_torrent_reports_nonexisting_file(make_CopyTorrentJob, mocker, tmp_path):
    source = tmp_path / 'foo.torrent'

    job = make_CopyTorrentJob(destination=tmp_path)

    mocks = Mock()
    job.signal.register('copying', mocks.copying)
    job.signal.register('copied', mocks.copied)
    mocks.attach_mock(mocker.patch('shutil.copy2'), 'copy2')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')

    await job._copy_torrent(source)

    assert mocks.mock_calls == [
        call.error(f'{source}: No such file'),
    ]


@pytest.mark.asyncio
async def test__copy_torrent_gets_large_file(make_CopyTorrentJob, mocker, tmp_path):
    source = tmp_path / 'foo.torrent'
    with open(source, 'wb') as f:
        f.truncate(CopyTorrentJob.MAX_FILE_SIZE + 1)  # Sparse file

    job = make_CopyTorrentJob(destination=tmp_path)

    mocks = Mock()
    job.signal.register('copying', mocks.copying)
    job.signal.register('copied', mocks.copied)
    mocks.attach_mock(mocker.patch('shutil.copy2'), 'copy2')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')

    await job._copy_torrent(source)

    assert mocks.mock_calls == [
        call.error(f'{source}: File is too large'),
    ]


@pytest.mark.asyncio
async def test__copy_torrent_adds_destination_path_on_success(make_CopyTorrentJob, mocker, tmp_path):
    source = tmp_path / 'foo.torrent'
    source.write_bytes(b'content')
    destination = tmp_path / 'bar.torrent'

    job = make_CopyTorrentJob(destination=destination)

    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')

    await job._copy_torrent(source)

    assert mocks.mock_calls == [
        call.add_output(str(destination)),
    ]
    assert os.path.exists(destination)


@pytest.mark.asyncio
async def test__copy_torrent_adds_source_path_on_failure(make_CopyTorrentJob, mocker, tmp_path):
    source = tmp_path / 'foo.torrent'
    source.write_bytes(b'content')
    destination = tmp_path / 'bar.torrent'

    job = make_CopyTorrentJob(destination=destination)

    mocks = Mock()
    job.signal.register('copying', mocks.copying)
    job.signal.register('copied', mocks.copied)
    mocks.attach_mock(mocker.patch('shutil.copy2', side_effect=PermissionError('Permission denied')), 'copy2')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')

    await job._copy_torrent(source)

    assert job.output == ()
    assert mocks.mock_calls == [
        call.copying(source),
        call.copy2(source, str(destination)),
        call.error(f'Failed to copy {source} to {job._destination}: Permission denied'),
    ]


@pytest.mark.asyncio
async def test_info_property(make_CopyTorrentJob, mocker, tmp_path):
    job = make_CopyTorrentJob(destination=tmp_path)

    assert job.info == ''
    job.signal.emit('copying', 'path/to/foo')
    assert job.info == 'Copying foo'
    job.signal.emit('copied', 'path/to/foo')
    assert job.info == ''

    job.info = 'some info'
    job.signal.emit('error', 'some error')
    assert job.info == ''

    job.info = 'some info'
    job.signal.emit('finished', job)
    assert job.info == ''
