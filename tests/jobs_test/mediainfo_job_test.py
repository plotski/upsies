import errno
import re
import sys
from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from upsies import errors, utils
from upsies.jobs import mediainfo


@pytest.fixture
def job(tmp_path):
    return mediainfo.MediainfoJob(
        home_directory=tmp_path,
        cache_directory=tmp_path,
        content_path='path/to/content',
    )


def test_MediainfoJob_cache_id(job):
    assert job.cache_id is None


def test_MediainfoJob_content_path(job):
    assert job.content_path is job._content_path


def test_MediainfoJob_initialize():
    job = mediainfo.MediainfoJob(
        content_path='path/to/my/content',
        from_all_videos='maybe?',
        exclude_files=('foo', 'bar', 'baz'),
        format='my format',
    )
    assert job._content_path == 'path/to/my/content'
    assert job._from_all_videos == 'maybe?'
    assert job._exclude_files == ('foo', 'bar', 'baz')
    assert job._format == 'my format'
    assert job._reports_by_file == {}
    assert job._mediainfo_process is None
    assert 'generating_report' in job.signal.signals
    assert 'generated_report' in job.signal.signals
    assert job._store_report_by_file in job.signal.signals['generated_report']
    assert job._hide_job in job.signal.signals['finished']


def test_MediainfoJob__hide_job(job):
    assert job.hidden is False
    job._hide_job(job)
    assert job.hidden is True


@pytest.mark.parametrize(
    argnames='is_disc, exp_process_calls',
    argvalues=(
        pytest.param(
            True,
            [call._process_playlists()],
            id='Playlist',
        ),
        pytest.param(
            False,
            [call._process_file_or_directory()],
            id='File or directory',
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_MediainfoJob_run(is_disc, exp_process_calls, job, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.DaemonProcess', return_value=Mock(join=AsyncMock())), 'DaemonProcess')
    mocks.attach_mock(mocks.DaemonProcess.return_value.start, 'DaemonProcess_start')
    mocks.attach_mock(mocks.DaemonProcess.return_value.join, 'DaemonProcess_join')
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_disc', return_value=is_disc), 'is_disc')
    mocks.attach_mock(mocker.patch.object(job, '_process_playlists'), '_process_playlists')
    mocks.attach_mock(mocker.patch.object(job, '_process_file_or_directory'), '_process_file_or_directory')
    mocks.attach_mock(mocker.patch.object(job, '_add_mediainfo_reports'), '_add_mediainfo_reports')

    return_value = await job.run()
    assert return_value is None

    assert mocks.mock_calls == [
        call.DaemonProcess(
            target=mediainfo._mediainfo_process,
            kwargs={
                'from_all_videos': job._from_all_videos,
                'exclude_files': job._exclude_files,
                'cache_directory': job.cache_directory,
                'ignore_cache': job.ignore_cache,
            },
            info_callback=job._handle_info,
            error_callback=job._handle_error,
        ),
        call.DaemonProcess_start(),
        call.is_disc(job._content_path, multidisc=True),
        *exp_process_calls,
        call.DaemonProcess_join(),
        call._add_mediainfo_reports(),
    ]


@pytest.mark.parametrize(
    argnames='reason, exp_parent_terminate_calls',
    argvalues=(
        (None, [call.parent_terminate(reason=None)]),
        ('Because!', [call.parent_terminate(reason='Because!')]),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='mediainfo_process, exp_mediainfo_process_stop_calls',
    argvalues=(
        (None, []),
        (Mock(), [call.mediainfo_process_stop()]),
    ),
    ids=lambda v: repr(v),
)
def test_MediainfoJob_terminate(
        reason, exp_parent_terminate_calls,
        mediainfo_process, exp_mediainfo_process_stop_calls,
        job, mocker,
):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.jobs.base.JobBase.terminate'), 'parent_terminate')
    if mediainfo_process:
        mocks.attach_mock(mediainfo_process.stop, 'mediainfo_process_stop')
        mediainfo_process.stop.reset_mock()
    job._mediainfo_process = mediainfo_process

    if reason is None:
        job.terminate()
    else:
        job.terminate(reason=reason)

    exp_mock_calls = exp_mediainfo_process_stop_calls + exp_parent_terminate_calls
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.asyncio
async def test_MediainfoJob__process_playlists(job, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'receive_all'), 'receive_all')
    mocks.receive_all.return_value.__aiter__.return_value = (
        ('path/to/disc1', ('playlist1', 'playlist2', 'playlist3')),
        ('path/to/disc2', ('playlist2', 'playlist3', 'playlist4')),
    )
    mocks.attach_mock(mocker.patch.object(job, '_mediainfo_process'), '_mediainfo_process')

    return_value = await job._process_playlists()
    assert return_value is None

    assert mocks.mock_calls == [
        call.receive_all('playlists', 'playlists_selected', only_posargs=True),
        call.receive_all().__aiter__(),
        call._mediainfo_process.send(utils.daemon.MsgType.info, {'source': 'playlist1'}),
        call._mediainfo_process.send(utils.daemon.MsgType.info, {'source': 'playlist2'}),
        call._mediainfo_process.send(utils.daemon.MsgType.info, {'source': 'playlist3'}),
        call._mediainfo_process.send(utils.daemon.MsgType.info, {'source': 'playlist2'}),
        call._mediainfo_process.send(utils.daemon.MsgType.info, {'source': 'playlist3'}),
        call._mediainfo_process.send(utils.daemon.MsgType.info, {'source': 'playlist4'}),
        call._mediainfo_process.send(utils.daemon.MsgType.info, {'source': mediainfo._ALL_SOURCES_SELECTED}),
    ]


@pytest.mark.asyncio
async def test_MediainfoJob__process_file_or_directory(job, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, '_mediainfo_process'), '_mediainfo_process')

    return_value = await job._process_file_or_directory()
    assert return_value is None

    assert mocks.mock_calls == [
        call._mediainfo_process.send(utils.daemon.MsgType.info, {'source': job.content_path}),
        call._mediainfo_process.send(utils.daemon.MsgType.info, {'source': mediainfo._ALL_SOURCES_SELECTED}),
    ]


@pytest.mark.parametrize(
    argnames='is_finished, info, exp_exception, exp_mock_calls',
    argvalues=(
        (
            True,
            {'generating_report': 'path/to/file.mkv'},
            None,
            [],
        ),
        (
            False,
            {'generating_report': 'path/to/file.mkv'},
            None,
            [call.emit('generating_report', 'path/to/file.mkv')],
        ),
        (
            False,
            {'generated_report': '<mediainfo report>', 'video_filepath': 'path/to/file.mkv'},
            None,
            [call.emit('generated_report', 'path/to/file.mkv', '<mediainfo report>')],
        ),
        (
            False,
            {'foo': 'bar'},
            RuntimeError("Unexpected info: {'foo': 'bar'}"),
            [],
        ),
    ),
    ids=lambda v: repr(v),
)
def test_MediainfoJob__handle_info(is_finished, info, exp_exception, exp_mock_calls, job, mocker):
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=is_finished))
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            job._handle_info(info)
    else:
        return_value = job._handle_info(info)
        assert return_value is None

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='is_finished, error, exp_mock_calls',
    argvalues=(
        (
            True,
            errors.ContentError('Expected error'),
            [],
        ),
        (
            False,
            errors.ContentError('Expected error'),
            [call.error(errors.ContentError('Expected error'))],
        ),
        (
            False,
            errors.UpsiesError('Unexpected error'),
            [call.exception(errors.UpsiesError('Unexpected error'))],
        ),
    ),
    ids=lambda v: repr(v),
)
def test_MediainfoJob__handle_error(is_finished, error, exp_mock_calls, job, mocker):
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=is_finished))
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')
    mocks.attach_mock(mocker.patch.object(job, 'exception'), 'exception')

    return_value = job._handle_error(error)
    assert return_value is None

    assert mocks.mock_calls == exp_mock_calls


def test_MediainfoJob__add_mediainfo_reports(job, mocker):
    mocks = Mock()
    job._format = '[mediainfo]{MEDIAINFO}[/mediainfo]'
    mocker.patch.object(type(job), 'reports_by_file', PropertyMock(return_value={
        'path/to/file1.mkv': '<report for file1.mkv>',
        'path/to/file2.mkv': '<report for file2.mkv>',
        'path/to/file3.mkv': '<report for file3.mkv>',
    }))
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')

    return_value = job._add_mediainfo_reports()
    assert return_value is None

    assert mocks.mock_calls == [
        call.add_output('[mediainfo]<report for file1.mkv>[/mediainfo]'),
        call.add_output('[mediainfo]<report for file2.mkv>[/mediainfo]'),
        call.add_output('[mediainfo]<report for file3.mkv>[/mediainfo]'),
    ]


def test_MediainfoJob__store_report_by_file(job):
    job._store_report_by_file('path/to/file1.mkv', '<mediainfo for file1.mkv>')
    assert job.reports_by_file == {
        'path/to/file1.mkv': '<mediainfo for file1.mkv>',
    }
    job._store_report_by_file('path/to/file2.mkv', '<mediainfo for file2.mkv>')
    assert job.reports_by_file == {
        'path/to/file1.mkv': '<mediainfo for file1.mkv>',
        'path/to/file2.mkv': '<mediainfo for file2.mkv>',
    }
    job._store_report_by_file('path/to/file3.mkv', '<mediainfo for file3.mkv>')
    assert job.reports_by_file == {
        'path/to/file1.mkv': '<mediainfo for file1.mkv>',
        'path/to/file2.mkv': '<mediainfo for file2.mkv>',
        'path/to/file3.mkv': '<mediainfo for file3.mkv>',
    }


def test_MediainfoJob_reports_by_file(job):
    job._reports_by_file = {
        'path/to/file1.mkv': '<mediainfo for file1.mkv>',
        'path/to/file2.mkv': '<mediainfo for file2.mkv>',
        'path/to/file3.mkv': '<mediainfo for file3.mkv>',
    }
    reports_by_file = job.reports_by_file
    assert reports_by_file == {
        'path/to/file1.mkv': '<mediainfo for file1.mkv>',
        'path/to/file2.mkv': '<mediainfo for file2.mkv>',
        'path/to/file3.mkv': '<mediainfo for file3.mkv>',
    }
    reports_by_file.clear()
    assert job.reports_by_file == {
        'path/to/file1.mkv': '<mediainfo for file1.mkv>',
        'path/to/file2.mkv': '<mediainfo for file2.mkv>',
        'path/to/file3.mkv': '<mediainfo for file3.mkv>',
    }


def test__mediainfo_process(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.read_input_queue_key'), 'read_input_queue_key')
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.maybe_terminate'), 'maybe_terminate')
    mocks.attach_mock(mocker.patch('upsies.jobs.mediainfo._send_mediainfo_reports_for_source'), '_send_mediainfo_reports_for_source')
    mocks.read_input_queue_key.side_effect = ('source1', 'source2', 'source3', mediainfo._ALL_SOURCES_SELECTED, 'source4')

    return_value = mediainfo._mediainfo_process(
        output_queue=mocks.output_queue,
        input_queue=mocks.input_queue,
        from_all_videos='<from_all_videos>',
        exclude_files='<excluded_files>',
        cache_directory='path/to/cache',
        ignore_cache='<maybe ignore cache>',
    )
    assert return_value is None

    assert mocks.mock_calls == [
        call.read_input_queue_key(mocks.input_queue, 'source'),
        call.maybe_terminate(mocks.input_queue),
        call._send_mediainfo_reports_for_source(
            output_queue=mocks.output_queue,
            input_queue=mocks.input_queue,
            source='source1',
            from_all_videos='<from_all_videos>',
            exclude_files='<excluded_files>',
            cache_directory='path/to/cache',
            ignore_cache='<maybe ignore cache>',
        ),
        call.read_input_queue_key(mocks.input_queue, 'source'),
        call.maybe_terminate(mocks.input_queue),
        call._send_mediainfo_reports_for_source(
            output_queue=mocks.output_queue,
            input_queue=mocks.input_queue,
            source='source2',
            from_all_videos='<from_all_videos>',
            exclude_files='<excluded_files>',
            cache_directory='path/to/cache',
            ignore_cache='<maybe ignore cache>',
        ),
        call.read_input_queue_key(mocks.input_queue, 'source'),
        call.maybe_terminate(mocks.input_queue),
        call._send_mediainfo_reports_for_source(
            output_queue=mocks.output_queue,
            input_queue=mocks.input_queue,
            source='source3',
            from_all_videos='<from_all_videos>',
            exclude_files='<excluded_files>',
            cache_directory='path/to/cache',
            ignore_cache='<maybe ignore cache>',
        ),
        call.read_input_queue_key(mocks.input_queue, 'source'),
        call.maybe_terminate(mocks.input_queue),
    ]


def test__add_mediainfo_reports_for_source(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.jobs.mediainfo._get_filepaths_for_source'), '_get_filepaths_for_source')
    mocks._get_filepaths_for_source.return_value = (
        'path/to/file1.mkv',
        'path/to/file2.mkv',
        'path/to/file3.mkv',
        'path/to/file4.mkv',
        'path/to/file5.mkv',
    )
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.maybe_terminate'), 'maybe_terminate')
    mocks.attach_mock(mocker.patch('upsies.jobs.mediainfo._get_mediainfo_report'), '_get_mediainfo_report')
    mocks._get_mediainfo_report.side_effect = (
        '<mediainfo for file1.mkv>',
        '<mediainfo for file2.mkv>',
        '<mediainfo for file3.mkv>',
        errors.ContentError('Failed to generate mediainfo report for file4.mkv'),
        '<mediainfo for file5.mkv>',
    )

    return_value = mediainfo._send_mediainfo_reports_for_source(
        output_queue=mocks.output_queue,
        input_queue=mocks.input_queue,
        source='path/to/release',
        from_all_videos='<from_all_videos>',
        exclude_files='<excluded_files>',
        cache_directory='path/to/cache',
        ignore_cache='<maybe ignore cache>',
    )
    assert return_value is None

    assert mocks.mock_calls == [
        call._get_filepaths_for_source('path/to/release', '<from_all_videos>', '<excluded_files>'),

        call.maybe_terminate(mocks.input_queue),
        call.output_queue.put((utils.daemon.MsgType.info, {'generating_report': 'path/to/file1.mkv'})),
        call._get_mediainfo_report('path/to/file1.mkv', 'path/to/cache', '<maybe ignore cache>'),
        call.output_queue.put((utils.daemon.MsgType.info, {'generated_report': '<mediainfo for file1.mkv>', 'video_filepath': 'path/to/file1.mkv'})),

        call.maybe_terminate(mocks.input_queue),
        call.output_queue.put((utils.daemon.MsgType.info, {'generating_report': 'path/to/file2.mkv'})),
        call._get_mediainfo_report('path/to/file2.mkv', 'path/to/cache', '<maybe ignore cache>'),
        call.output_queue.put((utils.daemon.MsgType.info, {'generated_report': '<mediainfo for file2.mkv>', 'video_filepath': 'path/to/file2.mkv'})),

        call.maybe_terminate(mocks.input_queue),
        call.output_queue.put((utils.daemon.MsgType.info, {'generating_report': 'path/to/file3.mkv'})),
        call._get_mediainfo_report('path/to/file3.mkv', 'path/to/cache', '<maybe ignore cache>'),
        call.output_queue.put((utils.daemon.MsgType.info, {'generated_report': '<mediainfo for file3.mkv>', 'video_filepath': 'path/to/file3.mkv'})),

        call.maybe_terminate(mocks.input_queue),
        call.output_queue.put((utils.daemon.MsgType.info, {'generating_report': 'path/to/file4.mkv'})),
        call._get_mediainfo_report('path/to/file4.mkv', 'path/to/cache', '<maybe ignore cache>'),
        call.output_queue.put((utils.daemon.MsgType.error, errors.ContentError('Failed to generate mediainfo report for file4.mkv'))),

        call.maybe_terminate(mocks.input_queue),
        call.output_queue.put((utils.daemon.MsgType.info, {'generating_report': 'path/to/file5.mkv'})),
        call._get_mediainfo_report('path/to/file5.mkv', 'path/to/cache', '<maybe ignore cache>'),
        call.output_queue.put((utils.daemon.MsgType.info, {'generated_report': '<mediainfo for file5.mkv>', 'video_filepath': 'path/to/file5.mkv'})),
    ]


@pytest.fixture
def _get_mediainfo_report_mocks(mocker, tmp_path):
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.jobs.mediainfo._get_cache_filepath', return_value=str(tmp_path / 'cached.mediainfo')),
        '_get_cache_filepath',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.mediainfo.get_mediainfo_report', side_effect=lambda f: f'<mediainfo report for {f}>'),
        'get_mediainfo_report',
    )
    mocks.attach_mock(
        mocker.patch('builtins.open', mocker.mock_open(read_data='<cached mediainfo report>')),
        'open',
    )

    return mocks

def test__get_mediainfo_report__cache_is_ignored(_get_mediainfo_report_mocks, mocker):
    video_filepath = 'path/to/video.mkv'
    cache_directory = 'path/to/cache'
    mocks = _get_mediainfo_report_mocks

    return_value = mediainfo._get_mediainfo_report(
        video_filepath=video_filepath,
        cache_directory=cache_directory,
        ignore_cache=True,
    )
    assert return_value == f'<mediainfo report for {video_filepath}>'
    assert mocks.mock_calls == [
        call._get_cache_filepath(video_filepath, cache_directory),
        call.get_mediainfo_report(video_filepath),
        call.open(mocks._get_cache_filepath.return_value, 'w'),
        call.open().__enter__(),
        call.open().write(return_value),
        call.open().__exit__(None, None, None),
    ] + (
        # TODO: Remove this when Python 3.12 is no longer supported.
        # Python >= 3.13 calls close() while previous versions don't.
        [call.open().close()]
        if sys.version_info >= (3, 13, 0) else
        []
    )

def test__get_mediainfo_report__cache_is_read_successfully(_get_mediainfo_report_mocks, mocker):
    video_filepath = 'path/to/video.mkv'
    cache_directory = 'path/to/cache'
    mocks = _get_mediainfo_report_mocks

    return_value = mediainfo._get_mediainfo_report(
        video_filepath=video_filepath,
        cache_directory=cache_directory,
        ignore_cache=False,
    )
    assert return_value == '<cached mediainfo report>'
    assert mocks.mock_calls == [
        call._get_cache_filepath(video_filepath, cache_directory),
        call.open(mocks._get_cache_filepath.return_value, 'r'),
        call.open().__enter__(),
        call.open().read(),
        call.open().__exit__(None, None, None),
    ] + (
        # TODO: Remove this when Python 3.12 is no longer supported.
        # Python >= 3.13 calls close() while previous versions don't.
        [call.open().close()]
        if sys.version_info >= (3, 13, 0) else
        []
    )

def test__get_mediainfo_report__cache_is_read_unsuccessfully(_get_mediainfo_report_mocks, mocker):
    video_filepath = 'path/to/video.mkv'
    cache_directory = 'path/to/cache'
    mocks = _get_mediainfo_report_mocks
    mocks.open.side_effect = OSError(errno.ENOENT, 'No such file or directory')

    return_value = mediainfo._get_mediainfo_report(
        video_filepath=video_filepath,
        cache_directory=cache_directory,
        ignore_cache=False,
    )
    assert return_value == f'<mediainfo report for {video_filepath}>'
    assert mocks.mock_calls == [
        call._get_cache_filepath(video_filepath, cache_directory),
        call.open(mocks._get_cache_filepath.return_value, 'r'),
        call.get_mediainfo_report(video_filepath),
        call.open(mocks._get_cache_filepath.return_value, 'w'),
    ]

def test__get_mediainfo_report__cache_is_written_successfully(_get_mediainfo_report_mocks, mocker):
    video_filepath = 'path/to/video.mkv'
    cache_directory = 'path/to/cache'
    mocks = _get_mediainfo_report_mocks

    return_value = mediainfo._get_mediainfo_report(
        video_filepath=video_filepath,
        cache_directory=cache_directory,
        ignore_cache=True,
    )
    assert return_value == f'<mediainfo report for {video_filepath}>'
    assert mocks.mock_calls == [
        call._get_cache_filepath(video_filepath, cache_directory),
        call.get_mediainfo_report(video_filepath),
        call.open(mocks._get_cache_filepath.return_value, 'w'),
        call.open().__enter__(),
        call.open().write(return_value),
        call.open().__exit__(None, None, None),
    ] + (
        # TODO: Remove this when Python 3.12 is no longer supported.
        # Python >= 3.13 calls close() while previous versions don't.
        [call.open().close()]
        if sys.version_info >= (3, 13, 0) else
        []
    )

def test__get_mediainfo_report__cache_is_written_unsuccessfully(_get_mediainfo_report_mocks, mocker):
    video_filepath = 'path/to/video.mkv'
    cache_directory = 'path/to/cache'
    mocks = _get_mediainfo_report_mocks
    mocks.open.side_effect = OSError(errno.EACCES, 'Permission denied')

    return_value = mediainfo._get_mediainfo_report(
        video_filepath=video_filepath,
        cache_directory=cache_directory,
        ignore_cache=True,
    )
    assert return_value == f'<mediainfo report for {video_filepath}>'
    assert mocks.mock_calls == [
        call._get_cache_filepath(video_filepath, cache_directory),
        call.get_mediainfo_report(video_filepath),
        call.open(mocks._get_cache_filepath.return_value, 'w'),
    ]


@pytest.mark.parametrize(
    argnames='file_size, exp_file_size',
    argvalues=(
        (None, 0),
        (123456, 123456),
    ),
    ids=lambda v: repr(v),
)
def test__get_cache_filepath(file_size, exp_file_size, mocker):
    mocker.patch('upsies.utils.fs.file_size', return_value=file_size)
    video_filepath = 'path/to/video.mkv'
    cache_directory = 'another/path/to/cache'
    return_value = mediainfo._get_cache_filepath(video_filepath, cache_directory)
    assert return_value == f'another/path/to/cache/video.mkv.{exp_file_size}.mediainfo'


class MockPlaylist(utils.disc.Playlist):
    @property
    def largest_item(self):
        print('largest_item', self.items)
        for item in self.items:
            if item.isupper():
                print(item, item.isupper())
                return item

@pytest.mark.parametrize(
    argnames='source, from_all_videos, exp_return_value',
    argvalues=(
        (MockPlaylist(filepath='path/to/file.ifo', items=('foo', 'BAR', 'baz')), False, ('path/to/file.ifo', 'BAR')),
        (MockPlaylist(filepath='path/to/file.ifo', items=('foo', 'BAR', 'baz')), True, ('path/to/file.ifo', 'BAR')),
        (MockPlaylist(filepath='path/to/file.mpls', items=('foo', 'BAR', 'baz')), False, ('BAR',)),
        (MockPlaylist(filepath='path/to/file.mpls', items=('foo', 'BAR', 'baz')), True, ('BAR',)),
    ),
    ids=lambda v: repr(v),
)
def test__get_filepaths_for_source__with_playlists(source, from_all_videos, exp_return_value):
    return_value = mediainfo._get_filepaths_for_source(source, from_all_videos, '<excluded files>')
    assert return_value == exp_return_value

@pytest.mark.parametrize(
    argnames='source, main_videos, from_all_videos, exp_return_value',
    argvalues=(
        (
            'path/to/release',
            ('path/to/release/foo.mkv', 'path/to/release/bar.mkv', 'path/to/release/baz.mkv'),
            False,
            ('path/to/release/foo.mkv',),
        ),
        (
            'path/to/release',
            ('path/to/release/foo.mkv', 'path/to/release/bar.mkv', 'path/to/release/baz.mkv'),
            True,
            ('path/to/release/foo.mkv', 'path/to/release/bar.mkv', 'path/to/release/baz.mkv'),
        ),
    ),
    ids=lambda v: repr(v),
)
def test__get_filepaths_for_source__with_directory(source, main_videos, from_all_videos, exp_return_value, mocker, tmp_path):
    source = tmp_path / source
    source.mkdir(parents=True, exist_ok=True)
    exclude_files = '<excluded files>'

    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.fs.find_main_videos'), 'find_main_videos')
    mocks.find_main_videos.return_value = main_videos

    return_value = mediainfo._get_filepaths_for_source(str(source), from_all_videos, exclude_files)
    assert return_value == exp_return_value
    assert mocks.mock_calls == [
        call.find_main_videos(str(source), exclude=exclude_files),
    ]

def test__get_filepaths_for_source__with_file(mocker, tmp_path):
    return_value = mediainfo._get_filepaths_for_source('path/to/source', from_all_videos=True, exclude_files='<excluded_files>')
    assert return_value == ('path/to/source',)
    return_value = mediainfo._get_filepaths_for_source('path/to/source', from_all_videos=False, exclude_files='<excluded_files>')
    assert return_value == ('path/to/source',)
