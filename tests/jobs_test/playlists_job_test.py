import re
from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from upsies import errors, utils
from upsies.jobs import playlists


def make_job(tmp_path, *args, **kwargs):
    return playlists.PlaylistsJob(
        *args,
        **{
            'home_directory': tmp_path,
            'cache_directory': tmp_path,
            'content_path': 'path/to/content',
            'select_multiple': False,
            **kwargs,
        },
    )

@pytest.fixture
def job(tmp_path):
    return make_job(tmp_path)


@pytest.mark.parametrize('select_multiple, exp_select_multiple', ((True, 'select_multiple'), (False, None)))
def test_PlaylistsJob_cache_id(select_multiple, exp_select_multiple, job, mocker):
    mocker.patch.object(job, '_content_path', 'path/to/Foo.2000-ASDF')
    mocker.patch.object(job, '_select_multiple', select_multiple)
    exp_cache_id = ['Foo.2000-ASDF']
    if exp_select_multiple is not None:
        exp_cache_id.append(exp_select_multiple)
    assert job.cache_id == exp_cache_id


def test_PlaylistsJob_select_multiple(job, mocker):
    mocker.patch.object(job, '_select_multiple', '<select_multiple>')
    assert job.select_multiple == '<select_multiple>'


@pytest.mark.parametrize('select_multiple, exp_select_multiple', ((0, False), (1, True)))
def test_PlaylistsJob_initialize(select_multiple, exp_select_multiple, tmp_path):
    job = make_job(
        tmp_path,
        home_directory='path/to/home_directory',
        cache_directory='path/to/cache_directory',
        content_path='path/to/content',
        select_multiple=select_multiple,
    )
    assert job.home_directory == 'path/to/home_directory'
    assert job.cache_directory == 'path/to/cache_directory'
    assert job._content_path == 'path/to/content'
    assert job._select_multiple == exp_select_multiple
    assert job._selected_discpaths == set()
    assert job._processed_discpaths == set()
    assert job._selected_playlists == []
    assert job._playlists_process is None

    assert 'discs_available' in job.signal.signals
    assert 'discs_selected' in job.signal.signals
    assert 'playlists_available' in job.signal.signals
    assert 'playlists_selected' in job.signal.signals


@pytest.mark.asyncio
async def test_PlaylistsJob_run(job, mocker):
    mocks = Mock()
    daemon_process = Mock(join=AsyncMock())
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.DaemonProcess', return_value=daemon_process), 'DaemonProcess')
    mocks.attach_mock(daemon_process.start, 'DaemonProcess_start')
    mocks.attach_mock(daemon_process.join, 'DaemonProcess_join')

    def finalization():
        job._selected_playlists = [
            Mock(filepath='path/to/1.pls'),
            Mock(filepath='path/to/2.pls'),
            Mock(filepath='path/to/3.pls'),
        ]

    mocks.attach_mock(mocker.patch.object(job, 'finalization', side_effect=finalization), 'finalization')
    mocks.attach_mock(mocker.patch.object(job, 'add_output'), 'add_output')

    await job.run()

    assert mocks.mock_calls == [
        call.DaemonProcess(
            name=job.name,
            target=playlists._playlists_process,
            kwargs={
                'content_path': job._content_path,
            },
            info_callback=job._handle_info,
            error_callback=job._handle_error,
        ),
        call.DaemonProcess_start(),
        call.DaemonProcess_join(),
        call.finalization(),
        call.add_output('path/to/1.pls'),
        call.add_output('path/to/2.pls'),
        call.add_output('path/to/3.pls'),
    ]


@pytest.mark.parametrize(
    argnames='reason, exp_parent_terminate_calls',
    argvalues=(
        (None, [call.parent_terminate(reason=None)]),
        ('Because!', [call.parent_terminate(reason='Because!')]),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='playlists_process, exp_playlists_process_stop_calls',
    argvalues=(
        (None, []),
        (Mock(), [call.playlists_process_stop()]),
    ),
    ids=lambda v: repr(v),
)
def test_PlaylistsJob_terminate(
        reason, exp_parent_terminate_calls,
        playlists_process, exp_playlists_process_stop_calls,
        job, mocker,
):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.jobs.base.JobBase.terminate'), 'parent_terminate')
    if playlists_process:
        mocks.attach_mock(playlists_process.stop, 'playlists_process_stop')
        playlists_process.stop.reset_mock()
    job._playlists_process = playlists_process

    if reason is None:
        job.terminate()
    else:
        job.terminate(reason=reason)

    exp_mock_calls = exp_playlists_process_stop_calls + exp_parent_terminate_calls
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='is_finished, info, exp_mock_calls, exp_exception',
    argvalues=(
        pytest.param(
            True,
            {'discs_available': ('path/to/disc',)},
            [],
            None,
            id='is_finished',
        ),
        pytest.param(
            False,
            {'discs_available': ('path/to/disc',)},
            [call.discs_selected(('path/to/disc',))],
            None,
            id='discs_available (single disc)',
        ),
        pytest.param(
            False,
            {'discs_available': ('path/to/disc1', 'path/to/disc2', 'path/to/disc3')},
            [call.emit('discs_available', ('path/to/disc1', 'path/to/disc2', 'path/to/disc3'))],
            None,
            id='discs_available (multiple discs)',
        ),
        pytest.param(
            False,
            {'playlists_available': ('path/to/disc', ('<playlist1>',))},
            [call.playlists_selected('path/to/disc', ('<playlist1>',))],
            None,
            id='playlists_available (single disc)',
        ),
        pytest.param(
            False,
            {'playlists_available': ('path/to/disc', ('<playlist1>', '<playlist2>', '<playlist3>'))},
            [call.emit('playlists_available', 'path/to/disc', ('<playlist1>', '<playlist2>', '<playlist3>'))],
            None,
            id='playlists_available (multiple playlists)',
        ),
        pytest.param(
            False,
            {'foo': 'bar'},
            [],
            RuntimeError("Unexpected info: {'foo': 'bar'}"),
            id='Unexpected info',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_PlaylistsJob__handle_info(is_finished, info, exp_mock_calls, exp_exception, job, mocker):
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=is_finished))
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'discs_selected'), 'discs_selected')
    mocks.attach_mock(mocker.patch.object(job, 'playlists_selected'), 'playlists_selected')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            job._handle_info(info)
    else:
        return_value = job._handle_info(info)
        assert return_value is None

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='is_finished, error, exp_mock_calls',
    argvalues=(
        (True, errors.DependencyError('foo'), []),
        (False, errors.DependencyError('foo'), [call.error(errors.DependencyError('foo'))]),
        (False, errors.ContentError('bar'), [call.exception(errors.ContentError('bar'))]),
    ),
    ids=lambda v: repr(v),
)
def test_PlaylistsJob__handle_error(is_finished, error, exp_mock_calls, job, mocker):
    mocker.patch.object(type(job), 'is_finished', PropertyMock(return_value=is_finished))
    mocks = Mock()
    mocks.attach_mock(mocker.patch.object(job, 'error'), 'error')
    mocks.attach_mock(mocker.patch.object(job, 'exception'), 'exception')

    return_value = job._handle_error(error)
    assert return_value is None

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='previously_selected_discpaths, new_discpaths, exp_selected_discpaths, exp_mock_calls',
    argvalues=(
        pytest.param(
            {'previously/selected/disc1', 'previously/selected/disc2', 'previously/selected/disc3'},
            ('newly/selected/disc4', 'newly/selected/disc5', 'newly/selected/disc6'),
            {
                'previously/selected/disc1', 'previously/selected/disc2', 'previously/selected/disc3',
                'newly/selected/disc4', 'newly/selected/disc5', 'newly/selected/disc6',
            },
            [
                call.playlists_process.send(utils.daemon.MsgType.info, {'discs_selected': (
                    'newly/selected/disc4',
                    'newly/selected/disc5',
                    'newly/selected/disc6',
                )}),
                call.emit('discs_selected', (
                    'newly/selected/disc4',
                    'newly/selected/disc5',
                    'newly/selected/disc6',
                )),
            ],
            id='Discpaths selected',
        ),
        pytest.param(
            {'previously/selected/disc1', 'previously/selected/disc2', 'previously/selected/disc3'},
            (),
            {'previously/selected/disc1', 'previously/selected/disc2', 'previously/selected/disc3'},
            [
                call.playlists_process.send(utils.daemon.MsgType.info, {'discs_selected': ()}),
                call.emit('discs_selected', ()),
                call.finalize(),
            ],
            id='No discpaths selected',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_PlaylistsJob_discs_selected(previously_selected_discpaths, new_discpaths, exp_selected_discpaths, exp_mock_calls, job, mocker):
    mocks = Mock()
    job._selected_discpaths = previously_selected_discpaths
    mocks.attach_mock(mocker.patch.object(job, '_playlists_process'), 'playlists_process')
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')
    mocks.attach_mock(mocker.patch.object(job, 'finalize'), 'finalize')

    return_value = job.discs_selected(new_discpaths)
    assert return_value is None

    assert job._selected_discpaths == exp_selected_discpaths
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='select_multiple, discpath, playlists, selected_discpaths, processed_discpaths, exp_processed_discpaths, exp_mock_calls',
    argvalues=(
        pytest.param(
            True,
            'selected/disc2', ('playlist1', 'playlist2', 'playlist3'),  # discpath, playlists
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # selected_discpaths
            {'selected/disc1'},                                         # processed_discpaths
            {'selected/disc1', 'selected/disc2'},                       # exp_processed_discpaths
            [
                call.emit('playlists_selected', 'selected/disc2', ('playlist1', 'playlist2', 'playlist3')),
            ],
            id='Not all discs processed, some playlists selected, select_multiple=True',
        ),
        pytest.param(
            False,
            'selected/disc2', ('playlist1', 'playlist2', 'playlist3'),  # discpath, playlists
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # selected_discpaths
            {'selected/disc1'},                                         # processed_discpaths
            {'selected/disc1', 'selected/disc2'},                       # exp_processed_discpaths
            [
                call.emit('playlists_selected', 'selected/disc2', ('playlist1', 'playlist2', 'playlist3')),
                call.finalize(),
            ],
            id='Not all discs processed, some playlists selected, select_multiple=False',
        ),
        pytest.param(
            True,
            'selected/disc2', (),                                       # discpath, playlists
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # selected_discpaths
            {'selected/disc1'},                                         # processed_discpaths
            {'selected/disc1', 'selected/disc2'},                       # exp_processed_discpaths
            [
                call.emit('playlists_selected', 'selected/disc2', ()),
            ],
            id='Not all discs processed, no playlists selected, select_multiple=True',
        ),
        pytest.param(
            False,
            'selected/disc2', (),                                       # discpath, playlists
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # selected_discpaths
            {'selected/disc1'},                                         # processed_discpaths
            {'selected/disc1', 'selected/disc2'},                       # exp_processed_discpaths
            [
                call.emit('playlists_selected', 'selected/disc2', ()),
            ],
            id='Not all discs processed, no playlists selected, select_multiple=False',
        ),

        pytest.param(
            True,
            'selected/disc3', ('playlist1', 'playlist2', 'playlist3'),  # discpath, playlists
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # selected_discpaths
            {'selected/disc1', 'selected/disc2'},                       # processed_discpaths
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # exp_processed_discpaths
            [
                call.emit('playlists_selected', 'selected/disc3', ('playlist1', 'playlist2', 'playlist3')),
                call.finalize(),
            ],
            id='All discs processed, some playlists selected, select_multiple=True',
        ),
        pytest.param(
            False,
            'selected/disc3', ('playlist1', 'playlist2', 'playlist3'),  # discpath, playlists
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # selected_discpaths
            {'selected/disc1', 'selected/disc2'},                       # processed_discpaths
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # exp_processed_discpaths
            [
                call.emit('playlists_selected', 'selected/disc3', ('playlist1', 'playlist2', 'playlist3')),
                call.finalize(),
            ],
            id='All discs processed, some playlists selected, select_multiple=False',
        ),
        pytest.param(
            True,
            'selected/disc3', (),                                       # discpath, playlists
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # selected_discpaths
            {'selected/disc1', 'selected/disc2'},                       # processed_discpaths
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # exp_processed_discpaths
            [
                call.emit('playlists_selected', 'selected/disc3', ()),
                call.finalize(),
            ],
            id='All discs processed, no playlists selected, select_multiple=True',
        ),
        pytest.param(
            False,
            'selected/disc3', (),                                       # discpath, playlists
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # selected_discpaths
            {'selected/disc1', 'selected/disc2'},                       # processed_discpaths
            {'selected/disc1', 'selected/disc2', 'selected/disc3'},     # exp_processed_discpaths
            [
                call.emit('playlists_selected', 'selected/disc3', ()),
                call.finalize(),
            ],
            id='All discs processed, no playlists selected, select_multiple=False',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_PlaylistsJob_playlists_selected(select_multiple, discpath, playlists, selected_discpaths, processed_discpaths, exp_processed_discpaths, exp_mock_calls, job, mocker):
    mocks = Mock()
    job._select_multiple = select_multiple
    job._selected_discpaths = selected_discpaths
    job._processed_discpaths = processed_discpaths
    mocks.attach_mock(mocker.patch.object(job.signal, 'emit'), 'emit')
    mocks.attach_mock(mocker.patch.object(job, 'finalize'), 'finalize')

    return_value = job.playlists_selected(discpath, playlists)
    assert return_value is None

    assert job._processed_discpaths == exp_processed_discpaths
    assert mocks.mock_calls == exp_mock_calls


def test_PlaylistsJob__store_selected_playlists(job):
    job._selected_playlists = ['selected/disc/playlist1', 'selected/disc/playlist2']
    job._store_selected_playlists('selected/disc', ('selected/disc/playlist3', 'selected/disc/playlist4'))
    assert job._selected_playlists == [
        'selected/disc/playlist1', 'selected/disc/playlist2',
        'selected/disc/playlist3', 'selected/disc/playlist4',
    ]


def test_PlaylistsJob_selected_playlists(job):
    job._selected_playlists = ['selected/disc/playlist1', 'selected/disc/playlist2']
    assert job.selected_playlists == ('selected/disc/playlist1', 'selected/disc/playlist2')


@pytest.fixture
def _playlists_process_mocks(mocker):
    mocks = Mock()
    mocks.content_path = 'path/to/content'
    mocks.output_queue = Mock()
    mocks.input_queue = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_bluray', return_value=False), 'is_bluray')
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_dvd', return_value=False), 'is_dvd')
    mocks.attach_mock(mocker.patch('upsies.jobs.playlists._get_selected_discs', return_value=()), '_get_selected_discs')
    mocks.attach_mock(mocker.patch('upsies.jobs.playlists._report_available_playlists'), '_report_available_playlists')
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.maybe_terminate'), 'maybe_terminate')
    return mocks

def test__playlists_process__content_path_is_not_disc(_playlists_process_mocks, mocker):
    mocks = _playlists_process_mocks
    mocks.is_bluray.return_value = False
    mocks.is_dvd.return_value = False
    exp_exception = errors.ContentError('No BDMV or VIDEO_TS subdirectory found: path/to/content')
    with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
        playlists._playlists_process(
            mocks.output_queue, mocks.input_queue,
            content_path=mocks.content_path,
        )
    assert mocks.mock_calls == [
        call.is_bluray(mocks.content_path, multidisc=True),
        call.is_dvd(mocks.content_path, multidisc=True),
    ]

def test__playlists_process__no_discs_selected(_playlists_process_mocks, mocker):
    mocks = _playlists_process_mocks
    mocks.is_bluray.return_value = True
    mocks._get_selected_discs.return_value = ()
    return_value = playlists._playlists_process(
        mocks.output_queue, mocks.input_queue,
        content_path=mocks.content_path,
    )
    assert return_value is None
    assert mocks.mock_calls == [
        call.is_bluray(mocks.content_path, multidisc=True),
        call._get_selected_discs(
            mocks.output_queue, mocks.input_queue,
            content_path=mocks.content_path,
            disc_module=utils.disc.bluray,
        )
    ]

def test__playlists_process__content_path_is_bluray(_playlists_process_mocks, mocker):
    mocks = _playlists_process_mocks
    mocks.is_bluray.return_value = True
    mocks._get_selected_discs.return_value = ('selected/disc1', 'selected/disc2', 'selected/disc3')
    return_value = playlists._playlists_process(
        mocks.output_queue, mocks.input_queue,
        content_path=mocks.content_path,
    )
    assert return_value is None
    assert mocks.mock_calls == [
        call.is_bluray(mocks.content_path, multidisc=True),
        call._get_selected_discs(
            mocks.output_queue, mocks.input_queue,
            content_path=mocks.content_path,
            disc_module=utils.disc.bluray,
        ),
        call.maybe_terminate(mocks.input_queue),
        call._report_available_playlists(mocks.output_queue, discpath='selected/disc1', disc_module=utils.disc.bluray),
        call.maybe_terminate(mocks.input_queue),
        call._report_available_playlists(mocks.output_queue, discpath='selected/disc2', disc_module=utils.disc.bluray),
        call.maybe_terminate(mocks.input_queue),
        call._report_available_playlists(mocks.output_queue, discpath='selected/disc3', disc_module=utils.disc.bluray),
    ]

def test__playlists_process__content_path_is_dvd(_playlists_process_mocks, mocker):
    mocks = _playlists_process_mocks
    mocks.is_bluray.return_value = False
    mocks.is_dvd.return_value = True
    mocks._get_selected_discs.return_value = ('selected/disc1', 'selected/disc2', 'selected/disc3')
    return_value = playlists._playlists_process(
        mocks.output_queue, mocks.input_queue,
        content_path=mocks.content_path,
    )
    assert return_value is None
    assert mocks.mock_calls == [
        call.is_bluray(mocks.content_path, multidisc=True),
        call.is_dvd(mocks.content_path, multidisc=True),
        call._get_selected_discs(
            mocks.output_queue, mocks.input_queue,
            content_path=mocks.content_path,
            disc_module=utils.disc.dvd,
        ),
        call.maybe_terminate(mocks.input_queue),
        call._report_available_playlists(mocks.output_queue, discpath='selected/disc1', disc_module=utils.disc.dvd),
        call.maybe_terminate(mocks.input_queue),
        call._report_available_playlists(mocks.output_queue, discpath='selected/disc2', disc_module=utils.disc.dvd),
        call.maybe_terminate(mocks.input_queue),
        call._report_available_playlists(mocks.output_queue, discpath='selected/disc3', disc_module=utils.disc.dvd),
    ]


@pytest.fixture
def _get_selected_discs_mocks(mocker):
    mocks = Mock()
    mocks.content_path = 'path/to/content'
    mocks.disc_module = Mock(get_disc_paths=Mock(return_value=()))
    mocks.output_queue = Mock()
    mocks.input_queue = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.daemon.read_input_queue_key', return_value=()), 'read_input_queue_key')
    return mocks

def test__get_selected_discs__no_discs_found(_get_selected_discs_mocks, mocker):
    mocks = _get_selected_discs_mocks
    mocks.disc_module.get_disc_paths.return_value = ()

    return_value = playlists._get_selected_discs(
        mocks.output_queue, mocks.input_queue,
        content_path=mocks.content_path,
        disc_module=mocks.disc_module,
    )
    assert return_value == ()
    assert mocks.mock_calls == [
        call.disc_module.get_disc_paths(mocks.content_path),
        call.output_queue.put((utils.daemon.MsgType.error, errors.ContentError(f'No disc found: {mocks.content_path}'))),
    ]

def test__get_selected_discs__discs_found(_get_selected_discs_mocks, mocker):
    mocks = _get_selected_discs_mocks
    mocks.disc_module.get_disc_paths.return_value = ('path/to/disc20', 'path/to/disc1', 'path/to/disc3')

    return_value = playlists._get_selected_discs(
        mocks.output_queue, mocks.input_queue,
        content_path=mocks.content_path,
        disc_module=mocks.disc_module,
    )
    assert return_value == mocks.read_input_queue_key.return_value
    assert mocks.mock_calls == [
        call.disc_module.get_disc_paths(mocks.content_path),
        call.output_queue.put((utils.daemon.MsgType.info, {'discs_available': ('path/to/disc1', 'path/to/disc3', 'path/to/disc20')})),
        call.read_input_queue_key(mocks.input_queue, 'discs_selected'),
    ]


def test__report_available_playlists(mocker):
    mocks = Mock()
    mocks.disc_module.get_playlists.return_value = ('path/to/disc/playlist1', 'path/to/disc/playlist2')
    mocks.attach_mock(
        mocker.patch('upsies.jobs.playlists._extend_playlists_info', side_effect=lambda playlists: tuple(
            f'{playlist}.extended'
            for playlist in playlists
        )),
        '_extend_playlists_info',
    )

    return_value = playlists._report_available_playlists(
        mocks.output_queue,
        discpath='path/to/disc',
        disc_module=mocks.disc_module,
    )
    assert return_value is None
    assert mocks.mock_calls == [
        call.disc_module.get_playlists('path/to/disc'),
        call._extend_playlists_info(mocks.disc_module.get_playlists.return_value),
        call.output_queue.put((utils.daemon.MsgType.info, {'playlists_available': ('path/to/disc', (
            'path/to/disc/playlist1.extended',
            'path/to/disc/playlist2.extended',
        ))}))
    ]


def test__extend_playlists_info(mocker):
    class MockPlaylist(str):
        def __new__(cls, name, **kwargs):
            self = super().__new__(cls, name)
            self.kwargs = kwargs
            for name, value in kwargs.items():
                setattr(self, name, value)
            return self

    def __eq__(self, other):
        return (
            type(self) is type(other)
            and str(self) == str(other)
            and self.kwargs == other.kwargs
        )

    unextended_playlists = (
        MockPlaylist('playlist1', filepath='path/to/disc/playlist1.pls'),
        MockPlaylist('playlist10', filepath='path/to/disc/playlist10.pls'),
        MockPlaylist('playlist2', filepath='path/to/disc/playlist2.pls'),
        MockPlaylist('playlist11', filepath='path/to/disc/playlist11.pls'),
        MockPlaylist('playlist12', filepath='path/to/disc/playlist12.pls'),
        MockPlaylist('playlist3', filepath='path/to/disc/playlist3.pls'),
    )
    extended_playlists = playlists._extend_playlists_info(unextended_playlists)
    assert extended_playlists == (
        MockPlaylist('playlist1', filepath='path/to/disc/playlist1.pls'),
        MockPlaylist('playlist2', filepath='path/to/disc/playlist2.pls'),
        MockPlaylist('playlist3', filepath='path/to/disc/playlist3.pls'),
        MockPlaylist('playlist10', filepath='path/to/disc/playlist10.pls'),
        MockPlaylist('playlist11', filepath='path/to/disc/playlist11.pls'),
        MockPlaylist('playlist12', filepath='path/to/disc/playlist12.pls'),
    )
