import itertools
import re
import selectors
import subprocess
from unittest.mock import Mock, PropertyMock, call

import pytest

from upsies import errors
from upsies.utils import subproc


def mock_output(*lines):
    return Mock(__iter__=Mock(return_value=iter(lines)))


@pytest.mark.parametrize(
    argnames='lines, exp_lines, exp_mock_calls',
    argvalues=(
        (
            [
                {'line': 'foo\n', 'is_running': True, 'select': None},
                {'line': '', 'is_running': True, 'select': iter(('<keys and events>',))},
                {'line': 'bar\n', 'is_running': True, 'select': None},
                {'line': '', 'is_running': True, 'select': iter(())},
                {'line': '', 'is_running': True, 'select': iter(('<keys and events>',))},
                {'line': 'baz\n', 'is_running': True, 'select': iter(())},
                {'line': '', 'is_running': False, 'select': iter(())},
                {'line': '', 'is_running': False, 'select': iter(())},
                {'line': '', 'is_running': False, 'select': iter(())},
            ],
            ['foo\n', '', 'bar\n', '', '', 'baz\n'],
            [
                # One select() call per empty line until is_running is False.
                'call.DefaultSelector().select(timeout=0.1)',
                'call.DefaultSelector().select(timeout=0.1)',
                'call.DefaultSelector().select(timeout=0.1)',
            ],
        ),
    )
)
def test_Process__iterlines(lines, exp_lines, exp_mock_calls, mocker):

    def readline():
        linespec = lines.pop(0)
        mocker.patch.object(type(process), 'is_running', PropertyMock(return_value=linespec['is_running']))
        mocks.DefaultSelector.return_value.select.return_value = linespec['select']
        return linespec['line']

    process = subproc.Process(popen=Mock())
    fh = Mock(readline=Mock(side_effect=readline))

    mocks = Mock()
    mocks.attach_mock(mocker.patch('os.set_blocking'), 'set_blocking')
    mocks.attach_mock(mocker.patch('selectors.DefaultSelector'), 'DefaultSelector')

    for line, exp_line in itertools.zip_longest(process._iterlines(fh), exp_lines, fillvalue=''):
        print('>>> assert', repr(line), '==', repr(exp_line))
        assert line == exp_line

    exp_mock_calls = [
        call.set_blocking(fh.fileno(), False),
        call.DefaultSelector(),
        call.DefaultSelector().register(fh.fileno(), selectors.EVENT_READ),
    ] + [
        eval(exp_mock_call)
        for exp_mock_call in exp_mock_calls
    ]
    for mock_call, exp_mock_call in itertools.zip_longest(mocks.mock_calls, exp_mock_calls):
        print('>>> assert', repr(mock_call), '\n        ==', repr(exp_mock_call))
    assert mocks.mock_calls == exp_mock_calls


def test_Process_stdout(mocker):
    process = subproc.Process(popen=Mock())
    mocker.patch.object(process, '_iterlines', return_value=iter(('foo', 'bar', 'baz')))
    for line, exp_line in itertools.zip_longest(process.stdout, ('foo', 'bar', 'baz')):
        assert line == exp_line

    assert process._iterlines.call_args_list == [call(process._popen.stdout)]


@pytest.mark.parametrize(
    argnames='stderr, iterlines, exp_lines, exp_iterlines_calls',
    argvalues=(
        pytest.param(
            '<stderr>',
            Mock(return_value=iter(('foo', 'bar', 'baz'))),
            ('foo', 'bar', 'baz'),
            [call('<stderr>')],
            id='stderr is not redirected',
        ),
        pytest.param(
            None,
            Mock(return_value=iter(('foo', 'bar', 'baz'))),
            (),
            [],
            id='stderr is redirected',
        ),
    )
)
def test_Process_stderr(stderr, iterlines, exp_lines, exp_iterlines_calls, mocker):
    process = subproc.Process(popen=Mock(stderr=stderr))
    mocker.patch.object(process, '_iterlines', iterlines)
    for line, exp_line in itertools.zip_longest(process.stderr, exp_lines):
        assert line == exp_line

    assert process._iterlines.call_args_list == exp_iterlines_calls


@pytest.mark.parametrize(
    argnames='poll, exp_return_value',
    argvalues=(
        pytest.param(Mock(return_value=0), False, id='poll() returns 0'),
        pytest.param(Mock(return_value=1), False, id='poll() returns 1'),
        pytest.param(Mock(return_value=123), False, id='poll() returns 123'),
        pytest.param(Mock(return_value=None), True, id='poll() returns None'),
    )
)
def test_Process_is_running(poll, exp_return_value):
    popen = Mock(poll=poll)
    process = subproc.Process(popen)
    assert process.is_running is exp_return_value


def test_Process_wait():
    popen = Mock(wait=Mock())
    process = subproc.Process(popen)
    assert process.wait() is None
    assert popen.wait.call_args_list == [call()]


@pytest.mark.parametrize(
    argnames='is_running, wait_exception, exp_mock_calls',
    argvalues=(
        pytest.param(False, None, [], id='Process is not running'),
        pytest.param(
            True,
            None,
            [
                call.terminate(),
                call.wait(timeout=1),
            ],
            id='Process terminates',
        ),
        pytest.param(
            True,
            subprocess.TimeoutExpired(cmd='ls foo', timeout=1),
            [
                call.terminate(),
                call.wait(timeout=1),
                call.kill(),
            ],
            id='Process does not terminate',
        ),
    )
)
def test_Process_terminate(is_running, wait_exception, exp_mock_calls, mocker):
    popen = Mock()
    if wait_exception:
        popen.wait.side_effect = wait_exception
    process = subproc.Process(popen)
    mocker.patch.object(type(process), 'is_running', PropertyMock(return_value=is_running))

    mocks = Mock()
    mocks.attach_mock(popen.terminate, 'terminate')
    mocks.attach_mock(popen.wait, 'wait')
    mocks.attach_mock(popen.kill, 'kill')

    assert process.terminate() is None
    assert mocks.mock_calls == exp_mock_calls


def test_Process_exitcode():
    popen = Mock()
    process = subproc.Process(popen)
    assert process.exitcode is popen.returncode


@pytest.fixture
def run_mocks(mocker):
    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('pty.openpty', return_value=('stdin_master', 'stdin_slave')),
        'openpty',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.subproc.Process', return_value=Mock(
            stdout=(),
            stderr=(),
        )),
        'Process',
    )
    mocks.attach_mock(
        mocker.patch('subprocess.Popen'),
        'Popen',
    )
    mocks.attach_mock(
        mocks.Process.return_value.wait,
        'wait',
    )
    return mocks


@pytest.mark.parametrize('ignore_errors', (True, False), ids=('ignore_errors=True', 'ignore_errors=False'))
@pytest.mark.parametrize('join_stderr', (True, False), ids=('join_stderr=True', 'join_stderr=False'))
@pytest.mark.parametrize('communicate', (True, False), ids=('communicate=True', 'communicate=False'))
def test_run_with_missing_dependency(ignore_errors, join_stderr, communicate, run_mocks, mocker):
    argv = ['foo', 'bar', 'baz']

    run_mocks.Popen.side_effect = OSError('nope')

    with pytest.raises(errors.DependencyError, match=r'^Missing dependency: foo$'):
        subproc.run(
            argv,
            ignore_errors=ignore_errors,
            join_stderr=join_stderr,
            communicate=communicate,
        )

    assert run_mocks.mock_calls == [
        call.openpty(),
        call.Popen(
            tuple(argv),
            shell=False,
            text=True,
            universal_newlines=True,
            stdout=subprocess.PIPE,
            stderr=(
                subprocess.STDOUT
                if join_stderr else
                subprocess.PIPE
            ),
            stdin=run_mocks.openpty.return_value[1],
            # Line buffering
            bufsize=1,
        ),
    ]


@pytest.mark.parametrize('ignore_errors', (True, False), ids=('ignore_errors=True', 'ignore_errors=False'))
@pytest.mark.parametrize('join_stderr', (True, False), ids=('join_stderr=True', 'join_stderr=False'))
def test_run_with_communicate(ignore_errors, join_stderr, run_mocks, mocker):
    argv = ['foo', 'bar', 'baz']

    process = subproc.run(
        argv,
        ignore_errors=ignore_errors,
        join_stderr=join_stderr,
        communicate=True,
    )
    assert process is run_mocks.Process.return_value

    assert run_mocks.mock_calls == [
        call.openpty(),
        call.Popen(
            tuple(argv),
            shell=False,
            text=True,
            universal_newlines=True,
            stdout=subprocess.PIPE,
            stderr=(
                subprocess.STDOUT
                if join_stderr else
                subprocess.PIPE
            ),
            stdin=run_mocks.openpty.return_value[1],
            # Line buffering
            bufsize=1,
        ),
        call.Process(run_mocks.Popen.return_value),
    ]


@pytest.mark.parametrize(
    argnames='ignore_errors, stdout, stderr, exp_result',
    argvalues=(
        pytest.param(
            True,
            ('output 1\n', 'output 2\n', 'output 3\n'),
            ('error 1\n', 'error 2\n', 'error 3\n'),
            'output 1\noutput 2\noutput 3\n',
            id='stderr is ignored',
        ),
        pytest.param(
            False,
            ('output 1\n', 'output 2\n', 'output 3\n'),
            ('error 1\n', 'error 2\n', 'error 3\n'),
            errors.ProcessError('error 1\nerror 2\nerror 3\n'),
            id='stderr is not ignored',
        ),
    ),
)
def test_run_with_ignore_stderr(ignore_errors, stdout, stderr, exp_result, run_mocks, mocker):
    argv = ['foo', 'bar', 'baz']

    run_mocks.Process.return_value.stdout = mock_output(*stdout)
    run_mocks.Process.return_value.stderr = mock_output(*stderr)

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            subproc.run(argv, ignore_errors=ignore_errors)
    else:
        output = subproc.run(argv, ignore_errors=ignore_errors)
        assert output == exp_result

    assert run_mocks.mock_calls == [
        call.openpty(),
        call.Popen(
            tuple(argv),
            shell=False,
            text=True,
            universal_newlines=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=run_mocks.openpty.return_value[1],
            # Line buffering
            bufsize=1,
        ),
        call.Process(run_mocks.Popen.return_value),
        call.wait(),
    ]


@pytest.mark.parametrize(
    argnames='stdout, exitcode, return_exitcode, exp_return_value',
    argvalues=(
        ('output', 0, False, 'output'),
        ('output', 1, False, 'output'),
        ('output', 0, True, ('output', 0)),
        ('output', 1, True, ('output', 1)),
    ),
    ids=lambda v: repr(v),
)
def test_run_with_return_exitcode(stdout, exitcode, return_exitcode, exp_return_value, run_mocks, mocker):
    argv = ['foo', 'bar', 'baz']

    run_mocks.Process.return_value.stdout = mock_output(stdout)
    run_mocks.Process.return_value.exitcode = exitcode

    return_value = subproc.run(argv, return_exitcode=return_exitcode)
    assert return_value == exp_return_value
