import re
from unittest.mock import call

import pytest

from upsies import errors
from upsies.utils import image


def test_optimization_levels():
    assert image.optimization_levels == (
        'low',
        'medium',
        'high',
        'placebo',
        'none',
        'default',
    )


@pytest.mark.parametrize(
    argnames='output_file, exp_return_value',
    argvalues=(
        pytest.param(None, None, id='No output file provided (None)'),
        pytest.param('', None, id='No output file provided ("")'),
        pytest.param('path/to/optimized.png', 'sanitized:path/to/optimized.png', id='Output file provided'),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.parametrize(
    argnames='extension, level, cmd_output, exitcode, exp_cmd_called, exp_exception, exp_level',
    argvalues=(
        pytest.param(
            'png', None, '<ignored output>', 0, False, None, None,
            id='level=None',
        ),
        pytest.param(
            'png', 'none', '<ignored output>', 0, False, None, None,
            id='level=""',
        ),
        pytest.param(
            'png', 'default', '<ignored output>', 0, True, None, '2',
            id='level=default',
        ),
        pytest.param(
            'png', 'low', '<ignored output>', 0, True, None, '1',
            id='level=low',
        ),
        pytest.param(
            'png', 'medium', '<ignored output>', 0, True, None, '2',
            id='level=medium',
        ),
        pytest.param(
            'png', 'high', '<ignored output>', 0, True, None, '4',
            id='level=high',
        ),
        pytest.param(
            'png', 'foo', '<ignored output>', 0, False, errors.ImageOptimizeError('Invalid optimization level: foo'), None,
            id='Invalid level',
        ),
        pytest.param(
            'png', 'default', ' \n  Error message  \n ', 1, True, errors.ImageOptimizeError('Failed to optimize: Error message'), '2',
            id='Error message from command output (png)',
        ),
        pytest.param(
            'jpeg', 'default', ' \n  Error message  \n ', 1, True, None, '2',
            id='Error message from command output (jpeg)',
        ),
        pytest.param(
            'png', 'default', ' \n  \n ', 123, True, errors.ImageOptimizeError('Failed to optimize for unknown reason'), '2',
            id='No error message from command output (png)',
        ),
        pytest.param(
            'jpeg', 'default', ' \n  \n ', 123, True, None, '2',
            id='No error message from command output (jpeg)',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_optimize(
        extension, level, cmd_output, exitcode, exp_cmd_called, exp_level, exp_exception,
        output_file, exp_return_value,
        mocker,
):
    run_mock = mocker.patch('upsies.utils.subproc.run', return_value=(cmd_output, exitcode))
    mocker.patch('upsies.utils.fs.sanitize_path', side_effect=lambda p: f'sanitized:{p}')

    image_file = f'path/to/image.{extension}'

    exp_command = [
        'oxipng', '--preserve',
        '--opt', str(exp_level),
        '--interlace', '0',
        '--strip', 'safe',
        image_file,
    ]

    kwargs = {}
    if level is not None:
        kwargs['level'] = level
    if output_file is not None:
        kwargs['output_file'] = output_file
    if output_file:
        exp_command.extend(('--out', f'sanitized:{output_file}'))

    if isinstance(exp_exception, Exception):
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            image.optimize(image_file, **kwargs)
    else:
        image.optimize(image_file, **kwargs)

    if exp_cmd_called:
        assert run_mock.call_args_list == [call(
            exp_command,
            join_stderr=True,
            return_exitcode=True,
        )]
    else:
        assert run_mock.call_args_list == []
