import os
from unittest.mock import Mock, call

import pytest

from upsies.utils import disc


@pytest.mark.parametrize(
    argnames='content_path, existing_directories, multidisc, exp_return_value',
    argvalues=(
        pytest.param(
            'path/to/content',
            (),
            False,
            False,
            id='Non-existing directory',
        ),
        pytest.param(
            'path/to/content',
            (
                'path/to/content/BDMV',
            ),
            False,
            True,
            id='BDMV in directory',
        ),
        pytest.param(
            'path/to/content',
            (
                'path/to/content/ASDF',
            ),
            False,
            False,
            id='No BDMV in directory',
        ),
        pytest.param(
            'path/to/content',
            (
                'path/to/content/disc1/BDMV',
            ),
            False,
            False,
            id='BDMV in subdirectory (multidisc=False)',
        ),
        pytest.param(
            'path/to/content',
            (
                'path/to/content/disc1/BDMV',
            ),
            True,
            True,
            id='BDMV in subdirectory (multidisc=True)',
        ),
        pytest.param(
            'path/to/content',
            (
                'path/to/content/disc1/ASDF',
            ),
            True,
            False,
            id='No BDMV in subdirectory (multidisc=True)',
        ),
    ),
)
def test_is_bluray(content_path, existing_directories, multidisc, exp_return_value, tmp_path):
    for relative_path in existing_directories:
        (tmp_path / relative_path).mkdir(parents=True, exist_ok=True)
    content_path = str(tmp_path / content_path)
    return_value = disc.is_bluray(content_path, multidisc=multidisc)
    assert return_value is exp_return_value


@pytest.mark.parametrize(
    argnames='content_path, existing_directories, exp_return_value',
    argvalues=(
        pytest.param(
            'path/to/content',
            (),
            (),
            id='Non-existing directory',
        ),
        pytest.param(
            'path/to/content',
            (
                'path/to/content/BDMV',
            ),
            ('{tmp_path}/path/to/content',),
            id='BDMV in directory',
        ),
        pytest.param(
            'path/to/content',
            (
                'path/to/content/disc1/BDMV',
                'path/to/content/disc2/BDMV',
                'path/to/content/somethingelse/foo/bar',
            ),
            (
                '{tmp_path}/path/to/content/disc1',
                '{tmp_path}/path/to/content/disc2',
            ),
            id='BDMV in some subdirectories',
        ),
    ),
)
def test_get_disc_paths(content_path, existing_directories, exp_return_value, tmp_path):
    for relative_path in existing_directories:
        (tmp_path / relative_path).mkdir(parents=True, exist_ok=True)
    content_path = str(tmp_path / content_path)
    return_value = disc.bluray.get_disc_paths(content_path)
    assert return_value == tuple(
        path.format(tmp_path=tmp_path)
        for path in exp_return_value
    )


class MplsMock(dict):
    def __init__(self, mpls, clips):
        super().__init__(
            filename=f'{mpls:05d}.mpls',
            playlist={
                'play_items': tuple(
                    {
                        'clip_information_filename': f'{clip["clip_information_filename"]:05d}',
                        'intime': clip['intime'],
                        'outtime': clip['outtime'],
                    }
                    for clip in clips
                ),
            },
        )
        self.filepath = os.path.join('BDMV', 'PLAYLIST', self['filename'])


@pytest.mark.parametrize(
    argnames='content_path, playlist_directory, stream_directory, mplss, exp_playlists_kwargs',
    argvalues=(
        pytest.param(
            'path/to/content',
            'path/to/content/BDMV/PLAYLIST', 'path/to/content/BDMV/STREAM',
            (
                MplsMock(mpls=1, clips=(
                    {'clip_information_filename': 3, 'intime': 100 * 45_000, 'outtime': 220 * 45_000},
                    {'clip_information_filename': 7, 'intime': 200 * 45_000, 'outtime': 240 * 45_000},
                    {'clip_information_filename': 8, 'intime': 300 * 45_000, 'outtime': 319 * 45_000},
                )),
                MplsMock(mpls=3, clips=(
                    {'clip_information_filename': 4, 'intime': 2000 * 45_000, 'outtime': 2120 * 45_000},
                    {'clip_information_filename': 8, 'intime': 3200 * 45_000, 'outtime': 3240 * 45_000},
                    {'clip_information_filename': 9, 'intime': 4000 * 45_000, 'outtime': 4020 * 45_000},
                )),
                MplsMock(mpls=6, clips=(
                    {'clip_information_filename': 5, 'intime': 3000 * 45_000, 'outtime': 3120 * 45_000},
                    {'clip_information_filename': 9, 'intime': 4200 * 45_000, 'outtime': 4240 * 45_000},
                    {'clip_information_filename': 10, 'intime': 4300 * 45_000, 'outtime': 4380 * 45_000},
                )),
            ),
            (
                {
                    'discpath': '{tmp_path}/path/to/content',
                    'filepath': '{tmp_path}/path/to/content/BDMV/PLAYLIST/00001.mpls',
                    'duration': 179,
                    'items': (
                        '{tmp_path}/path/to/content/BDMV/STREAM/00003.m2ts',
                        '{tmp_path}/path/to/content/BDMV/STREAM/00007.m2ts',
                        '{tmp_path}/path/to/content/BDMV/STREAM/00008.m2ts',
                    ),
                },
                {
                    'discpath': '{tmp_path}/path/to/content',
                    'filepath': '{tmp_path}/path/to/content/BDMV/PLAYLIST/00003.mpls',
                    'duration': 180,
                    'items': (
                        '{tmp_path}/path/to/content/BDMV/STREAM/00004.m2ts',
                        '{tmp_path}/path/to/content/BDMV/STREAM/00008.m2ts',
                        '{tmp_path}/path/to/content/BDMV/STREAM/00009.m2ts',
                    ),
                },
                {
                    'discpath': '{tmp_path}/path/to/content',
                    'filepath': '{tmp_path}/path/to/content/BDMV/PLAYLIST/00006.mpls',
                    'duration': 240,
                    'items': (
                        '{tmp_path}/path/to/content/BDMV/STREAM/00005.m2ts',
                        '{tmp_path}/path/to/content/BDMV/STREAM/00009.m2ts',
                        '{tmp_path}/path/to/content/BDMV/STREAM/00010.m2ts',
                    ),
                },
            ),
            id='PLAYLIST and STREAM exist',
        ),
        pytest.param(
            'path/to/content',
            None, 'path/to/content/BDMV/STREAM',
            (),
            (),
            id='PLAYLIST does not exist',
        ),
        pytest.param(
            'path/to/content',
            'path/to/content/BDMV/PLAYLIST', None,
            (),
            (),
            id='STREAM does not exist',
        ),
    ),
)
def test_get_playlists(content_path, playlist_directory, stream_directory, mplss, exp_playlists_kwargs, tmp_path, mocker):
    content_path = tmp_path / content_path
    content_path.mkdir(parents=True, exist_ok=True)

    # BDMV/PLAYLIST and BDMV/STREAM directories.
    if playlist_directory is not None:
        playlist_directory = tmp_path / playlist_directory
        playlist_directory.mkdir(parents=True, exist_ok=True)
    if stream_directory is not None:
        stream_directory = tmp_path / stream_directory
        stream_directory.mkdir(parents=True, exist_ok=True)

    # .MPLS files.
    if playlist_directory:
        for mpls in mplss:
            playlist_filepath = playlist_directory / mpls['filename']
            print('playlist_filepath:', repr(playlist_filepath))
            playlist_filepath.write_text(f"<content of {mpls['filename']}>")
            # Fix `filepath` attribute
            mpls.filepath = str(content_path / mpls.filepath)

    # Mock Playlist instances
    mock_playlists = tuple(Mock(name=f'Playlist({i})', id=i) for i in range(1, 10))

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.bluray.Mpls', side_effect=mplss),
        'Mpls',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.Playlist', side_effect=mock_playlists),
        'Playlist',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.playlist.mark_main_playlists'),
        'mark_main_playlists',
    )
    mocker.patch('upsies.utils.disc.bluray._filter_mplss', side_effect=lambda mplss_generator: tuple(mplss_generator))
    mocker.patch('upsies.utils.disc.bluray._filter_playlists', side_effect=lambda playlists_generator: tuple(playlists_generator))

    # Fix paths in playlist arguments.
    for kwargs in exp_playlists_kwargs:
        kwargs['discpath'] = kwargs['discpath'].format(tmp_path=tmp_path)
        kwargs['filepath'] = kwargs['filepath'].format(tmp_path=tmp_path)
        kwargs['items'] = tuple(
            filepath.format(tmp_path=tmp_path)
            for filepath in kwargs['items']
        )

    playlists = disc.bluray.get_playlists(str(content_path))

    if playlist_directory and stream_directory:
        exp_playlists = mock_playlists[:3]
        exp_mock_calls = [
            call.Mpls(exp_playlist_kwargs['filepath'])
            for exp_playlist_kwargs in exp_playlists_kwargs
        ]
    else:
        exp_playlists = ()
        exp_mock_calls = []

    for exp_playlist, playlist in zip(exp_playlists, playlists):
        print('Exp playlist:', repr(exp_playlist.id))
        print('Got playlist:', repr(playlist.id))
    assert playlists == exp_playlists

    # NOTE: We can't check calls to _filter_mplss() and _filter_playlists() because they receive
    #       generator comprehensions (e.g. <generator object get_playlists.<locals>.<genexpr> at
    #       0xd34db33f>) which cannot be mocked.

    exp_mock_calls += [
        call.Playlist(
            discpath=kwargs['discpath'],
            filepath=kwargs['filepath'],
            items=kwargs['items'],
            duration=kwargs['duration'],
        )
        for kwargs in exp_playlists_kwargs
    ]
    exp_mock_calls += [
        call.mark_main_playlists(exp_playlists),
    ]
    for exp_mock_call, mock_call in zip(exp_mock_calls, mocks.mock_calls):
        print('Exp call:', repr(exp_mock_call))
        print('Got call:', repr(mock_call))
    assert mocks.mock_calls == exp_mock_calls


def test__filter_mplss():
    assert disc.bluray._filter_mplss((
        MplsMock(mpls=1, clips=(
            {'clip_information_filename': 1, 'intime': 100, 'outtime': 200},
            {'clip_information_filename': 2, 'intime': 300, 'outtime': 400},
            {'clip_information_filename': 3, 'intime': 500, 'outtime': 600},
        )),
        # Empty.
        MplsMock(mpls=2, clips=()),
        MplsMock(mpls=3, clips=(
            {'clip_information_filename': 2, 'intime': 1000, 'outtime': 2000},
            {'clip_information_filename': 3, 'intime': 3000, 'outtime': 4000},
            {'clip_information_filename': 4, 'intime': 5000, 'outtime': 6000},
        )),
        # Two identical clips is ok.
        MplsMock(mpls=4, clips=(
            {'clip_information_filename': 1, 'intime': 100, 'outtime': 200},
            {'clip_information_filename': 1, 'intime': 100, 'outtime': 200},
        )),
        # More than two identical clips is not ok.
        MplsMock(mpls=4, clips=(
            {'clip_information_filename': 1, 'intime': 10, 'outtime': 20},
            {'clip_information_filename': 2, 'intime': 100, 'outtime': 200},
            {'clip_information_filename': 3, 'intime': 20, 'outtime': 30},
            {'clip_information_filename': 2, 'intime': 100, 'outtime': 200},
            {'clip_information_filename': 4, 'intime': 30, 'outtime': 40},
            {'clip_information_filename': 2, 'intime': 100, 'outtime': 200},
        )),
        MplsMock(mpls=5, clips=(
            {'clip_information_filename': 5, 'intime': 10_000, 'outtime': 20_000},
            {'clip_information_filename': 6, 'intime': 30_000, 'outtime': 40_000},
            {'clip_information_filename': 7, 'intime': 50_000, 'outtime': 60_000},
        )),
    )) == (
        MplsMock(mpls=1, clips=(
            {'clip_information_filename': 1, 'intime': 100, 'outtime': 200},
            {'clip_information_filename': 2, 'intime': 300, 'outtime': 400},
            {'clip_information_filename': 3, 'intime': 500, 'outtime': 600},
        )),
        MplsMock(mpls=3, clips=(
            {'clip_information_filename': 2, 'intime': 1000, 'outtime': 2000},
            {'clip_information_filename': 3, 'intime': 3000, 'outtime': 4000},
            {'clip_information_filename': 4, 'intime': 5000, 'outtime': 6000},
        )),
        MplsMock(mpls=4, clips=(
            {'clip_information_filename': 1, 'intime': 100, 'outtime': 200},
            {'clip_information_filename': 1, 'intime': 100, 'outtime': 200},
        )),
        MplsMock(mpls=5, clips=(
            {'clip_information_filename': 5, 'intime': 10_000, 'outtime': 20_000},
            {'clip_information_filename': 6, 'intime': 30_000, 'outtime': 40_000},
            {'clip_information_filename': 7, 'intime': 50_000, 'outtime': 60_000},
        )),
    )


def test__filter_playlists():
    playlists = (
        Mock(name='playlist1', duration=1000),
        # Too short.
        Mock(name='playlist2', duration=179),
        Mock(name='playlist3', duration=180),
        Mock(name='playlist4', duration=2000),
    )
    assert disc.bluray._filter_playlists(playlists) == (
        playlists[0],
        playlists[2],
        playlists[3],
    )


def test_Mpls(data_dir):
    mpls_filepath = os.path.join(data_dir, 'video', 'test.mpls')
    mpls = disc.bluray.Mpls(mpls_filepath)
    assert mpls.filepath == mpls_filepath
    assert mpls == {
        'appinfo': {
            'misc_flags': 64,
            'playback_type': 0,
            'uo_mask_table': 4402597330944,
            'length': 14,
        },
        'header': {
            'extension_data_start_address': 0,
            'playlist_mark_start_address': 214,
            'playlist_start_address': 58,
            'type_indicator': 'MPLS',
            'version_number': '0200',
        },
        'playlist': {
            'length': 152,
            'nb_play_items': 1,
            'nb_sub_paths': 0,
            'play_items': (
                {
                    'length': 144,
                    'clip_information_filename': '50201',
                    'clip_codec_identifier': 'M2TS',
                    'misc_flags_1': 1,
                    'is_multi_angle': False,
                    'ref_to_stcid': 0,
                    'intime': 524280,
                    'outtime': 240858123,
                    'uo_mask_table': 0,
                    'misc_flags_2': 0,
                    'still_mode': 0,
                    'stn_table': {
                        'dv_stream_entries': (),
                        'length': 110,
                        'nb_prim_video_stream_entries': 1,
                        'nb_prim_audio_stream_entries': 2,
                        'nb_prim_pgs_stream_entries': 3,
                        'nb_prim_igs_stream_entries': 0,
                        'nb_seco_audio_stream_entries': 0,
                        'nb_seco_video_stream_entries': 0,
                        'nb_seco_pgs_stream_entries': 0,
                        'nb_dv_stream_entries': 0,
                        'prim_audio_stream_entries': (
                            (
                                {
                                    'length': 9,
                                    'ref_to_stream_pid': '0x1100',
                                    'stream_type': 1,
                                },
                                {
                                    'audio_format': 6,
                                    'language_code': 'eng',
                                    'length': 5,
                                    'samplerate': 1,
                                    'stream_coding_type': 134,
                                },
                            ),
                            (
                                {
                                    'length': 9,
                                    'ref_to_stream_pid': '0x1101',
                                    'stream_type': 1,
                                },
                                {
                                    'audio_format': 6,
                                    'language_code': 'deu',
                                    'length': 5,
                                    'samplerate': 1,
                                    'stream_coding_type': 130,
                                },
                            ),
                        ),
                        'prim_igs_stream_entries': (),
                        'prim_pgs_stream_entries': (
                            (
                                {
                                    'length': 9,
                                    'ref_to_stream_pid': '0x1200',
                                    'stream_type': 1,
                                },
                                {
                                    'language_code': 'eng',
                                    'length': 5,
                                    'stream_coding_type': 144,
                                },
                            ),
                            (
                                {
                                    'length': 9,
                                    'ref_to_stream_pid': '0x1201',
                                    'stream_type': 1,
                                },
                                {
                                    'language_code': 'deu',
                                    'length': 5,
                                    'stream_coding_type': 144,
                                },
                            ),
                            (
                                {
                                    'length': 9,
                                    'ref_to_stream_pid': '0x1202',
                                    'stream_type': 1,
                                },
                                {
                                    'language_code': 'deu',
                                    'length': 5,
                                    'stream_coding_type': 144,
                                },
                            ),
                        ),
                        'prim_video_stream_entries': (
                            (
                                {
                                    'length': 9,
                                    'ref_to_stream_pid': '0x1011',
                                    'stream_type': 1,
                                },
                                {
                                    'framerate': 1,
                                    'length': 5,
                                    'stream_coding_type': 234,
                                    'video_format': 6,
                                },
                            ),
                        ),
                        'seco_audio_stream_entries': (),
                        'seco_pgs_stream_entries': (),
                        'seco_video_stream_entries': (),
                    },
                },
            ),
            'sub_paths': (),
        },
    }
