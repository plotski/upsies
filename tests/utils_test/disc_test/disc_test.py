from unittest.mock import Mock, call

import pytest

from upsies.utils import disc


@pytest.mark.parametrize(
    argnames='content_path, multidisc, is_bluray, is_dvd, exp_return_value, exp_mock_calls',
    argvalues=(
        ('path/to/content', '<multidisc>', False, False, False, [
            call.is_bluray('path/to/content', multidisc='<multidisc>'),
            call.is_dvd('path/to/content', multidisc='<multidisc>'),
        ]),
        ('path/to/content', '<multidisc>', False, True, True, [
            call.is_bluray('path/to/content', multidisc='<multidisc>'),
            call.is_dvd('path/to/content', multidisc='<multidisc>'),
        ]),
        ('path/to/content', '<multidisc>', True, False, True, [
            call.is_bluray('path/to/content', multidisc='<multidisc>'),
        ]),
        ('path/to/content', '<multidisc>', True, True, True, [
            call.is_bluray('path/to/content', multidisc='<multidisc>'),
        ]),
    ),
    ids=lambda v: repr(v),
)
def test_is_disc(content_path, multidisc, is_bluray, is_dvd, exp_return_value, exp_mock_calls, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_bluray', return_value=is_bluray), 'is_bluray')
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_dvd', return_value=is_dvd), 'is_dvd')
    assert disc.is_disc(content_path, multidisc=multidisc) is exp_return_value
    assert mocks.mock_calls == exp_mock_calls
