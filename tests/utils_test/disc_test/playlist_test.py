from unittest.mock import call

import pytest

from upsies.utils import disc, types


@pytest.mark.parametrize(
    argnames='filename, exp_type',
    argvalues=(
        ('file.mpls', 'bluray'),
        ('file.Mpls', 'bluray'),
        ('file.mPLs', 'bluray'),
        ('file.MPLS', 'bluray'),
        ('file.ifo', 'dvd'),
        ('file.Ifo', 'dvd'),
        ('file.iFo', 'dvd'),
        ('file.IFO', 'dvd'),
        ('file.foo', ''),
    ),
    ids=lambda v: repr(v),
)
def test_Playlist_type(filename, exp_type):
    p = disc.Playlist(
        items=('foo', 'bar', 'baz'),
        filepath=f'path/to/{filename}',
    )
    assert p.type == exp_type


@pytest.mark.parametrize(
    argnames='filepath, discpath, exp_label',
    argvalues=(
        ('path/to/file.pls', None, 'file'),
        ('path/to/disc/playlists/file.pls', 'path/to/disc', 'disc[file]'),
    ),
    ids=lambda v: repr(v),
)
def test_Playlist_label(filepath, discpath, exp_label):
    p = disc.Playlist(
        items=('foo', 'bar', 'baz'),
        filepath=filepath,
        discpath=discpath,
    )
    assert p.label == exp_label


def test_Playlist_items():
    p = disc.Playlist(items=['foo', 'bar', 'baz'], filepath='path/to/file')
    assert p.items == ('foo', 'bar', 'baz')


def test_Playlist_largest_item(tmp_path):
    root = tmp_path / 'mydisc'
    root.mkdir()
    items = [
        root / 'foo',
        root / 'bar',
        root / 'baz',
    ]
    items[0].write_bytes(b'abc')
    items[1].write_bytes(b'abcdef')
    items[2].write_bytes(b'abcd')

    p = disc.Playlist(items=items, filepath='path/to/file')
    assert p.largest_item == root / 'bar'


def test_Playlist_filepath():
    p = disc.Playlist(items=['foo', 'bar', 'baz'], filepath='path/to/myfile')
    assert p.filepath == 'path/to/myfile'


def test_Playlist_filename():
    p = disc.Playlist(items=['foo', 'bar', 'baz'], filepath='path/to/myfile')
    assert p.filename == 'myfile'


@pytest.mark.parametrize(
    argnames='discpath, exp_discpath',
    argvalues=(
        (None, None),
        ('path/to/mydisc', 'path/to/mydisc'),
    ),
    ids=lambda v: repr(v),
)
def test_Playlist_discpath(discpath, exp_discpath):
    p = disc.Playlist(items=['foo', 'bar', 'baz'], filepath='path/to/mydisc/myfile.pls', discpath=discpath)
    assert p.discpath == exp_discpath


@pytest.mark.parametrize(
    argnames='discpath, exp_discname',
    argvalues=(
        (None, None),
        ('path/to/mydisc', 'mydisc'),
    ),
    ids=lambda v: repr(v),
)
def test_Playlist_discname(discpath, exp_discname):
    p = disc.Playlist(items=['foo', 'bar', 'baz'], filepath='path/to/mydisc/myfile.pls', discpath=discpath)
    assert p.discname == exp_discname


def test_Playlist_size(tmp_path):
    root = tmp_path / 'mydisc'
    root.mkdir()
    items = [
        root / 'foo',
        root / 'bar',
        root / 'baz',
    ]
    items[0].write_bytes(b'abc')
    items[1].write_bytes(b'abcdef')
    items[2].write_bytes(b'abcd')

    p = disc.Playlist(items=items, filepath='path/to/mydisc/myfile.pls')
    assert p.size == 13
    assert isinstance(p.size, types.Bytes)


@pytest.mark.parametrize(
    argnames='duration_argument, item_durations, exp_duration',
    argvalues=(
        (123, (), 123),
        (None, (10, 20, 30), 60),
    ),
    ids=lambda v: repr(v),
)
def test_Playlist_duration(duration_argument, item_durations, exp_duration, mocker):
    items = ['foo', 'bar', 'baz']
    get_duration_mock = mocker.patch('upsies.utils.mediainfo.get_duration', side_effect=item_durations)

    p = disc.Playlist(items=items, filepath='path/to/mydisc/myfile.pls', duration=duration_argument)
    assert p.duration == exp_duration
    assert isinstance(p.duration, types.Timestamp)
    if item_durations:
        assert get_duration_mock.call_args_list == [
            call(item) for item in items
        ]
    else:
        assert get_duration_mock.call_args_list == []


@pytest.mark.parametrize(
    argnames='is_main, exp_is_main',
    argvalues=(
        (False, False),
        (True, True),
        (0, False),
        (1, True),
    ),
    ids=lambda v: repr(v),
)
def test_Playlist_is_main(is_main, exp_is_main):
    p = disc.Playlist(items=('foo', 'bar', 'baz'), filepath='path/to/mydisc/myfile.pls', is_main=is_main)
    assert p.is_main == exp_is_main
    p.is_main = 'yes'
    assert p.is_main is True
    p.is_main = ''
    assert p.is_main is False


def test_Playlist_id(mocker):
    p = disc.Playlist(
        items=('foo', 'bar', 'baz'),
        filepath='path/to/mydisc/myfile.pls',
        discpath='path/to/mydisc',
        duration=123,
    )
    assert p.id == (
        'path/to/mydisc',
        'path/to/mydisc/myfile.pls',
        ('foo', 'bar', 'baz'),
        123,
    )


def test_Playlist___eq__():
    original_values = {
        'items': ('foo', 'bar', 'baz'),
        'filepath': 'path/to/mydisc/myfile.pls',
        'discpath': 'path/to/mydisc',
        'duration': 123,
        'is_main': False,
    }
    original_playlist = disc.Playlist(**original_values)
    assert original_playlist == disc.Playlist(**original_values)
    assert original_playlist != disc.Playlist(**{**original_values, 'items': ('fii', 'bar', 'baz')})
    assert original_playlist != disc.Playlist(**{**original_values, 'filepath': 'path/to/mydisc/myotherfile.pls'})
    assert original_playlist != disc.Playlist(**{**original_values, 'discpath': 'path/to/yourdisc'})
    assert original_playlist != disc.Playlist(**{**original_values, 'duration': 456})
    assert original_playlist == disc.Playlist(**{**original_values, 'is_main': False})
    assert original_playlist == disc.Playlist(**{**original_values, 'is_main': True})
    assert original_playlist.__eq__(123) is NotImplemented


def test_Playlist___hash__():
    p = disc.Playlist(
        items=('foo', 'bar', 'baz'),
        filepath='path/to/mydisc/myfile.pls',
        discpath='path/to/mydisc',
        duration=123,
    )
    assert hash(p) == hash(p.id)


def test_Playlist___repr__():
    p = disc.Playlist(items=('foo', 'bar', 'baz'), filepath='path/to/mydisc/myfile.pls')
    assert repr(p) == f'<Playlist {p.label}>'


class MockPlaylist:
    def __init__(self, size, duration):
        self.size = size
        self.duration = duration
        self.is_main = False

    def __eq__(self, other):
        return (
            type(self) is type(other)
            and self.size == other.size
            and self.duration == other.duration
        )

    def __repr__(self):
        return f'Playlist(size={self.size}, duration={self.duration})'

@pytest.mark.parametrize(
    argnames='playlists, exp_main_playlists',
    argvalues=(
        pytest.param([], [], id='No playlists'),
        pytest.param(
            [
                MockPlaylist(size=3e6, duration=4000),
            ],
            [
                MockPlaylist(size=3e6, duration=4000),
            ],
            id='Single playlist',
        ),
        pytest.param(
            [
                MockPlaylist(size=1e6, duration=700),
                MockPlaylist(size=2e6, duration=900),
                MockPlaylist(size=3e6, duration=699),
                MockPlaylist(size=4e6, duration=1300),
                MockPlaylist(size=5e6, duration=1301),
                MockPlaylist(size=6e6, duration=1000),
            ],
            [
                MockPlaylist(size=1e6, duration=700),
                MockPlaylist(size=2e6, duration=900),
                MockPlaylist(size=4e6, duration=1300),
                MockPlaylist(size=6e6, duration=1000),
            ],
            id='Multiple playlist',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_mark_main_playlists(playlists, exp_main_playlists):
    disc.playlist.mark_main_playlists(playlists)
    main_playlists = [p for p in playlists if p.is_main]
    assert main_playlists == exp_main_playlists
