from unittest.mock import call

import pytest

from upsies.utils import disc


@pytest.mark.parametrize(
    argnames='content_path, existing_directories, existing_files, multidisc, exp_return_value',
    argvalues=(
        pytest.param(
            'path/to/content',
            (),
            (),
            False,
            False,
            id='Non-existing directory',
        ),
        pytest.param(
            'path/to/content',
            ('path/to/content/VIDEO_TS',),
            (),
            False,
            True,
            id='VIDEO_TS in directory',
        ),
        pytest.param(
            'path/to/content',
            ('path/to/content/ASDF',),
            (),
            False,
            False,
            id='No VIDEO_TS in directory',
        ),
        pytest.param(
            'path/to/content',
            (),
            ('path/to/content/VIDEO_TS.IFO',),
            False,
            True,
            id='VIDEO_TS.IFO in content path',
        ),
        pytest.param(
            'path/to/content',
            ('path/to/content/disc1/VIDEO_TS',),
            (),
            False,
            False,
            id='VIDEO_TS in subdirectory (multidisc=False)',
        ),
        pytest.param(
            'path/to/content',
            ('path/to/content/disc1/VIDEO_TS',),
            (),
            True,
            True,
            id='VIDEO_TS in subdirectory (multidisc=True)',
        ),
        pytest.param(
            'path/to/content',
            ('path/to/content/disc1/ASDF',),
            (),
            True,
            False,
            id='No VIDEO_TS in subdirectory (multidisc=True)',
        ),
    ),
)
def test_is_dvd(content_path, existing_directories, existing_files, multidisc, exp_return_value, tmp_path):
    for relative_path in existing_directories:
        (tmp_path / relative_path).mkdir(parents=True, exist_ok=True)
    for relative_path in existing_files:
        (tmp_path / relative_path).parent.mkdir(parents=True, exist_ok=True)
        (tmp_path / relative_path).write_text(f'<content of {relative_path}>')
    content_path = str(tmp_path / content_path)
    return_value = disc.is_dvd(content_path, multidisc=multidisc)
    assert return_value is exp_return_value


@pytest.mark.parametrize(
    argnames='content_path, existing_directories, existing_files, exp_return_value',
    argvalues=(
        pytest.param(
            'path/to/content',
            (),
            (),
            (),
            id='Non-existing directory',
        ),
        pytest.param(
            'path/to/content',
            (
                'path/to/content/VIDEO_TS',
            ),
            (),
            ('{tmp_path}/path/to/content',),
            id='VIDEO_TS in content directory',
        ),
        pytest.param(
            'path/to/content',
            (),
            (
                'path/to/content/VIDEO_TS.IFO',
            ),
            ('{tmp_path}/path/to/content',),
            id='VIDEO_TS.IFO in content directory',
        ),
        pytest.param(
            'path/to/content',
            (
                'path/to/content/disc1/VIDEO_TS',
                'path/to/content/disc2/VIDEO_TS',
                'path/to/content/somethingelse/foo/bar',
            ),
            (),
            (
                '{tmp_path}/path/to/content/disc1',
                '{tmp_path}/path/to/content/disc2',
            ),
            id='VIDEO_TS in some subdirectories',
        ),
        pytest.param(
            'path/to/content',
            (),
            (
                'path/to/content/disc1/VIDEO_TS.IFO',
                'path/to/content/disc2/VIDEO_TS.IFO',
                'path/to/content/somethingelse/foo/bar',
            ),
            (
                '{tmp_path}/path/to/content/disc1',
                '{tmp_path}/path/to/content/disc2',
            ),
            id='VIDEO_TS.IFO in some subdirectories',
        ),
    ),
)
def test_get_disc_paths(content_path, existing_directories, existing_files, exp_return_value, tmp_path):
    for relative_path in existing_directories:
        (tmp_path / relative_path).mkdir(parents=True, exist_ok=True)
    for relative_path in existing_files:
        (tmp_path / relative_path).parent.mkdir(parents=True, exist_ok=True)
        (tmp_path / relative_path).write_text(f'<content of {relative_path}>')
    content_path = str(tmp_path / content_path)
    return_value = disc.dvd.get_disc_paths(content_path)
    assert return_value == tuple(
        path.format(tmp_path=tmp_path)
        for path in exp_return_value
    )


@pytest.mark.parametrize(
    argnames='content_path, video_ts_path, existing_files, exp_playlists_kwargs',
    argvalues=(
        pytest.param(
            'path/to/content', 'path/to/content/VIDEO_TS',
            (
                {'path': 'path/to/content/VIDEO_TS/VIDEO_TS.IFO', 'duration': -1},
                {'path': 'path/to/content/VIDEO_TS/VTS_01_0.BUP', 'duration': -1},
                {'path': 'path/to/content/VIDEO_TS/VTS_01_0.IFO', 'duration': 180},
                {'path': 'path/to/content/VIDEO_TS/VTS_01_1.VOB', 'duration': -1},
                {'path': 'path/to/content/VIDEO_TS/VTS_02_0.IFO', 'duration': 179},
                {'path': 'path/to/content/VIDEO_TS/VTS_02_1.VOB', 'duration': -1},
                {'path': 'path/to/content/VIDEO_TS/VTS_02_2.VOB', 'duration': -1},
                {'path': 'path/to/content/VIDEO_TS/VTS_02_3.VOB', 'duration': -1},
                {'path': 'path/to/content/VIDEO_TS/VTS_03_0.IFO', 'duration': 3210},
                {'path': 'path/to/content/VIDEO_TS/VTS_03_0.VOB', 'duration': -1},
                {'path': 'path/to/content/VIDEO_TS/VTS_03_1.VOB', 'duration': -1},
                {'path': 'path/to/content/VIDEO_TS/VTS_03_2.VOB', 'duration': -1},
                {'path': 'path/to/content/VIDEO_TS/VTS_03_2.VOB', 'duration': -1},
            ),
            (
                {
                    'discpath': '{tmp_path}/path/to/content',
                    'filepath': '{tmp_path}/path/to/content/VIDEO_TS/VTS_01_0.IFO',
                    'duration': 180,
                    'items': (
                        '{tmp_path}/path/to/content/VIDEO_TS/VTS_01_1.VOB',
                    ),
                },
                # VTS_02_0.IFO is ignored because its duration < 180.
                {
                    'discpath': '{tmp_path}/path/to/content',
                    'filepath': '{tmp_path}/path/to/content/VIDEO_TS/VTS_03_0.IFO',
                    'duration': 3210,
                    'items': (
                        '{tmp_path}/path/to/content/VIDEO_TS/VTS_03_0.VOB',
                        '{tmp_path}/path/to/content/VIDEO_TS/VTS_03_1.VOB',
                        '{tmp_path}/path/to/content/VIDEO_TS/VTS_03_2.VOB',
                    ),
                },
            ),
            id='VIDEO_TS directory is found',
        ),
        pytest.param(
            'path/to/content', None,
            (),
            (),
            id='VIDEO_TS directory is not found',
        ),
    ),
)
def test_get_playlists(content_path, video_ts_path, existing_files, exp_playlists_kwargs, tmp_path, mocker):
    content_path = (tmp_path / content_path)
    if video_ts_path is not None:
        video_ts_path = (tmp_path / video_ts_path)
        video_ts_path.mkdir(parents=True, exist_ok=True)

    get_duration_from_mediainfo_side_effects = []
    for fileinfo in existing_files:
        filepath = (tmp_path / fileinfo['path'])
        filepath.parent.mkdir(parents=True, exist_ok=True)
        filepath.write_text(f'<content of {fileinfo["path"]}>')
        get_duration_from_mediainfo_side_effects.append(fileinfo['duration'])

    def get_duration_from_mediainfo(filepath):
        for fileinfo in existing_files:
            if filepath.endswith(fileinfo['path']):
                return fileinfo['duration']

    mocker.patch('upsies.utils.mediainfo.get_duration_from_mediainfo', side_effect=get_duration_from_mediainfo)
    mocker.patch('upsies.utils.disc.dvd._get_video_ts_directory', return_value=video_ts_path)
    mark_main_playlists_mock = mocker.patch('upsies.utils.disc.playlist.mark_main_playlists')

    # Fix paths in playlist arguments.
    for exp_playlist_kwargs in exp_playlists_kwargs:
        exp_playlist_kwargs['discpath'] = exp_playlist_kwargs['discpath'].format(tmp_path=tmp_path)
        exp_playlist_kwargs['filepath'] = exp_playlist_kwargs['filepath'].format(tmp_path=tmp_path)
        exp_playlist_kwargs['items'] = tuple(
            filepath.format(tmp_path=tmp_path)
            for filepath in exp_playlist_kwargs['items']
        )

    playlists = disc.dvd.get_playlists(str(content_path))
    exp_playlists = tuple(
        disc.Playlist(**exp_playlist_kwargs)
        for exp_playlist_kwargs in exp_playlists_kwargs
    )
    for exp_playlist, playlist in zip(exp_playlists, playlists):
        print('Exp:', repr(exp_playlist))
        print('Got:', repr(playlist))
    assert playlists == exp_playlists

    if exp_playlists:
        assert mark_main_playlists_mock.call_args_list == [call(exp_playlists)]
    else:
        assert mark_main_playlists_mock.call_args_list == []


@pytest.mark.parametrize(
    argnames='discpath, existing_directories, existing_files, exp_return_value',
    argvalues=(
        pytest.param(
            'path/to/content',
            ('path/to/content/VIDEO_TS',),
            (),
            '{tmp_path}/path/to/content/VIDEO_TS',
            id='VIDEO_TS exists',
        ),
        pytest.param(
            'path/to/content',
            (),
            ('path/to/content/VIDEO_TS.IFO',),
            '{tmp_path}/path/to/content',
            id='VIDEO_TS.IFO exists',
        ),
        pytest.param(
            'path/to/content',
            (),
            (),
            None,
            id='Not a VIDEO_TS path',
        ),
    ),
)
def test__get_video_ts_directory(discpath, existing_directories, existing_files, exp_return_value, tmp_path):
    for relative_path in existing_directories:
        (tmp_path / relative_path).mkdir(parents=True, exist_ok=True)
    for relative_path in existing_files:
        (tmp_path / relative_path).parent.mkdir(parents=True, exist_ok=True)
        (tmp_path / relative_path).write_text(f'<content of {relative_path}>')
    discpath = str(tmp_path / discpath)
    return_value = disc.dvd._get_video_ts_directory(discpath)
    if exp_return_value is None:
        assert return_value is None
    else:
        assert return_value == exp_return_value.format(tmp_path=tmp_path)
