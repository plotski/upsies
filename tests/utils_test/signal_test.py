import ast
import asyncio
import re
import sys
from unittest.mock import AsyncMock, Mock, call

import pytest

from upsies.utils.signal import Signal


@pytest.mark.parametrize(
    argnames='flag, exp_bool, exp_str',
    argvalues=(
        (Signal.Stopped.false, False, 'Stopped.false'),
        (Signal.Stopped.true, True, 'Stopped.true'),
        (Signal.Stopped.immediately, True, 'Stopped.immediately'),
    ),
    ids=lambda v: repr(v),
)
def test_Stopped(flag, exp_bool, exp_str):
    assert bool(flag) is exp_bool
    assert str(flag) == exp_str


def test_id_provided_at_initialization():
    s = Signal(id='asdf', signals=('foo', 'bar', 'baz'))
    assert s.id == 'asdf'


@pytest.mark.parametrize(
    argnames='id, exp_id',
    argvalues=(
        ('myid', 'myid'),
        ('', 'anonymous'),
        (0, 0),
        (None, 'anonymous'),
        (123, 123),
        ((1, 2, 3), (1, 2, 3)),
    ),
    ids=lambda v: repr(v),
)
def test_id_provided_after_initialization(id, exp_id):
    s = Signal(signals=('foo', 'bar', 'baz'))
    assert s.id == 'anonymous'
    s.id = id
    assert s.id == exp_id


def test_signals_are_added():
    s = Signal(signals=('foo', 'bar', 'baz'))
    assert s.signals == {'foo': [], 'bar': [], 'baz': []}


def test_adding_unhashable_signal():
    s = Signal(id='asdf', signals=())
    with pytest.raises(TypeError, match=r'^asdf: Unhashable signal: \{\}$'):
        assert s.add({})

def test_adding_duplicate_signal():
    s = Signal(id='asdf', signals=('foo', 'bar', 'baz'))
    with pytest.raises(RuntimeError, match=r"^asdf: Signal already added: 'foo'$"):
        s.add('foo')

def test_adding_adds_signal():
    s = Signal(id='asdf', signals=())
    assert s.signals == {}
    assert s._record_signals == []
    s.add('foo')
    assert s.signals == {'foo': []}
    assert s._record_signals == []
    s.add('bar', record=True)
    assert s.signals == {'foo': [], 'bar': []}
    assert s._record_signals == ['bar']
    s.add('baz', record=False)
    assert s.signals == {'foo': [], 'bar': [], 'baz': []}
    assert s._record_signals == ['bar']


def test_record():
    s = Signal(id='asdf', signals=('foo', 'bar'))
    assert s.recording == []
    s.record('foo')
    assert s.recording == ['foo']
    s.record('bar')
    assert s.recording == ['foo', 'bar']
    with pytest.raises(ValueError, match=r"^asdf: Unknown signal: 'baz'$"):
        s.record('baz')
    assert s.recording == ['foo', 'bar']


def test_registering_for_unknown_signal():
    s = Signal(id='asdf', signals=('foo',))
    with pytest.raises(ValueError, match=r"^asdf: Unknown signal: 'bar'$"):
        s.register('bar', lambda: None)

def test_registering_noncallable():
    s = Signal(id='asdf', signals=('foo',))
    with pytest.raises(TypeError, match=r"^asdf: Not a callable: 'bar'$"):
        s.register('foo', 'bar')


def test_emit_after_stop_was_called():
    s = Signal(id='asdf', signals=('foo',))
    s.stop()
    exp_msg = "asdf: Cannot emit signal 'foo' after stop() was called"
    with pytest.raises(RuntimeError, match=rf'^{re.escape(str(exp_msg))}$'):
        s.emit('foo')

@pytest.mark.parametrize('kwargs', ({}, {'1': 2}), ids=lambda v: str(v))
@pytest.mark.parametrize('args', ((), (1,), (1, 2)), ids=lambda v: str(v))
def test_emit_to_no_callback(args, kwargs):
    s = Signal(id='asdf', signals=('foo',))
    s.emit('foo', *args, **kwargs)

@pytest.mark.parametrize('kwargs', ({}, {'1': 2}), ids=lambda v: str(v))
@pytest.mark.parametrize('args', ((), (1,), (1, 2)), ids=lambda v: str(v))
def test_emit_to_single_callback(args, kwargs):
    s = Signal(id='asdf', signals=('foo', 'bar'))
    cb = Mock()
    s.register('foo', cb)
    s.emit('foo', *args, **kwargs)
    assert cb.call_args_list == [call(*args, **kwargs)]
    s.emit('bar', *args, **kwargs)
    assert cb.call_args_list == [call(*args, **kwargs)]

@pytest.mark.parametrize('kwargs', ({}, {'1': 2}), ids=lambda v: str(v))
@pytest.mark.parametrize('args', ((), (1,), (1, 2)), ids=lambda v: str(v))
def test_emit_to_multiple_callbacks(args, kwargs):
    s = Signal(id='asdf', signals=('foo', 'bar'))
    cb = Mock()
    s.register('foo', cb.a)
    s.register('foo', cb.b)
    s.register('foo', cb.c)
    s.register('bar', cb.d)
    s.register('bar', cb.e)
    s.register('bar', cb.f)
    s.emit('foo', *args, **kwargs)
    assert cb.mock_calls == [
        call.a(*args, **kwargs),
        call.b(*args, **kwargs),
        call.c(*args, **kwargs),
    ]
    s.emit('bar')
    assert cb.mock_calls == [
        call.a(*args, **kwargs),
        call.b(*args, **kwargs),
        call.c(*args, **kwargs),
        call.d(),
        call.e(),
        call.f(),
    ]

def test_emit_records_call():
    s = Signal(id='asdf', signals=())
    s.add('foo', record=True)
    s.add('bar', record=True)
    s.add('baz', record=False)
    assert s.emissions == ()
    assert s.emissions_recorded == ()
    s.emit('foo', 123)
    s.emit('bar', 'hello', 'world')
    s.emit('foo', 'and', this='that')
    s.emit('baz', 0)
    s.emit('bar', four=56)
    assert s.emissions == (
        ('foo', {'args': (123,), 'kwargs': {}}),
        ('bar', {'args': ('hello', 'world'), 'kwargs': {}}),
        ('foo', {'args': ('and',), 'kwargs': {'this': 'that'}}),
        ('baz', {'args': (0,), 'kwargs': {}}),
        ('bar', {'args': (), 'kwargs': {'four': 56}}),
    )
    assert s.emissions_recorded == (
        ('foo', {'args': (123,), 'kwargs': {}}),
        ('bar', {'args': ('hello', 'world'), 'kwargs': {}}),
        ('foo', {'args': ('and',), 'kwargs': {'this': 'that'}}),
        ('bar', {'args': (), 'kwargs': {'four': 56}}),
    )


async def run_instructions(*instructions):
    if sys.version_info < (3, 11):
        pytest.skip(reason='TaskGroup requires python3.11 or higher')

    import logging
    _log = logging.getLogger(__name__)

    s = Signal(id='asdf', signals=())
    mocks = AsyncMock()

    async def receive_all(signal, callback, only_posargs):
        _log.debug(f'receive_for({callback!r}): Receiving {signal}')
        iterator = s.receive_all(signal, only_posargs=only_posargs)
        if only_posargs:
            async for posargs in iterator:
                _log.debug(f'receive_for({callback!r}): Received {signal}: {posargs!r}')
                await callback(*posargs)
        else:
            async for posargs, kwargs in iterator:
                _log.debug(f'receive_for({callback!r}): Received {signal}: {posargs!r}, {kwargs!r}')
                await callback(*posargs, **kwargs)

    async def receive_one(signal, callback, only_posargs):
        _log.debug(f'receive_one_for({callback!r}): Receiving {signal}')
        if only_posargs:
            posargs = await s.receive_one(signal, only_posargs=only_posargs)
            _log.debug(f'receive_one_for({callback!r}): Received {signal}: {posargs!r}')
            await callback(*posargs)
        else:
            posargs, kwargs = await s.receive_one(signal, only_posargs=only_posargs)
            _log.debug(f'receive_one_for({callback!r}): Received {signal}: {posargs!r}, {kwargs!r}')
            await callback(*posargs, **kwargs)

    async with asyncio.TaskGroup() as group:
        for inst in instructions:
            _log.debug('INSTRUCTION: %r', inst)
            if inst[0] == 'add_signal':
                s.add(inst[1])

            elif inst[0] == 'receive_all':
                signal = inst[1]
                callback = getattr(mocks, inst[2])
                only_posargs = (
                    ast.literal_eval(inst[3].split('=')[1])
                    if len(inst) >= 4 else
                    False
                )
                group.create_task(receive_all(signal, callback, only_posargs=only_posargs))

            elif inst[0] == 'receive_one':
                signal = inst[1]
                callback = getattr(mocks, inst[2])
                only_posargs = (
                    ast.literal_eval(inst[3].split('=')[1])
                    if len(inst) >= 4 else
                    False
                )
                group.create_task(receive_one(signal, callback, only_posargs=only_posargs))

            elif inst[0] == 'emit':
                signal, posargs, kwargs = inst[1:]
                s.emit(signal, *posargs, **kwargs)

            elif inst[0] == 'stop':
                immediately = bool(len(inst) >= 2 and inst[1] == 'immediately')
                s.stop(immediately=immediately)

            elif inst[0] == 'assert_mock_calls':
                # Lets things fall into place before we make assertions.
                assert mocks.mock_calls == inst[1]

            elif inst[0] == 'yield':
                # Allow instructions to take effect by yielding control to the event loop.
                _log.debug('=== EFFECTING INSTRUCTIONS ===')
                await asyncio.sleep(0.01)

            else:
                raise RuntimeError(f'Unknown instruction: {inst}')

        # Stop signals and give receive_(all|one)() calls a chance to react.
        s.stop()
        await asyncio.sleep(0.01)

@pytest.mark.parametrize('only_posargs', (True, False), ids=('only_posargs=True', 'only_posargs=False'))
@pytest.mark.asyncio
async def test_receive__emitted_signals_are_queued_up_for_active_receive_calls(only_posargs):
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('receive_all', 'foo', 'handle_foo', f'only_posargs={only_posargs}'),
        ('receive_all', 'bar', 'handle_bar', f'only_posargs={only_posargs}'),
        ('yield',),
        ('emit', 'foo', ('emission foo 1', 'emission foo 2', 'emission foo 3'), {'name': 'Foo'}),
        ('emit', 'bar', ('emission bar 1', 'emission bar 2', 'emission bar 3'), {'name': 'Bar'}),
        ('yield',),
        ('assert_mock_calls', [
            call.handle_foo('emission foo 1', 'emission foo 2', 'emission foo 3', **({} if only_posargs else {'name': 'Foo'})),
            call.handle_bar('emission bar 1', 'emission bar 2', 'emission bar 3', **({} if only_posargs else {'name': 'Bar'})),
        ]),
    )

@pytest.mark.parametrize('only_posargs', (True, False), ids=('only_posargs=True', 'only_posargs=False'))
@pytest.mark.asyncio
async def test_receive__old_emissions_made_before_emissions_are_received(only_posargs):
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('emit', 'foo', ('emission foo 1', 'emission foo 2', 'emission foo 3'), {'name': 'Foo'}),
        ('emit', 'bar', ('emission bar 1', 'emission bar 2', 'emission bar 3'), {'name': 'Bar'}),
        ('yield',),
        ('receive_all', 'foo', 'handle_foo', f'only_posargs={only_posargs}'),
        ('receive_all', 'bar', 'handle_bar', f'only_posargs={only_posargs}'),
        ('yield',),
        ('assert_mock_calls', [
            call.handle_foo('emission foo 1', 'emission foo 2', 'emission foo 3', **({} if only_posargs else {'name': 'Foo'})),
            call.handle_bar('emission bar 1', 'emission bar 2', 'emission bar 3', **({} if only_posargs else {'name': 'Bar'})),
        ]),
    )

@pytest.mark.asyncio
async def test_receive__stop_is_called_before_receive_and_only_old_emissions_are_received():
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('emit', 'foo', ('emission1',), {}),
        ('emit', 'bar', ('emission2',), {}),
        ('emit', 'baz', ('emission3',), {}),
        ('stop', 'not immediately'),
        ('receive_all', 'foo', 'handle_foo'),
        ('receive_all', 'bar', 'handle_bar'),
        ('yield',),
        ('assert_mock_calls', [
            call.handle_foo('emission1'),
            call.handle_bar('emission2'),
        ]),
    )

@pytest.mark.asyncio
async def test_receive__stop_immediately_is_called_before_receive_and_only_old_emissions_are_received():
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('emit', 'foo', ('emission1',), {}),
        ('emit', 'bar', ('emission2',), {}),
        ('emit', 'baz', ('emission3',), {}),
        ('stop', 'immediately'),
        ('receive_all', 'foo', 'handle_foo'),
        ('receive_all', 'bar', 'handle_bar'),
        ('yield',),
        ('assert_mock_calls', [
        ]),
    )


@pytest.mark.parametrize('only_posargs', (True, False), ids=('only_posargs=True', 'only_posargs=False'))
@pytest.mark.asyncio
async def test_receive_one__emitted_signals_are_queued_up_for_active_receive_calls(only_posargs):
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('receive_one', 'foo', 'handle_foo', f'only_posargs={only_posargs}'),
        ('receive_one', 'bar', 'handle_bar', f'only_posargs={only_posargs}'),
        ('yield',),
        ('emit', 'foo', ('emission foo 1', 'emission foo 2', 'emission foo 3'), {'name': 'Foo'}),
        ('emit', 'bar', ('emission bar 1', 'emission bar 2', 'emission bar 3'), {'name': 'Bar'}),
        ('yield',),
        ('assert_mock_calls', [
            call.handle_foo('emission foo 1', 'emission foo 2', 'emission foo 3', **({} if only_posargs else {'name': 'Foo'})),
            call.handle_bar('emission bar 1', 'emission bar 2', 'emission bar 3', **({} if only_posargs else {'name': 'Bar'})),
        ]),
    )

@pytest.mark.parametrize('only_posargs', (True, False), ids=('only_posargs=True', 'only_posargs=False'))
@pytest.mark.asyncio
async def test_receive_one__old_emissions_made_before_emissions_are_received(only_posargs):
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('emit', 'foo', ('emission foo 1', 'emission foo 2', 'emission foo 3'), {'name': 'Foo'}),
        ('emit', 'bar', ('emission bar 1', 'emission bar 2', 'emission bar 3'), {'name': 'Bar'}),
        ('yield',),
        ('receive_one', 'foo', 'handle_foo', f'only_posargs={only_posargs}'),
        ('receive_one', 'bar', 'handle_bar', f'only_posargs={only_posargs}'),
        ('yield',),
        ('assert_mock_calls', [
            call.handle_foo('emission foo 1', 'emission foo 2', 'emission foo 3', **({} if only_posargs else {'name': 'Foo'})),
            call.handle_bar('emission bar 1', 'emission bar 2', 'emission bar 3', **({} if only_posargs else {'name': 'Bar'})),
        ]),
    )

@pytest.mark.asyncio
async def test_receive_one__stop_is_called_before_receive_and_only_old_emissions_are_received():
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('emit', 'foo', ('emission1',), {}),
        ('emit', 'bar', ('emission2',), {}),
        ('emit', 'baz', ('emission3',), {}),
        ('stop', 'not immediately'),
        ('receive_one', 'foo', 'handle_foo'),
        ('receive_one', 'bar', 'handle_bar'),
        ('yield',),
        ('assert_mock_calls', [
            call.handle_foo('emission1'),
            call.handle_bar('emission2'),
        ]),
    )

@pytest.mark.asyncio
async def test_receive_one__stop_immediately_is_called_before_receive_and_only_old_emissions_are_received():
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('emit', 'foo', ('emission1',), {}),
        ('emit', 'bar', ('emission2',), {}),
        ('emit', 'baz', ('emission3',), {}),
        ('stop', 'immediately'),
        ('receive_one', 'foo', 'handle_foo'),
        ('receive_one', 'bar', 'handle_bar'),
        ('yield',),
        ('assert_mock_calls', [
            call.handle_foo(),
            call.handle_bar(),
        ]),
    )


@pytest.mark.asyncio
async def test_wait_for(mocker):
    s = Signal(id='asdf', signals=('foo', 'bar', 'baz'))
    mocks = Mock()

    foo_task = asyncio.get_running_loop().create_task(s.wait_for('foo'))
    foo_task.add_done_callback(mocks.handle_foo)

    bar_task = asyncio.get_running_loop().create_task(s.wait_for('bar'))
    bar_task.add_done_callback(mocks.handle_bar)

    s.emit('foo')
    await foo_task
    assert mocks.mock_calls == [call.handle_foo(foo_task)]

    s.emit('bar', 'ignored', 'arguments')
    await bar_task
    assert mocks.mock_calls == [call.handle_foo(foo_task), call.handle_bar(bar_task)]


@pytest.mark.asyncio
async def test_stop_immediately():
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('receive_all', 'foo', 'handle_foo'),
        ('receive_all', 'bar', 'handle_bar'),
        ('receive_all', 'baz', 'handle_bar'),
        ('yield',),
        # Queues for foo, bar, baz exist now.
        ('emit', 'foo', ('emission1',), {}),
        ('emit', 'bar', ('emission2',), {}),
        # baz is not emitted to test if an empty queue is no problem.
        # ('emit', 'baz', ('emission3',), {}),
        ('stop', 'immediately'),
        # Queues for foo, bar, baz should now be emptied by stop().
        ('yield',),
        # No emissions because queues were emptied before receive_all() calls got control via "yield".
        ('assert_mock_calls', [
        ]),
    )

@pytest.mark.asyncio
async def test_stop_not_immediately():
    await run_instructions(
        ('add_signal', 'foo'),
        ('add_signal', 'bar'),
        ('add_signal', 'baz'),
        ('receive_all', 'foo', 'handle_foo'),
        ('receive_all', 'bar', 'handle_bar'),
        ('receive_all', 'baz', 'handle_bar'),
        ('yield',),
        # Queues for foo, bar, baz exist now.
        ('emit', 'foo', ('emission1',), {}),
        ('emit', 'bar', ('emission2',), {}),
        # baz is not emitted to test if an empty queue is no problem.
        # ('emit', 'baz', ('emission3',), {}),
        ('stop', 'not immediately'),
        # Queues for foo, bar, baz should now be emptied by stop().
        ('yield',),
        # No emissions because queues were emptied before receive_all() calls got control via "yield".
        ('assert_mock_calls', [
            call.handle_foo('emission1'),
            call.handle_bar('emission2'),
        ]),
    )


def test_replay_emits():
    emissions = (
        ('foo', {'args': (123,), 'kwargs': {}}),
        ('bar', {'args': ('hello', 'world'), 'kwargs': {}}),
        ('foo', {'args': ('and',), 'kwargs': {'this': 'that'}}),
        ('bar', {'args': (), 'kwargs': {'four': 56}}),
    )
    s = Signal(id='asdf', signals=('foo', 'bar', 'baz'))
    cb = Mock()
    s.register('foo', cb.foo)
    s.register('bar', cb.bar)
    s.register('baz', cb.baz)
    s.replay(emissions)
    assert cb.mock_calls == [
        call.foo(123),
        call.bar('hello', 'world'),
        call.foo('and', this='that'),
        call.bar(four=56),
    ]


def test_suspend_blocks_emissions():
    s = Signal(id='asdf', signals=('foo', 'bar', 'baz'))
    cb = Mock()
    s.register('foo', cb.foo)
    s.register('bar', cb.bar)
    s.register('baz', cb.baz)

    s.emit('foo', 123, this='that')
    s.emit('bar', 456, hey='ho')
    s.emit('baz', 789, zip='zop')
    assert cb.mock_calls == [
        call.foo(123, this='that'),
        call.bar(456, hey='ho'),
        call.baz(789, zip='zop'),
    ]
    cb.reset_mock()

    with s.suspend('bar', 'foo'):
        s.emit('foo', 123, this='that')
        s.emit('bar', 456, hey='ho')
        s.emit('baz', 789, zip='zop')
    assert cb.mock_calls == [
        call.baz(789, zip='zop'),
    ]
    cb.reset_mock()

    s.emit('foo', 123, this='that')
    s.emit('bar', 456, hey='ho')
    s.emit('baz', 789, zip='zop')
    assert cb.mock_calls == [
        call.foo(123, this='that'),
        call.bar(456, hey='ho'),
        call.baz(789, zip='zop'),
    ]

def test_suspend_handles_unknown_signals():
    s = Signal(id='asdf', signals=('foo', 'bar', 'baz'))
    with pytest.raises(ValueError, match=r"asdf: Unknown signal: 'boo'"):
        with s.suspend('bar', 'boo'):
            pass
