import asyncio
import queue
import re
import time
from unittest.mock import Mock, call

import pytest

from upsies import errors
from upsies.utils import daemon

# Tests sometimes hang. Let's find out why.
pytestmark = [
    pytest.mark.setup_timeout(3),
    pytest.mark.execution_timeout(10),
    pytest.mark.teardown_timeout(3),
]


def target_raising_exception(output_queue, input_queue):
    raise ValueError('Kaboom!')

def target_raising_DaemonProcessTerminated(output_queue, input_queue):
    raise errors.DaemonProcessTerminated('Bye!')

def target_sending_to_init_callback(output_queue, input_queue):
    for i in range(3):
        output_queue.put((daemon.MsgType.init, i))

def target_sending_to_info_callback(output_queue, input_queue):
    for i in range(4):
        output_queue.put((daemon.MsgType.info, i))

def target_sending_to_error_callback(output_queue, input_queue):
    for i in range(2):
        output_queue.put((daemon.MsgType.error, errors.UpsiesError(f'Error #{i}')))

def target_sending_result_to_finished_callback(output_queue, input_queue):
    output_queue.put((daemon.MsgType.result, 'foo'))

def target_sending_no_result_to_finished_callback(output_queue, input_queue):
    pass

def target_never_terminating(output_queue, input_queue):
    output_queue.put((daemon.MsgType.info, 'something'))

    while True:
        try:
            typ, _msg = input_queue.get_nowait()
        except queue.Empty:
            pass
        else:
            if typ is daemon.MsgType.terminate:
                break

        time.sleep(1)

def target_logging_input_to_file(output_queue, input_queue, logfile):
    while True:
        typ, msg = input_queue.get()
        if typ is daemon.MsgType.terminate:
            break
        elif typ is daemon.MsgType.info:
            with open(logfile, 'a') as f:
                f.write(f'{msg}\n')

def target_taking_arguments(output_queue, input_queue, foo, bar, *, baz):
    output_queue.put((daemon.MsgType.info, foo))
    output_queue.put((daemon.MsgType.info, bar))
    output_queue.put((daemon.MsgType.info, baz))


@pytest.mark.asyncio
async def test_target_gets_positional_and_keyword_arguments():
    class LocalString(str):
        pass

    class LocalStringSubclass(LocalString):
        pass

    class LocalInteger(int):
        pass

    info_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_taking_arguments,
        args=(LocalString('Foo'), LocalStringSubclass('Bar')),
        kwargs={
            LocalString('baz'): LocalInteger(847),
        },
        info_callback=info_callback,
    )
    proc.start()
    await proc.join()
    assert info_callback.call_args_list == [call('Foo'), call('Bar'), call(847)]


@pytest.mark.asyncio
async def test_exceptions_from_target_are_reraised_when_process_is_joined():
    error_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_raising_exception,
        error_callback=error_callback,
    )
    proc.start()
    with pytest.raises(ValueError, match=r'^Kaboom!$') as exc_info:
        await proc.join()
    assert exc_info.value.original_traceback.startswith('Daemon process traceback:\n')
    assert 'target_raising_exception' in exc_info.value.original_traceback
    assert exc_info.value.original_traceback.endswith('ValueError: Kaboom!')
    assert len(error_callback.call_args_list) == 1
    assert isinstance(error_callback.call_args_list[0][0][0], ValueError)
    assert str(error_callback.call_args_list[0][0][0]) == 'Kaboom!'


@pytest.mark.asyncio
async def test_target_sends_to_init_callback():
    init_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_sending_to_init_callback,
        init_callback=init_callback,
    )
    proc.start()
    await proc.join()
    assert init_callback.call_args_list == [call(0), call(1), call(2)]


@pytest.mark.asyncio
async def test_target_sends_to_info_callback():
    info_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_sending_to_info_callback,
        info_callback=info_callback,
    )
    proc.start()
    await proc.join()
    assert info_callback.call_args_list == [call(0), call(1), call(2), call(3)]


@pytest.mark.asyncio
async def test_target_sends_to_error_callback():
    error_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_sending_to_error_callback,
        error_callback=error_callback,
    )
    proc.start()
    exp_msg = 'Error #1'
    with pytest.raises(errors.UpsiesError, match=rf'^{re.escape(str(exp_msg))}$'):
        await proc.join()
    assert error_callback.call_args_list == [
        call(errors.UpsiesError('Error #0')),
        call(errors.UpsiesError('Error #1')),
    ]


@pytest.mark.asyncio
async def test_target_terminates_via_DaemonProcessTerminated():
    error_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_raising_DaemonProcessTerminated,
        error_callback=error_callback,
    )
    proc.start()
    await proc.join()
    assert error_callback.call_args_list == []


@pytest.mark.asyncio
async def test_target_sends_to_result_callback():
    result_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_sending_result_to_finished_callback,
        result_callback=result_callback,
    )
    proc.start()
    await proc.join()
    assert result_callback.call_args_list == [call('foo')]


@pytest.mark.parametrize(
    argnames='target',
    argvalues=(
        target_raising_exception,
        target_sending_to_init_callback,
        target_sending_to_info_callback,
        target_sending_to_error_callback,
        target_sending_result_to_finished_callback,
        target_never_terminating,
    ),
)
@pytest.mark.asyncio
async def test_target_calls_finished_callback(target):
    finished_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target,
        finished_callback=finished_callback,
    )
    proc.start()
    if target is target_never_terminating:
        proc.stop()
    try:
        await proc.join()
    except BaseException as e:
        print('ignoring exception:', repr(e))
    assert finished_callback.call_args_list == [call()]


@pytest.mark.asyncio
async def test_stop():
    info_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_never_terminating,
        info_callback=info_callback,
    )
    proc.start()
    await asyncio.sleep(1)
    assert not proc.is_finished
    proc.stop()
    await proc.join()
    assert info_callback.call_args_list == [call('something')]
    # This is ok and has no effect.
    proc.stop()


@pytest.mark.asyncio
async def test_send(tmp_path):
    logfile = tmp_path / 'log.txt'
    proc = daemon.DaemonProcess(
        target=target_logging_input_to_file,
        kwargs={
            'logfile': str(logfile),
        },
    )
    proc.send(daemon.MsgType.info, '!')
    proc.start()
    try:
        await asyncio.sleep(2)
        assert proc.is_alive
        proc.send(daemon.MsgType.info, 'foo')
        await asyncio.sleep(0.1)
        assert logfile.read_text() == '!\nfoo\n'
        proc.send(daemon.MsgType.error, 'foo')
        await asyncio.sleep(0.1)
        assert logfile.read_text() == '!\nfoo\n'
        proc.send(daemon.MsgType.info, 'bar')
        await asyncio.sleep(0.1)
        assert logfile.read_text() == '!\nfoo\nbar\n'
    finally:
        proc.stop()
        await proc.join()

    # send() after process finished is not allowed.
    with pytest.raises(RuntimeError, match='Cannot send to finished process: '):
        proc.send(daemon.MsgType.info, 'baz')

    assert logfile.read_text() == '!\nfoo\nbar\n'


@pytest.mark.asyncio
async def test_is_alive_property():
    proc = daemon.DaemonProcess(
        target=target_never_terminating,
    )
    assert proc.is_alive is False
    proc.start()
    assert proc.is_alive is True
    proc.stop()
    await proc.join()
    assert proc.is_alive is False


@pytest.mark.asyncio
async def test_is_finished_property():
    proc = daemon.DaemonProcess(
        target=target_never_terminating,
    )
    assert proc.is_finished is False
    proc.start()
    assert proc.is_finished is False
    proc.stop()
    assert proc.is_finished is False
    await proc.join()
    assert proc.is_finished is True


@pytest.mark.asyncio
async def test_init_callback_raises_exception():
    exception = RuntimeError('Boo!')
    init_callback = Mock(side_effect=exception)
    error_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_sending_to_init_callback,
        init_callback=init_callback,
        error_callback=error_callback,
    )
    proc.start()
    with pytest.raises(RuntimeError, match=r'^Boo!$'):
        await proc.join()
    assert init_callback.call_args_list == [call(0)]
    assert proc.exception is exception
    assert error_callback.call_args_list == [call(exception)]

@pytest.mark.asyncio
async def test_info_callback_raises_exception():
    exception = RuntimeError('Boo!')
    info_callback = Mock(side_effect=exception)
    error_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_sending_to_info_callback,
        info_callback=info_callback,
        error_callback=error_callback,
    )
    proc.start()
    with pytest.raises(RuntimeError, match=r'^Boo!$'):
        await proc.join()
    assert info_callback.call_args_list == [call(0)]
    assert proc.exception is exception
    assert error_callback.call_args_list == [call(exception)]

@pytest.mark.asyncio
async def test_error_callback_raises_exception():
    exception = RuntimeError('Boo!')
    error_callback = Mock(side_effect=exception)
    proc = daemon.DaemonProcess(
        target=target_sending_to_error_callback,
        error_callback=error_callback,
    )
    proc.start()
    with pytest.raises(RuntimeError, match=r'^Boo!$'):
        await proc.join()
    assert proc.exception is exception
    assert error_callback.call_args_list == [call(errors.UpsiesError('Error #0')), call(exception)]

@pytest.mark.asyncio
async def test_finished_callback_raises_exception():
    exception = RuntimeError('Boo!')
    finished_callback = Mock(side_effect=exception)
    error_callback = Mock()
    proc = daemon.DaemonProcess(
        target=target_sending_result_to_finished_callback,
        finished_callback=finished_callback,
        error_callback=error_callback,
    )
    proc.start()
    with pytest.raises(RuntimeError, match=r'^Boo!$'):
        await proc.join()
    assert finished_callback.call_args_list == [call()]
    assert proc.exception is exception
    assert error_callback.call_args_list == [call(proc.exception)]


@pytest.mark.parametrize(
    argnames='queued_items, exp_exception, exp_remaining_items',
    argvalues=(
        pytest.param(
            [
                (daemon.MsgType.info, 'foo'),
                (daemon.MsgType.info, 'bar'),
                (daemon.MsgType.info, 'baz'),
            ],
            None,
            [
                (daemon.MsgType.info, 'foo'),
                (daemon.MsgType.info, 'bar'),
                (daemon.MsgType.info, 'baz'),
            ],
            id='Termination sentinel is found',
        ),
        pytest.param(
            [
                (daemon.MsgType.info, 'foo'),
                (daemon.MsgType.info, 'bar'),
                (daemon.MsgType.terminate, 'Now!'),
                (daemon.MsgType.info, 'baz'),
            ],
            errors.DaemonProcessTerminated('Terminated'),
            [
                (daemon.MsgType.info, 'foo'),
                (daemon.MsgType.info, 'bar'),
                (daemon.MsgType.info, 'baz'),
            ],
            id='Termination sentinel is not found',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_maybe_terminate(queued_items, exp_exception, exp_remaining_items, mocker):
    input_queue = Mock()
    mocker.patch('upsies.utils.daemon.read_input_queue_until_empty', return_value=queued_items)
    if exp_exception:
        with pytest.raises(type(exp_exception)):
            daemon.maybe_terminate(input_queue)
    else:
        daemon.maybe_terminate(input_queue)

    assert input_queue.put.call_args_list == [
        call(item)
        for item in exp_remaining_items
    ]


@pytest.mark.parametrize(
    argnames='queued_items, default, exp_return_value, exp_remaining_items',
    argvalues=(
        pytest.param(
            [
                (daemon.MsgType.info, 'foo'),
                (daemon.MsgType.info, 'bar'),
                (daemon.MsgType.info, 'baz'),
            ],
            None,
            (daemon.MsgType.info, 'foo'),
            [
                (daemon.MsgType.info, 'bar'),
                (daemon.MsgType.info, 'baz'),
            ],
            id='No default value',
        ),
        pytest.param(
            [
                (daemon.MsgType.info, 'foo'),
                (daemon.MsgType.info, 'bar'),
                (daemon.MsgType.info, 'baz'),
            ],
            ('a', 'b'),
            (daemon.MsgType.info, 'foo'),
            [
                (daemon.MsgType.info, 'bar'),
                (daemon.MsgType.info, 'baz'),
            ],
            id='Only one queud item is consumed',
        ),
        pytest.param(
            [],
            ('a', 'b'),
            ('a', 'b'),
            [],
            id='Queue is empty',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_read_input_queue(queued_items, default, exp_return_value, exp_remaining_items, mocker):
    def get_nowait():
        if queued_items:
            return queued_items.pop(0)
        else:
            raise queue.Empty()

    input_queue = Mock(get_nowait=Mock(side_effect=get_nowait))
    if default is None:
        return_value = daemon.read_input_queue(input_queue)
    else:
        return_value = daemon.read_input_queue(input_queue, default)
    assert return_value == exp_return_value

    remaining_items = []
    while True:
        try:
            remaining_items.append(get_nowait())
        except queue.Empty:
            break
    assert remaining_items == exp_remaining_items


@pytest.mark.parametrize(
    argnames='queued_items_batches, key, exp_result, exp_remaining_items',
    argvalues=(
        pytest.param(
            (
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz')],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': 'Bar.'}), (daemon.MsgType.result, 'baz')],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': 'BAR!'}), (daemon.MsgType.result, 'baz')],
            ),
            'bar',
            'Bar.',
            [
                (daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz'),
                (daemon.MsgType.init, 'foo'), (daemon.MsgType.result, 'baz'),
            ],
            id='Matching key is found',
        ),
        pytest.param(
            (
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz')],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': None}), (daemon.MsgType.result, 'baz')],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': 'BAR!'}), (daemon.MsgType.result, 'baz')],
            ),
            'bar',
            None,
            [
                (daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz'),
                (daemon.MsgType.init, 'foo'), (daemon.MsgType.result, 'baz'),
            ],
            id='Matching value is None',
        ),
        pytest.param(
            (
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz')],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': 'Bar.'}), (daemon.MsgType.info, {'bar': 'Barbar.'}), (daemon.MsgType.result, 'baz')],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': 'BAR!'}), (daemon.MsgType.result, 'baz')],
            ),
            'bar',
            'Bar.',
            [
                (daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz'),
                (daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': 'Barbar.'}), (daemon.MsgType.result, 'baz'),
            ],
            id='Multiple matching keys in single batch are found',
        ),
        pytest.param(
            (
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz')],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.terminate, 'Now'), (daemon.MsgType.info, {'bar': 'Bar.'})],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': 'BAR!'}), (daemon.MsgType.result, 'baz')],
            ),
            'bar',
            errors.DaemonProcessTerminated('Terminated'),
            [
                (daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz'),
                (daemon.MsgType.init, 'foo'),
            ],
            id='Terminator is found before key',
        ),
        pytest.param(
            (
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz')],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': 'Bar.'}), (daemon.MsgType.terminate, 'Now')],
                [(daemon.MsgType.init, 'foo'), (daemon.MsgType.info, {'bar': 'BAR!'}), (daemon.MsgType.result, 'baz')],
            ),
            'bar',
            errors.DaemonProcessTerminated('Terminated'),
            [
                (daemon.MsgType.init, 'foo'), (daemon.MsgType.error, 'bar'), (daemon.MsgType.result, 'baz'),
                (daemon.MsgType.init, 'foo'),
            ],
            id='Terminator is found after key',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_read_input_queue_key(queued_items_batches, key, exp_result, exp_remaining_items, mocker):
    input_queue = Mock()
    mocker.patch('upsies.utils.daemon.read_input_queue_until_empty', side_effect=queued_items_batches)

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            daemon.read_input_queue_key(input_queue, key)
    else:
        return_value = daemon.read_input_queue_key(input_queue, key)
        assert return_value == exp_result

    assert input_queue.put.call_args_list == [
        call(item)
        for item in exp_remaining_items
    ]


@pytest.mark.parametrize(
    argnames='queued_items, exp_result, exp_remaining_items',
    argvalues=(
        pytest.param(
            (
                (daemon.MsgType.init, 'foo'),
                (daemon.MsgType.error, 'bar'),
                (daemon.MsgType.info, 'baz'),
                (daemon.MsgType.info, 'yip'),
                (daemon.MsgType.result, 'yap'),
                queue.Empty(),
            ),
            (
                (daemon.MsgType.init, 'foo'),
                (daemon.MsgType.error, 'bar'),
                (daemon.MsgType.info, 'baz'),
                (daemon.MsgType.info, 'yip'),
                (daemon.MsgType.result, 'yap'),
            ),
            [],
            id='Terminator is not found',
        ),
        pytest.param(
            (
                (daemon.MsgType.init, 'foo'),
                (daemon.MsgType.error, 'bar'),
                (daemon.MsgType.info, 'baz'),
                (daemon.MsgType.terminate, 'now'),
                (daemon.MsgType.info, 'yip'),
                (daemon.MsgType.result, 'yap'),
                queue.Empty(),
            ),
            errors.DaemonProcessTerminated('Terminated'),
            [
                (daemon.MsgType.init, 'foo'),
                (daemon.MsgType.error, 'bar'),
                (daemon.MsgType.info, 'baz'),
            ],
            id='Terminator is found',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_read_input_queue_until_empty(queued_items, exp_result, exp_remaining_items, mocker):
    input_queue = Mock(
        get=Mock(side_effect=queued_items),
    )

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            daemon.read_input_queue_until_empty(input_queue)
    else:
        return_value = daemon.read_input_queue_until_empty(input_queue)
        assert return_value == exp_result

    assert input_queue.put.call_args_list == [
        call(item)
        for item in exp_remaining_items
    ]
