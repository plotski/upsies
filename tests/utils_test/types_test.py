import collections
import copy
import itertools
import pickle
import re

import pytest

from upsies.utils import imghosts, types


@pytest.mark.parametrize(
    argnames='value, exp_int',
    argvalues=(
        ('1', 1), ('-1', -1),
        ('0.1', 0), ('-0.1', 0),
        ('0.9', 0), ('-0.9', 0),
        ('100', 100), (100, 100), (100.0, 100),
    ),
)
def test_Integer_valid_values(value, exp_int):
    i = types.Integer(value)
    assert i == exp_int

@pytest.mark.parametrize(
    argnames='value',
    argvalues=('zero', 'foo'),
)
def test_Integer_invalid_values(value):
    with pytest.raises(ValueError, match=rf'^Invalid integer value: {value!r}$'):
        types.Integer(value)

def test_Integer_min_max_values():
    min = 0
    max = 10

    with pytest.raises(ValueError, match=rf'^Minimum is {min}$'):
        types.Integer(min - 1, min=min, max=max)
    with pytest.raises(ValueError, match=rf'^Maximum is {max}$'):
        types.Integer(max + 1, min=min, max=max)

    for value in range(min, max + 1):
        i = types.Integer(value, min=min, max=max)
        assert i == value

    i = types.Integer(min, min=min, max=max)
    assert i.min == min
    assert i.max == max

    with pytest.raises(ValueError, match=rf'^Minimum is {min}$'):
        type(i)(min - 1)
    with pytest.raises(ValueError, match=rf'^Maximum is {max}$'):
        type(i)(max + 1)

@pytest.mark.parametrize(
    argnames='value, min, max, exp_repr',
    argvalues=(
        (5, 0, 10, 'Integer(5, min=0, max=10)'),
        (5, 0, None, 'Integer(5, min=0)'),
        (5, None, 10, 'Integer(5, max=10)'),
    ),
)
def test_Integer_repr(value, min, max, exp_repr):
    i = types.Integer(value, min=min, max=max)
    assert repr(i) == exp_repr

def test_Integer_str():
    i = types.Integer(3, min=0, max=10)
    assert str(i) == '3'


@pytest.mark.parametrize(
    argnames=(
        'options1, empty_ok1, case_sensitive1,'
        'options2, empty_ok2, case_sensitive2,'
        'exp_same_class'
    ),
    argvalues=(
        (
            (1, 2, 3), False, False,
            (1, 2, 3), False, False,
            True,
        ),
        (
            (1, 2, 3), False, False,
            (1, 2, 3), False, True,
            False,
        ),
        (
            (1, 2, 3), False, False,
            (1, 2, 3), True, False,
            False,
        ),
        (
            (1, 2, 3), False, False,
            (1, 2, 3, 4), False, False,
            False,
        ),
        (
            (1, 2, 3), False, False,
            (1, 2), False, False,
            False,
        ),
        (
            (1, 2, 3), False, False,
            ('1', '2', '3'), False, False,
            False,
        ),
    ),
)
def test_Choice_returns_cached_class(options1, empty_ok1, case_sensitive1, options2, empty_ok2, case_sensitive2, exp_same_class):
    MyChoice1 = types.Choice(options=options1, empty_ok=empty_ok1, case_sensitive=case_sensitive1)
    MyChoice2 = types.Choice(options=options2, empty_ok=empty_ok2, case_sensitive=case_sensitive2)
    assert bool(MyChoice1 is MyChoice2) is exp_same_class

@pytest.mark.parametrize(
    argnames='value, options',
    argvalues=(
        ('1', (1, 2, 3, 4)),
        (2, (1, 2, 3, 4)),
        (3, ('1', '2', '3', '4')),
        ('4', ('1', '2', '3', '4')),
    ),
)
def test_Choice_valid_values(value, options):
    MyChoice = types.Choice(options)
    choice = MyChoice(value)
    assert choice == str(choice)
    assert isinstance(choice, str)

@pytest.mark.parametrize(
    argnames='value, options, exp_error',
    argvalues=(
        (0, (1, 2, 3, 4), 'Not one of 1, 2, 3, 4: 0'),
        ('foo', (1, 2, 3, 4), 'Not one of 1, 2, 3, 4: foo'),
    ),
)
def test_Choice_invalid_values(value, options, exp_error):
    MyChoice = types.Choice(options)
    with pytest.raises(ValueError, match=rf'^{re.escape(exp_error)}$'):
        MyChoice(value)

@pytest.mark.parametrize(
    argnames='empty_ok, error_expected',
    argvalues=(
        (False, True),
        (True, False),
    ),
)
def test_Choice_empty_ok(empty_ok, error_expected):
    MyChoice = types.Choice(options=('foo', 'bar'), empty_ok=empty_ok)
    if error_expected:
        with pytest.raises(ValueError, match=r'^Not one of bar, foo: $'):
            MyChoice('')
    else:
        choice = MyChoice('')
        assert choice == ''

@pytest.mark.parametrize(
    argnames='case_sensitive, value, options, error_expected',
    argvalues=(
        (False, 'foo', ('foo', 'bar', 'baz'), False),
        (True, 'foo', ('foo', 'bar', 'baz'), False),
        (False, 'Foo', ('foo', 'bar', 'baz'), False),
        (True, 'Foo', ('foo', 'bar', 'baz'), True),
        (False, 'foo', ('Foo', 'Bar', 'Baz'), False),
        (True, 'foo', ('Foo', 'Bar', 'Baz'), True),
        (False, 'Foo', ('Foo', 'Bar', 'Baz'), False),
        (True, 'Foo', ('Foo', 'Bar', 'Baz'), False),
    ),
)
def test_Choice_case_sensitive(case_sensitive, value, options, error_expected):
    MyChoice = types.Choice(options=options, case_sensitive=case_sensitive)
    if error_expected:
        options_str = ', '.join(sorted(options))
        with pytest.raises(ValueError, match=rf'^Not one of {options_str}: {value}$'):
            MyChoice(value)
    else:
        choice = MyChoice(value)
        assert choice == value

def test_Choice_repr():
    MyChoice = types.Choice(options=('foo', 'bar', 'baz'))
    choice = MyChoice('bar')
    assert repr(choice) == "Choice('bar', options=('bar', 'baz', 'foo'))"


@pytest.mark.parametrize('valid_image_host_name', imghosts.imghost_names())
@pytest.mark.parametrize(
    argnames='mutilator',
    argvalues=(
        None,
        str.upper,
        str.capitalize,
    ),
)
def test_ImageHost_valid_values(valid_image_host_name, mutilator):
    mutilated_name = (
        mutilator(valid_image_host_name)
        if mutilator else
        valid_image_host_name
    )
    cls = types.ImageHost()
    name = cls(mutilated_name)
    assert name == mutilated_name
    assert isinstance(name, cls)
    assert isinstance(name, str)

def test_ImageHost_invalid_value():
    valid_names = ', '.join(imghosts.imghost_names())
    with pytest.raises(ValueError, match=rf'^Not one of {valid_names}: foo$'):
        types.ImageHost()('foo')

@pytest.mark.parametrize('is_running_in_development_environment', (True, False), ids=('developing', 'not developing'))
@pytest.mark.parametrize(
    argnames='imghost_names, allowed, disallowed, exp_options',
    argvalues=(
        pytest.param(
            ('foo', 'bar', 'baz'),
            None,
            None,
            ('foo', 'bar', 'baz'),
            id='default allowed, default disallowed',
        ),
        pytest.param(
            ('foo', 'bar', 'baz'),
            ('foo', 'bar'),
            None,
            ('foo', 'bar'),
            id='some allowed, default disallowed',
        ),
        pytest.param(
            ('foo', 'bar', 'baz'),
            None,
            ('bar', 'baz'),
            ('foo',),
            id='default allowed, some disallowed',
        ),
        pytest.param(
            ('foo', 'bar', 'baz'),
            ('foo', 'bar'),
            ('baz',),
            ('foo', 'bar'),
            id='some allowed, some disallowed',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_ImageHost_allowed_and_disallowed(
        imghost_names, allowed, disallowed, is_running_in_development_environment,
        exp_options,
        mocker,
):
    mocker.patch('upsies.utils.imghosts.imghost_names', return_value=imghost_names)
    mocker.patch('upsies.utils.is_running_in_development_environment', return_value=is_running_in_development_environment)
    cls = types.ImageHost(allowed=allowed, disallowed=disallowed)
    if is_running_in_development_environment:
        exp_options = set(exp_options)
    exp_options = sorted(exp_options)
    with pytest.raises(ValueError, match=rf'^Not one of {", ".join(exp_options)}: asdf$'):
        cls('asdf')


@pytest.mark.parametrize(
    argnames='string, exp_bool',
    argvalues=(
        ('true', True), ('false', False),
        ('yes', True), ('no', False),
        ('1', True), ('0', False),
        ('on', True), ('off', False),
        ('aye', True), ('nay', False),
    ),
)
def test_Bool_valid_values(string, exp_bool):
    bool = types.Bool(string)
    if exp_bool:
        assert bool
    else:
        assert not bool
    assert str(bool) == string

@pytest.mark.parametrize(
    argnames='string',
    argvalues=('da', 'nyet'),
)
def test_Bool_invalid_values(string):
    with pytest.raises(ValueError, match=rf'^Invalid boolean value: {string!r}$'):
        types.Bool(string)

@pytest.mark.parametrize(
    argnames='a, b',
    argvalues=(
        (types.Bool('yes'), types.Bool('true')),
        (types.Bool('yes'), True),
        (types.Bool('yes'), 'yes'),
        (types.Bool('no'), 'no'),
    ),
)
def test_Bool_equality(a, b):
    assert a == b
    assert b == a

@pytest.mark.parametrize(
    argnames='a, b',
    argvalues=(
        (types.Bool('yes'), types.Bool('false')),
        (types.Bool('no'), types.Bool('true')),
        (types.Bool('yes'), False),
        (types.Bool('no'), True),
        (types.Bool('yes'), 'no'),
        (types.Bool('no'), 'yes'),
        (types.Bool('yes'), 0),
        (types.Bool('no'), 0),
        (types.Bool('yes'), ''),
        (types.Bool('no'), ''),
    ),
)
def test_Bool_inequality(a, b):
    assert a != b
    assert b != a

def test_Bool_attributes():
    assert types.Bool.truthy == ('true', 'yes', '1', 'on', 'aye')
    assert types.Bool.falsy == ('false', 'no', '0', 'off', 'nay')

@pytest.mark.parametrize(
    argnames='value, exp_repr',
    argvalues=(
        tuple(
            itertools.chain(
                ((value, f'Bool({value!r})') for value in types.Bool.truthy),
                ((value, f'Bool({value!r})') for value in types.Bool.falsy),
                ((value.title(), f'Bool({value.title()!r})') for value in types.Bool.truthy),
                ((value.title(), f'Bool({value.title()!r})') for value in types.Bool.falsy),
            ),
        )
    ),
)
def test_Bool___repr__(value, exp_repr):
    assert repr(types.Bool(value)) == exp_repr


@pytest.mark.parametrize('number', ('0', '1', '10', '11.5', '11.05', '99', '9999'))
@pytest.mark.parametrize('space', ('', ' '))
@pytest.mark.parametrize(
    argnames='prefix, multiplier',
    argvalues=(
        ('', 1),
        ('k', 1000),
        ('M', 1000**2),
        ('G', 1000**3),
        ('T', 1000**4),
        ('P', 1000**5),
        ('Ki', 1024),
        ('Mi', 1024**2),
        ('Gi', 1024**3),
        ('Ti', 1024**4),
        ('Pi', 1024**5),
    ),
)
@pytest.mark.parametrize('unit', ('', 'B'))
def test_Bytes_from_valid_string(number, space, prefix, multiplier, unit):
    bytes = types.Bytes(f'{number}{space}{prefix}{unit}')
    assert bytes == int(float(number) * multiplier)

@pytest.mark.parametrize(
    argnames='string, exp_msg',
    argvalues=(
        ('foo', 'Invalid size: foo'),
        ('10x', 'Invalid unit: x'),
        ('10kx', 'Invalid unit: kx'),
        ('10 Mx', 'Invalid unit: Mx'),
    ),
)
def test_Bytes_from_invalid_string(string, exp_msg):
    with pytest.raises(ValueError, match=rf'^{exp_msg}$'):
        types.Bytes(string)

@pytest.mark.parametrize(
    argnames='number, exp_string',
    argvalues=(
        (0, '0 B'),
        (1, '1 B'),
        (10, '10 B'),
        (999, '999 B'),
        (1000, '1 kB'),
        (1001, '1 kB'),
        (1000 * 1.5, '1.5 kB'),
        (1000 * 1.75, '1.75 kB'),
        (1000 * 1.799, '1.8 kB'),
        (1023, '1023 B'),
        (1024, '1 KiB'),
        (1025, '1 KiB'),
        (1024 * 1.5, '1.5 KiB'),
        (1024 * 1.75, '1.79 kB'),
        (1024 * 1.799, '1.8 KiB'),
        (1000**2, '1 MB'), (1000**3, '1 GB'), (1000**4, '1 TB'), (1000**5, '1 PB'),
        (1024**2, '1 MiB'), (1024**3, '1 GiB'), (1024**4, '1 TiB'), (1024**5, '1 PiB'),
    ),
)
def test_Bytes_as_string(number, exp_string):
    string = str(types.Bytes(number))
    assert string == exp_string

@pytest.mark.parametrize(
    argnames='number, decimal_places, prefix, trailing_zeros, exp_result',
    argvalues=(
        (0, 0, 'binary', True, '0 B'),
        (0, 0, 'decimal', False, '0 B'),
        (0, 1, 'binary', False, '0 B'),
        (0, 1, 'decimal', True, '0.0 B'),

        (1, 0, 'binary', True, '1 B'),
        (1, 0, 'decimal', False, '1 B'),
        (1, 3, 'binary', False, '1 B'),
        (1, 3, 'decimal', True, '1.000 B'),

        (999, 3, 'decimal', True, '999.000 B'),
        (1001, 3, 'decimal', True, '1.001 kB'),
        (1001, 4, 'decimal', True, '1.0010 kB'),
        (1001, 4, 'decimal', False, '1.001 kB'),

        (999, 3, 'binary', False, '999 B'),
        (1000, 2, 'binary', False, '1000 B'),
        (1000, 2, 'binary', True, '1000.00 B'),
        (1001, 1, 'binary', True, '1001.0 B'),

        (1024, 4, 'decimal', True, '1.0240 kB'),
        (1024, 3, 'decimal', True, '1.024 kB'),
        (1024, 2, 'decimal', True, '1.02 kB'),
        (1024, 1, 'decimal', True, '1.0 kB'),
        (1024, 1, 'decimal', False, '1 kB'),
        (1024, 0, 'decimal', True, '1 kB'),

        (1023, 0, 'binary', True, '1023 B'),
        (1023, 1, 'binary', True, '1023.0 B'),
        (1023, 1, 'binary', False, '1023 B'),

        (1024, 1, 'binary', True, '1.0 KiB'),
        (1024, 1, 'binary', False, '1 KiB'),
        (1024, 0, 'binary', True, '1 KiB'),

        (1025, 5, 'binary', True, '1.00098 KiB'),
        (1025, 4, 'binary', True, '1.0010 KiB'),
        (1025, 4, 'binary', False, '1.001 KiB'),
        (1025, 3, 'binary', True, '1.001 KiB'),
        (1025, 2, 'binary', True, '1.00 KiB'),
        (1025, 2, 'binary', False, '1 KiB'),

        (4000, 2, 'shortest', False, '4 kB'),
        (4096, 2, 'shortest', False, '4 KiB'),

        (100 * 1e6, 0, 'decimal', False, '100 MB'),
        (100, 3, 'foo', False, ValueError("Invalid prefix: 'foo'")),
    ),
)
def test_Bytes_format(number, prefix, decimal_places, trailing_zeros, exp_result):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            types.Bytes(number).format(
                prefix=prefix,
                decimal_places=decimal_places,
                trailing_zeros=trailing_zeros,
            )
    else:
        string = types.Bytes(number).format(
            prefix=prefix,
            decimal_places=decimal_places,
            trailing_zeros=trailing_zeros,
        )
        assert string == exp_result

def test_Bytes_repr():
    b = types.Bytes(123)
    assert repr(b) == 'Bytes(123)'


@pytest.mark.parametrize(
    argnames='value, exp_num, exp_str, exp_repr, exp_exc',
    argvalues=(
        ('01:23:45', 5025, '1:23:45', "Timestamp('1:23:45')", None),
        ((), None, None, None, TypeError('Not a string or number: ()')),
        (-1, None, None, None, ValueError('Timestamp must not be negative: -1')),
        (0, 0.0, '0:00:00', "Timestamp('0:00:00')", None),
        (1.0, 1.0, '0:00:01', "Timestamp('0:00:01')", None),
        (59.4, 59.4, '0:00:59.4', "Timestamp('0:00:59.4')", None),
        (60.56789, 60.568,'0:01:00.568', "Timestamp('0:01:00.568')", None),
        (61, 61,'0:01:01', "Timestamp('0:01:01')", None),
        (60 * 60 - 1, 3599,'0:59:59', "Timestamp('0:59:59')", None),
        (60 * 60, 3600, '1:00:00', "Timestamp('1:00:00')", None),
        (10 * 60 * 60 + 1, 36001, '10:00:01', "Timestamp('10:00:01')", None),
        (123 * 60 * 60 + 321, 443121, '123:05:21', "Timestamp('123:05:21')", None),
    ),
    ids=lambda v: repr(v),
)
def test_Timestamp(value, exp_num, exp_str, exp_repr, exp_exc):
    if exp_exc:
        with pytest.raises(type(exp_exc), match=rf'^{re.escape(str(exp_exc))}$'):
            types.Timestamp(value)
    else:
        timestamp = types.Timestamp(value)
        assert isinstance(timestamp, float), type(timestamp).__mro__
        assert float(timestamp) == exp_num
        assert str(timestamp) == exp_str
        assert repr(timestamp) == exp_repr


@pytest.mark.parametrize(
    argnames='value, exp_float, exp_exc',
    argvalues=(
        ('', None, ValueError("Invalid timestamp: ''")),
        (':', None, ValueError("Invalid timestamp: ':'")),
        ('0:1:2:3', None, ValueError('Invalid timestamp: 0:1:2:3')),
        ('foo', None, ValueError("Invalid timestamp: 'foo'")),
        ('1:00f', None, ValueError("Invalid timestamp: '1:00f'")),
        ('-1', None, ValueError('Timestamp must not be negative: -1')),
        ('-100', None, ValueError('Timestamp must not be negative: -100')),
        ('1:-2:3', None, ValueError('Timestamp must not be negative: 1:-2:3')),
        ('0', 0, None),
        ('59', 59, None),
        ('60', 60, None),
        ('123.2', 123.2, None),
        ('1:00', 60, None),
        ('1:01', 61, None),
        ('1:02:03', 3600 + (2 * 60) + 3, None),
        ('1:02:03.456', 3600 + (2 * 60) + 3 + 0.456, None),
        ('1:2:3', 3600 + (2 * 60) + 3, None),
        ('123:04:55', (123 * 3600) + (4 * 60) + 55, None),
    ),
    ids=lambda v: repr(v),
)
def test_Timestamp_from_string(value, exp_float, exp_exc):
    if exp_exc:
        with pytest.raises(type(exp_exc), match=rf'^{re.escape(str(exp_exc))}$'):
            types.Timestamp.from_string(value)
    else:
        timestamp = types.Timestamp.from_string(value)
        assert isinstance(timestamp, types.Timestamp), type(timestamp).__mro__
        assert float(timestamp) == exp_float


@pytest.mark.parametrize(
    argnames=('name', 'bool_value'),
    argvalues=(
        ('movie', True),
        ('series', True),
        ('season', True),
        ('episode', True),
        ('unknown', False),
    ),
)
def test_ReleaseType_truthiness(name, bool_value):
    assert bool(getattr(types.ReleaseType, name)) is bool_value

@pytest.mark.parametrize(
    argnames=('name', 'exp_str'),
    argvalues=(
        ('movie', 'movie'),
        ('season', 'season'),
        ('series', 'season'),
        ('episode', 'episode'),
        ('unknown', 'unknown'),
    ),
)
def test_ReleaseType_string(name, exp_str):
    assert str(getattr(types.ReleaseType, name)) == exp_str

@pytest.mark.parametrize(
    argnames=('name', 'exp_repr'),
    argvalues=(
        ('movie', 'ReleaseType.movie'),
        ('season', 'ReleaseType.season'),
        ('series', 'ReleaseType.season'),
        ('episode', 'ReleaseType.episode'),
        ('unknown', 'ReleaseType.unknown'),
    ),
)
def test_ReleaseType_repr(name, exp_repr):
    assert repr(getattr(types.ReleaseType, name)) == exp_repr


@pytest.mark.parametrize(
    argnames=('name', 'bool_value'),
    argvalues=(
        ('true', True),
        ('false', False),
        ('renamed', False),
        ('altered', False),
        ('unknown', False),
    ),
)
def test_SceneCheckResult_truthiness(name, bool_value):
    assert bool(getattr(types.SceneCheckResult, name)) is bool_value

@pytest.mark.parametrize(
    argnames=('name', 'exp_str'),
    argvalues=(
        ('true', 'true'),
        ('false', 'false'),
        ('renamed', 'renamed'),
        ('altered', 'altered'),
        ('unknown', 'unknown'),
    ),
)
def test_SceneCheckResult_string(name, exp_str):
    assert str(getattr(types.SceneCheckResult, name)) == exp_str

@pytest.mark.parametrize(
    argnames=('name', 'exp_repr'),
    argvalues=(
        ('true', 'SceneCheckResult.true'),
        ('false', 'SceneCheckResult.false'),
        ('renamed', 'SceneCheckResult.renamed'),
        ('altered', 'SceneCheckResult.altered'),
        ('unknown', 'SceneCheckResult.unknown'),
    ),
)
def test_SceneCheckResult_repr(name, exp_repr):
    assert repr(getattr(types.SceneCheckResult, name)) == exp_repr


@pytest.mark.parametrize(
    argnames=('pattern, exp_result, string, exp_groups'),
    argvalues=(
        (r'(.*) foo(.*)baz$', re.compile(r'(.*) foo(.*)baz$'), 'hey: foo bar baz', ('hey:', ' bar ')),
        (types.RegEx(r'(.*) foo(.*)baz$'), re.compile(r'(.*) foo(.*)baz$'), 'hey: foo bar baz', ('hey:', ' bar ')),
        (r'*foo\.bar$', ValueError(r'*foo\.bar$: Nothing to repeat at position 0'), None, None),
    ),
)
def test_RegEx(pattern, exp_result, string, exp_groups):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            types.RegEx(pattern)
    else:
        # Works like re.Pattern
        regex = types.RegEx(pattern)
        match = regex.search(string)
        assert match.groups() == exp_groups
        assert regex.pattern == exp_result.pattern

        # Better string representations
        assert str(regex) == exp_result.pattern
        assert repr(regex) == f'RegEx({exp_result.pattern!r})'

        # Equality and hashability
        assert regex == types.RegEx(exp_result.pattern)
        assert hash(regex) == hash(types.RegEx(exp_result.pattern))

        # Picklability
        pickled = pickle.dumps(regex)
        assert pickle.loads(pickled) == regex

        # copy.deepcopy()
        copied = copy.deepcopy(regex)
        assert repr(copied) == repr(regex)
        assert id(copied) != id(regex)


@pytest.mark.parametrize(
    argnames=('item_type, values, separator, exp_result, exp___doc__'),
    argvalues=(
        # Multiple values
        (int, ['10', '20', '45'], None, [10, 20, 45], 'Immutable list of int'),
        (int, None, None, ValueError('Invalid value: None'), 'Immutable list of int'),
        (int, ['10', 'foo', '45'], None, ValueError('Invalid value: foo'), 'Immutable list of int'),
        # Single value
        (int, 123, None, [123], 'Immutable list of int'),
        (int, ..., None, ValueError('Invalid value: Ellipsis'), 'Immutable list of int'),
        # Separator
        (str, 'foo,bar', None, ['foo,bar'], 'Immutable list of str'),
        (str, 'foo,bar', ',', ['foo', 'bar'], 'Immutable list of str'),
        (str, 'foo,,bar,baz,,', ',', ['foo', 'bar', 'baz'], 'Immutable list of str'),
        # Special case for re.Pattern because its __str__() behaves like __repr__()
        (types.RegEx, ['(.)'], None, [types.RegEx('(.)')], 'Immutable list of RegEx'),
        (types.RegEx, ['(.'], None, ValueError('Invalid value: (.'), 'Immutable list of RegEx'),
    ),
    ids=lambda v: repr(v),
)
def test_ListOf(item_type, values, separator, exp_result, exp___doc__):
    args = [item_type]
    kwargs = {}
    if separator is not None:
        kwargs['separator'] = separator

    list_of = types.ListOf(*args, **kwargs)

    # Return value is cached and returned if called with the same parameters.
    assert list_of is types.ListOf(*args, **kwargs)
    assert list_of is not types.ListOf(*args, separator='/')

    # __doc__
    assert list_of.__doc__ == exp___doc__

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            list_of(values)

    else:
        items = list_of(values)
        assert items == exp_result
        assert items == list_of(exp_result)
        assert isinstance(items, collections.abc.Sequence)
        assert not isinstance(items, collections.abc.MutableSequence)

        # __geitem__()
        for i, exp_item in enumerate(exp_result):
            assert items[i] == exp_item

        # __len__()
        assert len(items) == len(exp_result)

        # __eq__()
        assert items == exp_result
        assert items == list(exp_result)
        assert items == tuple(exp_result)
        unexp_result = [*exp_result, 'foo']
        if items:
            assert items != items[0:0]
        assert items != list(unexp_result)
        assert items != tuple(unexp_result)
        assert items != 123
        assert items != 'hello'

        # __hash__()
        class OtherType(str):
            pass
        assert hash(items) == hash(types.ListOf(items.item_type, separator=separator)(exp_result))
        assert hash(items) == hash(types.ListOf(items.item_type, separator='!')(exp_result))
        assert hash(items) != hash(types.ListOf(OtherType, separator=separator)(exp_result))

        # __str__()
        assert str(items) == ', '.join(
            (
                item.pattern
                if isinstance(item, re.Pattern) else
                str(item)
            )
            for item in exp_result
        )

        # __repr__()
        assert repr(items) == (
            type(items).__name__
            + '('
            + repr(tuple(exp_result))
            + ')'
        )

        # __name__
        assert type(items).__name__ == (
            'ListOf'
            + item_type.__name__[0].upper()
            + item_type.__name__[1:]
        )
        assert type(items).__qualname__ == type(items).__name__


@pytest.mark.parametrize(
    argnames=('string, exp_result'),
    argvalues=(
        ('foo -> bar', ('foo', 'bar')),
        ('foo->bar', ('foo', 'bar')),
        ('/foo/bar/baz->asdf', ('/foo/bar/baz', 'asdf')),
        ('/foo/bar/baz/ -> /bar/baz/foo', ('/foo/bar/baz', '/bar/baz/foo')),
        ('/foo/bar/baz -> /bar/baz/foo/', ('/foo/bar/baz', '/bar/baz/foo')),
        ('  foo  ->   bar  ', ('foo', 'bar')),
        ('-> bar', ValueError('Invalid path translation: -> bar')),
    ),
)
def test_PathTranslation(string, exp_result):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            types.PathTranslation(string)
    else:
        translation = types.PathTranslation(string)
        assert (translation.local, translation.remote) == exp_result

        # __str__()
        assert str(translation) == f'{translation.local} -> {translation.remote}'


@pytest.mark.parametrize(
    argnames=('path, translations, exp_result'),
    argvalues=(
        (
            '/foo/bar/baz',
            types.PathTranslations(),
            '/foo/bar/baz',
        ),
        (
            '/foo/bar/baz',
            types.PathTranslations((
                '/foo/bar/baz -> asdf',
                'this/that -> fdsa',
            )),
            'asdf',
        ),
        (
            '/foo/bar/baz',
            types.PathTranslations((
                'this/that -> fdsa',
                '/foo/bar/baz/bam -> asdf',
            )),
            '/foo/bar/baz',
        ),
        (
            '/foo/bar/baz',
            types.PathTranslations((
                'this/that -> fdsa',
                'oo/ba -> asdf',
            )),
            '/fasdfr/baz',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_PathTranslations(path, translations, exp_result):
    assert translations.translate(path) == exp_result

    # Test if an instance can be instantiated with itself.
    translations2 = types.PathTranslations(translations)
    assert translations2.translate(path) == exp_result
