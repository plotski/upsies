import re
from unittest.mock import Mock, call

import pytest

from upsies.utils.release import _translated_property


class MyThing:
    def __init__(
            self, *,
            not_translated, translated_string, translated_sequence,
            string_translated_by_callable, sequence_translated_by_callable,
            invalid_replacement='not accessible',
    ):
        self._not_translated = not_translated
        self._translated_string = translated_string
        self._translated_sequence = translated_sequence
        self._string_translated_by_callable = string_translated_by_callable
        self._sequence_translated_by_callable = sequence_translated_by_callable
        self._invalid_replacement = invalid_replacement

        def translate_string(value, obj):
            assert value == f'{self._string_translated_by_callable} via property'
            assert obj is self
            return f'{value} via translate_string()'

        exp_sequence_translated_by_callable = [*sequence_translated_by_callable, 'via', 'property']

        def translate_sequence(value, obj):
            assert obj is self
            assert value == exp_sequence_translated_by_callable.pop(0)
            return value.upper() + ' via translate_sequence()'

        self._translate = {
            'translated_string': {
                re.compile('(?i:translated_string)'): 'BAR',
            },
            'translated_sequence': {
                re.compile(r'(?i:r)'): 'R',
            },
            'string_translated_by_callable': Mock(name='translate_string', side_effect=translate_string),
            'sequence_translated_by_callable': Mock(name='translate_sequence', side_effect=translate_sequence),
            'invalid_replacement': ('invalid', 'replacement'),
        }

    @_translated_property
    def not_translated(self):
        """This is not_translated"""
        return self._get_value('_not_translated')

    @_translated_property
    def translated_string(self):
        """This is translated_string"""
        return self._get_value('_translated_string')

    @translated_string.setter
    def translated_string(self, value):
        self._translated_string = str(value)

    @_translated_property
    def translated_sequence(self):
        """This is translated_sequence"""
        return self._get_value('_translated_sequence')

    @_translated_property
    def string_translated_by_callable(self):
        """This is string_translated_by_callable"""
        return self._get_value('_string_translated_by_callable')

    @_translated_property
    def sequence_translated_by_callable(self):
        """This is sequence_translated_by_callable"""
        return self._get_value('_sequence_translated_by_callable')

    @_translated_property
    def invalid_replacement(self):
        """This is invalid_replacement"""
        return f'{self._invalid_replacement} via property'

    @_translated_property
    def settable_property(self):
        """This is settable_property"""
        return getattr(self, '_settable_property', 'not set')

    @settable_property.setter
    def settable_property(self, value):
        self._settable_property = value

    def _get_value(self, name):
        value = getattr(self, name)
        if isinstance(value, str):
            return f'{value} via property'
        elif isinstance(value, tuple):
            return (*value, 'via', 'property')
        else:
            return value


@pytest.fixture
def my_thing():
    return MyThing(
        not_translated='Foo.',
        translated_sequence=('B', 'a', 'r', '.'),
        translated_string='Baz.',
        string_translated_by_callable='Asdf.',
        sequence_translated_by_callable=('H', 'j', 'k', 'l', '.'),
    )


def test___init__():
    assert MyThing.not_translated.__doc__ == 'This is not_translated'
    assert MyThing.translated_string.__doc__ == 'This is translated_string'
    assert MyThing.translated_sequence.__doc__ == 'This is translated_sequence'
    assert MyThing.string_translated_by_callable.__doc__ == 'This is string_translated_by_callable'
    assert MyThing.sequence_translated_by_callable.__doc__ == 'This is sequence_translated_by_callable'
    assert MyThing.invalid_replacement.__doc__ == 'This is invalid_replacement'

    assert MyThing.not_translated._property_name == 'not_translated'
    assert MyThing.translated_string._property_name == 'translated_string'
    assert MyThing.translated_sequence._property_name == 'translated_sequence'
    assert MyThing.string_translated_by_callable._property_name == 'string_translated_by_callable'
    assert MyThing.sequence_translated_by_callable._property_name == 'sequence_translated_by_callable'
    assert MyThing.invalid_replacement._property_name == 'invalid_replacement'


def test_property_is_not_translated(my_thing):
    assert my_thing.not_translated == 'Foo. via property'


def test_string_value_is_translated(my_thing):
    assert my_thing.translated_string == 'Baz. via property'


def test_sequence_value_is_translated(my_thing):
    assert my_thing.translated_sequence == ('B', 'a', 'R', '.', 'via', 'pRopeRty')


def test_string_is_translated_by_callable(my_thing):
    assert my_thing.string_translated_by_callable == 'Asdf. via property via translate_string()'
    assert my_thing._translate['string_translated_by_callable'].call_args_list == [
        call('Asdf. via property', my_thing),
    ]


def test_sequence_is_translated_by_callable(my_thing):
    assert my_thing.sequence_translated_by_callable == (
        'H via translate_sequence()',
        'J via translate_sequence()',
        'K via translate_sequence()',
        'L via translate_sequence()',
        '. via translate_sequence()',
        'VIA via translate_sequence()',
        'PROPERTY via translate_sequence()',
    )
    assert my_thing._translate['sequence_translated_by_callable'].call_args_list == [
        call('H', my_thing),
        call('j', my_thing),
        call('k', my_thing),
        call('l', my_thing),
        call('.', my_thing),
        call('via', my_thing),
        call('property', my_thing),
    ]

def test_replacement_in_translation_table_is_invalid(my_thing):
    with pytest.raises(RuntimeError, match=r"^invalid_replacement: Not a dictionary or callable: \('invalid', 'replacement'\)$"):
        my_thing.invalid_replacement


def test_callable_returns_None(my_thing):
    my_thing._translate['string_translated_by_callable'] = Mock(name='translate_string_does_nothing', side_effect=lambda val, obj: None)
    assert my_thing.string_translated_by_callable == 'Asdf. via property'
    assert my_thing._translate['string_translated_by_callable'].call_args_list == [
        call('Asdf. via property', my_thing),
    ]


def test_unexpected_value_is_not_translated(my_thing):
    my_thing._translated_string = 123
    assert my_thing.translated_string == 123
    assert my_thing._translate['string_translated_by_callable'].call_args_list == []


def test_setter_is_defined(my_thing):
    assert my_thing.translated_string == 'Baz. via property'
    my_thing.translated_string = 'bazzz'
    assert my_thing.translated_string == 'bazzz via property'


def test_setter_is_not_defined(my_thing):
    with pytest.raises(AttributeError, match=r"^Can't set attribute$"):
        my_thing.translated_sequence = 'asdf'


def test___delete___(my_thing):
    with pytest.raises(AttributeError, match=r"^Can't delete attribute$"):
        del my_thing.translated_string
