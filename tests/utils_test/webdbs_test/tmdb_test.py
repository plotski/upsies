from itertools import zip_longest

import pytest

from upsies.utils.types import ReleaseType
from upsies.utils.webdbs import Query, SearchResult, tmdb

ids = {
    'blues brothers': 'movie/525',
    'card sharks': 'tv/525',
    'deadwood': 'tv/1406',
    'city slickers': 'movie/1406',
    'february': 'movie/334536',
    'karppi': 'tv/74802',
    'maske i marts': 'movie/1292039',         # Cast has empty character names
    'wind in the willows': 'movie/125244',
    'alone': 'movie/773336',                  # Short
    'lost and found': 'tv/66260',
    'le dolci signore': 'movie/3405',
    '36th chamber': 'movie/11841',            # Asian characters in title
    'bad boy billionaires': 'tv/111086',      # No runtimes
    'miss zombie': 'movie/221437',
    'the nest': 'movie/22156',
}


@pytest.fixture
def api():
    return tmdb.TmdbApi()


@pytest.mark.parametrize('category', ('movie', 'tv'))
@pytest.mark.parametrize(
    argnames='text, exp_id',
    argvalues=(
        ('Some Title', None),
        ('1984', None),
        ('foo/123', None),
        ('{category}/123', '{category}/123'),
        ('https://www.themoviedb.org/{category}/123', '{category}/123'),
        ('https://www.themoviedb.org/{category}/123-foo-bar-baz', '{category}/123'),
        ('junk {category}/123 more junk', '{category}/123'),
        ('junk https://www.themoviedb.org/{category}/123-foo-bar-baz more junk', '{category}/123'),
    ),
    ids=lambda value: str(value),
)
def test_get_id_from_text(text, category, exp_id, api, store_response):
    text = text.format(category=category)
    if exp_id:
        exp_id = exp_id.format(category=category)
    return_value = api.get_id_from_text(text)
    assert return_value == exp_id


@pytest.mark.asyncio
async def test_search_handles_id_in_query(api, store_response):
    results = await api.search(Query(id=ids['blues brothers']))
    assert len(results) == 1

    assert results[0].id == ids['blues brothers']
    assert results[0].title == 'The Blues Brothers'
    assert results[0].type == ReleaseType.movie
    assert results[0].url == 'http://themoviedb.org/movie/525'
    assert results[0].year == '1980'

    # Make sure we're not awaiting the same coroutine again on subsequent calls
    for _ in range(2):
        assert (await results[0].cast())[:3] == ('Dan Aykroyd', 'John Belushi', 'James Brown')
        assert (await results[0].countries()) == ()
        assert (await results[0].directors()) == ('John Landis',)
        assert (await results[0].genres()) == ('music', 'comedy', 'action', 'crime')
        assert (await results[0].summary()) == (
            'Jake Blues, just released from prison, puts his old band back together '
            'to save the Catholic home where he and his brother Elwood were raised.'
        )
        assert (await results[0].title_english()) == ''
        assert (await results[0].title_original()) == 'The Blues Brothers'


@pytest.mark.asyncio
async def test_search_handles_non_unique_id_in_query(api, store_response):
    results = await api.search(Query(id='525'))
    assert len(results) == 2
    assert results[0].id == ids['blues brothers']
    assert results[1].id == 'tv/525'


@pytest.mark.asyncio
async def test_search_returns_empty_list_if_title_is_empty(api, store_response):
    assert await api.search(Query('', year='2009')) == []

@pytest.mark.asyncio
async def test_search_returns_list_of_SearchResults(api, store_response):
    results = await api.search(Query('Star Wars'))
    for result in results:
        assert isinstance(result, SearchResult)


@pytest.mark.parametrize(
    argnames=('query', 'exp_result'),
    argvalues=(
        (Query('alien', year='1979', type=ReleaseType.movie), {'title': 'Alien', 'year': '1979'}),
        (Query('alien', year='1986', type=ReleaseType.movie), {'title': 'Aliens', 'year': '1986'}),
        (Query('january', year='2012', type=ReleaseType.movie), {'title': 'One Day in January', 'year': '2012'}),
        (Query('january', year='2015', type=ReleaseType.movie), {'title': 'January Hymn', 'year': '2015'}),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_for_year(query, exp_result, api, store_response):
    results = await api.search(query)
    results_title_year = [
        {'title': r.title, 'year': r.year}
        for r in results
    ]
    assert exp_result in results_title_year


@pytest.mark.parametrize(
    argnames=('query', 'exp_titles'),
    argvalues=(
        (Query('Lost & Found Music Studios', type=ReleaseType.series), ('Lost & Found Music Studios',)),
        (Query('Lost & Found Music Studios', type=ReleaseType.movie), ()),
        (Query('Deadwood', type=ReleaseType.movie), ('Deadwood: The Movie',)),
        (Query('Deadwood', type=ReleaseType.series), ('Deadwood',)),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_for_type(query, exp_titles, api, store_response):
    results = await api.search(query)
    titles = {r.title for r in results}
    if exp_titles:
        for exp_title in exp_titles:
            assert exp_title in titles
    else:
        assert not titles


@pytest.mark.parametrize(
    argnames=('query', 'exp_cast'),
    argvalues=(
        (Query('The Blues Brothers', year=1980, type=ReleaseType.movie), ('Dan Aykroyd', 'John Belushi')),
        (Query('February', year=2017, type=ReleaseType.movie), ('Ana Sofrenović', 'Aleksandar Đurica', 'Sonja Kolačarić')),
        (Query('Deadwood', year=2004, type=ReleaseType.series), ('Timothy Olyphant', 'Ian McShane')),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_cast(query, exp_cast, api, store_response):
    results = await api.search(query)
    result = [r for r in results if r.title == query.title][0]
    cast = await result.cast()
    for member in exp_cast:
        assert member in cast


@pytest.mark.asyncio
async def test_search_result_countries(api, store_response):
    results = await api.search(Query('Star Wars'))
    for result in results:
        countries = await result.countries()
        assert countries == ()


@pytest.mark.parametrize(
    argnames=('query', 'exp_id'),
    argvalues=(
        (Query('The Blues Brothers', year=1980, type=ReleaseType.movie), ids['blues brothers']),
        (Query('February', year=2017, type=ReleaseType.movie), 'movie/700711'),
        (Query('Deadwood', year=2004, type=ReleaseType.series), ids['deadwood']),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_id(query, exp_id, api, store_response):
    results = await api.search(query)
    result = [r for r in results if r.title == query.title][0]
    assert result.id == exp_id


@pytest.mark.parametrize(
    argnames=('query', 'exp_directors'),
    argvalues=(
        (Query('The Blues Brothers', year=1980, type=ReleaseType.movie), ('John Landis',)),
        (Query('Deadwood', year=2004, type=ReleaseType.series), ()),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_directors(query, exp_directors, api, store_response):
    results = await api.search(query)
    assert await results[0].directors() == exp_directors


@pytest.mark.parametrize(
    argnames=('query', 'exp_genres'),
    argvalues=(
        (Query('The Blues Brothers', year=1980, type=ReleaseType.movie), ('music', 'comedy', 'action', 'crime')),
        (Query('February', year=2017, type=ReleaseType.movie), ('drama',)),
        (Query('Deadwood', year=2004, type=ReleaseType.series), ('crime', 'western')),
        (Query('Farang', year=2017, type=ReleaseType.series), ('drama',)),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_genres(query, exp_genres, api, store_response):
    results = await api.search(query)
    result = [r for r in results if r.title == query.title][0]
    genres = await result.genres()
    if exp_genres:
        for kw in exp_genres:
            assert kw in genres
    else:
        assert not genres


@pytest.mark.parametrize(
    argnames=('query', 'exp_summary'),
    argvalues=(
        (Query('The Blues Brothers', year=1980, type=ReleaseType.movie), 'released from prison'),
        (Query('February', year=2017, type=ReleaseType.movie), 'Husband and wife are going to their vacation cottage to overcome marriage crises'),
        (Query('Deadwood', year=2004, type=ReleaseType.series), 'woven around actual historic events'),
        (Query('Lost & Found Music Studios', year=2015, type=ReleaseType.series), 'singers-songwriters in an elite music program form bonds of friendship'),
        (Query('January', year=1973, type=ReleaseType.movie), ''),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_summary(query, exp_summary, api, store_response):
    results = await api.search(query)
    result = [r for r in results if r.title == query.title][0]
    summary = await result.summary()
    if exp_summary:
        assert exp_summary in summary
    else:
        assert summary == ''


@pytest.mark.parametrize(
    argnames=('query', 'exp_title'),
    argvalues=(
        (Query('Blues Brothers', year=1980, type=ReleaseType.movie), 'The Blues Brothers'),
        (Query('February', year=2017, type=ReleaseType.movie), "The Blackcoat's Daughter"),
        (Query('Seytan goz qabaginda', year=1987, type=ReleaseType.movie), 'The Devil under the Windshield'),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_title(query, exp_title, api, store_response):
    results = await api.search(query)
    titles = [r.title for r in results]
    assert exp_title in titles


@pytest.mark.parametrize(
    argnames=('query', 'exp_title_english'),
    argvalues=(
        (Query('The Blues Brothers', year=1980, type=ReleaseType.movie), ''),
        (Query('Deadwood', year=2004, type=ReleaseType.series), ''),
        (Query('Le dolci signore', year=1968, type=ReleaseType.movie), 'Anyone Can Play'),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_title_english(query, exp_title_english, api, store_response):
    results = await api.search(query)
    assert await results[0].title_english() == exp_title_english


@pytest.mark.parametrize(
    argnames=('query', 'exp_title_original'),
    argvalues=(
        (Query('The Blues Brothers', year=1980, type=ReleaseType.movie), 'The Blues Brothers'),
        (Query('Deadwind', year=2018, type=ReleaseType.series), 'Karppi'),
        (Query('Anyone Can Play', year=1968, type=ReleaseType.movie), 'Le dolci signore'),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_title_original(query, exp_title_original, api, store_response):
    results = await api.search(query)
    assert await results[0].title_original() == exp_title_original


@pytest.mark.parametrize(
    argnames=('query', 'exp_type'),
    argvalues=(
        (Query('The Blues Brothers', year=1980, type=ReleaseType.movie), ReleaseType.movie),
        (Query('February', year=2017, type=ReleaseType.movie), ReleaseType.movie),
        (Query('Deadwood', year=2004, type=ReleaseType.series), ReleaseType.series),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_type(query, exp_type, api, store_response):
    results = await api.search(query)
    results_dict = {r.title: r for r in results}
    assert results_dict[query.title].type == exp_type


@pytest.mark.parametrize(
    argnames=('query', 'exp_url'),
    argvalues=(
        (Query('The Blues Brothers', year=1980, type=ReleaseType.movie), 'http://themoviedb.org/movie/525'),
        (Query('February', year=2017, type=ReleaseType.movie), 'http://themoviedb.org/movie/700711'),
        (Query('Deadwood', year=2004, type=ReleaseType.series), 'http://themoviedb.org/tv/1406'),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_url(query, exp_url, api, store_response):
    results = await api.search(query)
    results_urls = [r.url for r in results]
    assert exp_url in results_urls


@pytest.mark.parametrize(
    argnames=('query', 'exp_title', 'exp_year'),
    argvalues=(
        (Query('Blues Brothers', year=1980, type=ReleaseType.movie), 'The Blues Brothers', '1980'),
        (Query('February', year=2017, type=ReleaseType.movie), "The Blackcoat's Daughter", '2017'),
        (Query('Deadwood', year=2004, type=ReleaseType.series), 'Deadwood', '2004'),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_search_result_year(query, exp_title, exp_year, api, store_response):
    results = await api.search(query)
    results_dict = {r.title: r for r in results}
    assert results_dict[exp_title].year == exp_year

@pytest.mark.asyncio
async def test_search_result_parser_failure(api):
    result = tmdb._TmdbSearchResult(tmdb_api=api)
    assert result.id == ''
    assert result.title == ''
    assert result.type == ReleaseType.unknown
    assert result.url == ''
    assert result.year == ''
    assert await result.cast() == ()
    assert await result.countries() == ()
    assert await result.directors() == ()
    assert await result.genres() == ()
    assert await result.summary() == ''
    assert await result.title_english() == ''
    assert await result.title_original() == ''


@pytest.mark.parametrize(
    argnames=('id', 'exp_cast'),
    argvalues=(
        ('36th chamber', (('Gordon Liu Chia-hui', 'http://themoviedb.org/person/240171-gordon-liu-chia-hui', 'Liu Yu-de / Monk San Ta'),
                          ('Lo Lieh', 'http://themoviedb.org/person/70673-lo-lieh', 'General Tien Ta'))),
        ('alone', (('Thomas Wilson Brown', 'http://themoviedb.org/person/57421-thomas-wilson-brown', 'Hammer'),
                   ('Stephanie Barkley', 'http://themoviedb.org/person/1355603-stephanie-barkley', 'Kaya Torres'))),
        ('bad boy billionaires', (('Aayush Ailawadi', 'http://themoviedb.org/person/3701489-aayush-ailawadi', 'Self'),)),
        ('blues brothers', (('Dan Aykroyd', 'http://themoviedb.org/person/707-dan-aykroyd', 'Elwood Blues'),
                            ('John Belushi', 'http://themoviedb.org/person/7171-john-belushi', "'Joliet' Jake Blues"))),
        ('card sharks', (('Bill Rafferty', 'http://themoviedb.org/person/1216732-bill-rafferty', ''),
                         ('Bob Eubanks', 'http://themoviedb.org/person/180738-bob-eubanks', ''))),
        ('city slickers', (('Billy Crystal', 'http://themoviedb.org/person/7904-billy-crystal', 'Mitch Robbins'),
                           ('Daniel Stern', 'http://themoviedb.org/person/11511-daniel-stern', 'Phil Berquist'))),
        ('deadwood', (('Timothy Olyphant', 'http://themoviedb.org/person/18082-timothy-olyphant', 'Seth Bullock'),
                      ('Ian McShane', 'http://themoviedb.org/person/6972-ian-mcshane', 'Al Swearengen'))),
        ('february', (('Emma Roberts', 'http://themoviedb.org/person/34847-emma-roberts', 'Joan Marsh'),
                      ('Kiernan Shipka', 'http://themoviedb.org/person/934289-kiernan-shipka', 'Kat'))),
        ('karppi', (('Pihla Viitala', 'http://themoviedb.org/person/93564-pihla-viitala', 'Sofia Karppi'),
                    ('Lauri Tilkanen', 'http://themoviedb.org/person/124867-lauri-tilkanen', 'Sakari Nurmi'))),
        ('le dolci signore', (('Ursula Andress', 'http://themoviedb.org/person/9871-ursula-andress', 'Norma'),
                              ('Virna Lisi', 'http://themoviedb.org/person/31895-virna-lisi', 'Luisa'))),
        ('lost and found', (('Levi Randall', 'http://themoviedb.org/person/1997234-levi-randall', 'Theo'),
                            ('Alex Zaichkowski', 'http://themoviedb.org/person/1997236-alex-zaichkowski', 'John'))),
        ('maske i marts', (('Michael Brostrup', 'http://themoviedb.org/person/1208770-michael-brostrup', ''),
                           ('Petrine Agger', 'http://themoviedb.org/person/135945-petrine-agger', ''))),
        ('miss zombie', (('Ayaka Komatsu', 'http://themoviedb.org/person/127228-ayaka-komatsu', 'Sara'),
                         ('Makoto Togashi', 'http://themoviedb.org/person/552342-makoto-togashi', 'Shizuko'))),
        ('the nest', (('Samy Naceri', 'http://themoviedb.org/person/20666-samy-naceri', 'Nasser'),
                      ('Benoît Magimel', 'http://themoviedb.org/person/5442-benoit-magimel', 'Santino'))),
        ('wind in the willows', (('Basil Rathbone', 'http://themoviedb.org/person/8727-basil-rathbone', 'Narrator (voice)'),
                                 ('Eric Blore', 'http://themoviedb.org/person/30184-eric-blore', 'J. Thaddeus Toad (voice)'))),
        (None, ()),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_cast(id, exp_cast, api, store_response):
    if id:
        id = ids[id]
    cast = (await api.cast(id))[:2]
    if not cast:
        assert exp_cast == ()
    else:
        for person, (name, url, role) in zip_longest(cast, exp_cast, fillvalue=(None, None, None)):
            assert person == name
            assert person.url == url
            assert person.role == role


@pytest.mark.parametrize(argnames='id', argvalues=('blues brothers', 'deadwood'))
@pytest.mark.asyncio
async def test_countries(id, api, store_response):
    if id:
        id = ids[id]
    countries = await api.countries(id)
    assert countries == ()


@pytest.mark.parametrize(
    argnames=('id', 'exp_creators'),
    argvalues=(
        ('36th chamber', ()),
        ('alone', ()),
        ('bad boy billionaires', ()),
        ('blues brothers', ()),
        ('card sharks', (('Mark Goodson', 'http://themoviedb.org/person/1211893-mark-goodson'),
                         ('Bill Todman', 'http://themoviedb.org/person/1215279-bill-todman'))),
        ('city slickers', ()),
        ('deadwood', (('David Milch', 'http://themoviedb.org/person/151295-david-milch'),)),
        ('february', ()),
        ('karppi', (('Rike Jokela', 'http://themoviedb.org/person/140497-rike-jokela'),)),
        ('le dolci signore', ()),
        ('lost and found', (('Frank Van Keeken', 'http://themoviedb.org/person/1583767-frank-van-keeken'),)),
        ('maske i marts', ()),
        ('miss zombie', ()),
        ('the nest', ()),
        ('wind in the willows', ()),
        (None, ()),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_creators(id, exp_creators, api, store_response):
    if id:
        id = ids[id]
    creators = await api.creators(id)
    if not creators:
        assert exp_creators == ()
    else:
        for person, (name, url) in zip_longest(creators, exp_creators, fillvalue=(None, None)):
            assert person == name
            assert person.url == url


@pytest.mark.parametrize(
    argnames=('id', 'exp_directors'),
    argvalues=(
        ('36th chamber', (('Lau Kar-leung', 'http://themoviedb.org/person/26775-lau-kar-leung'),)),
        ('alone', (('William Hellmuth', 'http://themoviedb.org/person/2884520-william-hellmuth'),)),
        ('bad boy billionaires', ()),
        ('blues brothers', (('John Landis', 'http://themoviedb.org/person/4610-john-landis'),)),
        ('card sharks', ()),
        ('city slickers', (('Ron Underwood', 'http://themoviedb.org/person/33485-ron-underwood'),)),
        ('deadwood', ()),
        ('february', (('Osgood Perkins', 'http://themoviedb.org/person/90609-osgood-perkins'),)),
        ('karppi', ()),
        ('le dolci signore', (('Luigi Zampa', 'http://themoviedb.org/person/31890-luigi-zampa'),)),
        ('lost and found', ()),
        ('maske i marts', (('Mikkel Bjørn Kehlert', 'http://themoviedb.org/person/3779168-mikkel-bjorn-kehlert'),)),
        ('miss zombie', (('SABU', 'http://themoviedb.org/person/58614-sabu'),)),
        ('the nest', (('Florent-Emilio Siri', 'http://themoviedb.org/person/1011158-florent-emilio-siri'),)),
        ('wind in the willows', (('James Algar', 'http://themoviedb.org/person/5690-james-algar'),)),
        (None, ()),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_directors(id, exp_directors, api, store_response):
    if id:
        id = ids[id]
    directors = await api.directors(id)
    if not directors:
        assert exp_directors == ()
    else:
        for person, (name, url) in zip_longest(directors, exp_directors, fillvalue=(None, None)):
            assert person == name
            assert person.url == url


@pytest.mark.parametrize(
    argnames=('id', 'exp_genres'),
    argvalues=(
        ('36th chamber', ('action', 'adventure')),
        ('alone', ('science fiction', 'short')),
        ('bad boy billionaires', ('crime', 'documentary')),
        ('blues brothers', ('music', 'comedy', 'action', 'crime')),
        ('card sharks', ()),
        ('city slickers', ('comedy', 'western')),
        ('deadwood', ('crime', 'western')),
        ('february', ('horror', 'mystery')),
        ('karppi', ('drama', 'crime')),
        ('le dolci signore', ('comedy', 'drama', 'romance')),
        ('lost and found', ('family', 'drama')),
        ('maske i marts', ('drama',)),
        ('miss zombie', ('horror', 'drama', 'thriller')),
        ('the nest', ('crime', 'action')),
        ('wind in the willows', ('animation', 'family', 'short')),
        (None, ()),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_genres(id, exp_genres, api, store_response):
    if id:
        id = ids[id]
    genres = await api.genres(id)
    if exp_genres:
        for member in exp_genres:
            assert member in genres
    else:
        assert not genres


@pytest.mark.parametrize(
    argnames='id, exp_url',
    argvalues=(
        ('36th chamber', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/hv29eb8g7iCDr0WgAcDdwZinuoY.jpg'),
        ('alone', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/kFWri31qt2sWWTNmvJ8WXTqyfyK.jpg'),
        ('bad boy billionaires', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/dosaLkSyo4S98wjWgKsEaf1v5zQ.jpg'),
        ('blues brothers', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/rhYJKOt6UrQq7JQgLyQcSWW5R86.jpg'),
        ('card sharks', ''),
        ('city slickers', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/mQKoQ3EV0nLzrOV08TtW5ZpZ7A4.jpg'),
        ('deadwood', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/4Yp35DVbVOAWkfQUIQ7pbh3u0aN.jpg'),
        ('february', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/zvcdDfwxn0F3Rky3m3Dl2eSVX5X.jpg'),
        ('karppi', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/cUKqWS2v7D6DVKQze2Iz2netwRH.jpg'),
        ('le dolci signore', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/sf31ob8bHZsgCH01J8PutaOnaTA.jpg'),
        ('lost and found', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/2febzBrLWIQ9oAlZAvnm4XN0cu1.jpg'),
        ('maske i marts', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/iB1kO3pZLDeq0vd7LH0NoUlyL7N.jpg'),
        ('miss zombie', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/qK3KGQoAqx7V38FGgNO8IMHUZDU.jpg'),
        ('the nest', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/5mamCUxm8jaFVeMH7DFgrSticd5.jpg'),
        ('wind in the willows', 'https://media.themoviedb.org/t/p/w300_and_h450_bestv2/zMRokrWAWccuqgDCNvQoKJEPHlS.jpg'),
        (None, ''),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_poster_url(id, exp_url, api, store_response):
    if id:
        id = ids[id]
    poster_url = await api.poster_url(id)
    assert poster_url == exp_url


@pytest.mark.parametrize(
    argnames=('id', 'exp_runtimes'),
    argvalues=(
        ('36th chamber', {'default': 115}),
        ('alone', {'default': 20}),
        ('bad boy billionaires', {}),
        ('blues brothers', {'default': 133}),
        ('card sharks', {}),
        ('city slickers', {'default': 114}),
        ('deadwood', {}),
        ('february', {'default': 93}),
        ('karppi', {}),
        ('le dolci signore', {'default': 88}),
        ('lost and found', {}),
        ('maske i marts', {}),
        ('miss zombie', {'default': 85}),
        ('the nest', {'default': 107}),
        ('wind in the willows', {'default': 34}),
        (None, {}),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_runtimes(id, exp_runtimes, api, store_response):
    if id:
        id = ids[id]
    runtimes = await api.runtimes(id)
    assert runtimes == exp_runtimes


@pytest.mark.parametrize(
    argnames=('id', 'exp_rating'),
    argvalues=(
        ('36th chamber', 75.0),
        ('alone', 80.0),
        ('bad boy billionaires', 77.0),
        ('blues brothers', 77.0),
        ('card sharks', 68.0),
        ('city slickers', 64.0),
        ('deadwood', 81.0),
        ('february', 58.0),
        ('karppi', 68.0),
        ('le dolci signore', 39.0),
        ('lost and found', 81.0),
        ('maske i marts', 0.0),
        ('miss zombie', 58.0),
        ('the nest', 64.0),
        ('wind in the willows', 67.0),
        (None, None),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_rating(id, exp_rating, api, store_response):
    if id:
        id = ids[id]
    rating = await api.rating(id)
    assert rating == exp_rating


@pytest.mark.parametrize(
    argnames=('id', 'exp_summary_excerpt'),
    argvalues=(
        ('36th chamber', 'devotes himself to learning the martial arts in order to seek revenge.'),
        ('alone', 'Kaya Torres is circling a black hole in a pod'),
        ('bad boy billionaires', 'greed, fraud, and corruption'),
        ('blues brothers', 'released from prison'),
        ('card sharks', 'American television game show'),
        ('city slickers', 'Three New York businessmen decide to take a "Wild West" vacation'),
        ('deadwood', 'woven around actual historic events'),
        ('february', 'Two young women at a prestigious prep school'),
        ('karppi', 'the body of a young woman on a construction site'),
        ('le dolci signore', 'four beautiful women attempt to deal with their sexual frustrations'),
        ('lost and found', 'Teen singers-songwriters in an elite music program'),
        ('maske i marts', ''),
        ('miss zombie', 'A doctor and his family receive an unexpected delivery'),
        ('the nest', 'Laborie is a high-flying officer in the French special forces.'),
        ('wind in the willows', 'dapper, automobile loving fellow named Mr. Toad'),
        (None, ''),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_summary(id, exp_summary_excerpt, api, store_response):
    if id:
        id = ids[id]
    summary = await api.summary(id)
    if exp_summary_excerpt:
        assert exp_summary_excerpt in summary
    else:
        assert summary == ''


@pytest.mark.parametrize(
    argnames=('id', 'exp_title_english', 'exp_title_original'),
    argvalues=(
        ('36th chamber', 'The 36th Chamber of Shaolin', '少林三十六房'),
        ('alone', '', 'Alone'),
        ('bad boy billionaires', '', 'Bad Boy Billionaires: India'),
        ('blues brothers', '', 'The Blues Brothers'),
        ('card sharks', '', 'Card Sharks'),
        ('city slickers', '', 'City Slickers'),
        ('deadwood', '', 'Deadwood'),
        ('february', '', "The Blackcoat's Daughter"),
        ('karppi', 'Deadwind', 'Karppi'),
        ('le dolci signore', 'Anyone Can Play', 'Le dolci signore'),
        ('lost and found', '', 'Lost & Found Music Studios'),
        ('maske i marts', '', 'Måske i marts'),
        ('miss zombie', '', 'Miss ZOMBIE'),
        ('the nest', 'The Nest', 'Nid de guêpes'),
        ('wind in the willows', '', 'The Wind in the Willows'),
        (None, '', ''),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_title_english_original(id, exp_title_english, exp_title_original, api, store_response):
    if id:
        id = ids[id]
    assert await api.title_original(id) == exp_title_original
    assert await api.title_english(id) == exp_title_english
    assert await api.title_english(id, default_to_original=False) == exp_title_english
    if exp_title_english:
        assert await api.title_english(id, default_to_original=True) == exp_title_english
    else:
        assert await api.title_english(id, default_to_original=True) == exp_title_original


@pytest.mark.parametrize(
    argnames=('id', 'exp_type'),
    argvalues=(
        ('36th chamber', ReleaseType.movie),
        ('alone', ReleaseType.movie),
        ('bad boy billionaires', ReleaseType.series),
        ('blues brothers', ReleaseType.movie),
        ('card sharks', ReleaseType.series),
        ('city slickers', ReleaseType.movie),
        ('deadwood', ReleaseType.series),
        ('february', ReleaseType.movie),
        ('karppi', ReleaseType.series),
        ('le dolci signore', ReleaseType.movie),
        ('lost and found', ReleaseType.series),
        ('maske i marts', ReleaseType.movie),
        ('miss zombie', ReleaseType.movie),
        ('the nest', ReleaseType.movie),
        ('wind in the willows', ReleaseType.movie),
        (None, ReleaseType.unknown),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_type(id, exp_type, api, store_response):
    if id:
        id = ids[id]
    type = await api.type(id)
    assert type is exp_type


@pytest.mark.parametrize(
    argnames=('id', 'exp_url'),
    argvalues=(
        ('36th chamber', f'{tmdb.TmdbApi._url_base}/movie/11841'),
        ('alone', f'{tmdb.TmdbApi._url_base}/movie/773336'),
        ('bad boy billionaires', f'{tmdb.TmdbApi._url_base}/tv/111086'),
        ('blues brothers', f'{tmdb.TmdbApi._url_base}/movie/525'),
        ('card sharks', f'{tmdb.TmdbApi._url_base}/tv/525'),
        ('city slickers', f'{tmdb.TmdbApi._url_base}/movie/1406'),
        ('deadwood', f'{tmdb.TmdbApi._url_base}/tv/1406'),
        ('february', f'{tmdb.TmdbApi._url_base}/movie/334536'),
        ('karppi', f'{tmdb.TmdbApi._url_base}/tv/74802'),
        ('le dolci signore', f'{tmdb.TmdbApi._url_base}/movie/3405'),
        ('lost and found', f'{tmdb.TmdbApi._url_base}/tv/66260'),
        ('maske i marts', f'{tmdb.TmdbApi._url_base}/movie/1292039'),
        ('miss zombie', f'{tmdb.TmdbApi._url_base}/movie/221437'),
        ('the nest', f'{tmdb.TmdbApi._url_base}/movie/22156'),
        ('wind in the willows', f'{tmdb.TmdbApi._url_base}/movie/125244'),
        (None, ''),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_url(id, exp_url, api):
    if id:
        id = ids[id]
    assert await api.url(id) == exp_url


@pytest.mark.parametrize(
    argnames=('id', 'exp_year'),
    argvalues=(
        ('36th chamber', '1978'),
        ('alone', '2020'),
        ('bad boy billionaires', '2020'),
        ('blues brothers', '1980'),
        ('card sharks', '1978'),
        ('city slickers', '1991'),
        ('deadwood', '2004'),
        ('february', '2017'),
        ('karppi', '2018'),
        ('le dolci signore', '1967'),
        ('lost and found', '2015'),
        ('maske i marts', ''),
        ('miss zombie', '2013'),
        ('the nest', '2002'),
        ('wind in the willows', '1949'),
        (None, ''),
    ),
    ids=lambda value: str(value),
)
@pytest.mark.asyncio
async def test_year(id, exp_year, api, store_response):
    if id:
        id = ids[id]
    assert await api.year(id) == exp_year
