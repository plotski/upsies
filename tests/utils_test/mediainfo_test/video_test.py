import re
from unittest.mock import Mock, call

import pytest

from upsies import errors
from upsies.utils import mediainfo


@pytest.mark.parametrize(
    argnames='path, default, width, par, exp_result, exp_mock_calls',
    argvalues=(
        pytest.param(
            'some/path',
            mediainfo.video.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            1.0,
            errors.ContentError('No such file'),
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Width'), type=int),
            ],
            id='lookup(width) raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'some/path',
            'mydefault',
            errors.ContentError('No such file'),
            1.0,
            'mydefault',
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Width'), type=int),
            ],
            id='lookup(width) raises ContentError, default=foo',
        ),
        pytest.param(
            'some/path',
            None,
            errors.ContentError('No such file'),
            1.0,
            None,
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Width'), type=int),
            ],
            id='lookup(width) raises ContentError, default=None',
        ),

        pytest.param(
            'some/path',
            mediainfo.video.NO_DEFAULT_VALUE,
            1920,
            1.0,
            1920,
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Width'), type=int),
                call.lookup('some/path', ('Video', 'DEFAULT', 'PixelAspectRatio'), type=float, default=1.0),
            ],
            id='PixelAspectRatio=1.0',
        ),
        pytest.param(
            'some/path',
            mediainfo.video.NO_DEFAULT_VALUE,
            704,
            1.455,
            1024,
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Width'), type=int),
                call.lookup('some/path', ('Video', 'DEFAULT', 'PixelAspectRatio'), type=float, default=1.0),
            ],
            id='PixelAspectRatio>1.0',
        ),
        pytest.param(
            'some/path',
            mediainfo.video.NO_DEFAULT_VALUE,
            704,
            0.888,
            704,
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Width'), type=int),
                call.lookup('some/path', ('Video', 'DEFAULT', 'PixelAspectRatio'), type=float, default=1.0),
            ],
            id='PixelAspectRatio<1.0',
        ),
    ),
    ids=lambda v: str(v),
)
def test_get_width(path, default, width, par, exp_result, exp_mock_calls, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=(width, par)), 'lookup')

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.video.get_width(path, default=default)
    else:
        return_value = mediainfo.video.get_width(path, default=default)
        assert return_value == exp_result
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='path, default, height, par, exp_result, exp_mock_calls',
    argvalues=(
        pytest.param(
            'some/path',
            mediainfo.video.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            1.0,
            errors.ContentError('No such file'),
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Height'), type=int),
            ],
            id='lookup(height) raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'some/path',
            'mydefault',
            errors.ContentError('No such file'),
            1.0,
            'mydefault',
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Height'), type=int),
            ],
            id='lookup(height) raises ContentError, default=foo',
        ),
        pytest.param(
            'some/path',
            None,
            errors.ContentError('No such file'),
            1.0,
            None,
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Height'), type=int),
            ],
            id='lookup(height) raises ContentError, default=None',
        ),

        pytest.param(
            'some/path',
            mediainfo.video.NO_DEFAULT_VALUE,
            1080,
            1.0,
            1080,
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Height'), type=int),
                call.lookup('some/path', ('Video', 'DEFAULT', 'PixelAspectRatio'), type=float, default=1.0),
            ],
            id='PixelAspectRatio=1.0',
        ),
        pytest.param(
            'some/path',
            mediainfo.video.NO_DEFAULT_VALUE,
            560,
            1.455,
            560,
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Height'), type=int),
                call.lookup('some/path', ('Video', 'DEFAULT', 'PixelAspectRatio'), type=float, default=1.0),
            ],
            id='PixelAspectRatio>1.0',
        ),
        pytest.param(
            'some/path',
            mediainfo.video.NO_DEFAULT_VALUE,
            480,
            0.888,
            540,
            [
                call.lookup('some/path', ('Video', 'DEFAULT', 'Height'), type=int),
                call.lookup('some/path', ('Video', 'DEFAULT', 'PixelAspectRatio'), type=float, default=1.0),
            ],
            id='PixelAspectRatio<1.0',
        ),
    ),
    ids=lambda v: str(v),
)
def test_get_height(path, default, height, par, exp_result, exp_mock_calls, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=(height, par)), 'lookup')

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.video.get_height(path, default=default)
    else:
        return_value = mediainfo.video.get_height(path, default=default)
        assert return_value == exp_result
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='default, resolution_int, scan_type, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            'p',
            errors.ContentError('No such file'),
            id='get_resolution_int() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            errors.ContentError('No such file'),
            'p',
            'foo',
            id='get_resolution_int() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            errors.ContentError('No such file'),
            'p',
            None,
            id='get_resolution_int() raises ContentError, default=None',
        ),

        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            1080,
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='get_scan_type() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            1080,
            errors.ContentError('No such file'),
            'foo',
            id='get_scan_type() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            1080,
            errors.ContentError('No such file'),
            None,
            id='get_scan_type() raises ContentError, default=None',
        ),

        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            1080,
            'p',
            '1080p',
            id='Success',
        ),
    ),
    ids=lambda value: str(value),
)
def test_get_resolution(default, resolution_int, scan_type, exp_result, mocker):
    mocks = Mock()
    if isinstance(resolution_int, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_resolution_int', side_effect=resolution_int), 'get_resolution_int')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_resolution_int', return_value=resolution_int), 'get_resolution_int')
    if isinstance(scan_type, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_scan_type', side_effect=scan_type), 'get_scan_type')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_scan_type', return_value=scan_type), 'get_scan_type')

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.video.get_resolution('foo.mkv', default=default)
    else:
        return_value = mediainfo.video.get_resolution('foo.mkv', default=default)
        assert return_value == exp_result

    assert mocks.mock_calls == [call.get_resolution_int('foo.mkv')] + (
        [call.get_scan_type('foo.mkv')]
        if not isinstance(resolution_int, Exception) else
        []
    )


@pytest.mark.parametrize(
    argnames='default, width, height, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            456,
            errors.ContentError('No such file'),
            id='get_width() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            errors.ContentError('No such file'),
            456,
            'foo',
            id='get_width() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            errors.ContentError('No such file'),
            456,
            None,
            id='get_width() raises ContentError, default=None',
        ),

        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            123,
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='get_height() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            123,
            errors.ContentError('No such file'),
            'foo',
            id='get_height() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            123,
            errors.ContentError('No such file'),
            None,
            id='get_height() raises ContentError, default=None',
        ),

        (mediainfo.video.NO_DEFAULT_VALUE, 7680, 4320, 4320),
        (mediainfo.video.NO_DEFAULT_VALUE, 3840, 2160, 2160),
        (mediainfo.video.NO_DEFAULT_VALUE, 1920, 1080, 1080),
        (mediainfo.video.NO_DEFAULT_VALUE, 1920, 1044, 1080),
        (mediainfo.video.NO_DEFAULT_VALUE, 1918, 1040, 1080),
        (mediainfo.video.NO_DEFAULT_VALUE, 1920, 804, 1080),
        (mediainfo.video.NO_DEFAULT_VALUE, 1392, 1080, 1080),
        (mediainfo.video.NO_DEFAULT_VALUE, 1280, 534, 720),
        (mediainfo.video.NO_DEFAULT_VALUE, 768, 720, 720),
        (mediainfo.video.NO_DEFAULT_VALUE, 768, 576, 576),
        (mediainfo.video.NO_DEFAULT_VALUE, 720, 540, 540),
        (mediainfo.video.NO_DEFAULT_VALUE, 704, 400, 480),
    ),
    ids=lambda value: str(value),
)
def test_get_resolution_int(default, width, height, exp_result, mocker):
    mocks = Mock()
    if isinstance(width, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_width', side_effect=width), 'get_width')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_width', return_value=width), 'get_width')
    if isinstance(height, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_height', side_effect=height), 'get_height')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_height', return_value=height), 'get_height')

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.video.get_resolution_int('foo.mkv', default=default)
    else:
        return_value = mediainfo.video.get_resolution_int('foo.mkv', default=default)
        assert return_value == exp_result

    assert mocks.mock_calls == [call.get_width('foo.mkv')] + (
        [call.get_height('foo.mkv')]
        if not isinstance(width, Exception) else
        []
    )


@pytest.mark.parametrize(
    argnames='ScanType, exp_return_value',
    argvalues=(
        ('Interlaced', 'i'),
        ('MBaff', 'i'),
        ('PAFF', 'i'),
        ('anything else', 'p'),
    ),
    ids=lambda v: str(v),
)
def test_get_scan_type(ScanType, exp_return_value, mocker):
    path = 'some/path'
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=ScanType), 'lookup')
    return_value = mediainfo.video.get_scan_type(path)
    assert return_value == exp_return_value
    assert mocks.mock_calls == [call.lookup(path, ('Video', 'DEFAULT', 'ScanType'), default='p')]


def test_get_frame_rate(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value='some frame rate'), 'lookup')
    return_value = mediainfo.video.get_frame_rate('foo.mkv', default='mydefault')
    assert return_value == mocks.lookup.return_value
    assert mocks.mock_calls == [call.lookup(
        path='foo.mkv',
        keys=('Video', 'DEFAULT', 'FrameRate'),
        default='mydefault',
        type=float,
    )]


def test_get_bit_depth(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value='some bit depth'), 'lookup')
    return_value = mediainfo.video.get_bit_depth('foo.mkv', default='mydefault')
    assert return_value == mocks.lookup.return_value
    assert mocks.mock_calls == [call.lookup(
        path='foo.mkv',
        keys=('Video', 'DEFAULT', 'BitDepth'),
        default='mydefault',
        type=int,
    )]


@pytest.mark.parametrize(
    argnames='default, video_track, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='lookup() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            errors.ContentError('No such file'),
            'foo',
            id='lookup() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            errors.ContentError('No such file'),
            None,
            id='lookup() raises ContentError, default=None',
        ),
        (mediainfo.video.NO_DEFAULT_VALUE, {}, ()),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format': 'foo'}, ()),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format': 'Dolby Vision'}, ('DV',)),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format': 'Dolby Vision / SMPTE ST 2086'}, ('DV',)),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format_Compatibility': 'HDR10+ Profile A'}, ('HDR10+',)),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format_Compatibility': 'HDR10+ Profile B'}, ('HDR10+',)),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format_Compatibility': 'HDR10'}, ('HDR10',)),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format_Compatibility': 'foo HDR bar'}, ('HDR',)),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format': 'foo HDR bar'}, ('HDR',)),
        (mediainfo.video.NO_DEFAULT_VALUE, {'colour_primaries': 'BT.2020'}, ('HDR10',)),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format': 'Dolby Vision', 'HDR_Format_Compatibility': 'HDR10'}, ('DV', 'HDR10')),
        (mediainfo.video.NO_DEFAULT_VALUE, {'HDR_Format': 'Dolby Vision', 'HDR_Format_Compatibility': 'HDR10+'}, ('DV', 'HDR10+')),
    ),
    ids=lambda v: str(v),
)
def test_get_hdr_formats(default, video_track, exp_result, mocker):
    mocks = Mock()
    if isinstance(video_track, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=video_track), 'lookup')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=video_track), 'lookup')

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.video.get_hdr_formats('foo.mkv', default=default)
    else:
        return_value = mediainfo.video.get_hdr_formats('foo.mkv', default=default)
        assert return_value == exp_result
    assert mocks.mock_calls == [call.lookup('foo.mkv', ('Video', 'DEFAULT'))]


@pytest.mark.parametrize(
    argnames='default, is_color_matrix, resolution_int, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            (errors.ContentError('No such file'), False),
            720,
            errors.ContentError('No such file'),
            id='is_color_matrix(BT.601) raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            (errors.ContentError('No such file'), False),
            720,
            'foo',
            id='is_color_matrix(BT.601) raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            (errors.ContentError('No such file'), False),
            720,
            None,
            id='is_color_matrix(BT.601) raises ContentError, default=None',
        ),

        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            (False, errors.ContentError('No such file')),
            720,
            errors.ContentError('No such file'),
            id='is_color_matrix(BT.470 System B/G) raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            (False, errors.ContentError('No such file')),
            720,
            'foo',
            id='is_color_matrix(BT.470 System B/G) raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            (False, errors.ContentError('No such file')),
            720,
            None,
            id='is_color_matrix(BT.470 System B/G) raises ContentError, default=None',
        ),

        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            (False, False),
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='get_resolution_int() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            (False, False),
            errors.ContentError('No such file'),
            'foo',
            id='get_resolution_int() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            (False, False),
            errors.ContentError('No such file'),
            None,
            id='get_resolution_int() raises ContentError, default=None',
        ),

        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            (True, False),
            720,
            True,
            id='is_color_matrix(BT.601)',
        ),
        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            (False, True),
            720,
            True,
            id='is_color_matrix(BT.470 System B/G)',
        ),
        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            (False, False),
            719,
            True,
            id='SD',
        ),
        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            (False, False),
            720,
            False,
            id='Not SD',
        ),
    ),
    ids=lambda v: str(v),
)
def test_is_bt601(default, is_color_matrix, resolution_int, exp_result, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video._is_color_matrix', side_effect=is_color_matrix), '_is_color_matrix')
    if isinstance(resolution_int, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_resolution_int', side_effect=resolution_int), 'get_resolution_int')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video.get_resolution_int', return_value=resolution_int), 'get_resolution_int')

    path = 'some/path'
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.video.is_bt601(path, default=default)
    else:
        return_value = mediainfo.video.is_bt601(path, default=default)
        assert return_value == exp_result

    exp_mock_calls = [call._is_color_matrix(path, 'BT.601')]
    if not isinstance(is_color_matrix[0], Exception) and not is_color_matrix[0]:
        exp_mock_calls.append(call._is_color_matrix(path, 'BT.470 System B/G'))
        if not isinstance(is_color_matrix[1], Exception) and not is_color_matrix[1]:
            exp_mock_calls.append(call.get_resolution_int(path))
    assert mocks.mock_calls == exp_mock_calls


def test_is_bt709(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video._is_color_matrix', return_value='maybe'), '_is_color_matrix')

    path = 'some/path'
    default = 'mydefault'
    return_value = mediainfo.video.is_bt709(path, default=default)
    assert return_value is mocks._is_color_matrix.return_value
    assert mocks.mock_calls == [call._is_color_matrix(path, 'BT.709', default=default)]


def test_is_bt2020(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.video._is_color_matrix', return_value='maybe'), '_is_color_matrix')

    path = 'some/path'
    default = 'mydefault'
    return_value = mediainfo.video.is_bt2020(path, default=default)
    assert return_value is mocks._is_color_matrix.return_value
    assert mocks.mock_calls == [call._is_color_matrix(path, 'BT.2020', default=default)]


@pytest.mark.parametrize(
    argnames='default, matrix, video_tracks, exp_result',
    argvalues=(
        (mediainfo.video.NO_DEFAULT_VALUE, 'BT.123  B/W', [{}, {'matrix_coefficients': 'BT.123 B/W'}, {}], True),
        (mediainfo.video.NO_DEFAULT_VALUE, 'Bt.123\nB/W', [{}, {'matrix_coefficients': 'bt.123  b/w'}, {}], True),
        (mediainfo.video.NO_DEFAULT_VALUE, 'bt.123\tB/W', [{}, {'matrix_coefficients': 'Bt.123\nB/W  Foo'}, {}], True),
        (mediainfo.video.NO_DEFAULT_VALUE, 'bT.123 b/w', [{}, {'matrix_coefficients': 'BT.321'}, {}], False),
        (mediainfo.video.NO_DEFAULT_VALUE, 'BT.123', [{}, {'matrix_coefficients': 'BT.321'}, {}], False),
        (mediainfo.video.NO_DEFAULT_VALUE, 'BT.123', [{}, {}, {}], False),
        (mediainfo.video.NO_DEFAULT_VALUE, 'BT.123', [], False),
        (mediainfo.video.NO_DEFAULT_VALUE, 'BT.123', errors.ContentError('No such file'), errors.ContentError('No such file')),
        ('foo', 'BT.123', errors.ContentError('No such file'), 'foo'),
        (None, 'BT.123', errors.ContentError('No such file'), None),
    ),
    ids=lambda v: str(v),
)
def test__is_color_matrix(default, matrix, video_tracks, exp_result, mocker):
    mocks = Mock()
    if isinstance(video_tracks, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=video_tracks), 'lookup')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=video_tracks), 'lookup')

    path = 'some/path'
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.video._is_color_matrix(path, matrix, default=default)
    else:
        return_value = mediainfo.video._is_color_matrix(path, matrix, default=default)
        assert return_value == exp_result
    assert mocks.mock_calls == [call.lookup(path, ('Video',))]


@pytest.mark.parametrize(
    argnames='default, video_track, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.video.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='lookup() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            errors.ContentError('No such file'),
            'foo',
            id='lookup() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            errors.ContentError('No such file'),
            None,
            id='lookup() raises ContentError, default=None',
        ),

        (mediainfo.video.NO_DEFAULT_VALUE, {}, errors.ContentError('Unable to detect video format')),
        ('foo', {}, 'foo'),
        (None, {}, None),
        (mediainfo.video.NO_DEFAULT_VALUE, {'Encoded_Library_Name': 'XviD'}, 'XviD'),
        (mediainfo.video.NO_DEFAULT_VALUE, {'Encoded_Library_Name': 'x264'}, 'x264'),
        (mediainfo.video.NO_DEFAULT_VALUE, {'Encoded_Library_Name': 'x265'}, 'x265'),
        (mediainfo.video.NO_DEFAULT_VALUE, {'Format': 'AVC'}, 'H.264'),
        (mediainfo.video.NO_DEFAULT_VALUE, {'Format': 'HEVC'}, 'H.265'),
        (mediainfo.video.NO_DEFAULT_VALUE, {'Format': 'VP9'}, 'VP9'),
        (mediainfo.video.NO_DEFAULT_VALUE, {'Format': 'VC-1'}, 'VC-1'),
        (mediainfo.video.NO_DEFAULT_VALUE, {'Format': 'MPEG Video'}, 'MPEG-2'),
    ),
    ids=lambda v: str(v),
)
def test_get_video_format(default, video_track, exp_result, mocker):
    mocks = Mock()
    if isinstance(video_track, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=video_track), 'lookup')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=video_track), 'lookup')

    path = 'some/path'
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.video.get_video_format(path, default=default)
    else:
        return_value = mediainfo.video.get_video_format(path, default=default)
        assert return_value == exp_result
    assert mocks.mock_calls == [call.lookup(path, ('Video', 'DEFAULT'))]
