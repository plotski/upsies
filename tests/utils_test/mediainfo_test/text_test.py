import os
from unittest.mock import Mock, call

import pytest

from upsies import errors
from upsies.utils import mediainfo


@pytest.mark.parametrize('forced, exp_forced', (
    (None, False),
    (True, True),
    (False, False),
    ('', False),
    (1, True),
))
@pytest.mark.parametrize('format, exp_format', (
    (None, ''),
    ('SRT', 'SRT'),
    ('', ''),
))
@pytest.mark.parametrize(
    argnames='language, exp_language, exp_region, exp_code',
    argvalues=(
        ('foo-mx-Latn', '?', '', '?'),
        ('?', '?', '', '?'),

        ('pt', 'pt', '', 'pt'),
        ('PT', 'pt', '', 'pt'),
        ('por', 'pt', '', 'pt'),

        ('Pt-bR', 'pt', 'BR', 'pt-BR'),
        ('spa-419', 'es', '419', 'es-419'),
        ('zh-HANS', 'zh', '', 'zh'),
        ('gsw-u-sd-chzh', 'gsw', '', 'gsw'),
    ),
)
def test_Subtitle(
        language, exp_language, exp_region, exp_code,
        format, exp_format,
        forced, exp_forced,
):
    kwargs = {'language': language}
    if forced is not None:
        kwargs['forced'] = forced
    if format is not None:
        kwargs['format'] = format
    sub = mediainfo.text.Subtitle(**kwargs)
    assert sub.language == exp_language
    assert sub.region == exp_region
    assert sub.forced == exp_forced
    assert sub.format == exp_format

    assert repr(sub) == (
        'Subtitle('
        + f'language={exp_code!r}'
        + f', forced={exp_forced!r}'
        + f', format={exp_format!r}'
        + ')'
    )


@pytest.mark.parametrize(
    argnames='format, exp_format',
    argvalues=(
        ('', ''),
        ('[SRT]', 'SRT'),
        ('[srt]', 'srt'),
        ('[ SRT ]', 'SRT'),
    ),
)
@pytest.mark.parametrize(
    argnames='forced, exp_forced',
    argvalues=(
        ('', False),
        ('forced', True),
        ('(forced)', True),
        ('forpsed', False),
    ),
)
@pytest.mark.parametrize(
    argnames='string, exp_language, exp_region',
    argvalues=(
        ('.*#!', '?', ''),
        ('foo-mx-Latn', '?', ''),
        ('en', 'en', ''),
        ('en-au', 'en', 'AU'),
    ),
)
def test_Subtitle_from_string(string, exp_language, exp_region, forced, exp_forced, format, exp_format):
    if forced:
        string = f'{string} {forced}'
    if format:
        string = f'{string} {format}'
    print(repr(string))
    subtitle = mediainfo.text.Subtitle.from_string(string)
    print(repr(subtitle))
    assert subtitle.language == exp_language
    assert subtitle.region == exp_region
    assert subtitle.forced is exp_forced
    assert subtitle.format == exp_format


@pytest.mark.parametrize(
    argnames='subtitle, exp_string',
    argvalues=(
        (mediainfo.text.Subtitle(language='?'), '?'),
        (mediainfo.text.Subtitle(language='?', format='SRT'), '? [SRT]'),
        (mediainfo.text.Subtitle(language='?', forced=True), '? (forced)'),
        (mediainfo.text.Subtitle(language='?', format='SRT', forced=True), '? (forced) [SRT]'),
        (mediainfo.text.Subtitle(language='se'), 'se'),
        (mediainfo.text.Subtitle(language='se', format='SRT'), 'se [SRT]'),
        (mediainfo.text.Subtitle(language='se', forced=True), 'se (forced)'),
        (mediainfo.text.Subtitle(language='se', format='SRT', forced=True), 'se (forced) [SRT]'),
        (mediainfo.text.Subtitle(language='pt-BR'), 'pt-BR'),
        (mediainfo.text.Subtitle(language='pt-BR', format='SRT'), 'pt-BR [SRT]'),
        (mediainfo.text.Subtitle(language='pt-BR', forced=True), 'pt-BR (forced)'),
        (mediainfo.text.Subtitle(language='pt-BR', format='SRT', forced=True), 'pt-BR (forced) [SRT]'),
    ),
    ids=lambda v: repr(v),
)
def test_Subtitle___str__(subtitle, exp_string):
    string = str(subtitle)
    assert string == exp_string


@pytest.mark.parametrize(
    argnames='a, b, exp_equal',
    argvalues=(
        (
            mediainfo.text.Subtitle(language='foo', forced=False, format=None),
            mediainfo.text.Subtitle(language='FOO', forced=False, format=None),
            True,
        ),
        (
            mediainfo.text.Subtitle(language='foo', forced=False, format=None),
            mediainfo.text.Subtitle(language='bar', forced=False, format=None),
            False,
        ),
        (
            mediainfo.text.Subtitle(language='foo', forced=False, format=None),
            mediainfo.text.Subtitle(language='foo', forced=0, format=None),
            True,
        ),
        (
            mediainfo.text.Subtitle(language='foo', forced=False, format=None),
            mediainfo.text.Subtitle(language='foo', forced=1, format=None),
            False,
        ),
        (
            mediainfo.text.Subtitle(language='foo', forced=False, format=None),
            mediainfo.text.Subtitle(language='foo', forced=False, format=''),
            True,
        ),
        (
            mediainfo.text.Subtitle(language='foo', forced=False, format='FMT'),
            mediainfo.text.Subtitle(language='foo', forced=False, format='FMT2'),
            False,
        ),
    ),
)
def test_Subtitle___eq__(a, b, exp_equal):
    equal = a == b
    assert equal is exp_equal


@pytest.mark.parametrize(
    argnames='content_path, is_bluray, is_dvd, exp_return_value, exp_mock_calls',
    argvalues=(
        pytest.param(
            'some/path',
            True,
            False,
            (
                'get_subtitles_from_bluray', 'subtitles','from', 'call', 1,
                'get_subtitles_from_bluray', 'subtitles','from', 'call', 2,
                'get_subtitles_from_bluray', 'subtitles','from', 'call', 3,
            ),
            [
                call.is_bluray('some/path', multidisc=True),
                call.bluray_get_disc_paths('some/path'),
                call.get_subtitles_from_bluray('some/path/disc1'),
                call.get_subtitles_from_bluray('some/path/disc2'),
                call.get_subtitles_from_bluray('some/path/disc3'),
            ],
            id='BDMV',
        ),
        pytest.param(
            'some/path',
            False,
            True,
            (
                'get_subtitles_from_dvd', 'subtitles','from', 'call', 1,
                'get_subtitles_from_dvd', 'subtitles','from', 'call', 2,
                'get_subtitles_from_dvd', 'subtitles','from', 'call', 3,
            ),
            [
                call.is_bluray('some/path', multidisc=True),
                call.is_dvd('some/path', multidisc=True),
                call.dvd_get_disc_paths('some/path'),
                call.get_subtitles_from_dvd('some/path/disc1'),
                call.get_subtitles_from_dvd('some/path/disc2'),
                call.get_subtitles_from_dvd('some/path/disc3'),
            ],
            id='VIDEO_TS',
        ),
        pytest.param(
            'some/path',
            False,
            False,
            (
                'get_subtitles_from_mediainfo', 'subtitles','from', 'call', 1,
                'get_subtitles_from_text_files', 'subtitles','from', 'call', 1,
                'get_subtitles_from_idx_files', 'subtitles','from', 'call', 1,
            ),
            [
                call.is_bluray('some/path', multidisc=True),
                call.is_dvd('some/path', multidisc=True),
                call.get_subtitles_from_mediainfo('some/path'),
                call.get_subtitles_from_text_files('some/path'),
                call.get_subtitles_from_idx_files('some/path'),
            ],
            id='Default',
        ),
    ),
)
def test_get_subtitles(content_path, is_bluray, is_dvd, exp_return_value, exp_mock_calls, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_bluray', return_value=is_bluray), 'is_bluray')
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.bluray.get_disc_paths', return_value=(
            os.path.join(content_path, 'disc1'),
            os.path.join(content_path, 'disc2'),
            os.path.join(content_path, 'disc3'),
        )),
        'bluray_get_disc_paths',
    )
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_dvd', return_value=is_dvd), 'is_dvd')
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.dvd.get_disc_paths', return_value=(
            os.path.join(content_path, 'disc1'),
            os.path.join(content_path, 'disc2'),
            os.path.join(content_path, 'disc3'),
        )),
        'dvd_get_disc_paths',
    )
    for funcname in dir(mediainfo.text):
        if funcname.startswith('get_subtitles_from_'):
            mocks.attach_mock(
                mocker.patch(f'upsies.utils.mediainfo.text.{funcname}', side_effect=tuple(
                    [funcname, 'subtitles', 'from', 'call', i]
                    for i in range(1, 10)
                )),
                funcname,
            )

    subtitles = mediainfo.text.get_subtitles(content_path)
    assert subtitles == exp_return_value
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='text_tracks, exp_subtitles',
    argvalues=(
        pytest.param(errors.ContentError('Permission denied'), (), id='Unable to lookup subtitle tracks'),
        pytest.param((), (), id='No subtitle tracks'),
        pytest.param(
            (
                {},
                {'Forced': 'No'},
                {'Forced': 'Yes'},
                {'CodecID': 'S_HDMV/PGS'},
                {'CodecID': 'S_TEXT/UTF8'},
                {'Language': 'eng'},
                {'Language': 'zh-HANS'},
                {'Language': 'invalid language code', 'CodecID': 'S_TEXT/VTT'},
            ),
            (
                mediainfo.text.Subtitle(language='', forced=False, format=''),
                mediainfo.text.Subtitle(language='', forced=False, format=''),
                mediainfo.text.Subtitle(language='', forced=True, format=''),
                mediainfo.text.Subtitle(language='', forced=False, format='PGS'),
                mediainfo.text.Subtitle(language='', forced=False, format='SRT'),
                mediainfo.text.Subtitle(language='en', forced=False, format=''),
                mediainfo.text.Subtitle(language='zh-Hans', forced=False, format=''),
                mediainfo.text.Subtitle(language='', forced=False, format=''),
            ),
            id='Various subtitle tracks',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_subtitles_from_mediainfo(text_tracks, exp_subtitles, mocker):
    mocks = Mock()
    if isinstance(text_tracks, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=text_tracks), 'lookup')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=text_tracks), 'lookup')
    content_path = 'path/to/content'
    subtitles = mediainfo.text.get_subtitles_from_mediainfo(content_path)
    assert subtitles == exp_subtitles
    assert mocks.mock_calls == [call.lookup(content_path, ('Text',))]


@pytest.mark.parametrize(
    argnames='file_list, exp_subtitles',
    argvalues=(
        pytest.param(
            [],
            [],
            id='No files found',
        ),
        pytest.param(
            ['path/to/foo.srt', 'path/to/foo.ssa', 'path/to/foo.ass', 'path/to/foo.vtt', 'path/to/foo'],
            [
                mediainfo.text.Subtitle(language='', forced=False, format='SRT'),
                mediainfo.text.Subtitle(language='', forced=False, format='SSA'),
                mediainfo.text.Subtitle(language='', forced=False, format='ASS'),
                mediainfo.text.Subtitle(language='', forced=False, format='VTT'),
                mediainfo.text.Subtitle(language='', forced=False, format=''),
            ],
            id='No language in file name',
        ),
        pytest.param(
            ['path/to/foo.eng.srt', 'path/to/foo.fr.ssa', 'path/to/foo.invalid-code.ass', 'path/to/foo.vie.vtt'],
            [
                mediainfo.text.Subtitle(language='en', forced=False, format='SRT'),
                mediainfo.text.Subtitle(language='fr', forced=False, format='SSA'),
                mediainfo.text.Subtitle(language='', forced=False, format='ASS'),
                mediainfo.text.Subtitle(language='vie', forced=False, format='VTT'),
            ],
            id='Language in file name',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_subtitles_from_text_files(file_list, exp_subtitles, mocker):
    file_list_mock = mocker.patch('upsies.utils.fs.file_list', return_value=file_list)

    content_path = 'path/to/content'
    subtitles = mediainfo.text.get_subtitles_from_text_files(content_path)
    assert subtitles == exp_subtitles

    assert file_list_mock.call_args_list == [
        call(content_path, extensions=('srt', 'ssa', 'ass', 'vtt')),
    ]


@pytest.mark.parametrize(
    argnames='existing_files, nonexisting_files, exp_subtitles',
    argvalues=(
        pytest.param(
            {},
            ['nosuchfile.idx'],
            [],
            id='No subtitles',
        ),
        pytest.param(
            {
                'foo.idx': 'junk\nid: de, index: 0\nmore junk\n',
                'bar.idx': 'junk\nid: en, index: 1\nmore junk\n',
                'baz.idx': 'junk\nid: ru, index: 123\nmore junk\n',
                'none.idx': 'junk\n, index: 123\nmore junk\n',
                'nope.idx': 'junk\nid: invalidcode, index: 123\nmore junk\n',
            },
            ['nosuchfile.idx'],
            [
                mediainfo.text.Subtitle(language='de', forced=False, format='VobSub'),
                mediainfo.text.Subtitle(language='en', forced=False, format='VobSub'),
                mediainfo.text.Subtitle(language='ru', forced=False, format='VobSub'),
                mediainfo.text.Subtitle(language='', forced=False, format='VobSub'),
                mediainfo.text.Subtitle(language='', forced=False, format='VobSub'),
            ],
            id='Various subtitles',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_subtitles_from_idx_files(existing_files, nonexisting_files, exp_subtitles, tmp_path, mocker):
    content_path = tmp_path / 'content'
    content_path.mkdir(parents=True, exist_ok=True)
    for file, data in existing_files.items():
        (content_path / file).write_text(data)

    file_list_mock = mocker.patch('upsies.utils.fs.file_list', return_value=(
        str(content_path / file)
        for file in (list(existing_files) + nonexisting_files)
    ))

    subtitles = mediainfo.text.get_subtitles_from_idx_files(content_path)
    assert subtitles == exp_subtitles

    assert file_list_mock.call_args_list == [
        call(content_path, extensions=('idx',)),
    ]


def test_get_subtitles_from_dvd(mocker):
    content_path = 'path/to/content'
    playlists = (
        Mock(name='a', filepath='foo/video_ts/vts_01_a.ifo', is_main=True),
        Mock(name='b', filepath='foo/video_ts/vts_02_b.ifo', is_main=False),
        Mock(name='c', filepath='foo/video_ts/vts_03_c.ifo', is_main=True),
    )
    subtitles = {
        playlists[0].filepath: [
            mediainfo.text.Subtitle(language='a1', forced=False, format=''),
            mediainfo.text.Subtitle(language='a2', forced=False, format=''),
        ],
        playlists[1].filepath: [
            mediainfo.text.Subtitle(language='b1', forced=False, format=''),
        ],
        playlists[2].filepath: [
            mediainfo.text.Subtitle(language='c1', forced=False, format=''),
            mediainfo.text.Subtitle(language='c2', forced=False, format=''),
            mediainfo.text.Subtitle(language='c3', forced=False, format=''),
        ],
    }

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.dvd.get_playlists', return_value=playlists),
        'get_playlists',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.mediainfo.text.get_subtitles_from_mediainfo', side_effect=lambda ifo_filepath: subtitles[ifo_filepath]),
        'get_subtitles_from_mediainfo',
    )

    subtitles = mediainfo.text.get_subtitles_from_dvd(content_path)
    assert subtitles == [
        mediainfo.text.Subtitle(language='a1', forced=False, format='VobSub'),
        mediainfo.text.Subtitle(language='a2', forced=False, format='VobSub'),
        mediainfo.text.Subtitle(language='c1', forced=False, format='VobSub'),
        mediainfo.text.Subtitle(language='c2', forced=False, format='VobSub'),
        mediainfo.text.Subtitle(language='c3', forced=False, format='VobSub'),
    ]

    assert mocks.mock_calls == [
        call.get_playlists(content_path),
        call.get_subtitles_from_mediainfo('foo/video_ts/vts_01_a.ifo'),
        call.get_subtitles_from_mediainfo('foo/video_ts/vts_03_c.ifo'),
    ]


def test_get_subtitles_from_bluray(mocker):
    content_path = 'path/to/content'
    playlists = (
        Mock(name='a', filepath='foo/bdmv/playlists/00100.mpls', is_main=True),
        Mock(name='b', filepath='foo/bdmv/playlists/00200.mpls', is_main=False),
        Mock(name='c', filepath='foo/bdmv/playlists/00300.mpls', is_main=True),
    )
    subtitles = {
        playlists[0].filepath: [
            mediainfo.text.Subtitle(language='a1', forced=False, format=''),
            mediainfo.text.Subtitle(language='a2', forced=False, format=''),
        ],
        playlists[1].filepath: [
            mediainfo.text.Subtitle(language='b1', forced=False, format=''),
        ],
        playlists[2].filepath: [
            mediainfo.text.Subtitle(language='c1', forced=False, format=''),
            mediainfo.text.Subtitle(language='c2', forced=False, format=''),
            mediainfo.text.Subtitle(language='c3', forced=False, format=''),
        ],
    }

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.bluray.get_playlists', return_value=playlists),
        'get_playlists',
    )
    mocks.attach_mock(
        mocker.patch(
            'upsies.utils.mediainfo.text.get_subtitles_from_mediainfo',
            side_effect=lambda mpls_filepath: subtitles[mpls_filepath],
        ),
        'get_subtitles_from_mediainfo',
    )

    subtitles = mediainfo.text.get_subtitles_from_bluray(content_path)
    assert subtitles == [
        mediainfo.text.Subtitle(language='a1', forced=False, format='PGS'),
        mediainfo.text.Subtitle(language='a2', forced=False, format='PGS'),
        mediainfo.text.Subtitle(language='c1', forced=False, format='PGS'),
        mediainfo.text.Subtitle(language='c2', forced=False, format='PGS'),
        mediainfo.text.Subtitle(language='c3', forced=False, format='PGS'),
    ]

    assert mocks.mock_calls == [
        call.get_playlists(content_path),
        call.get_subtitles_from_mediainfo('foo/bdmv/playlists/00100.mpls'),
        call.get_subtitles_from_mediainfo('foo/bdmv/playlists/00300.mpls'),
    ]
