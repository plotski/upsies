import itertools
import re
from unittest.mock import Mock, call, patch

import pytest

from upsies import errors
from upsies.utils import mediainfo


@pytest.fixture
def _run_mediainfo_mocks(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.fs.find_main_video', return_value='main_video.mkv'), 'find_main_video')
    mocks.attach_mock(mocker.patch('upsies.utils.fs.assert_file_readable'), 'assert_file_readable')
    mocks.attach_mock(mocker.patch('upsies.utils.subproc.run'), 'run')
    return mocks

def test__run_mediainfo_succeeds_with_ifo_file(_run_mediainfo_mocks):
    with patch('os.path.isdir', return_value=False) as isdir_mock:
        _run_mediainfo_mocks.attach_mock(isdir_mock, 'isdir')
        return_value = mediainfo._run_mediainfo('some/path/VIDEO_TS/VTS_01_1.IFO')
    assert return_value is _run_mediainfo_mocks.run.return_value
    assert _run_mediainfo_mocks.mock_calls == [
        call.isdir('some/path/VIDEO_TS/VTS_01_1.IFO'),
        call.assert_file_readable('some/path/VIDEO_TS/VTS_01_1.IFO'),
        call.run((mediainfo._mediainfo_executable, 'some/path/VIDEO_TS/VTS_01_1.IFO')),
    ]

def test__run_mediainfo_succeeds_with_mpls_file(_run_mediainfo_mocks):
    with patch('os.path.isdir', return_value=False) as isdir_mock:
        _run_mediainfo_mocks.attach_mock(isdir_mock, 'isdir')
        return_value = mediainfo._run_mediainfo('some/path/BDMV/PLAYLIST/01234.mpls')
    assert return_value is _run_mediainfo_mocks.run.return_value
    assert _run_mediainfo_mocks.mock_calls == [
        call.isdir('some/path/BDMV/PLAYLIST/01234.mpls'),
        call.assert_file_readable('some/path/BDMV/PLAYLIST/01234.mpls'),
        call.run((mediainfo._mediainfo_executable, 'some/path/BDMV/PLAYLIST/01234.mpls')),
    ]

def test__run_mediainfo_succeeds_with_video_file(_run_mediainfo_mocks):
    return_value = mediainfo._run_mediainfo('some/path')
    assert return_value is _run_mediainfo_mocks.run.return_value
    assert _run_mediainfo_mocks.mock_calls == [
        call.find_main_video('some/path'),
        call.assert_file_readable('main_video.mkv'),
        call.run((mediainfo._mediainfo_executable, 'main_video.mkv')),
    ]

def test__run_mediainfo_catches_DependencyError(_run_mediainfo_mocks):
    _run_mediainfo_mocks.run.side_effect = errors.DependencyError('mediainfo is not installed')
    with pytest.raises(errors.ContentError, match=r'^mediainfo is not installed$'):
        mediainfo._run_mediainfo('some/path')
    assert _run_mediainfo_mocks.mock_calls == [
        call.find_main_video('some/path'),
        call.assert_file_readable('main_video.mkv'),
        call.run((mediainfo._mediainfo_executable, 'main_video.mkv')),
    ]

def test__run_mediainfo_does_not_catch_ProcessError(_run_mediainfo_mocks):
    _run_mediainfo_mocks.run.side_effect = errors.ProcessError('mediainfo got bad arguments')
    with pytest.raises(errors.ProcessError, match=r'^mediainfo got bad arguments$'):
        mediainfo._run_mediainfo('some/path')
    assert _run_mediainfo_mocks.mock_calls == [
        call.find_main_video('some/path'),
        call.assert_file_readable('main_video.mkv'),
        call.run((mediainfo._mediainfo_executable, 'main_video.mkv')),
    ]

def test__run_mediainfo_is_cached(_run_mediainfo_mocks):
    _run_mediainfo_mocks.run.side_effect = lambda *_, i=itertools.count(), **__: f'<mediainfo output {next(i)}>'
    return_values = tuple(
        mediainfo._run_mediainfo('some/path')
        for _ in range(3)
    )

    # All return_values should be identical.
    for return_value in return_values:
        assert return_value == '<mediainfo output 0>'

    assert _run_mediainfo_mocks.mock_calls == [
        call.find_main_video('some/path'),
        call.assert_file_readable('main_video.mkv'),
        call.run((mediainfo._mediainfo_executable, 'main_video.mkv')),
    ]


def generate_mediainfo_report(complete_name, unique_id):
    return (
        'General\n'
        + (
            ''
            if not unique_id else
            f'Unique ID                                : {unique_id}\n'
        )
        + f'Complete name                            : {complete_name}\n'
    )

@pytest.fixture
def get_mediainfo_report_mocks(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo._run_mediainfo'), '_run_mediainfo')
    return mocks

def test_get_mediainfo_report__file_name__unique_id(get_mediainfo_report_mocks):
    path = 'file.mkv'
    mocks = get_mediainfo_report_mocks
    mocks._run_mediainfo.return_value = generate_mediainfo_report(complete_name=path, unique_id='123456789 (0x1234ABCD)')
    return_value = mediainfo.get_mediainfo_report(path)
    assert return_value == generate_mediainfo_report(complete_name='file.mkv', unique_id='123456789 (0x1234ABCD)')
    assert mocks.mock_calls == [call._run_mediainfo(path)]

def test_get_mediainfo_report__file_path__unique_id(get_mediainfo_report_mocks):
    path = 'path/to/file.mkv'
    mocks = get_mediainfo_report_mocks
    mocks._run_mediainfo.return_value = generate_mediainfo_report(complete_name=path, unique_id='123456789 (0x1234ABCD)')
    return_value = mediainfo.get_mediainfo_report(path)
    assert return_value == generate_mediainfo_report(complete_name='file.mkv', unique_id='123456789 (0x1234ABCD)')
    assert mocks.mock_calls == [call._run_mediainfo(path)]

def test_get_mediainfo_report__file_name__no_unique_id(get_mediainfo_report_mocks):
    path = 'file.mkv'
    mocks = get_mediainfo_report_mocks
    mocks._run_mediainfo.return_value = generate_mediainfo_report(complete_name=path, unique_id='')
    return_value = mediainfo.get_mediainfo_report(path)
    assert return_value == generate_mediainfo_report(complete_name='file.mkv', unique_id='0 (0x0)')
    assert mocks.mock_calls == [call._run_mediainfo(path)]

def test_get_mediainfo_report__file_path__no_unique_id(get_mediainfo_report_mocks):
    path = 'path/to/file.mkv'
    mocks = get_mediainfo_report_mocks
    mocks._run_mediainfo.return_value = generate_mediainfo_report(complete_name=path, unique_id='')
    return_value = mediainfo.get_mediainfo_report(path)
    assert return_value == generate_mediainfo_report(complete_name='file.mkv', unique_id='0 (0x0)')
    assert mocks.mock_calls == [call._run_mediainfo(path)]


@pytest.mark.parametrize(
    argnames='path_exists, default, tracks_json, exp_result, exp_run_mediainfo_called',
    argvalues=(
        pytest.param(
            True, mediainfo.NO_DEFAULT_VALUE,
            (
                '{"media": {"track": ['
                '{"@type": "Video", "foo": "bar"}, '
                '{"@type": "Audio", "bar": "baz"}, '
                '{"@type": "Audio", "also": "this"}'
                ']}}'
            ),
            {
                'Video': [
                    {'@type': 'Video', 'foo': 'bar'},
                ],
                'Audio': [
                    {'@type': 'Audio', 'bar': 'baz'},
                    {'@type': 'Audio', 'also': 'this'},
                ],
            },
            True,
            id='Success',
        ),
        pytest.param(
            False, mediainfo.NO_DEFAULT_VALUE,
            '{"media": {"track": [{"@type": "Video", "foo": "bar"}]}}',
            {'Video': [{'@type': 'Video', 'foo': 'bar'}]},
            True,
            id='path does not exist, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            False, 'foo',
            '{"media": {"track": [{"@type": "Video", "foo": "bar"}]}}',
            'foo',
            False,
            id='path does not exist, default=foo',
        ),
        pytest.param(
            False, None,
            '{"media": {"track": [{"@type": "Video", "foo": "bar"}]}}',
            None,
            False,
            id='path does not exist, default=None',
        ),
        pytest.param(
            True, mediainfo.NO_DEFAULT_VALUE,
            '{"media": {"track": []',
            RuntimeError(
                '{video_filepath}: Unexpected mediainfo output: {{"media": {{"track": []: '
                "Expecting ',' delimiter: line 1 column 23 (char 22)"
            ),
            True,
            id='ValueError (bad JSON)',
        ),
        pytest.param(
            True, mediainfo.NO_DEFAULT_VALUE,
            '{"media": {"track": "Hello!"}}',
            RuntimeError(
                '{video_filepath}: Unexpected mediainfo output: {{"media": {{"track": "Hello!"}}}}: '
                # NOTE: Python >= 3.11 adds ", not 'str'" to the error message.
                "string indices must be integers"
            ),
            True,
            id='TypeError (unexpected type in JSON)',
        ),
        pytest.param(
            True, mediainfo.NO_DEFAULT_VALUE,
            '{"media": {"traque": []}}',
            RuntimeError(
                '{video_filepath}: Unexpected mediainfo output: {{"media": {{"traque": []}}}}: '
                "Missing field: 'track'"
            ),
            True,
            id='KeyError',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_tracks(path_exists, default, tracks_json, exp_result, exp_run_mediainfo_called, mocker, tmp_path):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo._run_mediainfo', return_value=tracks_json), '_run_mediainfo')

    video_filepath = 'some/path'

    with patch('os.path.exists', return_value=path_exists):
        if isinstance(exp_result, Exception):
            exp_msg = str(exp_result).format(video_filepath=video_filepath)
            with pytest.raises(type(exp_result), match=rf'^{re.escape(exp_msg)}'):
                mediainfo.get_tracks(video_filepath, default=default)
        else:
            return_value = mediainfo.get_tracks(video_filepath, default=default)
            assert return_value == exp_result

    if exp_run_mediainfo_called:
        assert mocks.mock_calls == [call._run_mediainfo(video_filepath, '--Output=JSON')]
    else:
        assert mocks.mock_calls == []


@pytest.mark.parametrize(
    argnames='tracks, exp_return_value',
    argvalues=(
        pytest.param(
            [
                {'foo': 1, 'Default': 'Yes'},
                {'bar': 2},
                {'baz': 3},
            ],
            {'foo': 1, 'Default': 'Yes'},
            id='Track 1 is marked as default',
        ),
        pytest.param(
            [
                {'foo': 1},
                {'bar': 2, 'Default': 'Yes'},
                {'baz': 3},
            ],
            {'bar': 2, 'Default': 'Yes'},
            id='Track 2 is marked as default',
        ),
        pytest.param(
            [
                {'foo': 1},
                {'bar': 2, 'Default': 'Yes'},
                {'baz': 3, 'Default': 'Yes'},
            ],
            {'bar': 2, 'Default': 'Yes'},
            id='Multiple tracks are marked as default',
        ),
        pytest.param(
            [
                {'foo': 1},
                {'bar': 2},
                {'baz': 3},
            ],
            {'foo': 1},
            id='No track is marked as default',
        ),
    ),
)
def test__get_default_track(tracks, exp_return_value, mocker):
    return_value = mediainfo._get_default_track(tracks)
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='tracks, keys, type_, default, exp_result',
    argvalues=(
        pytest.param(
            errors.ContentError('No such file'),
            ('General', 1, 'bar'),
            None,
            mediainfo.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            id='get_tracks() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            errors.ContentError('No such file'),
            ('General', 1, 'bar'),
            None,
            'foo',
            'foo',
            id='get_tracks() raises ContentError, default=foo',
        ),
        pytest.param(
            errors.ContentError('No such file'),
            ('General', 1, 'bar'),
            None,
            None,
            None,
            id='get_tracks() raises ContentError, default=None',
        ),

        pytest.param(
            {
                'General': [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General',),
            None,
            mediainfo.NO_DEFAULT_VALUE,
            [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}],
            id='Successful level 1 lookup',
        ),
        pytest.param(
            {
                'General': [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1),
            None,
            mediainfo.NO_DEFAULT_VALUE,
            {'foo': 123, 'bar': (4, 5, 6), 'baz': 789},
            id='Successful level 2 lookup',
        ),
        pytest.param(
            {
                'General': [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1, 'bar'),
            None,
            mediainfo.NO_DEFAULT_VALUE,
            (4, 5, 6),
            id='Successful level 3 lookup',
        ),

        pytest.param(
            {
                'General': [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1, 'bar', 'asdf'),
            None,
            mediainfo.NO_DEFAULT_VALUE,
            errors.ContentError(
                "Unable to lookup ('General', 1, 'bar', 'asdf')"
                ' in '
                "{'General': [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}], 'Video': [], 'Audio': []}",
            ),
            id='keys has more depth than tracks, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            {
                'General': [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1, 'bar', 'asdf'),
            None,
            'foo',
            'foo',
            id='keys has more depth than tracks, default=foo',
        ),
        pytest.param(
            {
                'General': [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1, 'bar', 'asdf'),
            None,
            None,
            None,
            id='keys has more depth than tracks, default=None',
        ),

        pytest.param(
            {
                'General': [{}, {'foo': 'this is it'}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1, 'foo', 'bar'),
            None,
            mediainfo.NO_DEFAULT_VALUE,
            errors.ContentError(
                "Unable to lookup ('General', 1, 'foo', 'bar')"
                ' in '
                "{'General': [{}, {'foo': 'this is it'}, {}], 'Video': [], 'Audio': []}",
            ),
            id='keys points to non-existing value, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            {
                'General': [{}, {'foo': 'this is it'}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1, 'foo', 'bar'),
            None,
            'foo',
            'foo',
            id='keys points to non-existing value, default=foo',
        ),
        pytest.param(
            {
                'General': [{}, {'foo': 'this is it'}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1, 'foo', 'bar'),
            None,
            None,
            None,
            id='keys points to non-existing value, default=None',
        ),

        pytest.param(
            {
                'General': [{'foo': 1, 'bar': 2, 'baz': 3}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {'c': 99}],
                'Video': [],
                'Audio': [],
            },
            ('General', 'DEFAULT', 'bar'),
            None,
            mediainfo.NO_DEFAULT_VALUE,
            2,
            id='DEFAULT track lookup, default track is not marked',
        ),
        pytest.param(
            {
                'General': [{'foo': 1, 'bar': 2, 'baz': 3}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789, 'Default': 'Yes'}, {'c': 99}],
                'Video': [],
                'Audio': [],
            },
            ('General', 'DEFAULT', 'bar'),
            None,
            mediainfo.NO_DEFAULT_VALUE,
            (4, 5, 6),
            id='DEFAULT track lookup, default track is marked',
        ),

        pytest.param(
            {
                'General': [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1, 'bar'),
            sum,
            mediainfo.NO_DEFAULT_VALUE,
            15,
            id='Successful type conversion',
        ),
        pytest.param(
            {
                'General': [{}, {'foo': 123, 'bar': (4, 5, 6), 'baz': 789}, {}],
                'Video': [],
                'Audio': [],
            },
            ('General', 1, 'bar'),
            int,
            mediainfo.NO_DEFAULT_VALUE,
            TypeError,
            id='Unsuccessful type conversion',
        ),
    ),
    ids=lambda v: str(v),
)
def test_lookup(tracks, keys, type_, default, exp_result, mocker):
    mocks = Mock()
    if isinstance(tracks, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.get_tracks', side_effect=tracks), 'get_tracks')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.get_tracks', return_value=tracks), 'get_tracks')

    kwargs = {
        'path': 'path/to/foo.mkv',
        'keys': keys,
    }
    if type_ is not None:
        kwargs['type'] = type_
    if default is not mediainfo.NO_DEFAULT_VALUE:
        kwargs['default'] = default

    if isinstance(exp_result, Exception):
        msg = str(exp_result).replace('{', '{{').replace('}', '}}').format(keys=keys, tracks=tracks)
        with pytest.raises(type(exp_result), match=rf'^{re.escape(msg)}$'):
            mediainfo.lookup(**kwargs)
    elif isinstance(exp_result, type) and issubclass(exp_result, Exception):
        with pytest.raises(exp_result):
            mediainfo.lookup(**kwargs)
    else:
        return_value = mediainfo.lookup(**kwargs)
        assert return_value == exp_result

    assert mocks.mock_calls == [call.get_tracks(kwargs['path'])]


@pytest.mark.parametrize('exception', (errors.DependencyError('wat'), errors.ProcessError('wat')), ids=lambda v: repr(v))
@pytest.mark.parametrize(
    argnames='mocked, exp_result, exp_mock_calls',
    argvalues=(
        pytest.param(
            {
                'get_duration_from_ffprobe': Mock(return_value=123),
                'get_duration_from_mediainfo': Mock(),
            },
            123,
            [
                call.get_duration_from_ffprobe('some/path', default='my default'),
            ],
            id='No exception from get_duration_from_ffprobe()',
        ),
        pytest.param(
            {
                'get_duration_from_ffprobe': Mock(side_effect=Exception),
                'get_duration_from_mediainfo': Mock(return_value=456),
            },
            456,
            [
                call.get_duration_from_ffprobe('some/path', default='my default'),
                call.get_duration_from_mediainfo('some/path', default='my default'),
            ],
            id='Exception from get_duration_from_ffprobe() and no Exception from get_duration_from_mediainfo()',
        ),
        pytest.param(
            {
                'get_duration_from_ffprobe': Mock(side_effect=Exception),
                'get_duration_from_mediainfo': Mock(side_effect=Exception),
            },
            Exception('wat'),
            [
                call.get_duration_from_ffprobe('some/path', default='my default'),
                call.get_duration_from_mediainfo('some/path', default='my default'),
            ],
            id='Exception from get_duration_from_ffprobe() and Exception from get_duration_from_mediainfo()',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_duration(mocked, exception, exp_result, exp_mock_calls, mocker):
    for m in mocked.values():
        if m.side_effect is Exception:
            m.side_effect = exception

    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.get_duration_from_ffprobe', mocked['get_duration_from_ffprobe']), 'get_duration_from_ffprobe')
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.get_duration_from_mediainfo', mocked['get_duration_from_mediainfo']), 'get_duration_from_mediainfo')

    video_filepath = 'some/path'
    default = 'my default'

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.get_duration(video_filepath, default=default)
    else:
        return_value = mediainfo.get_duration(video_filepath, default=default)
        assert return_value == exp_result
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='path_exists, default, ffprobe_output, exp_result, exp_find_main_video_called, exp_run_called',
    argvalues=(
        (False, mediainfo.NO_DEFAULT_VALUE, 'irrelevant', errors.ContentError('No main video found'), True, False),
        (False, None, 'irrelevant', None, False, False),
        (False, 'my default', 'irrelevant', 'my default', False, False),

        (True, mediainfo.NO_DEFAULT_VALUE, '123.456', 123.456, True, True),
        (True, None, '123.456', 123.456, True, True),
        (True, 'my default', '123.456', 123.456, True, True),

        (True, mediainfo.NO_DEFAULT_VALUE, 'wat', RuntimeError("Unexpected output from {cmd}: 'wat'"), True, True),
        (True, None, 'wat', RuntimeError("Unexpected output from {cmd}: 'wat'"), True, True),
        (True, 'my default', 'wat', RuntimeError("Unexpected output from {cmd}: 'wat'"), True, True),
    ),
    ids=lambda v: repr(v),
)
def test_get_duration_from_ffprobe(path_exists, default, ffprobe_output, exp_result, exp_find_main_video_called, exp_run_called, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.fs.find_main_video', return_value='main_video.mkv'), 'find_main_video')
    if not path_exists:
        mocks.find_main_video.side_effect = errors.ContentError('No main video found')
    mocks.attach_mock(mocker.patch('upsies.utils.subproc.run', return_value=ffprobe_output), 'run')

    path = 'some/path'
    exp_cmd = (
        mediainfo._ffprobe_executable,
        '-v', 'error', '-show_entries', 'format=duration',
        '-of', 'default=noprint_wrappers=1:nokey=1',
        f'file:{mocks.find_main_video.return_value}',
    )

    with patch('os.path.exists', return_value=path_exists):
        if isinstance(exp_result, Exception):
            exp_msg = str(exp_result).format(cmd=exp_cmd)
            with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_msg))}$'):
                mediainfo.get_duration_from_ffprobe(path, default=default)
        else:
            return_value = mediainfo.get_duration_from_ffprobe(path, default=default)
            assert return_value == exp_result
        assert mocks.mock_calls == (
            [call.find_main_video(path)]
            if exp_find_main_video_called else
            []
        ) + (
            [call.run(exp_cmd, ignore_errors=True)]
            if exp_run_called else
            []
        )


@pytest.mark.parametrize(
    argnames='default, all_tracks, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='get_tracks() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            errors.ContentError('No such file'),
            'foo',
            id='get_tracks() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            errors.ContentError('No such file'),
            None,
            id='get_tracks() raises ContentError, default=None',
        ),

        pytest.param(
            mediainfo.NO_DEFAULT_VALUE,
            {},
            RuntimeError('Unexpected tracks from {path}: {{}}'),
            id='No "General" key, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            {},
            RuntimeError('Unexpected tracks from {path}: {{}}'),
            id='No "General" key, default=foo',
        ),
        pytest.param(
            None,
            {},
            RuntimeError('Unexpected tracks from {path}: {{}}'),
            id='No "General" key, default=None',
        ),

        pytest.param(
            mediainfo.NO_DEFAULT_VALUE,
            {'General': []},
            RuntimeError("Unexpected tracks from {path}: {{'General': []}}"),
            id='No "General" tracks, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            {'General': []},
            RuntimeError("Unexpected tracks from {path}: {{'General': []}}"),
            id='No "General" tracks, default=foo',
        ),
        pytest.param(
            None,
            {'General': []},
            RuntimeError("Unexpected tracks from {path}: {{'General': []}}"),
            id='No "General" tracks, default=None',
        ),

        pytest.param(
            mediainfo.NO_DEFAULT_VALUE,
            {'General': [{'Duration': 'foo'}]},
            RuntimeError("Unexpected tracks from {path}: {{'General': [{{'Duration': 'foo'}}]}}"),
            id='"Duration" has invalid value, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            {'General': [{'Duration': 'foo'}]},
            RuntimeError("Unexpected tracks from {path}: {{'General': [{{'Duration': 'foo'}}]}}"),
            id='"Duration" has invalid value, default=foo',
        ),
        pytest.param(
            None,
            {'General': [{'Duration': 'foo'}]},
            RuntimeError("Unexpected tracks from {path}: {{'General': [{{'Duration': 'foo'}}]}}"),
            id='"Duration" has invalid value, default=None',
        ),

        pytest.param(
            mediainfo.NO_DEFAULT_VALUE,
            {'General': [{'Duration': 'foo'}]},
            RuntimeError("Unexpected tracks from {path}: {{'General': [{{'Duration': 'foo'}}]}}"),
            id='"Duration" has invalid type, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            {'General': [{'Duration': 'foo'}]},
            RuntimeError("Unexpected tracks from {path}: {{'General': [{{'Duration': 'foo'}}]}}"),
            id='"Duration" has invalid type, default=foo',
        ),
        pytest.param(
            None,
            {'General': [{'Duration': 'foo'}]},
            RuntimeError("Unexpected tracks from {path}: {{'General': [{{'Duration': 'foo'}}]}}"),
            id='"Duration" has invalid type, default=None',
        ),

        pytest.param(
            mediainfo.NO_DEFAULT_VALUE,
            {'General': [{'Duration': 123}]},
            123.0,
            id='Success',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_duration_from_mediainfo(default, all_tracks, exp_result, mocker):
    mocks = Mock()
    if isinstance(all_tracks, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.get_tracks', side_effect=all_tracks), 'get_tracks')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.get_tracks', return_value=all_tracks), 'get_tracks')

    if isinstance(exp_result, Exception):
        exp_msg = str(exp_result).format(path='foo.mkv')
        with pytest.raises(type(exp_result), match=rf'^{re.escape(exp_msg)}$'):
            mediainfo.get_duration_from_mediainfo('foo.mkv', default=default)
    else:
        return_value = mediainfo.get_duration_from_mediainfo('foo.mkv', default=default)
        assert return_value == exp_result

    assert mocks.mock_calls == [call.get_tracks('foo.mkv')]
