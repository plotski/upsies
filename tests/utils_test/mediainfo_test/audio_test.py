import re
from unittest.mock import Mock, call, patch

import pytest

from upsies import errors
from upsies.utils import mediainfo


@pytest.mark.parametrize(
    argnames='default, audio_tracks, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.audio.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='lookup() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            errors.ContentError('No such file'),
            'foo',
            id='lookup() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            errors.ContentError('No such file'),
            None,
            id='lookup() raises ContentError, default=None',
        ),
        (mediainfo.audio.NO_DEFAULT_VALUE, [], False),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{}, {}], False),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'en'}], False),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'fr'}], False),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'fr'}, {'Language': 'en'}], True),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'en'}, {'Language': 'fr'}], True),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'da'}, {'Language': 'fr'}], True),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'en-GB'}, {'Language': 'fr'}], True),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'en-GB'}, {'Language': 'en-US'}], False),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'en'}, {'Language': 'fr'}, {'Language': 'fr', 'Title': 'Commentary with Foo'}], True),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'fr'}, {'Language': 'en', 'Title': 'Commentary with Foo'}], False),
        (mediainfo.audio.NO_DEFAULT_VALUE, [{'Language': 'fr'}, {'Language': 'en-GB', 'Title': 'Commentary with Foo'}], False),
    ),
    ids=lambda v: str(v),
)
def test_has_dual_audio(default, audio_tracks, exp_result, mocker):
    mocks = Mock()
    if isinstance(audio_tracks, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=audio_tracks), 'lookup')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=audio_tracks), 'lookup')

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.audio.has_dual_audio('foo.mkv', default=default)
    else:
        return_value = mediainfo.audio.has_dual_audio('foo.mkv', default=default)
        assert return_value is exp_result
    assert mocks.mock_calls == [call.lookup('foo.mkv', ('Audio',))]


@pytest.mark.parametrize(
    argnames='default, audio_tracks, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.audio.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='lookup() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            errors.ContentError('No such file'),
            'foo',
            id='lookup() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            errors.ContentError('No such file'),
            None,
            id='lookup() raises ContentError, default=None',
        ),

        (
            mediainfo.audio.NO_DEFAULT_VALUE,
            ('audio track 1', 'audio commentary track 2', 'audio track 3'),
            True,
        ),
        (
            mediainfo.audio.NO_DEFAULT_VALUE,
            ('audio track 1', 'audio track 2', 'audio track 3'),
            False,
        ),
        (
            mediainfo.audio.NO_DEFAULT_VALUE,
            (),
            False,
        ),
    ),
    ids=lambda v: str(v),
)
def test_has_commentary(default, audio_tracks, exp_result, mocker):
    mocks = Mock()
    if isinstance(audio_tracks, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=audio_tracks), 'lookup')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=audio_tracks), 'lookup')

    def _is_commentary(track):
        return 'commentary' in track

    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.audio._is_commentary', side_effect=_is_commentary), '_is_commentary')

    path = 'some/path'
    exp_mock_calls = [
        call.lookup(path, ('Audio',))
    ]

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.audio.has_commentary(path, default=default)
    else:
        return_value = mediainfo.audio.has_commentary(path, default=default)
        assert return_value == exp_result

        if not isinstance(audio_tracks, Exception):
            commentary_track_found = False
            for track in audio_tracks:
                exp_mock_calls.append(call._is_commentary(track))
                if 'commentary' in track:
                    commentary_track_found = True
                if commentary_track_found:
                    break

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='audio_track, exp_return_value',
    argvalues=(
        ({}, False),
        ({'Title': 'Commentary with Foo'}, True),
        ({'Title': 'Shlommentary'}, False),
        ({'Title': "Foo's commentary"}, True),
        ({'Title': "THE FRICKIN' COMMENTARY, OMG"}, True),
        ({'Title': "Director's Comments"}, True),
        ({'Title': "Director's Commentses"}, False),
    ),
    ids=lambda v: str(v),
)
def test__is_commentary(audio_track, exp_return_value):
    return_value = mediainfo.audio._is_commentary(audio_track)
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='path, default, exclude_commentary, is_disc, exp_return_value, exp_mock_calls',
    argvalues=(
        pytest.param(
            'some/path', 'some default', 'exclude commentary', True,
            (
                'audio', 'languages', 'for', 'disc', 'some/path',
            ),
            [
                call.is_disc('some/path', multidisc=True),
                call.get_audio_languages_from_discs('some/path', default='some default', exclude_commentary='exclude commentary'),
            ],
            id='BDMV',
        ),
        pytest.param(
            'some/path', 'some default', 'exclude commentary', False,
            (
                'audio', 'languages', 'for', 'files', 'in', 'some/path',
            ),
            [
                call.is_disc('some/path', multidisc=True),
                call.get_audio_languages_from_mediainfo('some/path', default='some default', exclude_commentary='exclude commentary'),
            ],
            id='Video file',
        ),
    ),
    ids=lambda v: str(v),
)
def test_get_audio_languages(
        path, default, exclude_commentary, is_disc,
        exp_return_value, exp_mock_calls,
        mocker,
):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_disc', return_value=is_disc), 'is_disc')

    def get_audio_languages_from_discs():
        return ('audio', 'languages', 'for', 'disc', path)

    mocks.attach_mock(
        mocker.patch(
            'upsies.utils.mediainfo.audio.get_audio_languages_from_discs',
            side_effect=lambda path, *_, **__: ('audio', 'languages', 'for', 'disc', path),
        ),
        'get_audio_languages_from_discs',
    )
    mocks.attach_mock(
        mocker.patch(
            'upsies.utils.mediainfo.audio.get_audio_languages_from_mediainfo',
            side_effect=lambda path, *_, **__: ('audio', 'languages', 'for', 'files', 'in', path),
        ),
        'get_audio_languages_from_mediainfo',
    )

    exclude_commentary = 'exclude commentary'
    return_value = mediainfo.audio.get_audio_languages(path, default, exclude_commentary=exclude_commentary)
    assert return_value == exp_return_value
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='path, default, audio_tracks, exclude_commentary, exp_result, exp_mock_calls',
    argvalues=(
        pytest.param(
            'some/path',
            mediainfo.audio.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            True,
            errors.ContentError('No such file'),
            [
                call.lookup('some/path', ('Audio',)),
            ],
            id='lookup() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'some/path',
            'foo',
            errors.ContentError('No such file'),
            True,
            'foo',
            [
                call.lookup('some/path', ('Audio',)),
            ],
            id='lookup() raises ContentError, default=foo',
        ),
        pytest.param(
            'some/path',
            None,
            errors.ContentError('No such file'),
            True,
            None,
            [
                call.lookup('some/path', ('Audio',)),
            ],
            id='lookup() raises ContentError, default=None',
        ),

        pytest.param(
            'some/path',
            mediainfo.audio.NO_DEFAULT_VALUE,
            (
                {'Language': 'aa', 'Title': 'Some track'},
                {'Language': 'bb', 'Title': 'Some commentary track'},
                {'Language': 'cc'},
                {'Language': 'dd / DD', 'Title': 'Some weird track'},
                {'Title': 'Some track with no language'},
            ),
            True,
            ('language:aa', 'language:cc', 'language:dd'),
            [
                call.lookup('some/path', ('Audio',)),
                call._is_commentary({'Language': 'aa', 'Title': 'Some track'}),
                call.Language_get('aa'),
                call._is_commentary({'Language': 'bb', 'Title': 'Some commentary track'}),
                call._is_commentary({'Language': 'cc'}),
                call.Language_get('cc'),
                call._is_commentary({'Language': 'dd / DD', 'Title': 'Some weird track'}),
                call.Language_get('dd'),
                call._is_commentary({'Title': 'Some track with no language'}),
            ],
            id='exclude_commentary=True',
        ),

        pytest.param(
            'some/path',
            mediainfo.audio.NO_DEFAULT_VALUE,
            (
                {'Language': 'aa', 'Title': 'Some track'},
                {'Language': 'bb', 'Title': 'Some commentary track'},
                {'Language': 'cc'},
                {'Language': 'dd / DD', 'Title': 'Some weird track'},
                {'Title': 'Some track with no language'},
            ),
            False,
            ('language:aa', 'language:bb', 'language:cc', 'language:dd'),
            [
                call.lookup('some/path', ('Audio',)),
                call.Language_get('aa'),
                call.Language_get('bb'),
                call.Language_get('cc'),
                call.Language_get('dd'),
            ],
            id='exclude_commentary=False',
        ),

        pytest.param(
            'some/path',
            mediainfo.audio.NO_DEFAULT_VALUE,
            (
                {'Language': 'aa', 'Title': 'Some track'},
                {'Language': 'bb', 'Title': 'Some commentary track'},
                {'Language': '?!', 'Title': 'Not a valid language code'},
                {'Language': 'dd', 'Title': 'Some other track'},
                {'Title': 'Some track with no language'},
            ),
            False,
            ('language:aa', 'language:bb', '?!', 'language:dd'),
            [
                call.lookup('some/path', ('Audio',)),
                call.Language_get('aa'),
                call.Language_get('bb'),
                call.Language_get('?!'),
                call.Language_get('dd'),
            ],
            id='Invalid language code',
        ),

        pytest.param(
            'some/path',
            'unknown',
            (
                {'Language': 'aa', 'Title': 'Some track'},
                {'Language': 'bb', 'Title': 'Some commentary track'},
                {'Language': 'cc'},
                {'Language': 'dd / DD', 'Title': 'Some weird track'},
                {'Title': 'Some track with no language'},
            ),
            True,
            ('language:aa', 'language:cc', 'language:dd', 'unknown'),
            [
                call.lookup('some/path', ('Audio',)),
                call._is_commentary({'Language': 'aa', 'Title': 'Some track'}),
                call.Language_get('aa'),
                call._is_commentary({'Language': 'bb', 'Title': 'Some commentary track'}),
                call._is_commentary({'Language': 'cc'}),
                call.Language_get('cc'),
                call._is_commentary({'Language': 'dd / DD', 'Title': 'Some weird track'}),
                call.Language_get('dd'),
                call._is_commentary({'Title': 'Some track with no language'}),
            ],
            id='Unknown language with default provided',
        ),
    ),
    ids=lambda v: str(v),
)
def test_get_audio_languages_from_mediainfo(path, default, audio_tracks, exclude_commentary, exp_result, exp_mock_calls, mocker):
    mocks = Mock()
    if isinstance(audio_tracks, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=audio_tracks), 'lookup')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=audio_tracks), 'lookup')

    def _is_commentary(track):
        return 'commentary' in track.get('Title', '')

    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.audio._is_commentary', side_effect=_is_commentary), '_is_commentary')

    def Language_get(language):
        if language.isalpha():
            return Mock(language=f'language:{language}')
        else:
            raise ValueError(f'No such language: {language}')

    mocks.attach_mock(mocker.patch('langcodes.Language.get', side_effect=Language_get), 'Language_get')

    path = 'some/path'
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.audio.get_audio_languages_from_mediainfo(path, default=default, exclude_commentary=exclude_commentary)
    else:
        return_value = mediainfo.audio.get_audio_languages_from_mediainfo(path, default=default, exclude_commentary=exclude_commentary)
        assert return_value == exp_result

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames=(
        'path_exists, path, default, exclude_commentary,'
        'is_bluray, is_dvd, disc_paths, playlists,'
        'exp_result, exp_mock_calls,'
    ),
    argvalues=(
        pytest.param(
            False, 'some/path', mediainfo.audio.NO_DEFAULT_VALUE, '<exclude_commentary>',
            False, False, (), (),
            errors.ContentError('No such file or directory: some/path'),
            [call.path_exists('some/path')],
            id='Path does not exist, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            False, 'some/path', 'foo', '<exclude_commentary>',
            False, False, (), (),
            'foo',
            [call.path_exists('some/path')],
            id='Path does not exist, default=foo',
        ),
        pytest.param(
            False, 'some/path', None, '<exclude_commentary>',
            False, False, (), (),
            None,
            [call.path_exists('some/path')],
            id='Path does not exist, default=None',
        ),

        pytest.param(
            True, 'some/path', mediainfo.audio.NO_DEFAULT_VALUE, '<exclude_commentary>',
            True, False,  # is_bluray
            (
                'some/path/disc1',
                'some/path/disc2',
            ),
            (
                (
                    Mock(filepath='some/path/disc1/bdmv/playlist/00001.mpls', is_main=False),
                    Mock(filepath='some/path/disc1/bdmv/playlist/00002.mpls', is_main=True),
                    Mock(filepath='some/path/disc1/bdmv/playlist/00003.mpls', is_main=False),
                    Mock(filepath='some/path/disc1/bdmv/playlist/00004.mpls', is_main=True),
                ),
                (
                    Mock(filepath='some/path/disc2/bdmv/playlist/00001.mpls', is_main=False),
                    Mock(filepath='some/path/disc2/bdmv/playlist/00002.mpls', is_main=True),
                    Mock(filepath='some/path/disc2/bdmv/playlist/00003.mpls', is_main=True),
                    Mock(filepath='some/path/disc2/bdmv/playlist/00004.mpls', is_main=False),
                ),
            ),
            (
                'audio', 'languages', 'from', 'some/path/disc1/bdmv/playlist/00002.mpls',
                'audio', 'languages', 'from', 'some/path/disc1/bdmv/playlist/00004.mpls',
                'audio', 'languages', 'from', 'some/path/disc2/bdmv/playlist/00002.mpls',
                'audio', 'languages', 'from', 'some/path/disc2/bdmv/playlist/00003.mpls',
            ),
            [
                call.path_exists('some/path'),
                call.is_bluray('some/path', multidisc=True),
                call.bluray_get_disc_paths('some/path'),
                call.bluray_get_playlists('some/path/disc1'),
                call.get_audio_languages_from_mediainfo(
                    'some/path/disc1/bdmv/playlist/00002.mpls',
                    default=mediainfo.audio.NO_DEFAULT_VALUE,
                    exclude_commentary='<exclude_commentary>',
                ),
                call.get_audio_languages_from_mediainfo(
                    'some/path/disc1/bdmv/playlist/00004.mpls',
                    default=mediainfo.audio.NO_DEFAULT_VALUE,
                    exclude_commentary='<exclude_commentary>',
                ),
                call.bluray_get_playlists('some/path/disc2'),
                call.get_audio_languages_from_mediainfo(
                    'some/path/disc2/bdmv/playlist/00002.mpls',
                    default=mediainfo.audio.NO_DEFAULT_VALUE,
                    exclude_commentary='<exclude_commentary>',
                ),
                call.get_audio_languages_from_mediainfo(
                    'some/path/disc2/bdmv/playlist/00003.mpls',
                    default=mediainfo.audio.NO_DEFAULT_VALUE,
                    exclude_commentary='<exclude_commentary>',
                ),
            ],
            id='path is bluray',
        ),

        pytest.param(
            True, 'some/path', mediainfo.audio.NO_DEFAULT_VALUE, '<exclude_commentary>',
            False, True,  # is_dvd
            (
                'some/path/disc1',
                'some/path/disc2',
            ),
            (
                (
                    Mock(filepath='some/path/disc1/video_ts/vts_01_0.ifo', is_main=False),
                    Mock(filepath='some/path/disc1/video_ts/vts_02_0.ifo', is_main=True),
                    Mock(filepath='some/path/disc1/video_ts/vts_03_0.ifo', is_main=False),
                    Mock(filepath='some/path/disc1/video_ts/vts_04_0.ifo', is_main=True),
                ),
                (
                    Mock(filepath='some/path/disc2/video_ts/vts_01_0.ifo', is_main=False),
                    Mock(filepath='some/path/disc2/video_ts/vts_02_0.ifo', is_main=True),
                    Mock(filepath='some/path/disc2/video_ts/vts_03_0.ifo', is_main=True),
                    Mock(filepath='some/path/disc2/video_ts/vts_04_0.ifo', is_main=False),
                ),
            ),
            (
                'audio', 'languages', 'from', 'some/path/disc1/video_ts/vts_02_0.ifo',
                'audio', 'languages', 'from', 'some/path/disc1/video_ts/vts_04_0.ifo',
                'audio', 'languages', 'from', 'some/path/disc2/video_ts/vts_02_0.ifo',
                'audio', 'languages', 'from', 'some/path/disc2/video_ts/vts_03_0.ifo',
            ),
            [
                call.path_exists('some/path'),
                call.is_bluray('some/path', multidisc=True),
                call.is_dvd('some/path', multidisc=True),
                call.dvd_get_disc_paths('some/path'),
                call.dvd_get_playlists('some/path/disc1'),
                call.get_audio_languages_from_mediainfo(
                    'some/path/disc1/video_ts/vts_02_0.ifo',
                    default=mediainfo.audio.NO_DEFAULT_VALUE,
                    exclude_commentary='<exclude_commentary>',
                ),
                call.get_audio_languages_from_mediainfo(
                    'some/path/disc1/video_ts/vts_04_0.ifo',
                    default=mediainfo.audio.NO_DEFAULT_VALUE,
                    exclude_commentary='<exclude_commentary>',
                ),
                call.dvd_get_playlists('some/path/disc2'),
                call.get_audio_languages_from_mediainfo(
                    'some/path/disc2/video_ts/vts_02_0.ifo',
                    default=mediainfo.audio.NO_DEFAULT_VALUE,
                    exclude_commentary='<exclude_commentary>',
                ),
                call.get_audio_languages_from_mediainfo(
                    'some/path/disc2/video_ts/vts_03_0.ifo',
                    default=mediainfo.audio.NO_DEFAULT_VALUE,
                    exclude_commentary='<exclude_commentary>',
                ),
            ],
            id='path is dvd',
        ),

        pytest.param(
            True, 'some/path', mediainfo.audio.NO_DEFAULT_VALUE, '<exclude_commentary>',
            False, False,  # not is_bluray, not is_dvd
            (),
            (),
            (),
            [
                call.path_exists('some/path'),
                call.is_bluray('some/path', multidisc=True),
                call.is_dvd('some/path', multidisc=True),
            ],
            id='path is not a disc',
        ),
    ),
    ids=lambda v: str(v),
)
def test_get_audio_languages_from_discs(
        path_exists, path, default, exclude_commentary,
        is_bluray, is_dvd, disc_paths, playlists,
        exp_result, exp_mock_calls,
        mocker,
):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_bluray', return_value=is_bluray), 'is_bluray')
    mocks.attach_mock(mocker.patch('upsies.utils.disc.is_dvd', return_value=is_dvd), 'is_dvd')
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.bluray.get_disc_paths', return_value=disc_paths),
        'bluray_get_disc_paths',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.bluray.get_playlists', side_effect=playlists),
        'bluray_get_playlists',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.dvd.get_disc_paths', return_value=disc_paths),
        'dvd_get_disc_paths',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.disc.dvd.get_playlists', side_effect=playlists),
        'dvd_get_playlists',
    )

    def get_audio_languages_from_mediainfo(filepath, *_, **__):
        return ('audio', 'languages', 'from', filepath)

    mocks.attach_mock(
        mocker.patch('upsies.utils.mediainfo.audio.get_audio_languages_from_mediainfo', side_effect=get_audio_languages_from_mediainfo),
        'get_audio_languages_from_mediainfo',
    )

    path = 'some/path'
    if isinstance(exp_result, Exception):
        with patch('os.path.exists', return_value=path_exists) as exists_mock:
            mocks.attach_mock(exists_mock, 'path_exists')
            with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
                mediainfo.audio.get_audio_languages_from_discs(path, default=default, exclude_commentary=exclude_commentary)
    else:
        with patch('os.path.exists', return_value=path_exists) as exists_mock:
            mocks.attach_mock(exists_mock, 'path_exists')
            return_value = mediainfo.audio.get_audio_languages_from_discs(path, default=default, exclude_commentary=exclude_commentary)
            assert return_value == exp_result
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='default, all_tracks, audio_track, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.audio.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            None,
            errors.ContentError('No such file'),
            id='get_tracks() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            errors.ContentError('No such file'),
            None,
            'foo',
            id='get_tracks() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            errors.ContentError('No such file'),
            None,
            None,
            id='get_tracks() raises ContentError, default=None',
        ),

        pytest.param(
            mediainfo.audio.NO_DEFAULT_VALUE,
            {'Audio': ...},
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='lookup() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            {'Audio': ...},
            errors.ContentError('No such file'),
            'foo',
            id='lookup() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            {'Audio': ...},
            errors.ContentError('No such file'),
            None,
            id='lookup() raises ContentError, default=None',
        ),

        pytest.param(
            None,
            {},
            None,
            '',
            id='path contains no audio tracks',
        ),

        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'AAC', 'Format_AdditionalFeatures': 'LC'}, 'AAC'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'AC-3'}, 'DD'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'E-AC-3'}, 'DDP'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'E-AC-3', 'Format_Commercial_IfAny': 'Dolby Digital Plus with Dolby Atmos'}, 'DDP Atmos'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'MLP FBA', 'Format_Commercial_IfAny': 'Dolby TrueHD'}, 'TrueHD'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'MLP FBA', 'Format_Commercial_IfAny': 'Dolby TrueHD with Dolby Atmos'}, 'TrueHD Atmos'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'DTS'}, 'DTS'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'DTS', 'Format_Commercial_IfAny': 'DTS-ES Matrix'}, 'DTS-ES'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'DTS', 'Format_Commercial_IfAny': 'DTS-ES Discrete'}, 'DTS-ES'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'DTS', 'Format_Commercial_IfAny': 'DTS-HD High Resolution Audio', 'Format_AdditionalFeatures': 'XBR'}, 'DTS-HD'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'DTS', 'Format_Commercial_IfAny': 'DTS-HD Master Audio', 'Format_AdditionalFeatures': 'XLL'}, 'DTS-HD MA'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'DTS', 'Format_Commercial_IfAny': 'DTS-HD Master Audio', 'Format_AdditionalFeatures': 'XLL X'}, 'DTS:X'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'FLAC'}, 'FLAC'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'MPEG Audio'}, 'MP3'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'Vorbis'}, 'Vorbis'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'Opus'}, 'Opus'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Audio': ...}, {'Format': 'PCM'}, 'PCM'),

        pytest.param(
            mediainfo.audio.NO_DEFAULT_VALUE,
            {'Audio': ...},
            {'Format': 'Unknown Audio Format'},
            errors.ContentError('Unable to detect audio format'),
            id='Unknown audio track, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            {'Audio': ...},
            {'Format': 'Unknown Audio Format'},
            'foo',
            id='Unknown audio track, default=foo',
        ),
        pytest.param(
            None,
            {'Audio': ...},
            {'Format': 'Unknown Audio Format'},
            None,
            id='Unknown audio track, default=None',
        ),
    ),
    ids=lambda v: str(v),
)
def test_get_audio_format(default, audio_track, all_tracks, exp_result, mocker):
    mocks = Mock()
    if isinstance(audio_track, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=audio_track), 'lookup')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=audio_track), 'lookup')
    if isinstance(all_tracks, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.get_tracks', side_effect=all_tracks), 'get_tracks')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.get_tracks', return_value=all_tracks), 'get_tracks')

    path = 'some/path'
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.audio.get_audio_format(path, default=default)
    else:
        return_value = mediainfo.audio.get_audio_format(path, default=default)
        assert return_value == exp_result
    assert mocks.mock_calls == [
        call.get_tracks(path),
    ] + (
        []
        if (
                isinstance(all_tracks, Exception)
                or 'Audio' not in all_tracks
        ) else
        [call.lookup(path, ('Audio', 'DEFAULT'))]
    )


@pytest.mark.parametrize(
    argnames='default, audio_track, exp_result',
    argvalues=(
        pytest.param(
            mediainfo.audio.NO_DEFAULT_VALUE,
            errors.ContentError('No such file'),
            errors.ContentError('No such file'),
            id='lookup() raises ContentError, default=NO_DEFAULT_VALUE',
        ),
        pytest.param(
            'foo',
            errors.ContentError('No such file'),
            'foo',
            id='lookup() raises ContentError, default=foo',
        ),
        pytest.param(
            None,
            errors.ContentError('No such file'),
            None,
            id='lookup() raises ContentError, default=None',
        ),

        (mediainfo.audio.NO_DEFAULT_VALUE, {}, ''),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Channels': '1'}, '1.0'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Channels': '2'}, '2.0'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Channels': '3'}, '2.0'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Channels': '4'}, '2.0'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Channels': '5'}, '2.0'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Channels': '6'}, '5.1'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Channels': '7'}, '5.1'),
        (mediainfo.audio.NO_DEFAULT_VALUE, {'Channels': '8'}, '7.1'),
    ),
    ids=lambda value: str(value),
)
def test_get_audio_channels(default, audio_track, exp_result, mocker):
    mocks = Mock()
    if isinstance(audio_track, Exception):
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', side_effect=audio_track), 'lookup')
    else:
        mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.lookup', return_value=audio_track), 'lookup')

    path = 'some/path'
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            mediainfo.audio.get_audio_channels(path, default=default)
    else:
        return_value = mediainfo.audio.get_audio_channels(path, default=default)
        assert return_value == exp_result
    assert mocks.mock_calls == [call.lookup(path, ('Audio', 'DEFAULT'))]
