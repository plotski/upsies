import pytest


@pytest.fixture(autouse=True)
def clear_functools_cache(request):
    import inspect

    from upsies.utils import mediainfo

    for module in (mediainfo, mediainfo.audio, mediainfo.video, mediainfo.text):
        # getmembers() returns `(name, object)` tuples, but we are only interested in the `objects`.
        _function_names, functions = zip(*inspect.getmembers(module, predicate=callable))
        for function in functions:
            if hasattr(function, 'cache_clear'):
                print('Clearing functools.cache:', function)
                function.cache_clear()
