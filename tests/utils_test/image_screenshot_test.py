import os
import re
from unittest.mock import Mock, call, patch

import pytest

from upsies import errors, utils

VF_FLAGS = (
    'full_chroma_int',
    'full_chroma_inp',
    'accurate_rnd',
    'spline',
)

@pytest.mark.parametrize(
    argnames='video_file, timestamp, screenshot_file, color_matrix, is_bluray, exp_args',
    argvalues=(
        pytest.param(
            'video.mkv', 123, 'foo%.png', 'BT.2020', False,
            (
                '-y', '-hide_banner', '-loglevel', 'error',
                '-ss', '123',
                '-i', 'file:video.mkv',
                '-vf', ':'.join((
                    "scale='max(sar,1)*iw':'max(1/sar,1)*ih'",
                    'in_h_chr_pos=0',
                    'in_v_chr_pos=0',
                    'in_color_matrix=bt2020',
                    'flags=' + '+'.join(VF_FLAGS),
                )),
                '-pix_fmt', 'rgb24', '-vframes', '1',
                'file:foo%%.png',
            ),
            id='BT.2020, non-BDMV',
        ),
        pytest.param(
            'video.mkv', 123, 'foo%.png', 'BT.2020', True,
            (
                '-y', '-hide_banner', '-loglevel', 'error',
                '-ss', '123',
                '-i', 'file:video.mkv',
                '-vf', 'fps=1/60,' + ':'.join((
                    "scale='max(sar,1)*iw':'max(1/sar,1)*ih'",
                    'in_h_chr_pos=0',
                    'in_v_chr_pos=0',
                    'in_color_matrix=bt2020',
                    'flags=' + '+'.join(VF_FLAGS),
                )),
                '-pix_fmt', 'rgb24', '-vframes', '1',
                'file:foo%%.png',
            ),
            id='BT.2020, BDMV (is_bluray)',
        ),
        pytest.param(
            'video.mkv', 123, 'foo%.png', 'BT.709', False,
            (
                '-y', '-hide_banner', '-loglevel', 'error',
                '-ss', '123',
                '-i', 'file:video.mkv',
                '-vf', ':'.join((
                    "scale='max(sar,1)*iw':'max(1/sar,1)*ih'",
                    'in_h_chr_pos=0',
                    'in_v_chr_pos=128',
                    'in_color_matrix=bt709',
                    'flags=' + '+'.join(VF_FLAGS),
                )),
                '-pix_fmt', 'rgb24', '-vframes', '1',
                'file:foo%%.png',
            ),
            id='BT.709, non-BDMV',
        ),
        pytest.param(
            'video.m2ts', 123, 'foo%.png', 'BT.709', False,
            (
                '-y', '-hide_banner', '-loglevel', 'error',
                '-ss', '123',
                '-i', 'file:video.m2ts',
                '-vf', 'fps=1/60,' + ':'.join((
                    "scale='max(sar,1)*iw':'max(1/sar,1)*ih'",
                    'in_h_chr_pos=0',
                    'in_v_chr_pos=128',
                    'in_color_matrix=bt709',
                    'flags=' + '+'.join(VF_FLAGS),
                )),
                '-pix_fmt', 'rgb24', '-vframes', '1',
                'file:foo%%.png',
            ),
            id='BT.709, BDMV (m2ts)',
        ),
        pytest.param(
            'video.mkv', 123, 'foo%.png', 'BT.601', False,
            (
                '-y', '-hide_banner', '-loglevel', 'error',
                '-ss', '123',
                '-i', 'file:video.mkv',
                '-vf', ':'.join((
                    "scale='max(sar,1)*iw':'max(1/sar,1)*ih'",
                    'in_h_chr_pos=0',
                    'in_v_chr_pos=128',
                    'in_color_matrix=bt601',
                    'flags=' + '+'.join(VF_FLAGS),
                )),
                '-pix_fmt', 'rgb24', '-vframes', '1',
                'file:foo%%.png',
            ),
            id='BT.601, non-BDMV',
        ),
        pytest.param(
            'video.mkv', 123, 'foo%.png', 'BT.601', True,
            (
                '-y', '-hide_banner', '-loglevel', 'error',
                '-ss', '123',
                '-i', 'file:video.mkv',
                '-vf', 'fps=1/60,' + ':'.join((
                    "scale='max(sar,1)*iw':'max(1/sar,1)*ih'",
                    'in_h_chr_pos=0',
                    'in_v_chr_pos=128',
                    'in_color_matrix=bt601',
                    'flags=' + '+'.join(VF_FLAGS),
                )),
                '-pix_fmt', 'rgb24', '-vframes', '1',
                'file:foo%%.png',
            ),
            id='BT.601, BDMV',
        ),
        pytest.param(
            'video.mkv', 123, 'foo%.png', 'BT.foo', False,
            (
                '-y', '-hide_banner', '-loglevel', 'error',
                '-ss', '123',
                '-i', 'file:video.mkv',
                '-vf', ':'.join((
                    "scale='max(sar,1)*iw':'max(1/sar,1)*ih'",
                    'flags=' + '+'.join(VF_FLAGS),
                )),
                '-pix_fmt', 'rgb24', '-vframes', '1',
                'file:foo%%.png',
            ),
            id='No color matrix, non-BDMV',
        ),
        pytest.param(
            'video.m2ts', 123, 'foo%.png', 'BT.foo', False,
            (
                '-y', '-hide_banner', '-loglevel', 'error',
                '-ss', '123',
                '-i', 'file:video.m2ts',
                '-vf', 'fps=1/60,' + ':'.join((
                    "scale='max(sar,1)*iw':'max(1/sar,1)*ih'",
                    'flags=' + '+'.join(VF_FLAGS),
                )),
                '-pix_fmt', 'rgb24', '-vframes', '1',
                'file:foo%%.png',
            ),
            id='No color matrix, BDMV (m2ts)',
        ),
    ),
    ids=lambda v: str(v),
)
def test_make_screenshot_cmd(video_file, timestamp, screenshot_file, is_bluray, color_matrix, exp_args, mocker):
    mocker.patch('upsies.utils.image._ffmpeg_executable', return_value='ffmpeg.executable')
    mocker.patch('upsies.utils.disc.is_bluray', return_value=is_bluray)
    mocker.patch('upsies.utils.mediainfo.video.is_bt601', return_value=color_matrix == 'BT.601')
    mocker.patch('upsies.utils.mediainfo.video.is_bt709', return_value=color_matrix == 'BT.709')
    mocker.patch('upsies.utils.mediainfo.video.is_bt2020', return_value=color_matrix == 'BT.2020')

    cmd = utils.image._make_screenshot_cmd(video_file, timestamp, screenshot_file)
    assert cmd == ('ffmpeg.executable', *exp_args)


@pytest.fixture
def screenshot_mocks(mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.fs.assert_dir_usable'), 'assert_dir_usable')
    mocks.attach_mock(mocker.patch('upsies.utils.fs.assert_file_readable'), 'assert_file_readable')
    mocks.attach_mock(mocker.patch('upsies.utils.fs.sanitize_path'), 'sanitize_path')
    mocks.sanitize_path.side_effect = lambda path: f'sanitized:{path}'
    mocks.attach_mock(mocker.patch('upsies.utils.mediainfo.get_duration'), 'get_duration')
    mocks.get_duration.return_value = utils.types.Timestamp('1:00:00')
    mocks.attach_mock(mocker.patch('upsies.utils.image._make_screenshot_cmd'), '_make_screenshot_cmd')
    mocks.attach_mock(mocker.patch('upsies.utils.subproc.run'), 'run')
    return mocks


def test_screenshot_gets_unreadable_video_file(screenshot_mocks):
    mocks = screenshot_mocks
    mocks.assert_file_readable.side_effect = errors.ContentError('File is unreadable')
    with pytest.raises(errors.ScreenshotError, match=r'^File is unreadable$'):
        utils.image.screenshot('path/to/video_file', '01:23:45', 'path/to/screenshot.png')
    assert mocks.mock_calls == [
        call.assert_file_readable('path/to/video_file'),
    ]


@pytest.mark.parametrize(
    argnames='timestamp, exp_timestamp',
    argvalues=(
        ((1, 2, 3), errors.ScreenshotError('Invalid timestamp: (1, 2, 3)')),
        ('foo', errors.ScreenshotError("Invalid timestamp: 'foo'")),
    ),
    ids=lambda v: repr(v),
)
def test_screenshot_validates_timestamp(timestamp, exp_timestamp, screenshot_mocks):
    mocks = screenshot_mocks
    exp_exception = exp_timestamp
    with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
        utils.image.screenshot('path/to/video_file', timestamp, 'path/to/screenshot.png')
    assert mocks.mock_calls == [
        call.assert_file_readable('path/to/video_file'),
    ]


@pytest.mark.parametrize('timestamp', ('1:23:46',))
def test_screenshot_gets_timestamp_larger_than_duration(timestamp, screenshot_mocks):
    mocks = screenshot_mocks
    timestamp = utils.types.Timestamp(timestamp)
    mocks.get_duration.return_value = '1:23:45'
    exp_msg = f'Timestamp is too close to or after end of video (1:23:45): {timestamp}'
    with pytest.raises(errors.ScreenshotError, match=rf'^{re.escape(str(exp_msg))}'):
        utils.image.screenshot('path/to/video_file', timestamp, 'path/to/screenshot.png')
    assert mocks.mock_calls == [
        call.assert_file_readable('path/to/video_file'),
        call.sanitize_path('path/to/screenshot.png'),
        call.get_duration('path/to/video_file'),
    ]


@pytest.mark.parametrize(
    argnames='screenshot_file_created, exp_result',
    argvalues=(
        (False, errors.ScreenshotError(
            '{video_file}: Failed to create screenshot at {timestamp}: {run_return_value}\n'
            "ffmpeg -o '{screenshot_file}'"
        )),
        (True, 'sanitized:{screenshot_file}'),
    ),
    ids=lambda v: repr(v),
)
def test_screenshot_creates_screenshot_file(screenshot_file_created, exp_result, screenshot_mocks):
    mocks = screenshot_mocks
    video_file = 'path/to/video.mkv'
    timestamp = utils.types.Timestamp('12:34')
    screenshot_file = 'path/to/my screenshot.png'
    mocks.get_duration.return_value = '1:23:45'
    mocks._make_screenshot_cmd.return_value = ('ffmpeg', '-o', 'path/to/my screenshot.png')
    mocks.run.return_value = '<output from ffmpeg>'

    if isinstance(exp_result, Exception):
        exp_msg = str(exp_result).format(
            video_file=video_file,
            timestamp=timestamp,
            run_return_value=mocks.run.return_value,
            screenshot_file=screenshot_file,
        )
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_msg))}'):
            with patch('os.path.exists', return_value=screenshot_file_created):
                utils.image.screenshot(video_file, timestamp, screenshot_file)
    else:
        with patch('os.path.exists', return_value=screenshot_file_created):
            return_value = utils.image.screenshot(video_file, timestamp, screenshot_file)
        assert return_value == exp_result.format(screenshot_file=screenshot_file)

    assert mocks.mock_calls == [
        call.assert_file_readable(video_file),
        call.sanitize_path(screenshot_file),
        call.get_duration(video_file),
        call._make_screenshot_cmd(video_file, timestamp, f'sanitized:{screenshot_file}'),
        call.run(mocks._make_screenshot_cmd.return_value, ignore_errors=True, join_stderr=True),
    ]


def test_screenshot_has_correct_display_aspect_ratio(data_dir, tmp_path, mocker):
    video_file = os.path.join(data_dir, 'video', 'aspect_ratio.mkv')
    screenshot_file = str(tmp_path / 'image.jpg')
    screenshot_path = utils.image.screenshot(video_file, 0, screenshot_file)
    assert screenshot_path == screenshot_file

    mocker.patch('upsies.utils.fs.find_main_video', side_effect=lambda path: path)
    tracks = utils.mediainfo.get_tracks(screenshot_file)
    width = int(tracks['Image'][0]['Width'])
    height = int(tracks['Image'][0]['Height'])
    assert 1279 <= width <= 1280
    assert height == 534
