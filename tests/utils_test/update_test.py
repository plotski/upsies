import random
from unittest.mock import AsyncMock, Mock, call

import pytest

from upsies import __version__
from upsies.utils import update


def test_current_is_prerelease(mocker):
    parse_version_mock = mocker.patch('upsies.utils.update.parse_version', Mock())
    return_value = update.current_is_prerelease()
    assert return_value is parse_version_mock.return_value.is_prerelease
    assert parse_version_mock.call_args_list == [call(__version__)]


@pytest.mark.parametrize(
    argnames='current, release, exp_return_value',
    argvalues=(
        ('2024.08.14', '2024.08.15', '2024.08.15'),
        ('2024.08.15', '2024.08.15', None),
        ('2024.08.16', '2024.08.15', None),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_get_newer_release(current, release, exp_return_value, mocker):
    mocker.patch('upsies.utils.update.__version__', current)
    mocker.patch('upsies.utils.update._get_latest_release', AsyncMock(return_value=release))
    return_value = await update.get_newer_release()
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='current, prerelease, release, exp_return_value, exp_mock_calls',
    argvalues=(
        ('2024.08.14', '2024.08.15alpha', '<release>', '2024.08.15alpha', [
            call._get_latest_prerelease(),
        ]),
        ('2024.08.15', '2024.08.15alpha', '<release>', '<release>', [
            call._get_latest_prerelease(),
            call.get_newer_release(),
        ]),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_get_newer_prerelease(current, prerelease, release, exp_return_value, exp_mock_calls, mocker):
    mocks = Mock()
    mocker.patch('upsies.utils.update.__version__', current)
    mocks.attach_mock(
        mocker.patch('upsies.utils.update._get_latest_prerelease', AsyncMock(return_value=prerelease)),
        '_get_latest_prerelease',
    )
    mocks.attach_mock(
        mocker.patch('upsies.utils.update.get_newer_release', AsyncMock(return_value=release)),
        'get_newer_release',
    )
    return_value = await update.get_newer_prerelease()
    assert return_value == exp_return_value
    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='versions, exp_latest_version',
    argvalues=(
        (['2021.6.20', '2023.4.7', '2021.8.10'], '2023.4.7'),
        (['2021.8.27', '2021.10.9', '2021.9.20'], '2021.10.9'),
        (['2021.10.3', '2021.10.10', '2021.10.9'], '2021.10.10'),
        ([], None),
    ),
    ids=lambda v: str(v),
)
@pytest.mark.asyncio
async def test_get_latest_release(versions, exp_latest_version, mocker):
    random.shuffle(versions)
    response_mock = Mock(
        json=Mock(return_value={
            'releases': {v: f'info about {v!r}' for v in versions},
        }),
    )
    get_mock = mocker.patch('upsies.utils.http.get', AsyncMock(return_value=response_mock))
    latest_version = await update._get_latest_release()
    if exp_latest_version:
        assert latest_version == update._fix_version(exp_latest_version)
    else:
        assert latest_version is None
    assert get_mock.call_args_list == [
        call(
            url=update._PYPI_URL,
            timeout=update._REQUEST_TIMEOUT,
            cache=True,
            max_cache_age=update._MAX_CACHE_AGE,
        ),
    ]


@pytest.mark.parametrize(
    argnames='init_file_string, exp_latest_version',
    argvalues=(
        ("foo\n__version__='2021.06.20'\nbar\n", '2021.06.20'),
        ('foo\n__version__="2021.06.20"\nbar\n', '2021.06.20'),
        ("foo\n__version__ = '2021.06.20'\nbar\n", '2021.06.20'),
        ('foo\n__version__ = "2021.06.20"\nbar\n', '2021.06.20'),
        ("foo\n__venison__='2021.06.20'\nbar\n", None),
    ),
)
@pytest.mark.asyncio
async def test_get_latest_prerelease(init_file_string, exp_latest_version, mocker):
    get_mock = mocker.patch('upsies.utils.http.get', AsyncMock(return_value=init_file_string))
    latest_version = await update._get_latest_prerelease()
    if exp_latest_version:
        assert latest_version == exp_latest_version
    else:
        assert latest_version is None
    assert get_mock.call_args_list == [
        call(
            url=update._REPO_URL,
            timeout=update._REQUEST_TIMEOUT,
            cache=True,
            max_cache_age=update._MAX_CACHE_AGE,
        ),
    ]


@pytest.mark.parametrize(
    argnames='version, exp_version',
    argvalues=(
        ('0.0.1', '0.0.1'),
        ('2021.6.20', '2021.06.20'),
        ('2021.6.20.1', '2021.06.20.1'),
        ('2021.10.20', '2021.10.20'),
    ),
)
def test_fix_version(version, exp_version):
    assert update._fix_version(version) == exp_version


@pytest.mark.parametrize(
    argnames='changelogs, exp_return_value',
    argvalues=(
        pytest.param(
            (
                ('2024.??.??', 'Alpha changes'),
                ('2024.08.15', 'Stable changes'),
            ),
            'Stable changes',
            id='Top-most entry version is prerelease',
        ),
        pytest.param(
            (
                ('2024.08.16', 'Stable changes'),
                ('2024.08.15', 'Old stable changes'),
            ),
            'Stable changes',
            id='Top-most entry is regular release',
        ),
        pytest.param(
            (),
            '',
            id='No changelogs found',
        ),
    ),
)
@pytest.mark.asyncio
async def test_get_latest_release_changelog(changelogs, exp_return_value, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.update._get_recent_changelogs'), '_get_recent_changelogs')
    mocks._get_recent_changelogs.return_value = changelogs

    return_value = await update.get_latest_release_changelog()
    assert return_value == exp_return_value
    assert mocks.mock_calls == [call._get_recent_changelogs()]


@pytest.mark.parametrize(
    argnames='changelogs, exp_return_value',
    argvalues=(
        pytest.param(
            (
                ('2024.??.??', 'Alpha changes'),
                ('2024.08.15', 'Stable changes'),
            ),
            'Alpha changes',
            id='Top-most entry version is prerelease',
        ),
        pytest.param(
            (
                ('2024.08.16', 'Stable changes'),
                ('2024.08.15', 'Old stable changes'),
            ),
            'Stable changes',
            id='Top-most entry is regular release',
        ),
        pytest.param(
            (),
            '',
            id='No changelogs found',
        ),
    ),
)
@pytest.mark.asyncio
async def test_get_latest_changelog(changelogs, exp_return_value, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.update._get_recent_changelogs'), '_get_recent_changelogs')
    mocks._get_recent_changelogs.return_value = changelogs

    return_value = await update.get_latest_changelog()
    assert return_value == exp_return_value
    assert mocks.mock_calls == [call._get_recent_changelogs()]


@pytest.mark.parametrize(
    argnames='response, exp_changelogs',
    argvalues=(
        pytest.param(
            (
                '2024.??.??\n'
                '  * Stuff.\n'
                '  * Yadda yadda yadda.\n'
                '    Yip, yap, yup.\n'
                '  * More stuff.'
                '\n'
                '\n'
                '2024.08.15\n'
                '  * This.\n'
                '  * Bla, bla bla bla\n'
                '    bli blu blo blo bla.\n'
                '  * And that.'
            ),
            (
                ('2024.??.??', (
                    '  * Stuff.\n'
                    '  * Yadda yadda yadda.\n'
                    '    Yip, yap, yup.\n'
                    '  * More stuff.'
                )),
                ('2024.08.15', (
                    '  * This.\n'
                    '  * Bla, bla bla bla\n'
                    '    bli blu blo blo bla.\n'
                    '  * And that.'
                )),
            ),
            id='Most recent entry is unreleased',
        ),
        pytest.param(
            (
                '2024.09.20\n'
                '  * Stuff.\n'
                '  * Yadda yadda yadda.\n'
                '    Yip, yap, yup.\n'
                '  * More stuff.'
                '\n'
                '\n'
                '2024.08.15\n'
                '  * This.\n'
                '  * Bla, bla bla bla\n'
                '    bli blu blo blo bla.\n'
                '  * And that.'
            ),
            (
                ('2024.09.20', (
                    '  * Stuff.\n'
                    '  * Yadda yadda yadda.\n'
                    '    Yip, yap, yup.\n'
                    '  * More stuff.'
                )),
                ('2024.08.15', (
                    '  * This.\n'
                    '  * Bla, bla bla bla\n'
                    '    bli blu blo blo bla.\n'
                    '  * And that.'
                )),
            ),
            id='Most recent entry is released',
        ),
        pytest.param(
            (
                '2024.09.20\n'
                '  * A.\n'
                '\n'
                '\n'
                '2024.08.15\n'
                '  * B.\n'
                '\n'
                '\n'
                '2024.08.14\n'
                '  * C.\n'
                '\n'
                '\n'
                '2024.08.13\n'
                '  * D.\n'
                '\n'
                '\n'
                '2024.08.12\n'
                '  * E.\n'
                '\n'
                '\n'
                '2024.08.11\n'
                '  * F.\n'
            ),
            (
                ('2024.09.20', (
                    '  * A.'
                )),
                ('2024.08.15', (
                    '  * B.'
                )),
            ),
            id='Only recent entries are returned',
        ),
        pytest.param(
            'This is not a changelog response at all.',
            (),
            id='No changelog response',
        ),
    ),
)
@pytest.mark.asyncio
async def test__get_recent_changelogs(response, exp_changelogs, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('upsies.utils.http.get', return_value=response), 'get')
    assert await update._get_recent_changelogs() == exp_changelogs
    assert mocks.mock_calls == [
        call.get(
            url=update.__changelog__,
            timeout=update._REQUEST_TIMEOUT,
            cache=True,
            max_cache_age=update._MAX_CACHE_AGE,
        ),
    ]
