Installation
============

OS Support
----------

Any Linux or Unix distribution that provides the dependencies below should work.

For Windows, the instructions below reportedly work in WSL_.

Feel free to report any issues_ you find with your OS.

.. _issues: https://codeberg.org/plotski/upsies/issues
.. _WSL: https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux

Dependencies
------------

* `Python <https://www.python.org/>`_
* `mediainfo <https://mediaarea.net/en/MediaInfo>`_ (CLI version)
* `ffmpeg <https://ffmpeg.org/>`_ (optional: screenshot creation)
* `ffprobe <https://ffmpeg.org/>`_ (optional: faster video duration detection)
* `oxipng <https://github.com/shssoichiro/oxipng/>`_ (optional: shrink
  screenshots by ~40 %)

Installing Current Release
--------------------------

:ref:`pipx <installing/pipx>` is the recommended installation tool. It creates a
separate virtual environment for each installed Python package that contains all
the dependencies to avoid conflicts and to make it trivial to uninstall packges
completely.

.. _pipx: https://pipxproject.github.io/pipx/
.. _installing/pipx:

pipx (recommended)
^^^^^^^^^^^^^^^^^^

1. Try installing `pipx`_ with your package manager, e.g.

   .. code-block:: sh

      $ sudo apt install pipx

   If that fails, this command should install ``pipx`` in ``~/.local``:

   .. code-block:: sh

      $ pip install --user pipx

2. Make sure ``~/.local/bin`` is in your ``PATH``. If you don't know how to do
   that, try this command:

   .. code-block:: sh

      $ python3 -m pipx ensurepath

3. Install ``upsies`` with ``pipx``:

   .. code-block:: sh

      $ pipx install upsies

4. Uninstall ``upsies``:

   .. code-block:: sh

      $ pipx uninstall upsies

5. Upgrade to the latest release:

   .. code-block:: sh

      $ pipx uninstall upsies && pipx install upsies

   .. note:: You can also run ``pipx upgrade upsies``, but you might run into
             issues if dependencies change between releases.

pip
^^^

Installing with ``pip`` is messy because it installs everything in the same
environment and it doesn't provide any way to remove dependencies.

Only do this if you don't care or if :ref:`installing with pipx
<installing/pipx>` is not possible for some reason.

.. code-block:: sh

   $ # Install upsies
   $ pip install --user upsies
   $ # Update to the latest version
   $ pip install --user --upgrade upsies
   $ # Remove upsies (but not its dependencies)
   $ pip uninstall upsies

Installing from Git Repository
------------------------------

If you just want to run the current development version without having to clone,
you can also do that with `pipx`_.

You need pipx 0.15.0.0 or later.

.. code:: sh

   $ # Initial installation
   $ pipx install 'git+https://codeberg.org/plotski/upsies.git'
   $ # Upgrade existing installation to latest commit
   $ pipx install 'git+https://codeberg.org/plotski/upsies.git' --force

Installing Specific Version
---------------------------

You can install an older version if the installed release has a bug.

.. code-block:: sh

   $ pipx install upsies==<version> --force

See https://pypi.org/project/upsies/#history for a list of versions.

Podman
^^^^^^

There is a `shell script
<https://codeberg.org/plotski/upsies/src/branch/master/goodies/upsies-podman.sh>`_ that runs upsies
in a `Podman <https://podman.io/>`_ container base on `Alpine Linux
<https://www.alpinelinux.org/>`_.

.. code-block:: sh

   $ # Download script to your $PATH and make it executable.
   $ wget https://codeberg.org/plotski/upsies/raw/branch/master/goodies/upsies-podman.sh \
          -O ~/.local/bin/upsies
   $ chmod u+x ~/.local/bin/upsies
   $ # Read and follow the CONFIGURATION section and adjust the script.
   $ $EDITOR ~/.local/bin/upsies
   $ # Build the Podman image.
   $ upsies build
   $ # Use the script as you would use upsies normally.
   $ upsies submit --help
   $ # Upgrade the container image when there is a newer upsies version.
   $ upsies upgrade
   $ # Delete the upsies image and the Alpine base image.
   $ podman rmi upsies alpine
