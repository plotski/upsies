How to Set Up a Development Environment
=======================================

Setting up a development environment to work on a contribution or just play with the code is quite
simple.

1. Clone the repo.

   .. code:: sh

      $ git clone https://codeberg.org/plotski/upsies && cd upsies

2. Create a virtual environment that contains upsies and its dependencies as well as any development
   tools like `pytest <https://pytest.org>`_, `tox <https://tox.wiki>`_ and `ruff
   <https://docs.astral.sh/ruff/>`_.

   .. code:: sh

      $ make venv

3. Activate the virtual environment.

   .. code:: sh

      $ source venv/bin/activate

4. Run ``pytest`` to see if everything works as expected.

   .. code:: sh

      $ pytest

5. *(Optional)* Set up `pyenv <https://github.com/pyenv/pyenv>`_.

   This is very useful to run tests with older as well as newer Python versions.

   It's not as easy as it should be and way too complicated to explain here, but there's a tutorial:
   https://realpython.com/intro-to-pyenv/

   If you have multiple Python versions installed, you can run the tests for each one like this:

   .. code:: sh

      $ tox

From time to time it is a good idea to install a fresh virtual environment to make sure all the
latest versions are installed. I usually do this before making a new release.

.. code:: sh

   $ make clean && make venv && source venv/bin/activate
