Quickstart
==========

This is a very concise overview of how to get ``upsies`` up and running.

Read the :doc:`installation manual <./installation>` and the :doc:`user manual
<./manual>` for more details.

Installation
------------

.. code-block:: sh

   $ sudo apt install pipx ffmpeg mediainfo
   $ pipx install upsies

Help
----

.. code-block:: sh

   $ upsies -h
   $ upsies <command> -h
   $ upsies submit <tracker> --howto-setup

Configuration
-------------

.. code-block:: sh

   $ upsies set --dump  # Create ~/.config/upsies/*.ini
   $ $EDITOR ~/.config/upsies/*.ini
   $ upsies set  # List options
   $ upsies set -h  # Explain options
   $ upsies set <option> <value>

Submission
----------

.. code-block:: sh

   $ upsies submit -h
   $ upsies submit <tracker> -h
   $ upsies submit <tracker> path/to/content

Upgrading
---------

.. code-block:: sh

   $ pipx uninstall upsies ; pipx install upsies

Uninstalling
------------

.. code-block:: sh

   $ pipx uninstall upsies
