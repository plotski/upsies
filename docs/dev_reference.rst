Developer Reference
===================

.. _dev_reference-design:

Design
------

``upsies`` uses multiple abstraction layers for better maintability and extensibility. This also
makes it possible to use ``upsies`` as a toolkit for your own Python projects.

In general, functions and classes take everything they need as arguments to make them more
predictable and easier to replace if necessary.

Utilities
^^^^^^^^^

:mod:`Utilities <upsies.utils>` are the lowest abstraction layer. They do the most basic work like
performing an HTTP request or executing a subprocess.

Utilities also provide more high-level tools for creating torrents and screenshots, querying online
databases, getting information from a video container, etc. They produce convenient objects and
exceptions with useful error messages that are ready to be passed on to the user.

Jobs
^^^^

:mod:`Jobs <upsies.jobs>` are the intermediaries between utilities and a user interface. They pass
on data from utilities to the UI and they provide convenient methods for processing user input.

Each job consists of one or more :class:`tasks <asyncio.Task>`. While the job is running, more tasks
can be added and running tasks can be cancelled. A job is finished when all its tasks are done.

For example, :class:`~.jobs.screenshots.ScreenshotsJob` takes a video file, the desired number of
screenshots and an optional list of user-provided timestamps. It uses :mod:`.utils.types.Timestamp`
and :mod:`.utils.mediainfo` to validate the timestamps and add more timetamps until the desired
number of screenshots is reached and then loops over :func:`.utils.image.screenshot` to create the
screenshots.

The resulting screenshot paths are published via a :class:`~.utils.signal.Signal`. The UI uses that
signal to display the paths of created screenshots as they appear. Other jobs can also connect to
the same signal, e.g.  :class:`~.jobs.imghost.ImageHostJob` uploads screenshots as soon as they are
available.

User Interface
^^^^^^^^^^^^^^

The UI manages jobs, which basically means starting them and then wait until they are finished.

The UI also passes user input to jobs and displays output and errors from jobs.

For example, a :class:`subcommand <upsies.uis.tui.commands.base.CommandBase>` in the default UI
specifies a list of jobs and translates CLI arguments and configuration file contents into arguments
for those jobs.

Trackers
^^^^^^^^

:mod:`Trackers <upsies.trackers>` implement anything that is specific to a certain tracker. They
provide a list of jobs that generate the necessary metadata as well as an upload method to upload
that metadata.

If you want to add support for a tracker, there is a :doc:`guide that should get you started
<add_tracker>`.

Developing
----------

The ``master`` branch tracks the more recent release, either alpha or stable. Any features or bug
fixes should be worked on in other branches until they are ready to be merged into ``master``.

Virtual Environment
^^^^^^^^^^^^^^^^^^^

A virtual environment that contains all dependencies and tools like test runners can be created by
running ``make venv``. It can be removed by deleting the ``venv`` directory.

Run ``source venv /bin/activate`` to enter the virtual environment and ``deactivate`` to leave it.

Running Tests
^^^^^^^^^^^^^

`pytest <https://pytest.org/>`_ and `tox <https://tox.wiki/>`_ are used to run tests.

Call ``pytest`` to run all tests and ``pytest tests/jobs_test`` to only run the job tests.

Call ``tox`` to run all tests for all supported Python versions. Uninstalled Python versions are
skipped. Check out `pyenv <https://github.com/pyenv/pyenv>`_ for an easy way to install multiple
Python versions side by side.

Supported Python versions are tracked by ``envlist`` in ``tox.ini`` and documented in the
``classifiers`` in ``pyproject.toml``.

Index
-----

.. autosummary::
   :toctree:
   :template: autosummary-template.rst
   :nosignatures:
   :recursive:

   upsies.utils
   upsies.jobs
   upsies.uis
   upsies.trackers
   upsies.constants
   upsies.errors
