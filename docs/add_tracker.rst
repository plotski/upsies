How to Add Support for a Tracker
================================

This guide explains how to add support for a tracker. Because every tracker is unique and has its
own idiosyncracies, it only explains the basic steps. You should be fairly comfortable with Python
or willing to learn some new concepts.

* If you haven't done so yet, read the :ref:`Design section in the Developer Reference
  <dev_reference-design>` and make yourself familiar with the code structure in general.

* If something in this guide doesn't make sense, look at the existing tracker implementations. Code
  can be easier to understand than human words explaining code.

* Try to copy and adapt code from existing tracker implementations instead of writing everything
  from scratch. There are many ways to achieve one goal, but it is best to keep implementations
  similar if possible.

For the purpose of this guide, we assume the tracker you want to add is called "Association for
Sharing Data with Friends" or "ASDF" for short.

1. Create a new tracker package.

   .. code:: sh

      $ mkdir upsies/trackers/asdf
      $ touch upsies/trackers/asdf/__init__.py

   Every piece of code that is specific to a certain tracker should live beneath
   ``upsies.trackers.TRACKER`` where ``TRACKER`` is the short tracker name in lowercase. In our
   example, that would be ``upsies.trackers.asdf``. We should not have to touch any code outside of
   ``upsies.trackers.asdf``.

   The following three subclasses are required, which will be picked up automatically and integrated
   into the rest of the application:

      ``AsdfTrackerConfig`` (subclass of :class:`~.TrackerConfigBase`)
         provides access to user configuration from a config file and from CLI arguments. For
         example, this class gives us an API key or username/password and flags like
         ``--anonymous``.

      ``AsdfTrackerJobs`` (subclass of :class:`~.TrackerJobsBase`)
         provides a sequence of :class:`jobs <upsies.jobs.base.JobBase>` that generate the metadata
         for submission to the tracker. For example, this class typically provides a
         :class:`~.MediainfoJob` and a :class:`~.CreateTorrentJob`.

      ``AsdfTracker`` (subclass of :class:`~.TrackerBase`)
         handles communication with the ASDF server, primarily authentication and submission of
         generated metadata from ``AsdfTrackerJobs``.

   ``AsdfTracker`` is the only object that must be available as
   ``upsies.trackers.asdf.AsdfTracker``. ``AsdfTrackerConfig`` and ``AsdfTrackerJobs`` are
   piggybacking on ``AsdfTracker`` as attributes.

   .. note:: Technically, it does not matter whether ``upsies.trackers.asdf`` is a package
             (directory ``upsies/trackers/asdf``) or a module (file ``upsies/trackers/asdf.py``),
             but unless all classes are very simple, it should be a directory.

2. Implement ``AsdfTrackerConfig``.

   This class provides two dictionaries:

   :attr:`.TrackerConfigBase.defaults`

      defines the available configuration file settings the user can edit in
      :attr:`.constants.TRACKERS_FILEPATH` (usually ``$XDG_CONFIG_HOME/upsies/trackers.ini``). This
      is an INI file in which each section is the :attr:`~.TrackerBase.name` of a tracker
      (e.g. ``asdf``), and the keys are the allowed settings in that section.

      The values in this dictionary should all be return values of :func:`.configfiles.config_value`
      with a useful description for the user, *unless* the same key also exists in
      :attr:`.TrackerConfigBase._common_defaults`, which means it is available for all trackers.

      For example, ``apikey`` does not exist in :attr:`.TrackerConfigBase._common_defaults` so we
      should provide a description, but ``exclude`` exists in
      :attr:`.TrackerConfigBase._common_defaults` and its description is magically re-used for the
      same option in ``AsdfTrackerConfig``.

      Here is an example:

      .. code:: python

         defaults = {
             "apikey": utils.configfiles.config_value(
                 value="",
                 description="Your personal upload API key.",
             ),
             "exclude": (
                 exclude.images,
                 exclude.samples,
             ),
             "anonymous": utils.configfiles.config_value(
                 value=utils.types.Bool("no"),
                 description="Whether your username is displayed on your uploads.",
             ),
         }

      This creates the settings ``trackers.asdf.apikey``, ``trackers.asdf.exclude`` and
      ``trackers.asdf.anonymous``. ``trackers.asdf.apikey`` accepts arbitrary
      strings. ``trackers.asdf.exclude`` updates the default value from :class:`~.TrackerConfigBase`
      but uses the same description. ``trackers.asdf.anonymous`` only accepts boolean values
      (``0``/``1``, ``yes``/``no``, ``true``/``false``, etc).

      .. note:: Unless arbitrary strings are valid values, we should also consider passing something
                from :mod:`.utils.types` to :func:`.configfiles.config_value` as the ``value``. In
                case of misconfiguration by the user, this will raise the appropriate exception
                during configuration file parsing, and not much later when a particular setting is
                used eventually.

   :attr:`.TrackerConfigBase.argument_definitions`
      defines the available CLI arguments per subcommand.

      Every key in this dictionary is a subcommand, e.g. (``"submit"`` or ``"create-torrent"``), and
      every value is a dictionary in which the keys are CLI arguments for that subcommand
      (e.g. ``("--foo", "-f")``) and the values are keyword arguments for
      :meth:`argparse.ArgumentParser.add_argument` that specify the nature of the subcommand
      argument in the form of yet another nested dictionary.

      It's actually not that complicated if you look at this example:

      .. code:: python

         argument_definitions = {
             "submit": {
                 ("--anonymous", "-a"): {
                     "help": "Whether your username is displayed on your uploads",
                     "action": "store_true",
                 },
                 ("--tvmaze",): {
                     "help": "Provide TVmaze ID non-interactively",
                     "type": utils.argtypes.webdb_id("tvmaze"),
                 },
             },
             "create-torrent": {
                 ("--speed", "-s"): {
                     "help": "How fast you want to create the torrent (slow, fast or faster)",
                     "type": utils.argtypes.one_of(("slow", "fast", "faster")),
                 },
             },
         }

      With these ``argument_definitions`` we can provide a prepicked TVmaze ID and make an anonymous
      submission just this one time without editing ``trackers.ini``:

      .. code:: sh

         $ upsies submit asdf --tvmaze 1234 -a path/to/release

      We can also let upsies know how urgent we need a torrent file:

      .. code:: sh

         $ upsies create-torrent asdf -s faster path/to/release

      .. note:: Similar to values in :attr:`.TrackerConfigbase.defaults`, if an argument takes a
                parameters, it should have a ``type`` from :mod:`.utils.argtypes`. In the example
                above, ``--tvmaze ID`` validates ``ID`` with :func:`.utils.argtypes.webdb_id` and
                will fail immediately while parsing the CLI arguments. Without the ``type``, all
                jobs would be started and get busy until one of them notices that the TVmaze ID
                looks funky and slams the kill switch.

3. Implement ``AsdfTrackerJobs``.

   This class looks very simple from the outside. It only provides two attributes:

   :attr:`.TrackerJobsBase.jobs_before_upload`
      is a sequence of :class:`~.JobBase` subclasses that must finish successfully before
      :meth:`.TrackerBase.upload` can be called.

   :attr:`.TrackerJobsBase.jobs_after_upload`
      is a sequence of :class:`~.JobBase` subclasses that will be called after
      :meth:`.TrackerBase.upload` has returned successfully.

      .. note:: The base class already returns :class:`~.AddTorrentJob` and
                :class:`~.CopyTorrentJob` (if they are configured by the user), so we most likely
                don't need to implement :attr:`~.TrackerJobsBase.jobs_after_upload` in
                ``AsdfTrackerJobs``.

   On the inside, however, this class can be vast and complicated, depending on the tracker. It must
   create job instances on demand, coordinate dependencies between jobs (e.g. screenshots must be
   available before a description can be generated), autodetect metadata like the resolution and
   maybe prompt the user to correct or confirm it, etc.

   Lucky for us, every job that can be generalized between trackers should already be implemented in
   :class:`~.TrackerJobsBase` and we can simply return those in our
   :attr:`~.TrackerJobsBase.jobs_before_upload` property. There are also some general-purpose jobs
   like :class:`~.TextFieldJob`, :class:`~.ChoiceJob` and :class:`~.CustomJob` that can use methods
   like :meth:`.TrackerJobsBase.make_screenshots_grid` and :meth:`.TrackerJobsBase.read_nfo` to make
   our life easier.

   Every job is provided as a :func:`functools.cached_property` to make sure it is only created on
   demand (e.g. we don't want to prompt for a TMDb ID if the tracker has no need for it) and only
   once (i.e. we get the same object if the property is accessed multiple times).

   All the arguments the various jobs need for instantiation are provided in bulk to
   ``AsdfTrackerJobs`` when it is instantiated, which then forwards them to each job as required.

   ``AsdfTrackerJobs`` also gets the configuration :attr:`.TrackerJobsBase.options` in the form
   merged configuration file settings (:attr:`.TrackerConfigBase.defaults`) and CLI options
   (:attr:`.TrackerConfigBase.argument_definitions`) so that CLI options override and extend
   configuration file settings.

   Finally, ``AsdfTrackerJobs`` should assemble metadata from its jobs for easy access in
   :meth:`.TrackerBase.upload`. This is usually done via a property called ``post_data`` that
   returns a dictionary which is sent to ``upload.php`` in a ``POST`` request. This approach is not
   part of the protocol, but it made sense so far.

   It is highly recommended to use the convenience methods :meth:`.TrackerJobsBase.get_job_output`
   and :meth:`.TrackerJobsBase.get_job_attribute` for assembling ``post_data``.

3. Implement ``AsdfTracker``.

   ``AsdfTracker`` provides the other two required subclasses ``AsdfTrackerConfig`` and
   ``AsdfTrackerJobs`` via the attributes :attr:`.TrackerBase.TrackerConfig` and
   :attr:`.TrackerBase.TrackerJobs`. This makes it easier to pass a complete tracker implementation
   around internally.

   ``AsdfTracker`` gets the same configuration :attr:`~.TrackerBase.options` as ``AsdfTrackerJobs``
   in the form of a merged dictionary of configuration file settings and CLI options. For example,
   this is used to get an API key for authentication, determine if a submission should be anonymous,
   how many screenshots to make, etc.

   :meth:`.TrackerBase.login` and :meth:`.TrackerBase.logout` should start and end a user session
   if this is required for the submission. If the upload is authenticated with an API key, these
   functions should do nothing.

   :meth:`.TrackerBase.get_announce_url` must return the announce URL of the user. It should read
   the URL from the configuration file via :attr:`.TrackerBase.options`. As a fallback, it can also
   be fetched dynamically from the website so the user doesn't have to configure it.

   :meth:`.TrackerBase.upload` gets an ``AsdfTrackerJobs`` instance from which it can get the
   generated metadata (torrent, screenshots, mediainfo, etc), usually via a
   ``AsdfTrackerJobs.post_data`` attribute, and submits it to the server.

And that's pretty much it.

If something in this guide is unclear or you're stuck for other reasons, post in one of the
supported trackers' forum threads.

Happy hacking!
